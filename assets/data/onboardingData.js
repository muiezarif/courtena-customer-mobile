export default [
    {
        id:1,
        title:"Discover the most sought-after padel courts in your area",
        description: 'Don`t miss out on the best padel courts! Find and book popular courts preferred by fellow sport enthusiasts.',
        image:require('../images/guide1.png')
    },
    {
        id:2,
        title:"Explore pedal courts with top facilities",
        description: 'Elevate your game with premium facilities! Discover courts equipped with everything you need for an enjoyable playing experience.',
        image:require('../images/guide2.png')
    },
    {
        id:3,
        title:"Keep track of your favorite padel courts ",
        description: 'Never lose track of your go-to courts! Save your favorites for quick and convenient bookings, making your sports journey a breeze.',
        image:require('../images/guide3.png')
    },
    // {
    //     id:4,
    //     title:"Customize Everything",
    //     description: 'Adjust your system to speed up your checkout',
    //     image:require('../images/onboarding_four.png')
    // },
]