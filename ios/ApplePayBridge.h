#import <React/RCTBridgeModule.h>
#import <PassKit/PassKit.h>

@interface ApplePayBridge : NSObject <RCTBridgeModule, PKPaymentAuthorizationViewControllerDelegate>

@property (nonatomic, copy) RCTPromiseResolveBlock paymentResolve;
@property (nonatomic, copy) RCTPromiseRejectBlock paymentReject;

- (NSString *)serializePaymentMethod:(PKPaymentMethod *)paymentMethod;
- (NSString *)processPaymentWithToken:(PKPaymentToken *)paymentToken;
- (NSString *)serializePaymentData:(PKPayment *)payment;

@end
