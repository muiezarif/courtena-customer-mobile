#import "ApplePayBridge.h"
#import <React/RCTLog.h>
#import <PassKit/PassKit.h>
#import <React/RCTUtils.h>

@implementation ApplePayBridge
static BOOL isPaymentInProgress = NO;
RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(initiateApplePayPayment:(NSDictionary *)paymentDetails resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
{
    NSLog(@"initiateApplePayPayment called with paymentDetails: %@", paymentDetails);

    // Check if payment authorization is already in progress
    if (isPaymentInProgress) {
        NSString *errorMessage = @"Payment authorization is already in progress.";
        NSError *error = [NSError errorWithDomain:@"PAYMENT_IN_PROGRESS" code:1 userInfo:@{NSLocalizedDescriptionKey: errorMessage}];
        reject(@"PAYMENT_IN_PROGRESS", errorMessage, error);
        return;
    }

    // Set the flag to indicate that payment authorization is now in progress
    isPaymentInProgress = YES;

    if (![PKPaymentAuthorizationViewController canMakePaymentsUsingNetworks:@[PKPaymentNetworkAmex, PKPaymentNetworkVisa, PKPaymentNetworkMasterCard]]) {
        NSString *errorMessage = @"Apple Pay is not available on this device.";
        NSError *error = [NSError errorWithDomain:@"APPLE_PAY_NOT_AVAILABLE" code:1 userInfo:@{NSLocalizedDescriptionKey: errorMessage}];
        reject(@"APPLE_PAY_NOT_AVAILABLE", errorMessage, error);
        isPaymentInProgress = NO; // Reset the flag before returning
        return;
    }

    // Prepare the payment request
    PKPaymentRequest *paymentRequest = [[PKPaymentRequest alloc] init];
    paymentRequest.merchantIdentifier = @"merchant.com.courtena"; // Replace with your merchant identifier
    paymentRequest.supportedNetworks = @[PKPaymentNetworkAmex, PKPaymentNetworkVisa, PKPaymentNetworkMasterCard];
    paymentRequest.countryCode = @"SA"; // Replace with your country code
    paymentRequest.currencyCode = @"SAR"; // Replace with your currency code
    paymentRequest.merchantCapabilities = PKMerchantCapability3DS; // Use PKMerchantCapability3DS for 3D Secure support

    // Prepare the payment summary items (amount to be paid)
    NSDecimalNumber *amount = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@", paymentDetails[@"amount"]]];
    PKPaymentSummaryItem *item = [PKPaymentSummaryItem summaryItemWithLabel:paymentDetails[@"description"] amount:amount];
    paymentRequest.paymentSummaryItems = @[item];

    // Create the payment authorization view controller
    PKPaymentAuthorizationViewController *paymentAuthorizationViewController = [[PKPaymentAuthorizationViewController alloc] initWithPaymentRequest:paymentRequest];
    paymentAuthorizationViewController.delegate = self;

    // Save the resolve and reject blocks for later use
    self.paymentResolve = resolve;
    self.paymentReject = reject;

    // Present the payment authorization view controller on the main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        UIViewController *rootViewController = RCTPresentedViewController();
        [rootViewController presentViewController:paymentAuthorizationViewController animated:YES completion:^{
            NSLog(@"Payment authorization view controller presented.");
        }];
    });
}


#pragma mark - PKPaymentAuthorizationViewControllerDelegate

- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller didAuthorizePayment:(PKPayment *)payment handler:(void (^)(PKPaymentAuthorizationResult * _Nonnull))completion
{
    NSLog(@"Payment authorization called.");

    // Check if payment token is available
    if (payment.token) {
        // Process the payment token and complete the payment
        NSString *paymentId = [self processPaymentWithToken:payment.token];

        // Log the payment ID for debugging
        NSLog(@"Processed payment with ID: %@", paymentId);

        // Get detailed payment data
        NSString *paymentData = [self serializePaymentData:payment];

        // Complete the payment authorization with a success result
        PKPaymentAuthorizationResult *paymentResult = [[PKPaymentAuthorizationResult alloc] initWithStatus:PKPaymentAuthorizationStatusSuccess errors:nil];
        completion(paymentResult);

        // Resolve the promise with payment data
        self.paymentResolve(paymentData);
    } else {
        // Payment token not available, handle error
        NSLog(@"Error: Payment token not available.");

        // Complete the payment authorization with a failure result
        NSError *paymentError = [NSError errorWithDomain:@"YourDomain" code:1 userInfo:@{NSLocalizedDescriptionKey: @"Payment token not available."}];
        PKPaymentAuthorizationResult *paymentResult = [[PKPaymentAuthorizationResult alloc] initWithStatus:PKPaymentAuthorizationStatusFailure errors:@[paymentError]];
        completion(paymentResult);
      
      // Reject the promise with an error message
              [controller dismissViewControllerAnimated:YES completion:^{
                  self.paymentReject(@"PAYMENT_TOKEN_ERROR", @"Payment token not available.", nil);
              }];

        // Reject the promise with an error message
//        self.paymentReject(@"PAYMENT_TOKEN_ERROR", @"Payment token not available.", nil);
    }

    // After processing payment, reset the flag to indicate that payment authorization has finished
    isPaymentInProgress = NO;
}

- (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller
{
    NSLog(@"Payment did finish.");

    // Dismiss the payment authorization view controller
    [controller dismissViewControllerAnimated:YES completion:^{
        // Perform any cleanup or resource release here

        // Resolve the promise with a cancellation message
//        self.paymentReject(@"PAYMENT_CANCELLED", @"Payment was cancelled by the user.", nil);
    }];

    // After the payment authorization view controller is dismissed, reset the flag
    isPaymentInProgress = NO;
}

- (NSString *)processPaymentWithToken:(PKPaymentToken *)paymentToken
{
    // Process the payment token and complete the payment
    // Replace this with your actual payment processing logic

    // For example purposes, let's simulate a successful payment and return a payment ID
    NSString *paymentId = [NSString stringWithFormat:@"payment_%@", [[NSUUID UUID] UUIDString]];

    // Log the payment details for demonstration
    NSLog(@"Processed payment with ID: %@", paymentId);

    return paymentId;
}

- (NSString *)serializeContact:(PKContact *)contact
{
    if (!contact) {
        return @"";
    }

    // Serialize contact data into a JSON string
    NSMutableDictionary *contactDict = [NSMutableDictionary dictionary];
    contactDict[@"name"] = [NSString stringWithFormat:@"%@ %@", contact.name.givenName, contact.name.familyName];
    contactDict[@"email"] = contact.emailAddress;
    contactDict[@"phone"] = contact.phoneNumber.stringValue;
    // Add more fields as needed

    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:contactDict options:NSJSONWritingPrettyPrinted error:&error];

    if (!jsonData) {
        NSLog(@"Error serializing contact data: %@", error.localizedDescription);
        return @"";
    }

    NSString *contactData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return contactData;
}

- (NSString *)serializeShippingMethod:(PKShippingMethod *)shippingMethod
{
    if (!shippingMethod) {
        return @"";
    }

    // Serialize shipping method data into a JSON string
    NSMutableDictionary *shippingDict = [NSMutableDictionary dictionary];
    shippingDict[@"identifier"] = shippingMethod.identifier;
    shippingDict[@"detail"] = shippingMethod.detail;
    shippingDict[@"amount"] = [NSString stringWithFormat:@"%@", shippingMethod.amount];

    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:shippingDict options:NSJSONWritingPrettyPrinted error:&error];

    if (!jsonData) {
        NSLog(@"Error serializing shipping method data: %@", error.localizedDescription);
        return @"";
    }

    NSString *shippingData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return shippingData;
}

- (NSString *)serializePaymentData:(PKPayment *)payment
{
    // Serialize payment data into a JSON string
    NSMutableDictionary *paymentDataDict = [NSMutableDictionary dictionary];
    paymentDataDict[@"token"] = [self serializePaymentToken:payment.token];
    paymentDataDict[@"billingContact"] = [self serializeContact:payment.billingContact];
    paymentDataDict[@"shippingContact"] = [self serializeContact:payment.shippingContact];
    paymentDataDict[@"shippingMethod"] = [self serializeShippingMethod:payment.shippingMethod];

    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:paymentDataDict options:NSJSONWritingPrettyPrinted error:&error];

    if (!jsonData) {
        NSLog(@"Error serializing payment data: %@", error.localizedDescription);
        return @"";
    }

    NSString *paymentData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return paymentData;
}

- (NSString *)serializePaymentMethod:(PKPaymentMethod *)paymentMethod
{
    if (!paymentMethod) {
        return @"";
    }

    // Serialize payment method data into a JSON string
    NSMutableDictionary *paymentDict = [NSMutableDictionary dictionary];
    paymentDict[@"displayName"] = paymentMethod.displayName;
    paymentDict[@"network"] = paymentMethod.network;
    // Add more fields as needed

    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:paymentDict options:NSJSONWritingPrettyPrinted error:&error];

    if (!jsonData) {
        NSLog(@"Error serializing payment method data: %@", error.localizedDescription);
        return @"";
    }

    NSString *paymentData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return paymentData;
}


- (NSDictionary *)serializePaymentToken:(PKPaymentToken *)paymentToken
{
    // Serialize payment token into a JSON dictionary
    NSMutableDictionary *tokenDict = [NSMutableDictionary dictionary];
    tokenDict[@"transactionIdentifier"] = paymentToken.transactionIdentifier;
    tokenDict[@"paymentMethod"] = [self serializePaymentMethod:paymentToken.paymentMethod];
    tokenDict[@"paymentData"] = [self serializePaymentDataMOD:paymentToken.paymentData]; // Serialize payment data as JSON

    return tokenDict;
}

- (NSDictionary *)serializePaymentDataMOD:(NSData *)paymentData
{
    NSError *error;
    NSDictionary *paymentDataDict = [NSJSONSerialization JSONObjectWithData:paymentData options:kNilOptions error:&error];
    
    if (!paymentDataDict || error) {
        NSLog(@"Error serializing payment data: %@", error.localizedDescription);
        return @{};
    }

    return paymentDataDict;
}


// Implement serialization methods for paymentMethod, billingContact, shippingContact, and shippingMethod as needed

@end
