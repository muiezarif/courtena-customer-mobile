const COLORS = {
    white:"#fff",
    primary:"#2c3856",
    secondary:"#3d4e78",
    dark:"#162234",
    darkOpacity:"rgba(22,34,52,0.60)",
    accent:"#a5d9f3",
    accentLight:"#d2ecf9",
    star:"#FFAC1C",
    starInactive:"#a2a0a5",
    moneyGreen:"#41a372",
    onboardingTitle:"#E3F4FC",
    onboardingDescription:'rgba(227, 244, 252, 0.70)',
    tabBarDisabledText:'rgba(227, 244, 252, 0.70)',
    // darkGrey:'rgba(22, 34, 52, 0.60)',
    brandLight:"#F0F8FC",
    lightGrey:"rgba(22,34,52,0.6)",
    dividerBorderColor:"#00000012",
    red:"#D2140A"
}


export default COLORS