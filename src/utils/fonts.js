import { RFValue } from "react-native-responsive-fontsize"
const FONTS = {
    titleHeading:32,
    sectionHeading:20,
    normalHeading:24,
    lightFontWeight:300,
    extraBoldFontWeight:700,
    regularBoldFontWeight:600,
    regularLightBoldFontWeight:500,
    lightBoldFontWeight:400,
    textDescription:14,
    textDetail:20,
    smallTextDescription:12,
    textSmall:14,
    buttonTextSize:16,
    cardHeading:16,
    cardDescription:12,
    cardSmallText:10,
    cardRegularIconSize:25,
    cardMidIconSize:20,
    cardSmallIconSize:15,
    regularIconSize:30,
    smallIconSize:20,
    largeIconSize:40,
    cardBadgeTextSize:12,
    family:"Poppins-Regular",
    family600:"Poppins-SemiBold",
    family300:"Poppins-Light",
    family500:"Poppins-Medium",
    family700:"Poppins-Bold",
}


export default FONTS