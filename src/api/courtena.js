import axios from "axios";

export default axios.create({
    // baseURL:"http://localhost:8800/api"
    baseURL:"https://api.courtena.com/api"
    // baseURL:"http://10.0.2.2:8800/api"
})