import { View, Text } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import MainScreen from '../screens/MainScreen'
import LoginScreen from '../screens/Login'
import RegisterScreen from '../screens/Register'
import OTPVerificationScreen from '../screens/OTPVerification'
import OnboardingScreen from '../screens/Onboarding'
import AfterOnboardingScreen from '../screens/AfterOnboarding'
import CourtDetailScreen from '../screens/CourtDetail'
import VenueDetailScreen from '../screens/VenueDetail'
import PartnerCourtsScreen from '../screens/PartnerCourts'
import BookCourtScreen from '../screens/BookCourt'
import FirstTimeUserInfoAddScreen from '../screens/FirstTimeUserInfoAdd'
import AccountInfoEditScreen from '../screens/AccountInfoEdit'
import BookingDetailScreen from '../screens/BookingDetail'
import SearchScreen from '../screens/SearchScreen'
import LocationAccess from '../screens/LocationAccess'
import FavouriteCourts from '../screens/FavouriteCourts'
import PaymentMethods from '../screens/PaymentMethods'

const Stack = createStackNavigator()

const Router = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen options={{headerShown: false}} name="Onboarding" component={OnboardingScreen}/>
        <Stack.Screen options={{headerShown: false}} name="AfterOnboarding" component={AfterOnboardingScreen}/>
        <Stack.Screen options={{headerShown: false}} name="Login" component={LoginScreen}/>
        <Stack.Screen options={{headerShown: false}} name="Register" component={RegisterScreen}/>
        <Stack.Screen options={{headerShown: false}} name="OTPVerification" component={OTPVerificationScreen}/>
        <Stack.Screen options={{headerShown: false}} name="Main" component={MainScreen}/>
        <Stack.Screen options={{headerShown: false}} name="CourtDetail" component={CourtDetailScreen}/>
        <Stack.Screen options={{headerShown: false}} name="VenueDetail" component={VenueDetailScreen}/>
        <Stack.Screen options={{headerShown: false}} name="PartnerCourts" component={PartnerCourtsScreen}/>
        <Stack.Screen options={{headerShown: false}} name="BookCourt" component={BookCourtScreen}/>
        <Stack.Screen options={{headerShown: false}} name="OTP" component={OTPVerificationScreen}/>
        <Stack.Screen options={{headerShown: false}} name="FirstTimeUserInfoAdd" component={FirstTimeUserInfoAddScreen}/>
        <Stack.Screen options={{headerShown: false}} name="AccountInfoEdit" component={AccountInfoEditScreen}/>
        <Stack.Screen options={{headerShown: false}} name="BookingDetail" component={BookingDetailScreen}/>
        <Stack.Screen options={{headerShown: false}} name="SearchScreen" component={SearchScreen}/>
        <Stack.Screen options={{headerShown: false}} name="LocationAccess" component={LocationAccess}/>
        <Stack.Screen options={{headerShown: false}} name="FavouriteCourts" component={FavouriteCourts}/>
        <Stack.Screen options={{headerShown: false}} name="PaymentMethods" component={PaymentMethods}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default Router