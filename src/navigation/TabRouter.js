import { View, Text,Image, Dimensions, StyleSheet,Platform } from 'react-native'
import React, { useEffect } from 'react'
import { useNavigation } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import HomeScreen from '../screens/Home';
import LocationSearchScreen from '../screens/LocationSearch';
import SearchResultsScreen from '../screens/SearchResults';
import Ionicons from "react-native-vector-icons/Ionicons"
import CourtFilterSearchScreen from '../screens/CourtFilterSearch';
import COLORS from '../utils/colors';
import AllVenuesScreen from '../screens/AllVenues';
import AllCourtsScreen from '../screens/AllCourts';
import CustomerBookingsScreen from '../screens/CustomerBookings';
import SettingsScreen from '../screens/Settings';
import FONTS from '../utils/fonts';

const Tab = createBottomTabNavigator()

const CustomTabLabel = ({ label, focused }) => {
  useEffect(() => {
  
  })
  return (
    <View style={styles.tabLabelContainer}>
      {/* <Image source={require('../../assets/images/profile.png')} style={styles.tabIcon} /> */}
      <Text style={[styles.tabLabelText, focused && styles.tabLabelSelectedText]}>{label}</Text>
      {focused ? <View style={styles.selectedDot}/> :  <View style={styles.unselectedDot}/>}
      </View>
    
  );
};
const TabRouter = () => {
  return (
    <Tab.Navigator
      initialRouteName='Home'
      screenOptions={({route})=> ({
        tabBarActiveTintColor:COLORS.accent,
        tabBarInactiveTintColor:COLORS.tabBarDisabledText,
        tabBarStyle:{
          backgroundColor:COLORS.secondary,
          position:"absolute",
          // paddingTop:10,
          bottom:0,
          height:Platform.OS === "ios" ?"11%":"8%",
          borderTopRightRadius:20,
          borderTopLeftRadius:20,
          // paddingVertical:20
          
        },
        tabBarLabel: ({ focused }) => (
          <CustomTabLabel label={route.name} focused={focused} />
        ),
        tabBarLabelStyle:{
          fontFamily:FONTS.family,
          fontSize:10,
          // marginBottom:0
        },
        tabBarIcon:({focused,color,size}) => {
          let iconName;
          let rn = route.name;
          if(rn === "Discover"){
            return focused === false ? <Image source={require("../../assets/images/home.png")} style={{width:25,height:25,marginTop:7}}/> : <Image source={require("../../assets/images/home_selected.png")} style={{width:25,height:25,marginTop:7}}/>
          }else if(rn === "Venues"){
            // iconName = focused ? 'business' : 'business-outline'
          }else if(rn === "Bookings"){
            return focused === false ? <Image source={require("../../assets/images/bookings.png")} style={{width:25,height:25,marginTop:7}}/> : <Image source={require("../../assets/images/bookings_selected.png")} style={{width:25,height:25,marginTop:7}}/>
          }else if(rn === "Courts"){
            // iconName = focused ? 'file-tray' : 'file-tray-full'
          }else if(rn === "Search"){
            // iconName = focused ? 'person-circle' : 'person-circle-outline'
          }else if(rn === "Location Search"){
            // iconName = focused ? 'settings' : 'settings-outline'
          }else if(rn === "Profile"){
            return focused === false ? <Image source={require("../../assets/images/profile.png")} style={{width:25,height:25,marginTop:7}}/> : <Image source={require("../../assets/images/profile_selected.png")} style={{width:25,height:25,marginTop:7}}/>
          }
          // return <Ionicons name={iconName} color={color} size={size}/>
          // if(iconName){
          //   return <Image source={require("../../assets/images/"+iconName)}/>
          // }
          
        }
      })}
      >
        <Tab.Screen options={{headerShown: false}} name='Discover' component={HomeScreen}/>
        {/* <Tab.Screen options={{headerShown: false}} name='Venues' component={AllVenuesScreen}/> */}
        {/* <Tab.Screen options={{headerShown: false}} name='Courts' component={AllCourtsScreen}/> */}
        <Tab.Screen options={{headerShown: false}} name='Bookings' component={CustomerBookingsScreen}/>
        <Tab.Screen options={{headerShown: false}} name='Profile' component={SettingsScreen}/>
        {/* <Tab.Screen options={{headerShown: false}} name='Search' component={SearchResultsScreen}/> */}
        {/* <Tab.Screen options={{headerShown: false}} name='Court Search' component={CourtFilterSearchScreen}/> */}
        {/* <Tab.Screen options={{headerShown: false}} name='Location Search' component={LocationSearchScreen}/> */}
      </Tab.Navigator>
  )
}

const styles = StyleSheet.create({
  tabLabelContainer: {
    alignItems: 'center',
  },
  tabIcon: {
    width: 25,
    height: 25,
  },
  tabLabelText: {
    fontFamily: FONTS.family,
    fontSize: 10,
    marginTop: 4,
    marginBottom:4,
    color:COLORS.tabBarDisabledText
  },
  tabLabelSelectedText: {
    color: COLORS.accent, // Selected text color
  },
  selectedDot: {
    width: 4,
    height: 4,
    borderRadius: 3,
    backgroundColor: COLORS.accent, // Selected dot color
    marginTop:Platform.OS === "ios" ? 5 : 2,
    marginBottom:Platform.OS === "ios" ?4:6
  },
  unselectedDot: {
    width: 4,
    height: 4,
    borderRadius: 3,
    backgroundColor: COLORS.secondary, // Selected dot color
    marginTop:Platform.OS === "ios" ? 5:2,
    marginBottom:Platform.OS === "ios" ?4:6
  },
});

export default TabRouter