import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
import FONTS from "../../utils/fonts";
const styles = StyleSheet.create({
    container:{
        margin:20,
        backgroundColor:COLORS.secondary,
        borderRadius:10
    },
    innerContainer:{
        padding:10,
        margin:10
    },
    image:{
        width:"100%",
        aspectRatio:2/1,
        borderRadius:10,
        resizeMode:"cover"
    },
    textOne:{
        marginVertical:10,
        fontSize:FONTS.sectionHeading,
        fontWeight:FONTS.extraBoldFontWeight,
        color:COLORS.accentLight

    },
    textTwo:{
        fontSize:FONTS.textDescription,
        lineHeight:26,
        color:COLORS.accent
    },
    textThree:{
        fontSize:FONTS.textSmall,
        marginVertical:10,
        fontWeight:FONTS.regularBoldFontWeight,
        color:COLORS.accent
    },
    textFour:{
        color:'#5b5b5b',
        textDecorationLine:'underline'
    },
    old:{
        color:'#5b5b5b',
        textDecorationLine:'line-through',
    },
    new:{
        fontWeight:'bold'
    }
})


export default styles