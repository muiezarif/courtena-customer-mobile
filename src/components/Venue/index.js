import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import styles from './styles'
import Carousel from '../Carousel'
import feed from '../../../assets/data/feed'
const Venue = (props) => {
    // console.log(props)
  return (
    <TouchableOpacity onPress={() => props.navigation.navigate("VenueDetail",props.feed)}>
    <View style={styles.container}>
        {/* Image */}
        <Image style={styles.image} source={{uri:props.feed.photos[0]}} />
        {/* <Carousel data={feed}/> */}
        <View style={styles.innerContainer}>
        {/* bed & bedroom */}
        <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center"}}>
        <Text style={styles.textOne} >{props.feed.name}</Text>
        <Text style={styles.textTwo} >{props.feed.venuePhone}</Text>
        </View>
        
        {/* type & description  */}
        <Text style={styles.textTwo} numberOfLines={3}>{props.feed.description}</Text>
        <View style={{flexDirection:"row", justifyContent:"space-between"}}>
          <Text style={styles.textThree} numberOfLines={2}>{props.feed.city} - {props.feed.city}</Text>
          <Text style={styles.textThree} >Courts : {props.feed.courts.length}</Text>
        </View>
        {/* old price & new price */}
        {/* <Text style={styles.textThree} >
            <Text style={styles.old}>{props.feed.oldPrice} SAR </Text>
            <Text style={styles.new}> {props.feed.newPrice} SAR</Text>
        </Text>
        {/* total price  */}
        {/* <Text style={styles.textFour} >{props.feed.totalPrice} SAR</Text>  */}
      {/* <Text>index</Text> */}
      </View>
    </View></TouchableOpacity>
  )
}

export default Venue