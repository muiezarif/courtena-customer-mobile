import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
import FONTS from "../../utils/fonts";
const styles = StyleSheet.create({
    // container:{
    //     margin:20,
    //     backgroundColor:COLORS.secondary,
    //     borderRadius:10
    // },
    // innerContainer:{
    //     padding:10
    // },
    container: {
        flex: 1,
        paddingTop: 20,
        paddingHorizontal: 10,
        backgroundColor: '#fff',
      },
      itemContainer: {

        marginTop:12,
        paddingHorizontal: 20,
        height:50,
        // backgroundColor: COLORS.accent,
        borderRadius: 16,
        borderColor:COLORS.dividerBorderColor,
        borderWidth:1,
        justifyContent:"center",
        alignItems:"center"
      },
      itemText: {
        fontSize: 16,
        color:COLORS.dark,
        fontFamily:FONTS.family600
      },
      diagonalLineContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: 40,
        zIndex: 2, // To place it on top of the itemContainer
      },
      diagonalLine: {
        position: 'absolute',
        width: '99%',
        height: '100%',
        borderBottomWidth: 1, // Adjust the width of the diagonal line as needed
        borderBottomColor: COLORS.lightGrey, // Change the color as needed
        transform: [{ rotate: '-5deg' }], // Rotate the line to make it diagonal
        transformOrigin: 'left bottom', // Set the rotation origin to the bottom-left corner
      },
    
})


export default styles