import { View, Text } from 'react-native'
import React from 'react'
import styles from './styles'
import COLORS from '../../utils/colors';
import FONTS from '../../utils/fonts';

const TimeIntervalItem = ({ item, filteredTimeIntervalsByBooking }) => {
  const formatTimeToAMPM = (timeString) => {
    const [hour, minute] = timeString.split(':').map(Number);
    const formattedHour = hour > 12 ? hour - 12 : hour;
    const period = hour >= 12 ? 'PM' : 'AM';
    return `${formattedHour}:${String(minute).padStart(2, '0')} ${period}`;
  };
  
  const isTimeInRange = filteredTimeIntervalsByBooking.some(booking => {
    return booking.intervals.some(interval => {
      const [intervalHours, intervalMinutes] = interval.split(":").map(Number);
      const [selectedHours, selectedMinutes] = item.split(":").map(Number);

      const intervalInMinutes = intervalHours * 60 + intervalMinutes;
      const selectedInMinutes = selectedHours * 60 + selectedMinutes;

      return selectedInMinutes >= intervalInMinutes && selectedInMinutes < intervalInMinutes + 30; // Assuming each interval is 30 minutes
    });
  });
  
  return (
    <View>
      <View style={[styles.itemContainer]}>
        <Text style={[styles.itemText,{color:isTimeInRange?COLORS.lightGrey:COLORS.dark}]}>{formatTimeToAMPM(item)}</Text>
      </View>
      {isTimeInRange?<View style={{position:"absolute",right:0,top:10,right:0,borderRadius:20,backgroundColor:COLORS.accentLight,paddingHorizontal:8,paddingVertical:2,alignItems:"center",justifyContent:"center"}}>
        <Text style={{fontFamily:FONTS.family,color:COLORS.dark,fontSize:10,}}>Booked</Text>
      </View>:null}
    </View>
  );
}

export default TimeIntervalItem;
// {isTimeInRange && 
//   (
//   <View style={styles.diagonalLineContainer}>
//       {/* Add your diagonal line element here */}
//       <View style={styles.diagonalLine}></View>
//     </View>)
//     }