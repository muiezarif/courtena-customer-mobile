import React from 'react';
import { View, Image, Dimensions } from 'react-native';
import * as Animatable from 'react-native-animatable';
import COLORS from '../../utils/colors';

const Loader = ({ name,width,height }) => {

  return (
    <View style={{zIndex:4000,position: 'absolute',top: 0,left: 0,right: 0,bottom: 0,height:Dimensions.get("screen").height,width:Dimensions.get("screen").width,alignItems:"center",justifyContent:"center",alignContent:"center",backgroundColor:COLORS.primary,opacity:0.7}}>
        <Animatable.Image
        animation="rotate"
        easing="linear"
        iterationCount="infinite"
        duration={2000} // You can adjust the duration as needed
        style={{
          width: 200,
          height: 200,
        }}
        source={require('../../../assets/images/loader.png')}
      />
    </View>
  );
};

export default Loader;
