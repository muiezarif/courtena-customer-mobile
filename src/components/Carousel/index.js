import { View, Text, Animated, Dimensions, useWindowDimensions } from 'react-native'
import React, { useRef } from 'react'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { FlatList } from 'react-native-gesture-handler'
import CarouselItem from '../CarouselItem'
import styles from './styles'
import COLORS from '../../utils/colors'
import Paginator from '../OnboardingItem/Paginator'
import FONTS from '../../utils/fonts'

const windowWidth = Dimensions.get('window').width;
const Carousel = (props) => {
    const scrollX = new Animated.Value(0)
    let position = Animated.divide(scrollX,windowWidth)
    const {width} = useWindowDimensions()
    if(props.data && props.data.length){
        return (
            <View>
              <Animated.FlatList
                data={props.data}
                keyExtractor= {(item,index) => 'key' + index}
                horizontal={true}
                pagingEnabled={true}
                scrollEnabled={true}
                snapToAlignment="center"
                scrollEventThrottle={16}
                decelerationRate={"fast"}
                showsHorizontalScrollIndicator={false}
                renderItem={({item,index}) => {
                    return <CarouselItem images={item} index={index}/>
                }} 
                onScroll={Animated.event([{nativeEvent:{contentOffset:{x:scrollX}}}],{useNativeDriver:true})}
              />
              {/* <View style={styles.dotView}>
              <Paginator data={props.data} scrollX={scrollX}/>
              </View> */}
              <View style={styles.dotView}>
                    {props.data.map((_,i) => {
                      // const inputRange = [(i - 1) * width, i * width, ( i + 1 ) * width]
                      // // // console.log(inputRange)
                      const dotWidth = position.interpolate({
                        inputRange:[i-1,i,i+1],
                        outputRange: [10, 20, 10],
                        extrapolate: 'clamp',
                      });
                      console.log(dotWidth)
                      // console.log(dotWidth)
                        let opacity = position.interpolate({
                            inputRange:[i-1,i,i+1],
                            outputRange:[0.3,1,0.3],
                            extrapolate:"clamp"
                    })
                    if(dotWidth){
                      return <Animated.View key={i.toString()} style={[styles.dot,{opacity,width:10}]}/>  
                    }
                    
                    
                    })}
              </View>
              <View style={{flexDirection:"row",position:"absolute",zIndex:100,bottom:20,right:40,justifyContent:"center",alignItems:"center",backgroundColor:COLORS.accentLight,paddingHorizontal:6,paddingVertical:4,borderRadius:30}}>
              <FontAwesome5 name='location-arrow' size={12} color={COLORS.secondary}/>
                    <Text style={{color:COLORS.secondary,marginLeft:5,fontSize:12,fontFamily:FONTS.family300}}>{props.distance !== "Allow GPS"?props.distance+" km":props.distance}</Text>
              </View>
            </View>
          )
    }
}

export default Carousel