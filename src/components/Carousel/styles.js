import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
const styles = StyleSheet.create({
    dotView:{
        flexDirection:"row",
        justifyContent:"center",
        position:"absolute",
        zIndex:100,
        bottom:20,
        left:40,
        paddingVertical:8,
        paddingHorizontal:12,
        backgroundColor:"#00000070",
        borderRadius:20
    },
    dot:{
        height:10,backgroundColor:COLORS.accent,marginRight:5,borderRadius:20
    }
})


export default styles