import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import COLORS from '../../utils/colors';
import FONTS from '../../utils/fonts';

class RadioButton extends Component {
  render() {
    const { label, selected, onPress } = this.props;

    return (
      <TouchableOpacity onPress={onPress}>
        <View style={styles.radioButtonContainer}>
          <Text style={{fontFamily:FONTS.family,fontSize:16}}>{label}</Text>
          <View
            style={[
              styles.radioButton,
              selected ? styles.radioButtonSelected : null,
            ]}
          />
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  radioButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:"space-between"
  },
  radioButton: {
    width: 20,
    height: 20,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: COLORS.secondary, // Change the color as desired
    marginRight: 10,
  },
  radioButtonSelected: {
    backgroundColor: COLORS.secondary, // Change the color as desired
  },
});

export default RadioButton;
