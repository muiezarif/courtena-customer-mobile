import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import styles from './styles'
import Carousel from '../Carousel'
import feed from '../../../assets/data/feed'
import COLORS from '../../utils/colors'
import FONTS from '../../utils/fonts'
import IonIcon from "react-native-vector-icons/Ionicons"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
const Court = (props) => {
  const [pricingFrom,setPricingFrom] = useState(0)
    const [pricingTo,setPricingTo] = useState(0)
    // console.log(props)
    const getPricingRange = () => {

      for (let index = 0; index < props.feed.pricing.pricing.length; index++) {
          const element = props.feed.pricing.pricing[index];
          if(element.active){
              setPricingFrom(element.price)
              break;
          }
      }
      for (let index = props.feed.pricing.pricing.length -1; index >= 0 ; index--) {
          const element = props.feed.pricing.pricing[index];
          if(element.active){
              setPricingTo(element.price)
              break;
          }
      }
  }
  useEffect(()=> {
    getPricingRange()
  },[])
  return (
    // <View style={styles.container}>
        
    //     {/* Image */}
    //     <Image style={styles.image} source={{uri:props.feed.image}} />
    //     {/* <Carousel data={feed}/> */}
    //     <View style={styles.innerContainer}>
    //     {/* bed & bedroom */}
    //     <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center"}}>
    //     <Text style={styles.textOne} >{props.feed.title}</Text>
    //     <Text style={styles.textTwo} >{props.feed.courtType}</Text>
    //     </View>
        
        
    //     {/* type & description  */}
    //     <Text style={styles.textTwo} numberOfLines={3}>{props.feed.description}</Text>
    //     <View style={{flexDirection:"row",justifyContent:"space-between"}}>
    //     <Text style={styles.textThree} >Price Range: {pricingFrom} SAR - {pricingTo} SAR</Text>
    //     <Text style={styles.textThree} >Max People: {props.feed.maxPeople}</Text>
    //     </View>
    //     {/* old price & new price */}
    //     {/* <Text style={styles.textThree} >
    //         <Text style={styles.old}>{props.feed.oldPrice} SAR </Text>
    //         <Text style={styles.new}> {props.feed.newPrice} SAR</Text>
    //     </Text>
    //     {/* total price  */}
    //     {/* <Text style={styles.textFour} >{props.feed.totalPrice} SAR</Text>  */}
    //   {/* <Text>index</Text> */}
    //   </View>
    // </View>
    // <TouchableOpacity onPress={() => props.navigation.navigate("CourtDetail",props.feed)}>
      <View style={{...styles.courtCard}}>
        {/* <View style={styles.courtPriceTag}>
          <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardBadgeTextSize,fontWeight:FONTS.extraBoldFontWeight}}>SAR {pricingFrom} - SAR {pricingTo}</Text>
        </View> */}
        <View style={{flexDirection:"column"}}>
          <View style={{flexDirection:"row"}}>
          <Image source={{uri:props.feed.image.replace("http://","https://")}} style={styles.courtCardImage}/>
          <View style={{flexDirection:"column",marginLeft:12,marginTop:10}}>
            <View style={{flexDirection:"row"}}>
            <Text style={{width:180,fontSize:FONTS.cardHeading,fontWeight:FONTS.regularBoldFontWeight,color:COLORS.dark,fontFamily:FONTS.family}}>{props.feed.title}</Text>
            {/* <IonIcon name='bookmark-outline' size={20} style={{marginLeft:14,justifyContent:"flex-end"}}/> */}
            </View>
            <Text style={{fontSize:FONTS.cardDescription, color:COLORS.lightGrey,fontFamily:FONTS.family}}>Location</Text>
            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
            <Text style={{marginTop:10,color:COLORS.secondary,fontWeight:FONTS.extraBoldFontWeight,width:180,fontFamily:FONTS.family}}>SAR {pricingFrom} - SAR {pricingTo}</Text>
            <View style={{flexDirection:"row",borderRadius:20, backgroundColor:COLORS.accent,justifyContent:"center",alignItems:"center",padding:5}}>
            <FontAwesome5 name='location-arrow' size={8} color={COLORS.lightGrey}/>
            <Text style={{color:COLORS.lightGrey,fontWeight:FONTS.lightBoldFontWeight,fontSize:10,marginLeft:2,fontFamily:FONTS.family}}>1.5km</Text>
            </View>
            </View>
          </View>
          {/* <View style={{flexDirection:"column"}}>
          <Text>Court Name</Text>
          </View> */}
          </View>
          <View style={{borderWidth:1,borderColor:"#00000014",marginTop:10,marginHorizontal:20}}/>
          <View style={{flexDirection:"row",marginLeft:12,marginTop:10}}>
            <View style={{width:50,height:25,borderRadius:20,borderWidth:1,borderColor:"#00000014",justifyContent:"center",alignItems:"center",marginLeft:5}}>
                <Text style={{textAlign:"center",fontSize:12,fontFamily:FONTS.family}}>11:00</Text>
            </View>
            <View style={{width:50,height:25,borderRadius:20,borderWidth:1,borderColor:"#00000014",justifyContent:"center",alignItems:"center",marginLeft:5}}>
                <Text style={{textAlign:"center",fontSize:12,fontFamily:FONTS.family}}>11:00</Text>
            </View>
            <View style={{width:50,height:25,borderRadius:20,borderWidth:1,borderColor:"#00000014",justifyContent:"center",alignItems:"center",marginLeft:5}}>
                <Text style={{textAlign:"center",fontSize:12,fontFamily:FONTS.family}}>11:00</Text>
            </View>
            <View style={{width:50,height:25,borderRadius:20,borderWidth:1,borderColor:"#00000014",justifyContent:"center",alignItems:"center",marginLeft:5}}>
                <Text style={{textAlign:"center",fontSize:12,fontFamily:FONTS.family}}>11:00</Text>
            </View>
          </View>
        </View>
        
        {/* <View style={styles.courtCardDetails}>
          <View style={{flexDirection:"row",justifyContent:"space-between"}}>
            <View style={{width:"70%"}}>
                <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardHeading,fontWeight:"bold"}} numberOfLines={1}>{props.data.title}</Text>
                <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardDescription,marginTop:10}} numberOfLines={2}>{props.data.description}</Text>
                <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardDescription,marginTop:5}} numberOfLines={2}>Max People: {props.data.maxPeople}</Text>
            </View>
            <View style={{width:"30%"}}>
                <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardDescription}} numberOfLines={2}>{props.data.courtType}</Text>
            </View> */}
            {/* <Fontisto name='favorite' size={FONTS.cardRegularIconSize} color={COLORS.accentLight}/> */}
          {/* </View> */}
          {/* <View style={{flexDirection:"row",justifyContent:"space-between",marginTop:10}}>
            <View style={{flexDirection:"row"}}>
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.star} />
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.star} />
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.star} />
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.star} />
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.starInactive} />
            </View>
            <Text style={{fontSize:FONTS.cardSmallText,color:COLORS.accentLight}}>365 reviews</Text>

          </View> */}
        {/* </View> */}
      </View>
      // </TouchableOpacity>
  )
}

export default Court