import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import styles from './styles'

const BookingItem = (props) => {
    // console.log(props.feed.dateTimeInfo.timeFrom)
    const formatTimeToAMPM = (timeString) => {
      // console.log(timeString)
      // timeString.split()
      const [hour, minute] = timeString?.split(':').map(Number);
      const formattedHour = hour > 12 ? hour - 12 : hour;
      const period = hour >= 12 ? 'PM' : 'AM';
      return `${formattedHour}:${String(minute).padStart(2, '0')} ${period}`;
    };
  return (
    <View style={styles.container}>
        
        {/* Image */}
        {/* <Image style={styles.image} source={{uri:props.feed.image}} /> */}
        {/* <Carousel data={feed}/> */}
        <View style={styles.innerContainer}>
        {/* bed & bedroom */}
        <Text style={styles.textOne} >{props.feed.duration}</Text>
        
        {/* type & description  */}
        <Text style={styles.textTwo} numberOfLines={3}>{props.feed.date}</Text>
        <Text style={styles.textTwo} numberOfLines={3}>Time: {formatTimeToAMPM(props.feed.dateTimeInfo.timeFrom+"")} - {formatTimeToAMPM(props.feed.dateTimeInfo.timeTo+"")}</Text>
        <Text style={styles.textThree} >Payment: {props.feed.paymentAmount} SAR</Text>
        {/* old price & new price */}
        {/* <Text style={styles.textThree} >
            <Text style={styles.old}>{props.feed.oldPrice} SAR </Text>
            <Text style={styles.new}> {props.feed.newPrice} SAR</Text>
        </Text>
        {/* total price  */}
        {/* <Text style={styles.textFour} >{props.feed.totalPrice} SAR</Text>  */}
      {/* <Text>index</Text> */}
      </View>
    </View>
  )
}

export default BookingItem