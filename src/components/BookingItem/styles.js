import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
import FONTS from "../../utils/fonts";
const styles = StyleSheet.create({
    container:{
        margin:20,
        backgroundColor:COLORS.dark,
        borderRadius:10
    },
    innerContainer:{
        padding:10
    },
    image:{
        width:"100%",
        aspectRatio:4/2,
        borderRadius:10,
        resizeMode:"cover",
    },
    textOne:{
        marginVertical:10,
        fontSize:FONTS.sectionHeading,
        color:COLORS.accentLight

    },
    textTwo:{
        fontSize:FONTS.textDescription,
        lineHeight:26,
        color:COLORS.accent
    },
    textThree:{
        fontSize:FONTS.cardDescription,
        marginVertical:10,
        color:COLORS.accent
    },
    textFour:{
        color:'#5b5b5b',
        textDecorationLine:'underline'
    },
    old:{
        color:'#5b5b5b',
        textDecorationLine:'line-through',
    },
    new:{
        fontWeight:'bold'
    }
})


export default styles