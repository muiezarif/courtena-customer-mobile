// LocationContext.js
import React, { createContext, useContext, useState } from 'react';

const LocationContext = createContext();

export const useLocation = () => useContext(LocationContext);

export const LocationProvider = ({ children }) => {
  const [location, setLocation] = useState({ lat: initialLatValue, lng: initialLngValue });

  const updateLocation = (newLat, newLng) => {
    setLocation({ lat: newLat, lng: newLng });
  };

  return (
    <LocationContext.Provider value={{ location, updateLocation }}>
      {children}
    </LocationContext.Provider>
  );
};