import { StyleSheet, Text, TouchableOpacity, View, Animated } from 'react-native'
import React,{useEffect,useRef,useState} from 'react'
import Svg,{ G,Circle } from 'react-native-svg'
import AntDesign from "react-native-vector-icons/AntDesign"
import COLORS from '../../utils/colors'
const NextButton = (props) => {
  const size = 128
  const strokeWidth = 3
  const center = size / 2
  const radius = size / 2 - strokeWidth / 2
  const circumference = 2 * Math.PI * radius

  const progressAnimation = useRef(new Animated.Value(0)).current
  const progressRef = useRef(null)

  const animation = (toValue) => {
    return Animated.timing(progressAnimation,{
      toValue,
      duration:250,
      useNativeDriver:true
    }).start()
  }
  useEffect(() => {
    animation(props.percentage)
  },[props.percentage])
  useEffect(() => {
    progressAnimation.addListener((value) => {
      const strokeDashoffset = circumference - (circumference * value.value) / 100
      if(progressRef?.current){
        progressRef.current.setNativeProps({
          strokeDashoffset
        })
      }
    },[props.percentage])

    return () => {
      progressAnimation.removeAllListeners()
    }
  },[])
  return (
    <View style={styles.container}>
      {/* <Svg width={size} height={size}>
        <G rotation="-90" origin={center}>
          <Circle stroke={COLORS.accentLight} cx={center} cy={center} r={radius} strokeWidth={strokeWidth}/>
          <Circle ref={progressRef} stroke={COLORS.accent} cx={center} cy={center} r={radius} strokeWidth={strokeWidth} strokeDasharray={circumference} strokeDashoffset={circumference}/>
        </G>
      </Svg> */}
      <TouchableOpacity onPress={props.scrollTo} style={styles.button} activeOpacity={0.6}>
        <AntDesign name="arrowright" size={22} color={COLORS.secondary}/>
      </TouchableOpacity>
    </View>
  )
}

export default NextButton

const styles = StyleSheet.create({
    container:{
        // flex:1,
        justifyContent:'center',
        // alignItems:'center',
        marginTop:100,
        flexDirection:'row',
        height:64
    },
    button:{
        position:'absolute',
        backgroundColor:COLORS.accentLight,
        borderRadius:16,
        padding:10,
        width:50,height:50,
        alignItems:"center",
        justifyContent:"center"
    }
})