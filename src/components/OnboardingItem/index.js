import { View, Text,Image,useWindowDimensions } from 'react-native'
import React from 'react'
import styles from './styles'
import { SvgUri } from 'react-native-svg'

const OnboardingItem = (props) => {
    const {width} = useWindowDimensions()
  return (
    <View style={[styles.container]}>
      <Image source={props.item.image} style={[styles.image]}/>
      <View style={styles.imageOverlay}/>
      
      {/* <SvgUri uri={props.item.image} style={[styles.image]}/> */}
      <View style={styles.onboardingInfoContainer}>
      <Image source={require('../../../assets/images/onboarding_logo.png')} style={{width:200,height:190,zIndex:101,alignSelf:"center",marginBottom:85}}/>
        <Text style={styles.title}>{props.item.title}</Text>
        <Text style={styles.description}>{props.item.description}</Text>
      </View>
    </View>
  )
}

export default OnboardingItem