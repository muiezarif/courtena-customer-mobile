import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
import FONTS from "../../utils/fonts";
const styles = StyleSheet.create({
    container:{
        // flex:1,
        // backgroundColor:'#000',
        justifyContent:'center',
        alignItems:'center',
        height:Dimensions.get('screen').height,
        width:Dimensions.get('screen').width,
        
    },
    image:{
        // flex:1,
        // justifyContent:'center',
        
        height:Dimensions.get('screen').height,
        width:Dimensions.get('screen').width,
        position:"absolute",
        resizeMode:"cover",
        top:0,
    },
    imageOverlay:{
        height:Dimensions.get('screen').height,
        backgroundColor:COLORS.primary,
        position:"absolute",
        zIndex:100,
        opacity:0.7,
        width:Dimensions.get('screen').width,
        // borderRadius:15
    },
    onboardingInfoContainer:{
        position:"absolute",
        bottom:100,
        zIndex:101,
    },
    title:{
        // fontWeight:600,
        fontSize:32,
        // marginBottom:10,
        color:COLORS.onboardingTitle,
        textAlign:'left',
        paddingHorizontal:16,
        // wordWrap:"break-word",
        lineHeight:48,
        fontFamily:FONTS.family600
        
        // marginVertical:20
    },
    description:{
        fontSize:14,
        // fontWeight:FONTS.lightFontWeight,
        color:COLORS.onboardingDescription,
        textAlign:'left',
        paddingHorizontal:16,
        marginTop:16,
        fontFamily:FONTS.family
    }
})


export default styles