import { StyleSheet, Text,Animated, View,useWindowDimensions } from 'react-native'
import React from 'react'
import COLORS from '../../utils/colors'

const Paginator = (props) => {
    const {width} = useWindowDimensions()
  return (
    <View style={styles.container}>
      {props.data.map((_,i) => {
        // console.log(i)
        const inputRange = [(i - 1) * width, i * width, ( i + 1 ) * width]
        const dotWidth = props.scrollX.interpolate({
            inputRange,
            outputRange:[10,30,10],
            extrapolate:'clamp'
        })

        const opacity = props.scrollX.interpolate({
          inputRange,
          outputRange:[0.3,1,0.3],
          extrapolate:'clamp'
      })
        return <Animated.View style={[styles.dot,{ width: dotWidth,opacity }]} key={i.toString()}/>
      })}
    </View>
  )
}

export default Paginator

const styles = StyleSheet.create({
    container:{
        marginTop:100,
        flexDirection:'row',
        height:64,
        alignItems:"center",
    },
    dot:{
        height:10,
        borderRadius:5,
        backgroundColor:"#E3F4FC",
        marginHorizontal:10
    }
})