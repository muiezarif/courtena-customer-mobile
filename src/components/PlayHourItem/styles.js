import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
import FONTS from "../../utils/fonts";
const styles = StyleSheet.create({
    container:{
        margin:10,
        // backgroundColor:COLORS.accent,
        borderWidth:1,
        borderColor:COLORS.dividerBorderColor,
        borderRadius:16,
        height:50,
        justifyContent:"center"
    },
    innerContainer:{
        // padding:20,
        justifyContent:"center"
    },
      itemContainer: {
        marginRight: 10,
        paddingHorizontal: 20,
        // paddingVertical: 10,
        flexDirection:"row",
        // backgroundColor: COLORS.accent,
        borderRadius: 8,
        alignItems:"center",
        justifyContent:"space-between"
      },
      itemText: {
        fontSize: 16,
      },
    
})


export default styles