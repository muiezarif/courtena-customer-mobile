import { View, Text } from 'react-native'
import React from 'react'
import styles from './styles'
import FONTS from '../../utils/fonts';
import COLORS from '../../utils/colors';
const PlayHourItem = ({item}) => {
  const formatTimeToAMPM = (timeString) => {
    const [hour, minute] = timeString.split(':').map(Number);
    const formattedHour = hour > 12 ? hour - 12 : hour;
    const period = hour >= 12 ? 'PM' : 'AM';
    return `${formattedHour}:${String(minute).padStart(2, '0')} ${period}`;
  };
  return (
    <View style={styles.container}>
      <View style={styles.innerContainer}>
        <View style={styles.itemContainer}>
          <Text style={{fontSize:16,color:COLORS.dark,fontFamily:FONTS.family600}}>{item.interval}</Text>
          <Text style={{fontSize:16,color:COLORS.lightGrey,fontFamily:FONTS.family300}}>SAR {item.price}</Text>
        </View>
      </View>
    </View>
  )
}

export default PlayHourItem