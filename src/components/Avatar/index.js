import React from 'react';
import { View, Text } from 'react-native';
import { Svg, Circle, Text as SvgText } from 'react-native-svg';
import COLORS from '../../utils/colors';

const Avatar = ({ name,width,height }) => {
  // Generate a unique color based on the user's name
  const generateColor = (str) => {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    const color = Math.floor(Math.abs((Math.sin(hash) * 10000) % 1 * 16777216)).toString(16);
    return `#${color}`;
  };

  // Get the first letter of the name
  const firstLetter = name.charAt(0).toUpperCase();

  // Generate a color based on the first letter
  const avatarColor = generateColor(firstLetter);

  return (
    <View>
      <Svg width={width} height={height}>
        <Circle cx={width / 2} cy={height / 2} r={width / 2 - 1} fill={COLORS.accent} />
        <SvgText x="50%" y="50%" textAnchor="middle" alignmentBaseline="central" fontSize={width / 3} fill={COLORS.secondary}>
          {firstLetter}
        </SvgText>
      </Svg>
    </View>
  );
};

export default Avatar;
