import { View, Text, Image } from 'react-native'
import React from 'react'
import FONTS from '../../utils/fonts'
import COLORS from '../../utils/colors'
const NoData = () => {
  return (
    <View style={{justifyContent:"center",alignItems:"center"}}>
        <Image source={require('../../../assets/images/no_data.png')} style={{aspectRatio:2/2,width:300,height:300}}/>
      <Text style={{fontSize:FONTS.sectionHeading,fontWeight:FONTS.extraBoldFontWeight,color:COLORS.accent}}>No Data Found</Text>
    </View>
  )
}

export default NoData