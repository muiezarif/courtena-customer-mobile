import { View, Text,Image,Dimensions } from 'react-native'
import React from 'react'
import styles from './styles'

const {width,height} = Dimensions.get("screen")
const CarouselItem = (props) => {
    // console.log(props)
  return (
    <View style={styles.cardView}>
        <Image source={{uri:props.images.replace("http://","https://")}} style={styles.image}/>
    </View>
  )
}

export default CarouselItem