import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
const styles = StyleSheet.create({
    cardView:{
        flex:1,
        width:Dimensions.get("screen").width - 20,
        height:Dimensions.get("screen").height/ 3,
        // backgroundColor:COLORS.secondary,
        margin:10,
        borderRadius:30,
        shadowColor:COLORS.dark,
        shadowOffset:{width:0.5,height:0.5},
        shadowOpacity:0.5,
        shadowRadius:3,
        elevation:5
    },
    image:{
        width:Dimensions.get("screen").width - 20,
        height:Dimensions.get("screen").height / 3,
        borderRadius:30,
        elevation:0,
        shadowColor:"white"
    }
})


export default styles