import { View, Text, Image } from 'react-native'
import React from 'react'
import FONTS from '../../utils/fonts'
import COLORS from '../../utils/colors'
const ServerError = (props) => {
  return (
    <View style={{justifyContent:"center",alignItems:"center"}}>
        <Image source={require('../../../assets/images/server_error.png')} style={{aspectRatio:2/2,width:300,height:300}}/>
      <Text style={{fontSize:FONTS.sectionHeading,fontWeight:FONTS.extraBoldFontWeight,color:COLORS.accent}}>Server Error</Text>
    </View>
  )
}

export default ServerError