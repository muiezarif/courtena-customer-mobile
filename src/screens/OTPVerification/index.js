import { View, Text, SafeAreaView, Image, Dimensions, Platform, KeyboardAvoidingView } from 'react-native'
import React, { useEffect, useState } from 'react'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
// import { useNavigation } from '@react-navigation/native'
import * as Animatable from 'react-native-animatable';
import styles from './styles';
import COLORS from '../../utils/colors';
import FONTS from '../../utils/fonts';
import courtena from '../../api/courtena';
import { CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell } from 'react-native-confirmation-code-field';
import { useRef } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../../components/Loader';
import { request,PERMISSIONS,RESULTS } from 'react-native-permissions'
const CELL_COUNT = 4;

import Geolocation from 'react-native-geolocation-service';
import Geocoding from 'react-native-geocoding';

const GOOGLE_MAPS_API = "AIzaSyCbknAy3ZfqL2hw9HKAc16jVDkWydyMAwE"

const OTPVerificationScreen = ({ navigation, route }) => {
    const [isLoading, setIsLoading] = useState(false)
    const [code, setCode] = useState("")
    const [value, setValue] = useState('');
    const [countdown, setCountdown] = useState(60); // Initial countdown time
  const [resetDisabled, setResetDisabled] = useState(true); // Initial button state
    const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });
    const item = route.params
    const input1Ref = useRef(null);
    const input2Ref = useRef(null);
    const input3Ref = useRef(null);
    const input4Ref = useRef(null);

    useEffect(() => {
        let intervalId;
    
        if (countdown > 0 && resetDisabled) {
          intervalId = setInterval(() => {
            setCountdown(prevCountdown => prevCountdown - 1);
          }, 1000);
        } else {
          setResetDisabled(false);
        }
    
        return () => clearInterval(intervalId);
      }, [countdown, resetDisabled]);

    const updateMyLocation = async () => {

        Geocoding.init(GOOGLE_MAPS_API);
    
        // Get the device's location
        await Geolocation.getCurrentPosition(
          async (position) => {
            // alert(position)
            // Extract latitude and longitude from the position object
            const { latitude, longitude } = position.coords;
            if(latitude && longitude){
              await AsyncStorage.setItem("location", JSON.stringify({lat:latitude,lng:longitude}))
              navigation.navigate("Main")
            }
    
    
          },
          (error) => console.error(error),
          { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
      }

    const askForPermission = (permission) => {
        console.log("in permission")
        request(permission).then((result) => {
          if(result === RESULTS.DENIED || result === RESULTS.BLOCKED){
            console.log("Denied")
            alert("Location permission is recommended to get the most out of courtena. Go to your settings and allow location permssion")
            navigation.navigate("Main")
          }
          if(result === RESULTS.GRANTED){
            console.log("Granted")
            updateMyLocation()
          }
        });
      }




    const handleTextChange = (text, ref, index) => {
        if (text.length === 1 && ref.current) {
            ref.current.focus();
        }
        const updatedCode = code.slice(0, index) + text.slice(0, 1) + code.slice(index + 1);
        setCode(updatedCode);
    };
    const [errMsg, setErrMsg] = useState("")
    // const []
    //   const navigation = useNavigation()

    const sendVerificationCode = async () => {
        const data = { phone: item.phoneNo, password: item.password }
        await courtena.post("/auth/send-verification-code-customer", { ...data }, {
            headers: {
                "Content-Type": 'application/x-www-form-urlencoded',
                "Accept": '*/*'
            }
        })
    }

    const resendVerificationCode = async () => {
        // Your code to resend the verification code
    // For example: sendVerificationCode();
    setResetDisabled(true);
    setCountdown(60); // Reset the countdown timer
        const data = { phone: item.phoneNo, password: item.password }
        await courtena.post("/auth/resend-verification-code-customer", { ...data }, {
            headers: {
                "Content-Type": 'application/x-www-form-urlencoded',
                "Accept": '*/*'
            }
        })
    }

    useEffect(() => {
        sendVerificationCode()
    },[])


    const verifyOTP = async () => {
        // setIsLoading(true)
        const data = { phone: item.phoneNo, password: item.password,code:code }

        await courtena.post("/auth/verify-customer-phone", { ...data }, {
            headers: {
                "Content-Type": 'application/x-www-form-urlencoded',
                "Accept": '*/*'
            }
        }).then(async(response) => {
            // if(code === '0000'){
                if(response.data.success){
            if (item.login) {
                setIsLoading(true)
                await courtena.post("/auth/login-customer", { ...data }, {
                    headers: {
                        "Content-Type": 'application/x-www-form-urlencoded',
                        "Accept": '*/*'
                    }
                }).then(async (res) => {
                    setIsLoading(false)
                    if (res.data.success) {
                        // console.log(res.data)
                        await AsyncStorage.setItem("customer", JSON.stringify(res.data.result))
                        await AsyncStorage.setItem("token", JSON.stringify(res.data.result.token))
                        await AsyncStorage.setItem("customerRemainLoggedin", "true")
                        const firstTimeUserInfo = await AsyncStorage.getItem("firstTimeUserInfoAdd")
                        // alert(res.data.message)
                        if (res.data.result.isFirstTime) {
                            await AsyncStorage.setItem("firstTimeUserInfoAdd", "true")
                            // if(Platform.OS === "android"){
                            //     askForPermission(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
                            //   }else if(Platform.OS === "ios"){
                            //     askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS)
                                navigation.navigate("Main")
                                navigation.reset({
                                    index: 0,
                                    routes: [{ name: "Main" }],
                                });
                            //   }
                            
                        } else {
                            if (firstTimeUserInfo) {
                                // if(Platform.OS === "android"){
                                //     askForPermission(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
                                //   }else if(Platform.OS === "ios"){
                                //     askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS)
                                    navigation.navigate("Main")
                                    navigation.reset({
                                        index: 0,
                                        routes: [{ name: "Main" }],
                                    });
                                //   }
                                
                            } else {
                                navigation.navigate("FirstTimeUserInfoAdd")
                                // navigation.reset({
                                //     index: 0,
                                //     routes: [{ name: "FirstTimeUserInfoAdd" }],
                                // });
                            }
                        }

                    } else {
                        console.log("error")
                        console.log(res.data)
                        alert(res.data.message)
                    }
                }).catch((err) => {
                    setIsLoading(false)
                    alert(err.message)
                })
            } else {
                setIsLoading(true)
                const data = { phone: item.phoneNo, password: item.password,email:"" }
                await courtena.post("/auth/register-customer/", { ...data }).then(async (res) => {
                    
                    if (res.data.success) {
                        await courtena.post("/auth/login-customer", { ...data }, {
                            headers: {
                                "Content-Type": 'application/x-www-form-urlencoded',
                                "Accept": '*/*'
                            }
                        }).then(async (res) => {
                            setIsLoading(false)
                            if (res.data.success) {
                                console.log(res.data)
                                await AsyncStorage.setItem("customer", JSON.stringify(res.data.result))
                                await AsyncStorage.setItem("token", JSON.stringify(res.data.result.token))
                                await AsyncStorage.setItem("customerRemainLoggedin", "true")
                                const firstTimeUserInfo = await AsyncStorage.getItem("firstTimeUserInfoAdd")
                                // alert(res.data.message)
                                if (res.data.result.isFirstTime) {
                                    await AsyncStorage.setItem("firstTimeUserInfoAdd", "true")
                                    navigation.navigate("Main")
                                    navigation.reset({
                                        index: 0,
                                        routes: [{ name: "Main" }],
                                    });
                                } else {
                                    if (firstTimeUserInfo) {
                                        navigation.navigate("Main")
                                        navigation.reset({
                                            index: 0,
                                            routes: [{ name: "Main" }],
                                        });
                                    } else {
                                        navigation.navigate("FirstTimeUserInfoAdd")
                                        // navigation.reset({
                                        //     index: 0,
                                        //     routes: [{ name: "FirstTimeUserInfoAdd" }],
                                        // });
                                    }
                                }

                            } else {
                                setIsLoading(false)
                                console.log("error")
                                console.log(res.data)
                                alert(res.data.message)
                            }
                        }).catch((err) => {
                            setIsLoading(false)
                            alert(err.message)
                        })
                    } else {
                        setIsLoading(false)
                        alert(res.data.error)
                    }

                }).catch(err => {
                    setIsLoading(false)

                    alert(err)
                })
                // navigation.navigate("Login")
                // navigation.reset({
                //     index: 0,
                //     routes: [{ name: "Login" }],
                // });
            }}else{
                alert(response.data.message)
            }
        // }else{
        //         alert("invalid code")
        //     }
        }).catch(err => {
            alert(err.message)
        })
        

        
    }
    return (
        <SafeAreaView style={[styles.container]}>
            {isLoading?<Loader/>:null}
            <Image style={styles.image} source={require("../../../assets/images/login_image.png")} />
            <View style={styles.imageOverlay} />
            <Image style={{ position: "absolute", zIndex: 101, width: 200, height: 190, top: 200 }} source={require("../../../assets/images/onboarding_logo.png")} />
            <View style={{ flexDirection: "row", justifyContent: "space-between", position: "absolute", zIndex: 101, top: 50, width: Dimensions.get("screen").width, paddingHorizontal: 12 }}>
                <Text onPress={() => {
                    navigation.goBack()
                }} style={{ color: COLORS.brandLight, fontSize: 16, fontFamily: FONTS.family }}>Back</Text>

            </View>
            <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null} style={styles.containerInner}>
                {/* <Image style={styles.image} source={require("../../../assets/images/otp_verify.png")}/> */}
                <View style={{ paddingBottom: 30 }}>
                    <Text style={{ fontSize: FONTS.titleHeading, lineHeight: 48, color: COLORS.secondary, textAlign: "left", marginTop: 30, fontFamily: FONTS.family600 }}>Verification code</Text>
                    <Text style={{ fontSize: FONTS.textDescription, lineHeight: 24, color: COLORS.dark, marginTop: 10, textAlign: "left", fontFamily: FONTS.family300 }}>Enter verification code. We sent code to</Text>
                    <Text style={{ fontSize: FONTS.textDescription, lineHeight: 24, color: COLORS.lightGrey, marginTop: 10, textAlign: "left", fontFamily: FONTS.family300 }}>{item.phoneNo}</Text>

                    <Animatable.View
                        style={{ flexDirection: "row", justifyContent: "center", marginTop: 20 }}
                        ref={this.validateInput}
                    >
                        {/* <CodeField
                        ref={ref}
                        {...props}
                        // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
                        value={value}
                        onChangeText={setValue}
                        cellCount={CELL_COUNT}
                        rootStyle={styles.codeFieldRoot}
                        keyboardType="number-pad"
                        textContentType="oneTimeCode"
                        renderCell={({index, symbol, isFocused}) => (
                        <Text
                            key={index}
                            style={[styles.cell, isFocused && styles.focusCell]}
                            onLayout={getCellOnLayoutHandler(index)}>
                            {symbol || (isFocused ? <Cursor /> : null)}
                        </Text>
                        )}
                    /> */}
                        <TextInput
                            ref={input1Ref}
                            style={errMsg ? styles.errcell : styles.cell}
                            placeholder=""
                            maxLength={1}
                            keyboardType='number-pad'
                            placeholderTextColor={COLORS.accentLight}
                            onChangeText={(text) => {
                                setErrMsg(''),
                                    // setCode(text)
                                    handleTextChange(text, input2Ref, 0)
                            }
                            }
                        />
                        <TextInput
                            ref={input2Ref}
                            style={errMsg ? styles.errcell : styles.cell}
                            placeholder=""
                            maxLength={1}
                            placeholderTextColor={COLORS.accentLight}
                            keyboardType='number-pad'
                            onChangeText={(text) => {
                                setErrMsg(''),
                                    // setCode(text)
                                    handleTextChange(text, input3Ref, 1)
                            }
                            }
                        />
                        <TextInput
                            ref={input3Ref}
                            style={errMsg ? styles.errcell : styles.cell}
                            placeholder=""
                            maxLength={1}
                            placeholderTextColor={COLORS.accentLight}
                            keyboardType='number-pad'
                            onChangeText={(text) => {
                                setErrMsg(''),
                                    // setCode(text)
                                    handleTextChange(text, input4Ref, 2)
                            }
                            }
                        />
                        <TextInput
                            ref={input4Ref}
                            style={errMsg ? styles.errcell : styles.cell}
                            placeholder=""
                            maxLength={1}
                            placeholderTextColor={COLORS.accentLight}
                            keyboardType='number-pad'
                            onChangeText={(text) => {
                                setErrMsg(''),
                                    // setCode(text)
                                    handleTextChange(text, input4Ref, 3)
                            }
                            }
                        />



                    </Animatable.View>
                    <Text style={{ color: 'red', textAlign: 'center', marginTop: 10 }}>{errMsg}</Text>
                    <TouchableOpacity onPress={resendVerificationCode} disabled={resetDisabled}>
                        <Text style={{ color: resetDisabled?"#999": COLORS.dark, textAlign: 'center',fontFamily:FONTS.family }}>{resetDisabled ? `Resend Code (${countdown}s)` : 'Resend Code'}</Text>
                        </TouchableOpacity>


                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 10 }}>
                        <TouchableOpacity
                            onPress={verifyOTP}
                            style={{ width: Dimensions.get("screen").width - 64, backgroundColor: COLORS.secondary, padding: 12, alignItems: 'center', justifyContent: 'center', borderRadius: 16, marginTop: 0 }}
                        >
                            <Text style={{ textAlign: 'center', color: COLORS.brandLight, fontSize: FONTS.buttonTextSize,fontFamily:FONTS.family600 }} >Verify</Text>
                        </TouchableOpacity>

                    </View>
                </View>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

export default OTPVerificationScreen