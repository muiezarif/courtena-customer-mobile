import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
const styles = StyleSheet.create({
    courtCard:{
        height:150,
        // width:"100%",
        width:Dimensions.get('screen').width-40,
        elevation:15,
        shadowOffset: { width: 0, height: 5 },
        shadowColor: COLORS.lightGrey,
        shadowOpacity: 0.5,
        marginBottom:10,
        // marginHorizontal:5,
        borderRadius:15,
        backgroundColor:COLORS.white,
        shadowRadius:10
    },
    courtCardImage:{
        height:80,
        width:132,
        borderRadius:10,
        marginLeft:10,
        marginTop:10
    },
})


export default styles