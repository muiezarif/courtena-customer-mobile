import { View, Text, SafeAreaView, Dimensions, TouchableOpacity, FlatList, Image, ScrollView, ActivityIndicator, Pressable, Platform } from 'react-native'
import React, { useEffect, useState } from 'react'
import styles from "./styles"
import COLORS from '../../utils/colors'
import FONTS from '../../utils/fonts'
import { useNavigation } from '@react-navigation/native'
import courtena from '../../api/courtena'
import AsyncStorage from '@react-native-async-storage/async-storage'
import IonIcon from "react-native-vector-icons/Ionicons"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'
const courtCardWidth = Dimensions.get('screen').width / 1.8
const FavouriteCourts = ({ }) => {
  const navigation = useNavigation()
  const [venues, setVenues] = useState([])
  const [noData, setNoData] = useState(false)
  const [serverError, setServerError] = useState(false)
  const [resError, setResError] = useState(false)
  const [loading, setLoading] = useState(false)
  const [platformMarginTop, setPlatformMarginTop] = useState(0)


  const getHomeData = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const customerToken = await AsyncStorage.getItem("token")
    setLoading(true)
    setNoData(false)
    setResError(false)
    setServerError(false)
    await courtena.get("/customer/get-customer-fav-venues/" + customerData._id, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      setLoading(false)
      console.log(res.data)
      if (res.data.success) {
        // setCourts(res.data.result.courts)
        setVenues(res.data.result)
        if (res.data.result.length === 0) {
          setNoData(true)
        }
      } else {
        setResError(true)
        alert(res.data.message)
      }
    }).catch((err) => {
      setLoading(false)
      setServerError(true)
      console.log(err)
    })
  }

  const toggleVenueFav = async (venueId) => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const data = { customer: customerData._id, venue: venueId }
    await courtena.post("/customer/toggle-fav-venue/", { ...data }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      if (res.data.success) {
        getHomeData()
      } else {
        alert("Error removing favourite")
      }

    }).catch((err) => {
      console.log(err)
    })
  }
  useEffect(() => {
    getHomeData()
    if (Platform.OS === "android") {
      setPlatformMarginTop(40)
    } else {
      setPlatformMarginTop(0)
    }
  }, [])

  const Venues = (props) => {
    // console.log(props.data.venue)
    const inputRange = [
      (props.index - 1) * courtCardWidth,
      props.index * courtCardWidth,
      (props.index + 1) * courtCardWidth
    ]
    //   const opacity = scrollX.interpolate({inputRange,outputRange:[0.3,0,0.3]})
    //   const scale = scrollX.interpolate({inputRange,outputRange:[0.8,1,0.8]})
    return (
      // <TouchableOpacity onPress={() => navigation.navigate("VenueDetail",props.data)}>
      // <Animated.View style={{...styles.venueCard,transform:[{scale}]}}>
      //   <Animated.View style={{...styles.venueCardOverlay,opacity}}/>
      //   <Image source={{uri:props.data.photos[0]}} style={styles.venueCardImage}/>
      //   <View style={styles.venueCardDetails}>
      //     <View style={{flexDirection:"row",justifyContent:"space-between"}}>
      //       <View style={{width:"70%"}}>
      //           <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardHeading,fontWeight:"bold"}} numberOfLines={1}>{props.data.name}</Text>
      //           <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardDescription,marginTop:5}} numberOfLines={2}>{props.data.description} </Text>
      //           {/* <View style={{flexDirection:"row",justifyContent:"space-between"}}> */}
      //           <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardSmallText,marginTop:5}} numberOfLines={2}>{props.data.city} - {props.data.address}</Text>
      //           <Text style={{fontSize:FONTS.cardSmallText,fontWeight:FONTS.regularBoldFontWeight,color:COLORS.accent,marginTop:3}}>Contact: {props.data.venuePhone}</Text>
      //           {/* </View> */}
      //       </View>
      //       <View style={{width:"30%"}}>
      //       <Text style={{fontSize:FONTS.cardSmallText,fontWeight:FONTS.regularBoldFontWeight,color:COLORS.accent}}>Courts: {props.data.courts.length}</Text>
      //       </View>
      //     </View>

      //   </View>
      // </Animated.View>
      // </TouchableOpacity>
      
        <View style={{ ...styles.courtCard }}>
          {/* <View style={styles.courtPriceTag}>
          <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardBadgeTextSize,fontWeight:FONTS.extraBoldFontWeight}}>SAR {pricingFrom} - SAR {pricingTo}</Text>
        </View> */}
          <View style={{ flexDirection: "column" }}>
          <Pressable onPress={() => {
        // console.log(props.data)
        navigation.navigate("VenueDetail", props.data.venue)
      }}>
            <View style={{ flexDirection: "row" }}>
              <Image source={{ uri: props.data.venue.photos[0].replace("http://", "https://") }} style={styles.courtCardImage} />
              <View style={{ flexDirection: "column", marginLeft: 12, marginTop: 10 }}>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ width: 180, fontSize: FONTS.cardHeading, color: COLORS.dark, fontFamily: FONTS.family600 }}>{props.data.venue.name}</Text>
                  {/* <TouchableOpacity onPress={() => toggleVenueFav(props.data.venue._id)} style={{marginLeft:14,justifyContent:"flex-end"}}>
              <Image onPress source={require("../../../assets/images/selected_fav.png")} />
            </TouchableOpacity> */}
                </View>
                <Text style={{ fontSize: FONTS.cardDescription, color: COLORS.lightGrey, fontFamily: FONTS.family, width: 150 }} numberOfLines={2}>{props.data.venue.address}</Text>
                <View style={{ flexDirection: "row" }}>
                  {/* <Text style={{marginTop:10,color:COLORS.secondary,fontWeight:FONTS.extraBoldFontWeight,fontFamily:FONTS.family}}>SAR 50 - SAR 100</Text> */}
                  {/* <View style={{flexDirection:"row",borderRadius:20, backgroundColor:COLORS.accent,justifyContent:"center",alignItems:"center",padding:5}}>
            <FontAwesome5 name='location-arrow' size={8} color={COLORS.lightGrey}/>
            <Text style={{color:COLORS.lightGrey,fontWeight:FONTS.lightBoldFontWeight,fontSize:10,marginLeft:2,fontFamily:FONTS.family}}>1.5km</Text>
            </View> */}
                </View>
              </View>
              {/* <View style={{flexDirection:"column"}}>
          <Text>Court Name</Text>
          </View> */}
            </View></Pressable>
            <View style={{ borderWidth: 1, borderColor: "#00000014", marginTop: 10, marginHorizontal: 20 }} />
            <ScrollView showsHorizontalScrollIndicator={false} horizontal style={{ flexDirection: "row", marginHorizontal: 12, marginTop: 10, zIndex: 1000 }}>
              <View style={{ height: 25, paddingHorizontal: 5, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.venue.timing.mondayOn ? "M " + props.data.venue.timing.mondayFrom : "M Off"}</Text>
              </View>
              <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.venue.timing.tuesdayOn ? "T " + props.data.venue.timing.tuesdayFrom : "T Off"}</Text>
              </View>
              <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.venue.timing.wedOn ? "W " + props.data.venue.timing.wedFrom : "W Off"}</Text>
              </View>
              <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.venue.timing.thursdayOn ? "T " + props.data.venue.timing.thursdayFrom : "T Off"}</Text>
              </View>
              <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.venue.timing.fridayOn ? "F " + props.data.venue.timing.fridayFrom : "F Off"}</Text>
              </View>
              <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.venue.timing.satOn ? "S " + props.data.venue.timing.satFrom : "S Off"}</Text>
              </View>
              <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.venue.timing.sunOn ? "S " + props.data.venue.timing.sunFrom : "S Off"}</Text>
              </View>
            </ScrollView>
          </View>

          {/* <View style={styles.courtCardDetails}>
          <View style={{flexDirection:"row",justifyContent:"space-between"}}>
            <View style={{width:"70%"}}>
                <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardHeading,fontWeight:"bold"}} numberOfLines={1}>{props.data.title}</Text>
                <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardDescription,marginTop:10}} numberOfLines={2}>{props.data.description}</Text>
                <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardDescription,marginTop:5}} numberOfLines={2}>Max People: {props.data.maxPeople}</Text>
            </View>
            <View style={{width:"30%"}}>
                <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardDescription}} numberOfLines={2}>{props.data.courtType}</Text>
            </View> */}
          {/* <Fontisto name='favorite' size={FONTS.cardRegularIconSize} color={COLORS.accentLight}/> */}
          {/* </View> */}
          {/* <View style={{flexDirection:"row",justifyContent:"space-between",marginTop:10}}>
            <View style={{flexDirection:"row"}}>
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.star} />
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.star} />
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.star} />
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.star} />
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.starInactive} />
            </View>
            <Text style={{fontSize:FONTS.cardSmallText,color:COLORS.accentLight}}>365 reviews</Text>

          </View> */}
          {/* </View> */}
        </View>
      // </Pressable>
    )
  }

  const VenuesSkeleton = () => {
    return (
      <SkeletonPlaceholder borderRadius={1}>
        
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
      </SkeletonPlaceholder>
    )


  }


  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.brandLight }}>
      <View style={{ marginTop: platformMarginTop }}>
        <View style={{ flexDirection: "row", alignItems: "center", marginTop: 20 }}>
          <TouchableOpacity style={{ marginLeft: 20, position: "absolute", zIndex: 100 }} onPress={() => navigation.goBack()}>
            <Text style={{ fontSize: FONTS.buttonTextSize, color: COLORS.secondary, fontFamily: FONTS.family }}>Back</Text>
          </TouchableOpacity>
          <View style={{ position: "absolute", flexDirection: "row", justifyContent: "center", marginTop: 16, width: Dimensions.get("screen").width }}>
            <Text style={{ alignSelf: "center", color: COLORS.secondary, fontSize: FONTS.sectionHeading, fontFamily: FONTS.family500 }}>Favorite Courts</Text>
          </View>
        </View>
        {loading ? <View style={{  justifyContent: "center" }}><VenuesSkeleton /></View> : null}

        {noData === false ?
          <FlatList
            style={{ height: "100%" }}
            data={venues}
            contentContainerStyle={{ paddingVertical: 30, paddingLeft: 20, paddingRight: courtCardWidth / 2 - 40 }}
            showsHorizontalScrollIndicator={false}
            horizontal={false}
            renderItem={({ item, index }) => <Venues data={item} index={index} />}
            snapToInterval={courtCardWidth}
          />


          : <View style={{ marginTop: 20, justifyContent: "center", alignItems: "center", height: "100%" }}>
            <Text style={{ fontSize: FONTS.sectionHeading, color: COLORS.lightGrey, fontFamily: FONTS.family500 }}>Nothing saved just yet</Text>
            <Text style={{ marginTop: 8, fontSize: FONTS.textDescription, color: COLORS.lightGrey, fontFamily: FONTS.family }}>Save your court to favorites</Text>
            <View style={{ backgroundColor: COLORS.secondary, paddingHorizontal: 10, paddingVertical: 5, borderRadius: 50, marginTop: 16 }}>
              <Text style={{ color: COLORS.brandLight, fontSize: FONTS.textDescription, fontFamily: FONTS.family600 }}>Find court</Text>
            </View>
          </View>}
      </View>
    </SafeAreaView>
  )
}

export default FavouriteCourts