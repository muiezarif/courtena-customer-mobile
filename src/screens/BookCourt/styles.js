import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:COLORS.primary
    },
    headerImage:{
        flex:1,
        height:200,
        borderRadius:10,
        overflow:"hidden",
        marginHorizontal:10
    },
    iconContainer:{
        position:"absolute",
        height:60,
        width:60,
        backgroundColor:COLORS.secondary,
        top:-30,
        right:10,
        borderRadius:30,
        justifyContent:"center",
        alignItems:"center",
        paddingLeft:3
    },
    timeListContainer: {
        flex: 1,
        padding: 20,
        backgroundColor: COLORS.dark,
        borderRadius:20,
        marginTop:20
      },
    playHoursContainer: {
        flex: 1,
        padding: 20,
        backgroundColor: COLORS.dark,
        borderRadius:20,
        marginTop:20
      },
})


export default styles