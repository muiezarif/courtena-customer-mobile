import { View, Text, SafeAreaView, ImageBackground,TouchableOpacity, FlatList } from 'react-native'
import React, { useEffect, useState } from 'react'
import styles from './styles'
import { ScrollView } from 'react-native-gesture-handler'
import COLORS from '../../utils/colors'
import IonIcon from 'react-native-vector-icons/Ionicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import FONTS from '../../utils/fonts'
import ReactNativeModernDatepicker,{getFormatedDate} from 'react-native-modern-datepicker'
import moment from 'moment'
import TimeIntervalItem from '../../components/TimeIntervalItem'
import PlayHourItem from '../../components/PlayHourItem'
import courtena from '../../api/courtena'
import AsyncStorage from '@react-native-async-storage/async-storage'
// import DatePicker from 'react-native-datepicker'
const BookCourtScreen = ({navigation,route}) => {
    const item = route.params
    const today = new Date()
    const [courtInfo,setCourtInfo] = useState({})
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
    // console.log(today)
    const startDate = getFormatedDate(today.setDate(today.getDate()),"YYYY/MM/DD")
    const endDate = getFormatedDate(today.setDate(today.getDate()+30),"YYYY/MM/DD")
    const [date,setDate] = useState(startDate)
    const [selectedTimeRange,setSelectedTimeRange] = useState("")
    const [timeRange,setTimeRange] = useState([])
    const [playHours,setPlayHours] = useState([])
    const getCourtInfo = async () => {
        const customer = await AsyncStorage.getItem("customer")
        const customerData = JSON.parse(customer)
        await courtena.get("/customer/get-court-info/"+item._id,{
            headers:{
                'Content-Type':'application/x-www-form-urlencoded',
                'Accept': '*/*',
                'Authorization': customerData.token
            }
        }).then((res) => {
            // console.log("Get Court Data")
            // console.log(res.data.result)
            if(res.data.success){
                setCourtInfo(res.data.result)
                // console.log(courtInfo.pricing[0].pricing)
                
            }else{

            }
        }).catch((err) => {
            console.log(err)
        })
    }
    const bookCourt  = async (itemNew) => {
        const customer = await AsyncStorage.getItem("customer")
        const customerData = JSON.parse(customer)
        const { day, month, year } = extractDateComponents(date);
        const newInterval = removeMinAndConvertToNumber(itemNew.interval)

        console.log(selectedTimeRange)
        for (let index = 0; index < courtInfo.bookingInfo.length; index++) {
            const element = courtInfo.bookingInfo[index];
            const bookedIntervals = generateTimeIntervals(element.dateTimeInfo.timeFrom,element.dateTimeInfo.timeTo,30)
            const selectedBookedIntervals = generateTimeIntervals(selectedTimeRange,selectedTimeRange + newInterval,30)
            console.log(areArraysEqual(selectedBookedIntervals,bookedIntervals))
            if(day == element.dateTimeInfo.day && month == element.dateTimeInfo.month && year == element.dateTimeInfo.year){
                
                if(areArraysEqual(selectedBookedIntervals,bookedIntervals)){
                    alert(selectedTimeRange+" Time already booked")
                    return;
                }
            }else{
                // alert("not in")
            }
            
        }
        // console.log("return after")

        if(date && selectedTimeRange){
            const data = {duration:itemNew.interval,date:date,time:selectedTimeRange,paymentAmount:itemNew.price,dateTimeInfo:{day:day,month:month,year:year,timeFrom:selectedTimeRange,timeTo:addMinutes(selectedTimeRange,newInterval)},court:item._id,partner:item.partner,venue:item.venue,sports:item.sports,customer:customerData._id}
            courtena.post("/customer/customer-create-booking",{...data},{
                headers:{
                    'Content-Type':'application/x-www-form-urlencoded',
                    'Accept': '*/*',
                    'Authorization': customerData.token
                }
            }).then((res) => {
                console.log(res.data)
                if(res.data.success){
                    alert("Court Booked")
                }else{
    
                }
            }).catch((err) => {
                console.log(err)
            })
        }else{
            alert("Please select the date and time to book the court")
        }
        
        
        
    }
    function areArraysEqual(arr1, arr2) {
        for (let i = 0; i < arr1.length; i++) {
            if (arr2.includes(arr1[i])) {
              return true;
            }
          }
        
          return false;
      }
    function removeMinAndConvertToNumber(text) {
        const updatedText = text.replace('min', '').trim();
        const numberValue = parseInt(updatedText, 10);
        return numberValue;
      }
    const extractDateComponents = (dateString) => {
        // const date = new Date(dateString);
      
        const [year, month, day] = dateString.split('/');

        return { day: parseInt(day), month: parseInt(month), year: parseInt(year) };
      };
    const addMinutes = (timeString,addition) => {
        const [hours, minutes] = timeString.split(':');
      
        // Create a new Date object with the provided time
        const date = new Date();
        date.setHours(parseInt(hours, 10));
        date.setMinutes(parseInt(minutes, 10));
      
        // Add 30 minutes to the time
        date.setMinutes(date.getMinutes() + addition);
      
        // Get the updated time as a localized string
        const updatedTimeString = date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit',hour12:false });
        console.log(updatedTimeString)
        return updatedTimeString;
      };
    const formatTimeToAMPM = (timeString) => {
        const [hour, minute] = timeString.split(':').map(Number);
        const formattedHour = hour > 12 ? hour - 12 : hour;
        const period = hour >= 12 ? 'PM' : 'AM';
        return `${formattedHour}:${String(minute).padStart(2, '0')} ${period}`;
      };
    const generateTimeIntervals = (startTime, endTime, intervalMinutes) => {
        const intervals = [];
        const [startHour, startMinute] = startTime.split(':').map(Number);
        const [endHour, endMinute] = endTime.split(':').map(Number);
      
        let currentHour = startHour;
        let currentMinute = startMinute;
      
        while (currentHour < endHour || (currentHour === endHour && currentMinute <= endMinute)) {
          const formattedHour = String(currentHour).padStart(2, '0');
          const formattedMinute = String(currentMinute).padStart(2, '0');
          intervals.push(`${formattedHour}:${formattedMinute}`);
      
          currentMinute += intervalMinutes;
          if (currentMinute >= 60) {
            currentHour++;
            currentMinute -= 60;
          }
        }
      
        return intervals;
      };
      useEffect(() => {
        if(courtInfo.pricing !== undefined){
            console.log(courtInfo)
            const startTime = courtInfo.pricing[0].dateTime.startTime;
            const endTime = courtInfo.pricing[0].dateTime.endTime;
            const intervalMinutes = 30;
            const intervals = generateTimeIntervals(startTime, endTime, intervalMinutes);
            setTimeRange(intervals)
            // addMinutes("20:00",30)
        }
        if(courtInfo){

        }
        
        // getCourtInfo()
      },[courtInfo])
      useEffect(() => {
        // const startTime = item.pricing[0].dateTime.startTime;
        // const endTime = item.pricing[0].dateTime.endTime;
        // const intervalMinutes = 30;
        // const intervals = generateTimeIntervals(startTime, endTime, intervalMinutes);
        // setTimeRange(intervals)
        getCourtInfo()
      },[])

  return (
    <SafeAreaView style={styles.container}>
        <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
            paddingBottom: 20,
        }}
        >
            <ImageBackground style={styles.headerImage} source={{uri:item.image}}>
                <View style={styles.header}>
                    <IonIcon name='arrow-back-circle' size={FONTS.largeIconSize} color={COLORS.dark} onPress={navigation.goBack}/>
                </View>
            </ImageBackground>
            <View>
                
                <View style={{marginTop:20,paddingHorizontal:20}}>
                    <Text style={{fontSize:FONTS.normalHeading,fontWeight:FONTS.extraBoldFontWeight,color:COLORS.accent}}>{courtInfo.title}</Text>
                    <Text style={{fontSize:FONTS.textDescription,fontWeight:FONTS.lightFontWeight,color:COLORS.accentLight,marginTop:5}}>{courtInfo.description}</Text>
                    <Text style={{fontSize:FONTS.sectionHeading,fontWeight:FONTS.regularBoldFontWeight,color:COLORS.accent,marginTop:20}}>Select Date: {date}</Text>
                    <ReactNativeModernDatepicker
                        mode='datePicker'
                        // disableDateChange={true}
                        onDateChange={(date) => {
                            
                            const dateNew = moment(date,"YYYY/MM/DD").format('YYYY-MM-DD')
                            var dateDayName = new Date(dateNew);
                            const dayName = dateDayName.toLocaleDateString("en-Us",{weekday:"long"})
                            console.log(dateDayName.getFullYear())
                            if(days[dateDayName.getDay()] == "Sunday"){
                                alert("Sunday is not available for this court")
                                setDate(startDate)
                            }else{
                                setDate(date)
                            }
                            
                            // console.log(dateNew)
                            
                        }
                        }
                        minimumDate={startDate}
                        maximumDate={endDate}
                        selected={date}
                        style={{borderRadius:20,marginTop:20}}
                        options={{
                            backgroundColor:COLORS.dark,
                            textDefaultColor:COLORS.accentLight,
                            selectedTextColor:COLORS.dark,
                            mainColor:COLORS.accent,
                            textSecondaryColor:COLORS.accent,
                            textHeaderColor:COLORS.accent,
                            borderColor:COLORS.accentLight,
                            textFontSize:18
                        }}
                    />
                    <Text style={{fontSize:FONTS.sectionHeading,fontWeight:FONTS.regularBoldFontWeight,color:COLORS.accent,marginTop:20}}>Select Time: {selectedTimeRange? formatTimeToAMPM(selectedTimeRange): null}</Text>
                    <View style={styles.timeListContainer}>
                        {timeRange ?<FlatList
                        data={timeRange}
                        keyExtractor={(item) => item}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        renderItem={({ item }) => {
                            return (
                            <TouchableOpacity key={item._id} onPress={() => setSelectedTimeRange(item)}>
                                <TimeIntervalItem item={item} />
                            </TouchableOpacity>
                            )
                        }}
                    />:null}
                    </View>
                    <Text style={{fontSize:FONTS.sectionHeading,fontWeight:FONTS.regularBoldFontWeight,color:COLORS.accent,marginTop:20}}>Select Play Hours</Text>
                    <View style={styles.playHoursContainer}>
                    {timeRange ?<FlatList
                        data={item.pricing[0].pricing}
                        keyExtractor={(item) => item}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        renderItem={({ item }) => {
                            if(item.active){
                                return (
                                    <TouchableOpacity key={item._id} onPress={() => bookCourt(item)}>
                                        <PlayHourItem item={item} />
                                    </TouchableOpacity>
                                    )
                            }
                            
                        }}
                    />:null}
                    </View>
                </View>
            </View>
        </ScrollView>
    </SafeAreaView>
  )
}

export default BookCourtScreen