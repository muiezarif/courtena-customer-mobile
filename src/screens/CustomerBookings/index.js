import { View, Text, FlatList, TextInput, SafeAreaView, TouchableOpacity, ImageBackground, Image, Dimensions, Modal, ActivityIndicator, Platform, Pressable, ScrollView, Linking } from 'react-native'
import React, { useEffect, useState, useRef } from 'react'
import styles from './styles'
import IonIcon from "react-native-vector-icons/Ionicons"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import feed from '../../../assets/data/feed'
import AntDesign from "react-native-vector-icons/AntDesign"
import FONTS from '../../utils/fonts'
import COLORS from '../../utils/colors'
import BookingItem from '../../components/BookingItem'
import courtena from '../../api/courtena'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useIsFocused, useNavigation } from '@react-navigation/native'
import NoData from '../../components/NoData'
import Loading from '../../components/Loading'
import ResponseError from '../../components/ResponseError'
import ServerError from '../../components/ServerError'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import Loader from '../../components/Loader'
const CustomerBookingsScreen = ({  route }) => {
  const navigation = useNavigation()
  const [pastBookings, setPastBookings] = useState([]);
  const [bookings, setBookings] = useState([])
  const [noData, setNoData] = useState(false)
  const [serverError, setServerError] = useState(false)
  const [resError, setResError] = useState(false)
  const [loading, setLoading] = useState(false)
  const [upcomingBooking, setUpcomingBooking] = useState([])
  const isFocused = useIsFocused()
  const [isCancelModalVisible, setCancelModalVisible] = useState(false);
  const [isPastModalVisible, setPastModalVisible] = useState(false);
  const [viewPlace, setViewPlace] = useState()
  const [platformMarginTop, setPlatformMarginTop] = useState(0)
  const scrollViewRef = useRef(null);
  const topFlatListRef = useRef(null);
  const bottomFlatListRef = useRef(null);
  const [isLoading,setIsLoading] = useState(false)

  const handleScroll = event => {
    const yOffset = event.nativeEvent.contentOffset.y;
    if (yOffset < scrollViewHeight) {
      // Scrolling in the top view
      topFlatListRef.current.scrollToOffset({ offset: yOffset, animated: false });
    } else {
      // Scrolling in the bottom view
      bottomFlatListRef.current.scrollToOffset({ offset: yOffset - scrollViewHeight, animated: false });
    }
  };

  const scrollViewHeight = Dimensions.get("screen").height // calculate the scrollView height based on your screen dimensions

  const toggleCancelModal = () => {
    setCancelModalVisible(!isCancelModalVisible);
  };
  const togglePastModal = () => {
    setPastModalVisible(!isPastModalVisible);
  };
  const convertDateFormat = (inputDate) => {
    // Step 1: Split the input date string into year, month, and day components
    const [year, month, day] = inputDate.split('/');

    // Step 2: Create a Date object using individual components
    const dateObj = new Date(`${year}-${month}-${day}`);

    // Step 3: Get the day from the Date object
    const dayNumber = dateObj.getDate();

    // Step 4: Convert numeric month to month abbreviation
    const monthAbbreviation = new Intl.DateTimeFormat('en-US', { month: 'short' }).format(dateObj);

    // Step 5: Combine the month abbreviation and day in the desired format
    const result = `${monthAbbreviation} ${dayNumber}`;

    return result;
  };

  const formatTimeToAMPM = (timeString) => {
    const [hour, minute] = timeString.split(':').map(Number);
    const formattedHour = hour > 12 ? hour - 12 : hour;
    const period = hour >= 12 ? 'PM' : 'AM';
    return `${formattedHour}:${String(minute).padStart(2, '0')} ${period}`;
  };

  const [details, setDetails] = useState()
  const getBookingDetail = async (itemId, upcoming) => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    setIsLoading(true)
    await courtena.get("/customer/get-booking-detail/" + itemId + "/" + customerData._id, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      setIsLoading(false)
      console.log(res.data)
      setDetails(res.data.result)
      if (upcoming) {
        setCancelModalVisible(true)
      } else {
        setPastModalVisible(true)
      }

    }).catch((err) => {
      setIsLoading(false)
      console.log(err)
    })
  }
  const getCustomerBookings = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)

    setLoading(true)
    setNoData(false)
    setResError(false)
    setServerError(false)
    await courtena.get("/customer/get-customer-bookings/" + customerData._id, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      setLoading(false)
      if (res.data.success) {
        setBookings(res.data.result.reverse())
        if (res.data.result.length === 0) {
          setNoData(true)
        }
      } else {
        setResError(true)
        console.log(res.data.message)
      }
    }).catch((err) => {
      setLoading(false)
      setServerError(true)
      console.log(err)
    })
  }

  const cancelBookingReservation = async () => {
    const customer = await AsyncStorage.getItem('customer');
    const customerData = JSON.parse(customer);
    // const data = {}
    setIsLoading(true)
    const data = { paymentId: details.bookings.payment.id, bookingId: details.bookings._id, court: details.bookings.court }
    await courtena.post("/customer/customer-cancel-reservation/", { ...data }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token,
      },
    }).then(async (res) => {
      setIsLoading(false)
      // console.log(res.data)
      if (res.data.success) {
        alert(res.data.message)
        toggleCancelModal()
      } else {
        await courtena.post("/customer/customer-refund-reservation/", { ...data }, {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': '*/*',
            'Authorization': customerData.token,
          },
        }).then((res2) => {
          setIsLoading(false)
          if (res2.data.success) {
            alert(res2.data.message)
            toggleCancelModal()
          } else {
            alert(res2.data.message)
          }

        }).catch((err) => {
          setIsLoading(false)
        })
      }
      alert(res.data.message)
    }).catch((err) => {
      setIsLoading(false)
      alert("Error Canceling Booking")
      alert(err)
    })

  }
  useEffect(() => {
    getCustomerBookings()
    if (Platform.OS === "android") {
      setPlatformMarginTop(40)
    } else {
      setPlatformMarginTop(0)
    }
  }, [isFocused])



  useEffect(() => {
    const now = new Date();
    const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

    const upcomingToday = bookings.filter(booking => {
      const [year, month, day] = booking.date.split('/');
      const [hours, minutes] = booking.time.split(':');
      const bookingDate = new Date(year, month - 1, day, hours, minutes);

      return bookingDate >= now && bookingDate >= today;
    });

    const upcomingFuture = bookings.filter(booking => {
      const [year, month, day] = booking.date.split('/');
      const [hours, minutes] = booking.time.split(':');
      const bookingDate = new Date(year, month - 1, day, hours, minutes);

      return bookingDate >= now && bookingDate > today;
    });

    const upcomingBookings = upcomingToday.length > 0 ? upcomingToday : upcomingFuture;

    // Filter past bookings (remove upcoming bookings)
    const pastBookings = bookings.filter(booking => !upcomingBookings.includes(booking));

    setPastBookings(pastBookings);

    if (upcomingBookings.length > 0) {
      setUpcomingBooking(upcomingBookings);
    } else {
      setUpcomingBooking(null);
    }

  }, [bookings]);


  const BookingSkeleton = () => {
    return (
      <SkeletonPlaceholder borderRadius={1}>
        <SkeletonPlaceholder.Item height={30} width={"40%"} style={{ marginHorizontal: 20 }} />
        <SkeletonPlaceholder.Item borderRadius={16} height={Dimensions.get("screen").height / 3} width={"90%"} style={{ marginHorizontal: 20, marginTop: 20 }} />
        <SkeletonPlaceholder.Item height={30} width={"40%"} style={{ marginHorizontal: 20, marginTop: 20 }} />
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
      </SkeletonPlaceholder>
    )


  }

  const openURL = (url) => {
    Linking.openURL(url)
      .then((supported) => {
        if (!supported) {
          console.log(`Can't handle url: ${url}`);
        } else {
          return Linking.openURL(url);
        }
      })
      .catch((err) => console.error('An error occurred', err));
  };

  const BookingItem = (props) => {
    // console.log(props.feed)
    const formatTimeToAMPM = (timeString) => {
      // console.log(timeString)
      // timeString.split()
      const [hour, minute] = timeString?.split(':').map(Number);
      const formattedHour = hour > 12 ? hour - 12 : hour;
      const period = hour >= 12 ? 'PM' : 'AM';
      return `${formattedHour}:${String(minute).padStart(2, '0')} ${period}`;
    };

    return (
      // <View style={styles.container}>

      //     {/* Image */}
      //     {/* <Image style={styles.image} source={{uri:props.feed.image}} /> */}
      //     {/* <Carousel data={feed}/> */}
      //     <View style={styles.innerContainer}>
      //     {/* bed & bedroom */}
      //     <Text style={styles.textOne} >{props.feed.duration}</Text>

      //     {/* type & description  */}
      //     <Text style={styles.textTwo} numberOfLines={3}>{props.feed.date}</Text>
      //     <Text style={styles.textTwo} numberOfLines={3}>Time: {formatTimeToAMPM(props.feed.dateTimeInfo.timeFrom+"")} - {formatTimeToAMPM(props.feed.dateTimeInfo.timeTo+"")}</Text>
      //     <Text style={styles.textThree} >Payment: {props.feed.paymentAmount} SAR</Text>
      //     {/* old price & new price */}
      //     {/* <Text style={styles.textThree} >
      //         <Text style={styles.old}>{props.feed.oldPrice} SAR </Text>
      //         <Text style={styles.new}> {props.feed.newPrice} SAR</Text>
      //     </Text>
      //     {/* total price  */}
      //     {/* <Text style={styles.textFour} >{props.feed.totalPrice} SAR</Text>  */}
      //   {/* <Text>index</Text> */}
      //   </View>

      // </View>'
      <View style={{ ...styles.courtCard }}>
        {/* <View style={styles.courtPriceTag}>
          <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardBadgeTextSize,fontWeight:FONTS.extraBoldFontWeight}}>SAR {pricingFrom} - SAR {pricingTo}</Text>
        </View> */}
        <View style={{ flexDirection: "column" }}>
          <View style={{ flexDirection: "row", width: "100%" }}>
            <Image source={{ uri: props.feed.court.image.replace("http", "https") }} style={styles.courtCardImage} />
            <View style={{ flexDirection: "column", marginLeft: 12, marginTop: 10 }}>
              <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={{ width: 120, fontSize: FONTS.cardHeading, color: COLORS.dark, fontFamily: FONTS.family600 }} numberOfLines={1}>{props.feed.court.title}</Text>
                {/* <Image source={require("../../../assets/images/unselected_fav.png")} style={{alignSelf:"flex-end"}}/> */}
              </View>
              <Text style={{ fontSize: FONTS.cardDescription, color: COLORS.lightGrey, fontFamily: FONTS.family, width: 120 }} numberOfLines={1}>{props.feed.venue.address}</Text>
              {/* <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}> */}
              <Text style={{ marginTop: 5, fontSize: 12, color: COLORS.secondary, width: 200, fontFamily: FONTS.family600 }}>{convertDateFormat(props.feed.date)},{formatTimeToAMPM(props.feed.dateTimeInfo.timeFrom + "")} - {formatTimeToAMPM(props.feed.dateTimeInfo.timeTo + "")}</Text>
              <Text style={{ color: COLORS.secondary, fontSize: 10, width: 180, fontFamily: FONTS.family500 }}>{props.feed.duration}</Text>

              {/* </View> */}
            </View>
            {/* <View style={{flexDirection:"column"}}>
          <Text>Court Name</Text>
          </View> */}
          </View>


        </View>

        {/* <View style={styles.courtCardDetails}>
          <View style={{flexDirection:"row",justifyContent:"space-between"}}>
            <View style={{width:"70%"}}>
                <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardHeading,fontWeight:"bold"}} numberOfLines={1}>{props.data.title}</Text>
                <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardDescription,marginTop:10}} numberOfLines={2}>{props.data.description}</Text>
                <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardDescription,marginTop:5}} numberOfLines={2}>Max People: {props.data.maxPeople}</Text>
            </View>
            <View style={{width:"30%"}}>
                <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardDescription}} numberOfLines={2}>{props.data.courtType}</Text>
            </View> */}
        {/* <Fontisto name='favorite' size={FONTS.cardRegularIconSize} color={COLORS.accentLight}/> */}
        {/* </View> */}
        {/* <View style={{flexDirection:"row",justifyContent:"space-between",marginTop:10}}>
            <View style={{flexDirection:"row"}}>
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.star} />
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.star} />
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.star} />
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.star} />
              <AntDesign name='star' size={FONTS.cardSmallIconSize} color={COLORS.starInactive} />
            </View>
            <Text style={{fontSize:FONTS.cardSmallText,color:COLORS.accentLight}}>365 reviews</Text>

          </View> */}
        {/* </View> */}
      </View>
    )
  }
  return (
    <SafeAreaView style={styles.container}>
      {isLoading?<Loader/>:null}
      {loading === false ? <ScrollView
        showsVerticalScrollIndicator={false}
        ref={scrollViewRef}
        style={{ flex: 1 }}

        scrollEventThrottle={16}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={isCancelModalVisible}
          onRequestClose={toggleCancelModal}
        >
          <View style={styles.modalContainer}>
            <View style={styles.fragment}>
              <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Reservation Details</Text>
              <TouchableOpacity onPress={() => toggleCancelModal()} style={styles.closeButton} >
                <IonIcon name='close' size={30} color={COLORS.secondary} />
              </TouchableOpacity>
              <View style={{ flexDirection: "column", marginTop: 30, marginBottom: 20 }}>
                {details ? <View style={{ marginHorizontal: 20, marginTop: 20 }}>
                  <View style={{ flexDirection: "column" }}>
                    <Text style={{ color: COLORS.lightGrey, fontSize: 14, fontFamily: FONTS.family600 }}>Duration</Text>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                      <Text style={{ fontSize: 16, color: COLORS.dark, fontFamily: FONTS.family }}>{details.bookings.duration}</Text>
                      {/* <View style={{borderRadius:30,borderWidth:1,borderColor:COLORS.dividerBorderColor,paddingVertical:5,paddingHorizontal:7}}>
                        <Text style={{fontWeight:FONTS.regularBoldFontWeight,fontSize:14,fontFamily:FONTS.family}}>Change</Text>
                      </View> */}
                    </View>
                    <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                  </View>
                  <View style={{ flexDirection: "column", marginTop: 5 }}>
                    <Text style={{ color: COLORS.lightGrey, fontSize: 14, fontFamily: FONTS.family600 }}>Date</Text>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                      <Text style={{ fontSize: 16, color: COLORS.dark, fontFamily: FONTS.family }}>{convertDateFormat(details.bookings.date)}, {formatTimeToAMPM(details.bookings.dateTimeInfo.timeFrom)}</Text>
                      {/* <View style={{borderRadius:30,borderWidth:1,borderColor:COLORS.dividerBorderColor,paddingVertical:5,paddingHorizontal:7}}>
                        <Text style={{fontWeight:FONTS.regularBoldFontWeight,fontSize:14,fontFamily:FONTS.family}}>Change</Text>
                      </View> */}
                    </View>
                    <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                  </View>
                  <View style={{ flexDirection: "column", marginTop: 5 }}>
                    <Text style={{ color: COLORS.lightGrey, fontSize: 14, fontFamily: FONTS.family600 }}>Court #</Text>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                      <Text style={{ fontSize: 16, color: COLORS.dark, fontFamily: FONTS.family }}>{details.court_details.title}</Text>
                      {/* <View style={{borderRadius:30,borderWidth:1,borderColor:COLORS.dividerBorderColor,paddingVertical:5,paddingHorizontal:7}}>
                        <Text style={{fontWeight:FONTS.regularBoldFontWeight,fontSize:14,fontFamily:FONTS.family}}>Change</Text>
                      </View> */}
                    </View>
                    <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                  </View>
                  <View style={{ flexDirection: "column", marginTop: 5 }}>
                    <Text style={{ color: COLORS.lightGrey, fontSize: 14, fontFamily: FONTS.family600 }}>Paid</Text>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                      <Text style={{ fontSize: 16, color: COLORS.dark, fontFamily: FONTS.family }}>{details.bookings.paymentAmount} SAR</Text>

                    </View>
                    <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                  </View>
                </View> : null}

                <TouchableOpacity onPress={() => {
                  toggleCancelModal()
                  navigation.navigate("VenueDetail", viewPlace)
                }} style={{ borderRadius: 16, borderWidth: 1, borderColor: COLORS.dividerBorderColor, alignItems: "center", justifyContent: "center", height: 50, marginTop: 12 }}>
                  <Text style={{ color: COLORS.dark, fontFamily: FONTS.family600 }}>View the place</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={cancelBookingReservation} style={{ borderRadius: 16, borderWidth: 1, borderColor: COLORS.dividerBorderColor, alignItems: "center", justifyContent: "center", height: 50, marginTop: 12, backgroundColor: COLORS.red }}>
                  <Text style={{ color: COLORS.white, fontFamily: FONTS.family600 }}>Cancel Reservation</Text>
                </TouchableOpacity>
                {details?.bookings?.invoice_url ?<TouchableOpacity onPress={()=> {openURL(details?.bookings?.invoice_url)}} style={{ borderRadius: 16, borderWidth: 1, borderColor: COLORS.dividerBorderColor, alignItems: "center", justifyContent: "center", height: 50, marginTop: 12, backgroundColor: COLORS.secondary }}>
                  <Text style={{ color: COLORS.white, fontFamily: FONTS.family600 }}>Download Invoice</Text>
                </TouchableOpacity>:null}
              </View>



            </View>
          </View>
        </Modal>
        <Modal
          animationType="slide"
          transparent={true}
          visible={isPastModalVisible}
          onRequestClose={togglePastModal}
        >
          <View style={styles.modalContainer}>
            <View style={styles.fragment}>
              <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Completed Booking</Text>
              {/* <TouchableOpacity style={styles.closeButton} > */}
              <TouchableOpacity style={styles.closeButton} onPress={() => togglePastModal()}>
                <IonIcon name='close' size={30} color={COLORS.secondary} />
              </TouchableOpacity>
              <View style={{ flexDirection: "column", marginTop: 30, marginBottom: 20 }}>
                {details ? <View style={{ marginHorizontal: 20, marginTop: 20 }}>
                  <View style={{ flexDirection: "column" }}>
                    <Text style={{ color: COLORS.lightGrey, fontSize: 14, fontFamily: FONTS.family600 }}>Duration</Text>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                      <Text style={{ fontSize: 16, color: COLORS.dark, fontFamily: FONTS.family }}>{details.bookings.duration}</Text>
                      {/* <View style={{borderRadius:30,borderWidth:1,borderColor:COLORS.dividerBorderColor,paddingVertical:5,paddingHorizontal:7}}>
                        <Text style={{fontWeight:FONTS.regularBoldFontWeight,fontSize:14,fontFamily:FONTS.family}}>Change</Text>
                      </View> */}
                    </View>
                    <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                  </View>
                  <View style={{ flexDirection: "column", marginTop: 5 }}>
                    <Text style={{ color: COLORS.lightGrey, fontSize: 14, fontFamily: FONTS.family600 }}>Date</Text>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                      <Text style={{ fontSize: 16, color: COLORS.dark, fontFamily: FONTS.family }}>{convertDateFormat(details.bookings.date)}, {formatTimeToAMPM(details.bookings.dateTimeInfo.timeFrom)}</Text>
                      {/* <View style={{borderRadius:30,borderWidth:1,borderColor:COLORS.dividerBorderColor,paddingVertical:5,paddingHorizontal:7}}>
                        <Text style={{fontWeight:FONTS.regularBoldFontWeight,fontSize:14,fontFamily:FONTS.family}}>Change</Text>
                      </View> */}
                    </View>
                    <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                  </View>
                  <View style={{ flexDirection: "column", marginTop: 5 }}>
                    <Text style={{ color: COLORS.lightGrey, fontSize: 14, fontFamily: FONTS.family600 }}>Court #</Text>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                      <Text style={{ fontSize: 16, color: COLORS.dark, fontFamily: FONTS.family }}>{details.court_details.title}</Text>
                      {/* <View style={{borderRadius:30,borderWidth:1,borderColor:COLORS.dividerBorderColor,paddingVertical:5,paddingHorizontal:7}}>
                        <Text style={{fontWeight:FONTS.regularBoldFontWeight,fontSize:14,fontFamily:FONTS.family}}>Change</Text>
                      </View> */}
                    </View>
                    <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                  </View>
                  <View style={{ flexDirection: "column", marginTop: 5 }}>
                    <Text style={{ color: COLORS.lightGrey, fontSize: 14, fontFamily: FONTS.family600 }}>Paid</Text>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                      <Text style={{ fontSize: 16, color: COLORS.dark, fontFamily: FONTS.family }}>{details.bookings.paymentAmount} SAR</Text>

                    </View>
                    <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                  </View>
                </View> : null}
                <TouchableOpacity onPress={() => {
                  togglePastModal()
                  navigation.navigate("VenueDetail", viewPlace)
                }} style={{ borderRadius: 16, borderWidth: 1, borderColor: COLORS.dividerBorderColor, alignItems: "center", justifyContent: "center", height: 50, marginTop: 12 }}>
                  <Text style={{ color: COLORS.dark, fontFamily: FONTS.family600 }}>View the place</Text>
                </TouchableOpacity>
                {details?.bookings?.invoice_url ?<TouchableOpacity onPress={()=> {openURL(details?.bookings?.invoice_url)}} style={{ borderRadius: 16, borderWidth: 1, borderColor: COLORS.dividerBorderColor, alignItems: "center", justifyContent: "center", height: 50, marginTop: 12, backgroundColor: COLORS.secondary }}>
                  <Text style={{ color: COLORS.white, fontFamily: FONTS.family600 }}>Download Invoice</Text>
                </TouchableOpacity>:null}
                {/* <TouchableOpacity style={{borderRadius:16,borderWidth:1,borderColor:COLORS.dividerBorderColor,alignItems:"center",justifyContent:"center",height:50,marginTop:12,backgroundColor:COLORS.secondary}}>
                    <Text style={{color:COLORS.white,fontWeight:FONTS.regularBoldFontWeight,fontFamily:FONTS.family}}>Repeat Reservation</Text>
                </TouchableOpacity> */}
              </View>



            </View>
          </View>
        </Modal>

        <View>

          {/* <View style={styles.searchInputContainer}>
            <AntDesign name='search1' size={FONTS.regularIconSize} color={COLORS.accent} style={{marginLeft:20}} />
            <TextInput placeholder='Search by name' style={{fontSize:20,paddingLeft:10,width:"80%"}}/>
        </View> */}
          {resError ? <View style={{ alignItems: "center", justifyContent: "center", height: "80%" }}><ResponseError /></View> : null}
          {serverError ? <View style={{ alignItems: "center", justifyContent: "center", height: "80%" }}><ServerError /></View> : null}
          {/* {loading? <View style={{alignItems:"center",justifyContent:"center", height:"80%"}}><Loading/></View>:null} */}
          {noData ? <View style={{ justifyContent: "center", alignItems: "center", height: Dimensions.get("screen").height }}>
            <Text style={{ fontSize: FONTS.sectionHeading, color: COLORS.lightGrey, fontFamily: FONTS.family500 }}>Nothing here</Text>
            <Text style={{ marginTop: 8, fontSize: FONTS.textDescription, color: COLORS.lightGrey, fontFamily: FONTS.family }}>You haven’t reserved any court yet.</Text>
            <TouchableOpacity onPress={() =>{navigation.navigate("Discover")}} style={{ backgroundColor: COLORS.secondary, paddingHorizontal: 10, paddingVertical: 5, borderRadius: 50, marginTop: 16 }}>
              <Text style={{ color: COLORS.brandLight, fontSize: FONTS.textDescription, fontFamily: FONTS.family600 }}>Find court</Text>
            </TouchableOpacity>
          </View> : null}
          {upcomingBooking ? <Text style={{ marginHorizontal: 20, color: COLORS.secondary, fontSize: FONTS.sectionHeading, fontFamily: FONTS.family500, marginTop: platformMarginTop }}>Upcoming Bookings</Text> : null}
          {upcomingBooking ? upcomingBooking.map(item => {
            return (
              <TouchableOpacity onPress={() => {
                setViewPlace(item.venue)
                getBookingDetail(item._id, true)
              }}>
                <View style={{ width: "100%", alignContent: "center", justifyContent: "center", marginTop: 16 }}>
                  <Image style={[styles.headerImage]} source={{ uri: item.court.image.replace("http://", "https://") }} />
                  <View style={{ position: "absolute", width: Dimensions.get("screen").width - 40, alignSelf: "center", bottom: "5%", backgroundColor: COLORS.white, borderRadius: 16 }}>
                    <View style={{ margin: 12 }}>
                      <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                        <Text style={{ fontSize: FONTS.cardHeading, fontFamily: FONTS.family600 }}>{item.court.title}</Text>
                        {/* <Image source={require("../../../assets/images/unselected_fav.png")}/> */}
                      </View>
                      <Text style={{ width: 200, fontSize: FONTS.cardDescription, color: COLORS.lightGrey, fontFamily: FONTS.family }} numberOfLines={2}>{item.venue.address}</Text>
                      <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                        <Text style={{ fontSize: FONTS.textSmall, color: COLORS.secondary, marginTop: 8, fontFamily: FONTS.family700 }}>{formatTimeToAMPM(item.dateTimeInfo.timeFrom)} - {formatTimeToAMPM(item.dateTimeInfo.timeTo)}</Text>
                        {/* <View style={{flexDirection:"row",borderRadius:20, backgroundColor:COLORS.accent,justifyContent:"center",alignItems:"center",padding:5}}>
                      <FontAwesome5 name='location-arrow' size={8} color={COLORS.lightGrey}/>
                      <Text style={{color:COLORS.lightGrey,fontWeight:FONTS.lightBoldFontWeight,fontSize:10,marginLeft:2,fontFamily:FONTS.family}}>1.5km</Text>
                      </View> */}
                      </View>
                    </View>
                  </View>
                </View></TouchableOpacity>
            )
          }) : null}

          {/* </ImageBackground> */}
          {noData === false ?<Text style={{ marginHorizontal: 20, color: COLORS.secondary, fontSize: FONTS.sectionHeading, marginVertical: 20, fontFamily: FONTS.family500, marginTop:Platform.OS === "ios" ? 20: 40 }}>Past Bookings</Text>:null}
          <View style={{ width: Dimensions.get("screen").width, justifyContent: "center", alignItems: "center", paddingBottom: "20%" }}>{pastBookings ? pastBookings.map(item => {
            return (
              <Pressable onPress={() => {
                setViewPlace(item.venue)
                getBookingDetail(item._id, false)
              }}>
                <BookingItem feed={item} />
              </Pressable>)
          }) : null}</View>

        </View>
      </ScrollView> : <BookingSkeleton />}
    </SafeAreaView>
  )
}

export default CustomerBookingsScreen