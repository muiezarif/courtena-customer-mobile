import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:COLORS.brandLight
    },
    headerImage:{
        height:Dimensions.get("screen").height/3,
        // width:"100%",
        marginHorizontal:10,
        borderRadius:20,
        overflow:"hidden",
    },
    searchInputContainer:{
        height:50,
        backgroundColor:COLORS.white,
        marginTop:15,
        marginHorizontal:20,
        borderRadius:10,
        flexDirection:"row",
        alignItems:'center'
    },
    header:{
        marginTop:20,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"space-between",
        marginHorizontal:20
    },
    courtCard:{
        // height:150,
        // width:"100%",
        width:Dimensions.get('screen').width-40,
        elevation:15,
        shadowOffset: { width: 0, height: 5 },
        shadowColor: COLORS.lightGrey,
        shadowOpacity: 0.5,
        marginBottom:10,
        // marginHorizontal:5,
        borderRadius:15,
        backgroundColor:COLORS.white,
        shadowRadius:4
    },
    courtCardImage:{
        height:80,
        width:132,
        borderRadius:10,
        marginLeft:10,
        marginVertical:10
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
      },
      fragment: {
        backgroundColor: '#fff',
        padding: 16,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
      },
      fragmentText: {
        fontSize: 16,
        marginBottom: 10,
        color:COLORS.secondary,
        textAlign:"center",
        fontSize:20
      },
      closeButton: {
        
        position:"absolute",
        top:10,
        left:10
      },
      closeButtonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
      },
})


export default styles