import { View, Text, SafeAreaView,Image, TouchableOpacity, StatusBar, Dimensions, Pressable, Platform } from 'react-native'
import React, { useEffect, useState } from 'react'
import styles from './style'
import { useNavigation } from '@react-navigation/native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import COLORS from '../../utils/colors'
import FONTS from '../../utils/fonts'
import courtena from '../../api/courtena'
import Loader from '../../components/Loader'
import { request,PERMISSIONS,RESULTS } from 'react-native-permissions'
import Geolocation from 'react-native-geolocation-service';
import Geocoding from 'react-native-geocoding';

const GOOGLE_MAPS_API = "AIzaSyCbknAy3ZfqL2hw9HKAc16jVDkWydyMAwE"
const AfterOnboardingScreen = ({navigation}) => {
  // const navigation = useNavigation()
  const [isLoading, setIsLoading] = useState(false)
  const checkLoggedIn = async () => {
    const loggedIn = await AsyncStorage.getItem("customerRemainLoggedin")
    if(loggedIn){
      navigation.navigate("Main")
      // navigation.reset({
      //   index:0,
      //   routes:[{name:"Main"}]
      // })
    }
  }
  const askForPermission = (permission) => {
    request(permission).then((result) => {
      if(result === RESULTS.DENIED || RESULTS.BLOCKED){
        alert("Location permission is recommended to get the most out of courtena. Go to your settings and allow location permssion")
        console.log("Denied")
        navigation.navigate("Main")
      }
      if(result === RESULTS.GRANTED){
        console.log("Granted")
        updateMyLocation()
      }
      
    });
  }
 
  const updateMyLocation = async () => {

    Geocoding.init(GOOGLE_MAPS_API);

    // Get the device's location
    await Geolocation.getCurrentPosition(
      async (position) => {
        // alert(position)
        // Extract latitude and longitude from the position object
        const { latitude, longitude } = position.coords;
        if(latitude && longitude){
          await AsyncStorage.setItem("location", JSON.stringify({lat:latitude,lng:longitude}))
          navigation.navigate("Main")
        }


      },
      (error) => console.error(error),
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }
  

  const guestLogin = async() => {
    setIsLoading(true)
                const data = { phone: "+966567728989", password: "test1234" }
                await courtena.post("/auth/login-customer", { ...data }, {
                    headers: {
                        "Content-Type": 'application/x-www-form-urlencoded',
                        "Accept": '*/*'
                    }
                }).then(async (res) => {
                    setIsLoading(false)
                    if (res.data.success) {
                        console.log(res.data)
                        await AsyncStorage.setItem("customer", JSON.stringify(res.data.result))
                        await AsyncStorage.setItem("token", JSON.stringify(res.data.result.token))
                        await AsyncStorage.setItem("customerRemainLoggedin", "true")
                        const firstTimeUserInfo = await AsyncStorage.getItem("firstTimeUserInfoAdd")
                        // alert(res.data.message)
                        if (res.data.result.isFirstTime) {
                            await AsyncStorage.setItem("firstTimeUserInfoAdd", "true")
                            // if(Platform.OS === "android"){
                            //   askForPermission(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
                            // }else if(Platform.OS === "ios"){
                            //   askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS)
                              navigation.navigate("Main")
                            //   navigation.reset({
                            //     index: 0,
                            //     routes: [{ name: "Main" }],
                            // });
                            // }
                            
                        } else {
                            if (firstTimeUserInfo) {
                              // if(Platform.OS === "android"){
                              //   askForPermission(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
                              // }else if(Platform.OS === "ios"){
                              //   askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS)
                                navigation.navigate("Main")
                                // navigation.reset({
                                //     index: 0,
                                //     routes: [{ name: "Main" }],
                                // });
                              // }
                                
                            } else {
                                navigation.navigate("LocationAccess")
                                // navigation.reset({
                                //     index: 0,
                                //     routes: [{ name: "FirstTimeUserInfoAdd" }],
                                // });
                            }
                        }

                    } else {
                      setIsLoading(false)
                        console.log("error")
                        console.log(res.data)
                        alert(res.data.message)
                    }
                }).catch((err) => {
                    setIsLoading(false)
                    alert(err.message)
                })
  }

  useEffect(() => {
    checkLoggedIn()
  },[])
  return (
    <SafeAreaView style={[styles.container]}>
      <StatusBar translucent backgroundColor="transparent"/>
      {isLoading?<Loader/>:null}
      <Image style={styles.image} source={require("../../../assets/images/after_onboarding.png")}/>
      <View style={styles.imageOverlay}/>
      
      <Image style={{position:"absolute",zIndex:101,width:200,height:190,top:"30%"}} source={require("../../../assets/images/onboarding_logo.png")}/>
      <TouchableOpacity style={{position:"absolute",right:20,top:Platform.OS === "ios"? 50:"6%",zIndex:500}} onPress={guestLogin}><Text style={{ color: COLORS.white, fontSize: 16, fontFamily: FONTS.family }}>Skip</Text></TouchableOpacity>
    <View  style={styles.containerInner}>
      
      <View style={{flexDirection:'column',width:Dimensions.get("screen").width,paddingHorizontal:16}}>
      <Text style={styles.textOne}>Find the best padel experiences</Text>
      {/* <Text style={styles.textTwo}>Welcome to Courtena. One place for everything</Text> */}
      {/* <View>
        <TouchableOpacity style={styles.skipButton} onPress={guestLogin}>
          <Text style={{textAlign:"center",color:COLORS.accentLight,fontSize:FONTS.buttonTextSize,fontFamily:FONTS.family600}}>Skip</Text>
        </TouchableOpacity>
      </View> */}
      <View style={{flexDirection:'row',justifyContent:"space-between",marginTop:10}}>
        <TouchableOpacity style={styles.loginButton} onPress={async () => {
            
            navigation.navigate("Login")
            }}>
          <Text style={{textAlign:"center",color:COLORS.accentLight,fontSize:FONTS.buttonTextSize,fontFamily:FONTS.family600}}>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.registerButton} onPress={() => navigation.navigate("Register")}>
          <Text style={{textAlign:"center",color:COLORS.dark,fontSize:FONTS.buttonTextSize,fontFamily:FONTS.family600}}>Register</Text>
        </TouchableOpacity>
      </View>
      </View>
      
    </View>
    </SafeAreaView>
  )
}

export default AfterOnboardingScreen