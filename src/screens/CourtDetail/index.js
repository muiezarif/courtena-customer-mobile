import { View, Text, SafeAreaView, ImageBackground, StatusBar,Icon, TouchableOpacity, Image } from 'react-native'
import React, { useEffect, useState } from 'react'
import styles from "./styles"
import { ScrollView } from 'react-native-gesture-handler'
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons"
import EntypoIcon from "react-native-vector-icons/FontAwesome"
import COLORS from '../../utils/colors'
import AntDesign from 'react-native-vector-icons/AntDesign'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import FONTS from '../../utils/fonts'
const CourtDetailScreen = ({navigation,route}) => {
    const item = route.params
    const [pricingFrom,setPricingFrom] = useState(0)
    const [pricingTo,setPricingTo] = useState(0)
    console.log(item)

    const getPricingRange = () => {

        for (let index = 0; index < item.pricing[0].pricing.length; index++) {
            const element = item.pricing[0].pricing[index];
            if(element.active){
                setPricingFrom(element.price)
                break;
            }
        }
        for (let index = item.pricing[0].pricing.length -1; index >= 0 ; index--) {
            const element = item.pricing[0].pricing[index];
            if(element.active){
                setPricingTo(element.price)
                break;
            }
        }
    }
    useEffect(() => {
        getPricingRange()
    },[])
  return (
    <>
    <SafeAreaView style={styles.container}>
        <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
            paddingBottom: 20,
        }}
        >
            <StatusBar barStyle="light-content" translucent={true} backgroundColor="rgba(0,0,0,0)"/>
            <ImageBackground style={styles.headerImage} source={{uri:item.image}}>
                <View style={styles.header}>
                <TouchableOpacity style={{backgroundColor:COLORS.white,padding:10,borderRadius:30,alignContent:"center",justifyContent:"center",marginTop:4,marginLeft:4}} onPress={navigation.goBack}>
                        {/* <IonIcon name='arrow-back-circle' size={FONTS.largeIconSize} color={COLORS.dark} /> */}
                        <Image source={require("../../../assets/images/venue_detail_back.png")}/>
                        </TouchableOpacity>
                    {/* <IonIcon name='bookmark' size={FONTS.largeIconSize} color={COLORS.dark}/> */}
                </View>
            </ImageBackground>
            <View>
                {/* <View style={styles.iconContainer}>
                    <IonIcon name='time-outline' size={FONTS.regularIconSize} color={COLORS.secondary}/>
                </View> */}
                <View style={{marginTop:20,paddingHorizontal:20}}>
                    <Text style={{fontSize:FONTS.titleHeading,fontWeight:FONTS.extraBoldFontWeight,color:COLORS.dark}}>{item.title}</Text>
                    <View style={{flexDirection:"row",alignItems:"center",marginTop:20}}>
                            <View style={{flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                                {/* <Image source={require("../../../assets/images/clock.png")}/> */}
                                <EntypoIcon name='users' size={16} color={COLORS.dark}/>
                                <Text style={{marginLeft:5,fontSize:14,color:COLORS.dark}}>{item.maxPeople}</Text>
                            </View>
                            <View style={{flexDirection:"row",marginLeft:24}}>
                            <Image source={require("../../../assets/images/outdoor_icon.png")}/>
                                <Text style={{marginLeft:5,fontSize:14,color:COLORS.dark}}>{item.courtType}</Text>
                            </View>
                            <View style={{flexDirection:"row",marginLeft:24,alignContent:"center",justifyContent:"center"}}>
                                {item.advancedSettings.courtActive ?<MaterialCommunityIcon name='door-open'  size={16}/>:
                                <MaterialCommunityIcon name='curtains-closed'  size={16}/>}
                            {/* <Image source={require("../../../assets/images/rules.png")}/> */}
                                <Text style={{marginLeft:5,fontSize:14,color:COLORS.dark}}>{item.advancedSettings.courtActive ? "Active" : "Inactive"}</Text>
                            </View>
                        </View>
                    <Text style={{fontSize:FONTS.textSmall,fontWeight:FONTS.regularBoldFontWeight,color:"rgba(22, 34, 52, 0.60)",marginTop:20}}>Description</Text>
                    <Text style={{fontSize:FONTS.textDescription,fontWeight:FONTS.lightFontWeight,color:COLORS.dark,marginTop:5}}>{item.description}</Text>
                    <Text style={{fontSize:FONTS.textSmall,fontWeight:FONTS.regularBoldFontWeight,color:"rgba(22, 34, 52, 0.60)",marginTop:10}}>Features</Text>

                    <View style={{flexDirection:"row",marginTop:10}}>
                    {item.advancedSettings.bookableOnline?<View style={styles.featuresChips}>
                            <Text style={{fontSize:FONTS.textSmall,fontWeight:FONTS.regularBoldFontWeight}}>Bookable Online</Text>
                        </View>:null}
                        {item.courtFeature.wall?<View style={styles.featuresChips}>
                            <Text style={{fontSize:FONTS.textSmall,fontWeight:FONTS.regularBoldFontWeight}}>Wall</Text>
                        </View>:null}
                        {item.courtFeature.crystal?<View style={styles.featuresChips}>
                            <Text style={{fontSize:FONTS.textSmall,fontWeight:FONTS.regularBoldFontWeight}}>Standard</Text>
                        </View>:null}
                        {item.courtFeature.panoramic?<View style={styles.featuresChips}>
                            <Text style={{fontSize:FONTS.textSmall,fontWeight:FONTS.regularBoldFontWeight}}>Panoramic</Text>
                        </View>:null}
                        {item.courtFeature.single?<View style={styles.featuresChips}>
                            <Text style={{fontSize:FONTS.textSmall,fontWeight:FONTS.regularBoldFontWeight}}>Single</Text>
                        </View>:null}
                        {item.courtFeature.double?<View style={styles.featuresChips}>
                            <Text style={{fontSize:FONTS.textSmall,fontWeight:FONTS.regularBoldFontWeight}}>Double</Text>
                        </View>:null}
                    </View>
                {/* <View
                    style={{
                        marginTop:10,
                        flexDirection:"row",
                        justifyContent:"space-between",
                        alignItems:"center"
                    }}>
                    <View style={{flexDirection:"row", alignItems:"center"}}>
                        <View style={{flexDirection:"row"}}>
                            <IonIcon name='star' size={FONTS.cardMidIconSize} color={COLORS.star}/>
                            <IonIcon name='star' size={FONTS.cardMidIconSize} color={COLORS.star}/>
                            <IonIcon name='star' size={FONTS.cardMidIconSize} color={COLORS.star}/>
                            <IonIcon name='star' size={FONTS.cardMidIconSize} color={COLORS.star}/>
                            <IonIcon name='star' size={FONTS.cardMidIconSize} color={COLORS.starInactive}/>
                        </View> 
                        <Text style={{fontSize:FONTS.textDescription,marginLeft:10,color:COLORS.accentLight}}>4.0</Text>
                    </View>
                    <Text style={{fontSize:FONTS.smallTextDescription,color:COLORS.accentLight}}>250 reviews</Text>
                </View> */}
                <View style={{marginTop:20}}>
                    {/* <Text style={{lineHeight:20,fontSize:FONTS.textDescription, color:COLORS.accentLight}}>{item.description}</Text> */}
                </View>
                {/* <Text style={{fontSize:FONTS.textSmall,fontWeight:FONTS.regularBoldFontWeight,color:"rgba(22, 34, 52, 0.60)",marginTop:10}}>Information</Text> */}
                {/* <View style={{marginTop:20,flexDirection:"column"}}>
                        <View style={{flexDirection:"row", justifyContent:"space-between",paddingHorizontal:10,marginTop:20}}>
                            <View style={{flexDirection:"row"}}>
                                <View style={styles.amenitiesIconContainer}>
                                    <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,fontWeight:FONTS.regularBoldFontWeight,textAlign:"center"}}>Max People</Text> */}
                                    {/* <IonIcon name='star' size={FONTS.regularIconSize} color={COLORS.accentLight}/> */}
                                    {/* <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,marginTop:10}}>{item.maxPeople}</Text> */}
                                {/* </View> */}
                            {/* </View> */}
                            {/* <View style={{flexDirection:"row"}}>
                                <View style={styles.amenitiesIconContainer}>
                                <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,fontWeight:FONTS.regularBoldFontWeight,textAlign:"center"}}>Court Type</Text> */}
                                    {/* <IonIcon name='star' size={FONTS.regularIconSize} color={COLORS.accentLight}/> */}
                                    {/* <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,marginTop:10}}>{item.courtType}</Text> */}
                                {/* </View> */}
                            {/* </View> */}
                        {/* </View> */}
                        
                        {/* <View style={{flexDirection:"row", justifyContent:"space-between",paddingHorizontal:10,marginTop:20}}>
                            <View style={{flexDirection:"row"}}>
                                <View style={styles.amenitiesIconContainer}>
                                <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,fontWeight:FONTS.regularBoldFontWeight,textAlign:"center"}}>Active</Text> */}
                                    {/* <IonIcon name='star' size={FONTS.regularIconSize} color={COLORS.accentLight}/> */}
                                    {/* <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,marginTop:10}}>{item.advancedSettings.courtActive ? "Yes": "No"}</Text> */}
                                {/* </View> */}
                            {/* </View> */}
                            {/* <View style={{flexDirection:"row"}}>
                                <View style={styles.amenitiesIconContainer}>
                                <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,fontWeight:FONTS.regularBoldFontWeight,textAlign:"center"}}>Bookable Online</Text> */}
                                    {/* <IonIcon name='star' size={FONTS.regularIconSize} color={COLORS.accentLight}/> */}
                                    {/* <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,marginTop:10}}>{item.advancedSettings.bookableOnline? "Yes": "No"}</Text> */}
                                {/* </View> */}
                            {/* </View> */}
                        {/* </View> */}
                    {/* </View> */}
                {/* <View style={{tion,color:COLORS.dark,fontWeight:FONTS.regularBoldFontWeight,textAlign:"center"}}>Wall</Text> */}
                                    {/* <IonIcon name='star' size={FONTS.regularIconSize} color={COLORS.accentLight}/> */}
                                    {/* <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,marginTop:10}}>{item.courtFeature.wall ? "Yes":"No"}</Text>
                                </View>
                            </View> */}
                            {/* <View style={{flexDirection:"row"}}>
                                <View style={styles.amenitiesIconContainer}>
                                <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,fontWeight:FONTS.regularBoldFontWeight,textAlign:"center"}}>Crystal</Text> */}
                                    {/* <IonIcon name='star' size={FONTS.regularIconSize} color={COLORS.accentLight}/> */}
                                    {/* <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,marginTop:10}}>{item.courtFeature.crystal ? "Yes":"No"}</Text>
                                </View>
                            </View> */}
                        {/* </View> */}
                        {/* <View style={{flexDirection:"row", justifyContent:"space-between",paddingHorizontal:10,marginTop:20}}>
                            <View style={{flexDirection:"row"}}>
                                <View style={styles.amenitiesIconContainer}>
                                <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,fontWeight:FONTS.regularBoldFontWeight,textAlign:"center"}}>Panoramic</Text> */}
                                    {/* <IonIcon name='star' size={FONTS.regularIconSize} color={COLORS.accentLight}/> */}
                                    {/* <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,marginTop:10}}>{item.courtFeature.panoramic ? "Yes":"No"}</Text>
                                </View>
                                
                            </View> */}
                            {/* <View style={{flexDirection:"row"}}>
                                <View style={styles.amenitiesIconContainer}>
                                <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,fontWeight:FONTS.regularBoldFontWeight,textAlign:"center"}}>Single</Text> */}
                                    {/* <IonIcon name='star' size={FONTS.regularIconSize} color={COLORS.accentLight}/> */}
                                    {/* <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,marginTop:10}}>{item.courtFeature.single ? "Yes":"No"}</Text>
                                </View>
                                
                            </View>
                        </View> */}
                        {/* <View style={{flexDirection:"row", justifyContent:"space-between",paddingHorizontal:10,marginTop:20}}> */}
                            {/* <View style={{flexDirection:"row"}}> */}
                                {/* <View style={styles.amenitiesIconContainer}> */}
                                {/* <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,fontWeight:FONTS.regularBoldFontWeight,textAlign:"center"}}>Double</Text> */}
                                    {/* <IonIcon name='star' size={FONTS.regularIconSize} color={COLORS.accentLight}/> */}
                                    {/* <Text style={{fontSize:FONTS.textDescription,color:COLORS.dark,marginTop:10}}>{item.courtFeature.double ? "Yes":"No"}</Text> */}
                                {/* </View> */}
                            {/* </View> */}
                            {/* <View style={{flexDirection:"row"}}>
                                <View style={styles.amenitiesIconContainer}>
                                <Text style={{fontSize:FONTS.textDescription,color:COLORS.accent,fontWeight:FONTS.regularBoldFontWeight,textAlign:"center"}}>Reservations</Text>
                                    {/* <IonIcon name='star' size={FONTS.regularIconSize} color={COLORS.accentLight}/> */}
                                    {/* <Text style={{fontSize:FONTS.textDescription,color:COLORS.accentLight,marginTop:10}}>{item.bookingInfo.length}</Text> */}
                                {/* </View> */}
                            {/* </View> */} 
                        {/* </View>
                    </View> */}
                </View>
                {/* <Text style={{fontSize:FONTS.normalHeading,fontWeight:FONTS.regularBoldFontWeight,color:COLORS.accent,marginTop:20}}>Pricing</Text> */}
                <View style={{marginTop:20,flexDirection:"row",justifyContent:"space-between",alignItems:"center",paddingLeft:20}}>
                    <Text style={{fontSize:FONTS.sectionHeading,fontWeight:FONTS.regularLightBoldFontWeight,color:COLORS.dark}}>{pricingFrom} SAR - {pricingTo} SAR</Text>
                    <TouchableOpacity style={styles.bookCourtButton} onPress={() => navigation.navigate("BookCourt",item)}>
                        <Text style={{fontSize:FONTS.buttonTextSize,fontWeight:FONTS.regularBoldFontWeight,color:COLORS.brandLight,marginRight:5}}>Book Court</Text>
                        <AntDesign name='arrowright' size={24} color={COLORS.brandLight}/>
                    </TouchableOpacity>
                </View>
                
            </View>
        </ScrollView>
    </SafeAreaView>
    </>
  )
}

export default CourtDetailScreen