import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
import { StatusBar } from "react-native";
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:COLORS.white
    },
    headerImage:{
        flex:1,
        height:300,
        marginHorizontal:10,
        borderRadius:40,
        overflow:"hidden"
    },
    statusBar: {
        height: StatusBar.height,
        backgroundColor: 'transparent',
    },
    header:{
        marginTop:20,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"space-between",
        marginHorizontal:20
    },
    iconContainer:{
        position:"absolute",
        height:60,
        width:60,
        backgroundColor:COLORS.accentLight,
        top:-30,
        right:10,
        borderRadius:30,
        justifyContent:"center",
        alignItems:"center",
        paddingLeft:3
    },
    priceRangeContainer:{
        height:50,
        width:160,
        borderTopLeftRadius:20,
        borderBottomLeftRadius:20,
        // borderRadius:20,
        backgroundColor:COLORS.dark,
        alignItems:"center",
        justifyContent:"center"
    },
    amenitiesIconContainer:{
        height:100,
        width:180,
        backgroundColor:COLORS.brandLight,
        borderRadius:10,
        justifyContent:"center",
        alignItems:"center",
        // padding:10
    },
    bookCourtButton:{
        height:60,
        width:"40%",
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:COLORS.secondary,
        borderRadius:20,
        margin:20
    },
    featuresChips:{
        borderWidth:1,
        borderColor:"#0000001F",
        paddingHorizontal:10,
        paddingVertical:5,
        borderRadius:30,
        marginRight:5
    }
})


export default styles