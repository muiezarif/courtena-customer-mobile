import { View, Text } from 'react-native'
import React from 'react'
import TabRouter from '../../navigation/TabRouter'

const MainScreen = () => {
  return (
    <>
      <TabRouter/>
    </>
  )
}

export default MainScreen