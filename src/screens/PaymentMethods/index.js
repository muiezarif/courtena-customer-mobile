import { View, Text, SafeAreaView, Dimensions, TouchableOpacity, FlatList, Image, Modal, TextInput, Platform, KeyboardAvoidingView } from 'react-native'
import React, { useEffect, useState } from 'react'
import styles from "./styles"
import COLORS from '../../utils/colors'
import FONTS from '../../utils/fonts'
import { useNavigation } from '@react-navigation/native'
import courtena from '../../api/courtena'
import AsyncStorage from '@react-native-async-storage/async-storage'
import IonIcon from "react-native-vector-icons/Ionicons"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import { WebView } from 'react-native-webview';
import axios from 'axios'
import { encode } from 'base-64'
const PUBLISHABLE_MOYASAR_API_KEY = "pk_test_ih4qvc964KdbD2dxwGH6B7hRvbrWv46FbLnt2Z2N"
const SECRET_MOYASAR_API_KEY = "pk_test_ih4qvc964KdbD2dxwGH6B7hRvbrWv46FbLnt2Z2N"
const courtCardWidth = Dimensions.get('screen').width / 1.8
const PaymentMethods = ({}) => {
    const navigation = useNavigation()
    const [cards,setCards] = useState([])
    const [card,setCard] = useState({})
    const [name,setName] = useState("")
    const [cvc,setCvc] = useState("")
    const [number,setNumber] = useState("")
    const [date,setDate] = useState("")
    const [cardToVerify,setCardToVerify] = useState()
    const [callback_url,setCallbackUrl] = useState("https://api.courtena.com/api/customer/thanks-verify-card/")
  const [noData,setNoData] = useState(false)
  const [serverError,setServerError] = useState(false)
  const [resError,setResError] = useState(false)
  const [loading,setLoading] = useState(false)
  const [confirm_payment_url,setConfirmPaymentUrl] = useState(false)
  const [isEditModalVisible, setEditModalVisible] = useState(false);
  const [isConfirmCardModal, setConfirmCardModal] = useState(false);
  const [isAddModalVisible, setAddModalVisible] = useState(false);
  const [isAddCardModalVisible,setIsAddCardModalVisible] = useState(false)
  const [onceSavedCard,setOnceSavedCard] = useState(false)
  const [platformMarginTop,setPlatformMarginTop] = useState(0)


  const toggleEditModal = () => {
    setEditModalVisible(!isEditModalVisible);
  };
  const toggleAddModal = () => {
    setIsAddCardModalVisible(!isAddCardModalVisible);
  };
  const toggleConfirmCardModal = () => {
    setConfirmCardModal(!isConfirmCardModal);
  };

  function splitExpirationDate(expiration) {
    const [month, year] = expiration.split('/');
    return {
      month: month || '',
      year: year || '',
    };
  }

  const handleNavigationStateChange = (newNavState) => {
    console.log("state change")

    // Extract the new URL from the navigation state
    const newUrl = newNavState.url;
    console.log(newUrl)
    // Check if the new URL matches the expected structure
    const expectedUrlPattern = /https:\/\/api\.courtena\.com\/api\/customer\/thanks-verify-card\//;
    if (expectedUrlPattern.test(newUrl)) {
      const queryStartIndex = newUrl.indexOf('?');
      if (queryStartIndex !== -1) {
        const queryString = newUrl.substring(queryStartIndex + 1);
        const queryParams = queryString.split('&').reduce((params, param) => {
          const [key, value] = param.split('=');
          params[key] = decodeURIComponent(value);
          return params;
        }, {});

        const status = queryParams['status'];
        const message = queryParams['message'];
        console.log("message",message)
        console.log("status",status)

        // Check if status is 'paid' and message is 'APPROVED'
        if (message === 'APPROVED' && !onceSavedCard) {
          console.log("approved")

          // Perform your desired actions here
          // For example, display a success message or update the UI
          saveCard()
          setOnceSavedCard(true)
        }
      }
    }else {
      console.log("url not in")
    }
    
    // setCurrentUrl(newUrl);

    // You can perform any required actions here based on the new URL
    // For example, you might want to check if the new URL matches a certain pattern
    // and take appropriate actions.
  };

  const saveCard = async () => {
    console.log("in save card")
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const newData = {card:cardToVerify,customer:customerData._id}
        await courtena.post("/customer/save-customer-card/",{...newData},{
          headers: {
            'Content-Type':'application/x-www-form-urlencoded',
                'Accept': '*/*',
            'Authorization': customerData.token,
          },
        }).then((response) => {
          if (response.data.success) {
            
            getSavedCards()
            alert(response.data.message)
          } else {
            alert(response.data.message)
          }

        }).catch((err) => {
          alert(err.message)
        })
  }


  const addCard = async() => {
    setOnceSavedCard(false)
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const { month, year } = splitExpirationDate(date);
    const apiKey = PUBLISHABLE_MOYASAR_API_KEY;
    const encodedApiKey = encode(apiKey);

    const data = {name:name,cvc:cvc,number:number,month:month,year:year,callback_url:callback_url}
    const formData = new FormData()
    formData.append("name",name)
    formData.append("cvc",cvc)
    formData.append("number",number)
    formData.append("month",month)
    formData.append("year",year)
    formData.append("callback_url",callback_url)
    console.log(data)
    if(name && cvc && number && month && year && callback_url){
      await axios.post("https://api.moyasar.com/v1/tokens/",data,{
        headers: {
          Authorization: `Basic ${encodedApiKey}`,
        },
      }).then(async(res) => {
        console.log(res.data)
        setConfirmPaymentUrl(res.data.verification_url)
        setCardToVerify(res.data)
        toggleAddModal()
        toggleConfirmCardModal()
        
        // setLoading(false)
        
      }).catch((err) => {
        setLoading(false)
        setServerError(true)
        alert(err.message)
      })
    }else{
      alert("Please fill all fields first")
    }
    
  }

  const deleteCard = async() => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const apiKey = SECRET_MOYASAR_API_KEY;
    const encodedApiKey = encode(apiKey);
    
      await courtena.delete("/customer/delete-customer-card/"+card.card.id+"/"+customerData._id,{
        headers: {
          'Content-Type':'application/x-www-form-urlencoded',
              'Accept': '*/*',
          'Authorization': customerData.token,
        },
      }).then((res2) =>{
        if(res2){
          alert("Card deleted")
          toggleEditModal()
          getSavedCards()
        }else{
          alert("Error deleting card")
          // getSavedCards()
        }
        
      }).catch(err => {
        alert(err)
      }) 
  }

  const handleExpiryDateChange = (text) => {
    // Remove any non-digit characters
    const formattedText = text.replace(/\D/g, '');

    // Add a "/" after the first two characters
    let formattedExpiryDate = '';
    for (let i = 0; i < formattedText.length; i++) {
      if (i === 2) {
        formattedExpiryDate += '/';
      }
      formattedExpiryDate += formattedText[i];
    }

    // Update the state with the formatted text
    setDate(formattedExpiryDate);
  };

  const getSavedCards = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const customerToken = await AsyncStorage.getItem("token")
    setLoading(true)
    setNoData(false)
    setResError(false)
    setServerError(false)
    await courtena.get("/customer/get-saved-cards/"+customerData._id,{
      headers:{
        'Content-Type':'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      setLoading(false)
      if(res.data.success){
        // setCourts(res.data.result.courts)
        setCards(res.data.result)
        if(res.data.result.courts.length === 0){
          setNoData(true)
        }
      }else{
        setResError(true)
        alert(res.data.message)
      }
    }).catch((err) => {
      setLoading(false)
      setServerError(true)
      console.log(err)
    })
   }
   useEffect(() => {
    getSavedCards()
    if(Platform.OS === "android"){
      setPlatformMarginTop(40)
    }else{
      setPlatformMarginTop(0)
    }
   },[])

   const Venues = (props) => {
    const inputRange = [
      (props.index-1) * courtCardWidth ,
      props.index * courtCardWidth,
      (props.index+1) * courtCardWidth
     ]

  return(

    <TouchableOpacity onPress={() => navigation.navigate("VenueDetail",props.data)}>
      <View style={{...styles.courtCard}}>

        <View style={{flexDirection:"column"}}>
          <View style={{flexDirection:"row"}}>
          <Image source={{uri:props.data.photos[0]}} style={styles.courtCardImage}/>
          <View style={{flexDirection:"column",marginLeft:12,marginTop:10}}>
            <View style={{flexDirection:"row"}}>
            <Text style={{width:180,fontSize:FONTS.cardHeading,fontWeight:FONTS.regularBoldFontWeight,color:COLORS.dark}}>{props.data.name}</Text>
            <IonIcon name='bookmark-outline' size={20} style={{marginLeft:14,justifyContent:"flex-end"}}/>
            </View>
            <Text style={{fontSize:FONTS.cardDescription, color:COLORS.lightGrey}}>Location</Text>
            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
            <Text style={{marginTop:10,color:COLORS.secondary,fontWeight:FONTS.extraBoldFontWeight,width:180}}>SAR 50 - SAR 100</Text>
            <View style={{flexDirection:"row",borderRadius:20, backgroundColor:COLORS.accent,justifyContent:"center",alignItems:"center",padding:5}}>
            <FontAwesome5 name='location-arrow' size={8} color={COLORS.lightGrey}/>
            <Text style={{color:COLORS.lightGrey,fontWeight:FONTS.lightBoldFontWeight,fontSize:10,marginLeft:2}}>1.5km</Text>
            </View>
            </View>
          </View>

          </View>
          <View style={{borderWidth:1,borderColor:"#00000014",marginTop:10,marginHorizontal:20}}/>
          <View style={{flexDirection:"row",marginLeft:12,marginTop:10}}>
            <View style={{width:50,height:25,borderRadius:20,borderWidth:1,borderColor:"#00000014",justifyContent:"center",alignItems:"center",marginLeft:5}}>
                <Text style={{textAlign:"center",fontSize:12}}>11:00</Text>
            </View>
            <View style={{width:50,height:25,borderRadius:20,borderWidth:1,borderColor:"#00000014",justifyContent:"center",alignItems:"center",marginLeft:5}}>
                <Text style={{textAlign:"center",fontSize:12}}>11:00</Text>
            </View>
            <View style={{width:50,height:25,borderRadius:20,borderWidth:1,borderColor:"#00000014",justifyContent:"center",alignItems:"center",marginLeft:5}}>
                <Text style={{textAlign:"center",fontSize:12}}>11:00</Text>
            </View>
            <View style={{width:50,height:25,borderRadius:20,borderWidth:1,borderColor:"#00000014",justifyContent:"center",alignItems:"center",marginLeft:5}}>
                <Text style={{textAlign:"center",fontSize:12}}>11:00</Text>
            </View>
          </View>
        </View>

      </View>
      </TouchableOpacity>
  )
  }
  return (
    <SafeAreaView style={{flex:1,backgroundColor:COLORS.brandLight}}>
        <Modal
        animationType="slide"
        transparent={true}
        visible={isEditModalVisible}
        onRequestClose={toggleEditModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.fragment}>
            <Text style={[styles.fragmentText,{fontFamily:FONTS.family}]}>Selected Method Name</Text>
            {/* <TouchableOpacity style={styles.closeButton} > */}
              <IonIcon name='close' size={30} style={styles.closeButton} color={COLORS.secondary} onPress={() => toggleEditModal()}/>
            {/* </TouchableOpacity> */}
            <View style={{flexDirection:"column",marginTop:30,marginBottom:20}}>
                {/* <View style={{borderRadius:16,borderWidth:1,borderColor:COLORS.dividerBorderColor,alignItems:"center",justifyContent:"center",height:50}}>
                    <Text style={{color:COLORS.dark,fontWeight:FONTS.regularBoldFontWeight,fontFamily:FONTS.family}}>Edit Card Details</Text>
                </View> */}
                <View style={{borderRadius:16,borderWidth:1,borderColor:COLORS.dividerBorderColor,alignItems:"center",justifyContent:"center",height:50,marginTop:12}}>
                    <Text style={{color:COLORS.dark,fontWeight:FONTS.regularBoldFontWeight,fontFamily:FONTS.family}}>Set as Default</Text>
                </View>
                <TouchableOpacity onPress={() => deleteCard()} style={{borderRadius:16,borderWidth:1,borderColor:COLORS.dividerBorderColor,alignItems:"center",justifyContent:"center",height:50,marginTop:12,backgroundColor:COLORS.red}}>
                    <Text style={{color:COLORS.white,fontWeight:FONTS.regularBoldFontWeight,fontFamily:FONTS.family}}>Remove</Text>
                </TouchableOpacity>
            </View>
            
            
            
          </View>
        </View>
      </Modal>

      <Modal
        animationType="slide"
        transparent={true}
        visible={isConfirmCardModal}
        onRequestClose={toggleConfirmCardModal}
      >
        <View style={styles.modalContainer}>
          <View style={[styles.fragment,{height:Dimensions.get("screen").height/1.5}]}>
            <Text style={[styles.fragmentText,{fontFamily:FONTS.family}]}>Confirm Card</Text>
            <IonIcon name='close' size={30} style={styles.closeButton} color={COLORS.secondary} onPress={() => toggleConfirmCardModal()}/>
            {confirm_payment_url ?<WebView onNavigationStateChange={handleNavigationStateChange} source={{ uri: confirm_payment_url }}/>:null}
            
            
          </View>
        </View>
      </Modal>  

      {/* Modal for adding new card */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={isAddCardModalVisible}
        onRequestClose={toggleAddModal}
      >

        <View style={styles.modalContainer}>
        <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null} style={styles.containerInner}>

          <View style={styles.fragment}>
            <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Add Card</Text>
            {/* Close button */}
            <TouchableOpacity style={styles.closeButton} onPress={() => {
              toggleAddModal()
            }}>
              <IonIcon name='close' size={30} color={COLORS.secondary} />
            </TouchableOpacity>
            
            <View style={{ paddingBottom: 30 }}>
              <View style={{backgroundColor:COLORS.secondary,padding:16,borderRadius:12}}>
                  <View>
                      <Text style={{color:COLORS.brandLight,fontFamily:FONTS.family,fontSize:12}}>Card number</Text>
                      <TextInput style={{backgroundColor:COLORS.white,borderRadius:12,height:50,paddingHorizontal:16,marginTop:5}} value={number} onChangeText={(text) => setNumber(text)} placeholder='0000 0000 0000 0000' maxLength={16}/>
                  </View>
                  <View style={{flexDirection:"row",marginTop:10}}>
                    <View style={{width:"69%"}}>
                    <Text style={{color:COLORS.brandLight,fontFamily:FONTS.family,fontSize:12}}>Expiration date</Text>
                    <TextInput style={{backgroundColor:COLORS.white,borderRadius:12,height:50,paddingHorizontal:16,marginTop:5}} value={date} onChangeText={handleExpiryDateChange} placeholder='00/00' maxLength={5}/>
                    </View>
                    <View style={{width:"29%",marginLeft:10}}>
                    <Text style={{color:COLORS.brandLight,fontFamily:FONTS.family,fontSize:12}}>Security Code</Text>
                    <TextInput style={{backgroundColor:COLORS.white,borderRadius:12,height:50,paddingHorizontal:16,marginTop:5}} value={cvc} onChangeText={(text) => setCvc(text)} placeholder='000' maxLength={3}/>
                    </View>
                  </View>
                  <View style={{marginTop:10}}>
                  <Text style={{color:COLORS.brandLight,fontFamily:FONTS.family,fontSize:12}}>Card holder name</Text>
                      <TextInput style={{backgroundColor:COLORS.white,borderRadius:12,height:50,paddingHorizontal:16,marginTop:5}} value={name} onChangeText={(text) => setName(text)} placeholder='John Dou'/>
                  </View>
              </View>
              <Text style={{fontSize:12,fontFamily:FONTS.family,color:COLORS.darkOpacity,marginTop:16}}>The seurity code is the 3-digit code on the back of your credit or debit card on the right side of the signature strip.</Text>
            </View>
            <TouchableOpacity style={{
              height:50,
              alignItems:"center",
              justifyContent:"center",
              backgroundColor:COLORS.secondary,
              borderRadius:16,
              marginBottom:20
            }} onPress={addCard}>
              <Text style={{ fontSize: FONTS.buttonTextSize, fontWeight: FONTS.regularBoldFontWeight, color: COLORS.brandLight,fontFamily: FONTS.family }}>Add Card</Text>
            </TouchableOpacity>
          </View>
          </KeyboardAvoidingView>
        </View>
        
      </Modal>
      <View style={{flexDirection:"column",justifyContent:"space-between",paddingBottom:"20%",marginTop:platformMarginTop}}>
    <View style={{flexDirection:"row",alignItems:"center",marginTop:20}}>
        <TouchableOpacity style={{marginLeft:20,position:"absolute",zIndex:100}} onPress={() => navigation.goBack()}>
        <Text style={{fontSize:FONTS.buttonTextSize,color:COLORS.secondary,fontFamily:FONTS.family}}>Cancel</Text>
        </TouchableOpacity>
    <View style={{position:"absolute",flexDirection:"row",justifyContent:"center",marginTop:16,width:Dimensions.get("screen").width}}>
    <Text style={{alignSelf:"center",color:COLORS.secondary,fontSize:FONTS.sectionHeading,fontFamily:FONTS.family500}}>Payment Method</Text>
    </View>
    </View>
    {cards? 
    <FlatList
    style={{height:"100%"}}
    data={cards}
    contentContainerStyle={{paddingVertical:30,paddingHorizontal:20,marginTop:10}}
    showsHorizontalScrollIndicator={false}
    horizontal={false}
    renderItem={({item,index}) => (
      <View>
    <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center",height:60}}>
        <View style={{flexDirection:"row",alignItems:"center"}}>
          
            {item.card.brand === "master" && (<Image source={require("../../../assets/images/mastercard.png")} style={{width:36,height:24}}/>)}
            {item.card.brand === "visa" && (<Image source={require("../../../assets/images/visacard.png")} style={{width:36,height:24}}/>)}
            {item.card.brand === "amex" && (<Image source={require("../../../assets/images/amexcard.png")} style={{width:36,height:24}}/>)}

            <Text style={{fontSize:16,fontWeight:FONTS.lightBoldFontWeight,color:COLORS.dark,marginLeft:12,fontFamily:FONTS.family}}>•••• {item.card.last_four}</Text>
        </View>

        <TouchableOpacity onPress={() => {
          setCard(item)
          setEditModalVisible(true)
          }}><Image style={{alignSelf:"baseline"}} source={require("../../../assets/images/more_img.png")}/></TouchableOpacity>
          
    </View>
    <View style={{width:"100%",backgroundColor:COLORS.dividerBorderColor,height:0.5}}/>

    </View>
    ) }
    snapToInterval={courtCardWidth}
  />
    
    
    :<View style={{marginTop:20,justifyContent:"center",alignItems:"center",height:"100%"}}>
        <Text style={{fontSize:FONTS.sectionHeading,fontWeight:FONTS.regularLightBoldFontWeight,colo:COLORS.lightGrey}}>Nothing saved just yet</Text>
        <Text style={{marginTop:8,fontSize:FONTS.textDescription,color:COLORS.lightGrey}}>Save your court to favorites</Text>
        <View style={{backgroundColor:COLORS.secondary,paddingHorizontal:10,paddingVertical:5,borderRadius:50,marginTop:16}}>
            <Text style={{color:COLORS.brandLight,fontWeight:FONTS.regularBoldFontWeight,fontSize:FONTS.textDescription}}>Find court</Text>
        </View>
    </View>}
      <TouchableOpacity onPress={toggleAddModal} style={{height:50,backgroundColor:COLORS.secondary,marginHorizontal:20,borderRadius:16,justifyContent:"center",alignItems:"center"}}>
        <Text style={{color:COLORS.brandLight,fontSize:16,fontFamily:FONTS.family600}}>Add New Card</Text>
      </TouchableOpacity>
    </View>
    </SafeAreaView>
  )
}

export default PaymentMethods