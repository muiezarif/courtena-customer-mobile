
import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
const styles = StyleSheet.create({
    courtCard:{
        height:150,
        // width:"100%",
        width:Dimensions.get('screen').width-40,
        elevation:15,
        shadowOffset: { width: 0, height: 5 },
        shadowColor: COLORS.secondary,
        shadowOpacity: 1,
        marginBottom:10,
        // marginHorizontal:5,
        borderRadius:15,
        backgroundColor:COLORS.white,
        shadowRadius:4
    },
    courtCardImage:{
        height:80,
        width:132,
        borderRadius:10,
        marginLeft:10,
        marginTop:10
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
      },
      fragment: {
        backgroundColor: '#fff',
        padding: 16,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
      },
      fragmentText: {
        fontSize: 16,
        marginBottom: 10,
        color:COLORS.secondary,
        textAlign:"center",
        fontSize:20
      },
      closeButton: {
        
        position:"absolute",
        top:10,
        left:10
      },
      closeButtonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
      },
})


export default styles