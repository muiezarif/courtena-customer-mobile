import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:COLORS.brandLight,
        width:Dimensions.get("screen").width,
        alignItems:'center',
        justifyContent:'center'
    },
    header:{
        marginTop:20,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"space-between",
        marginHorizontal:20,
        position:"absolute",
        top:40,
        left:0,
    }, 
    containerInner:{
        // flex:1,
        // backgroundColor:"#fff",
        width:Dimensions.get("screen").width,
        alignItems:'center',
        justifyContent:'center'
    }, 
    image:{
        width:Dimensions.get('screen').width,
        height:200,
        resizeMode:'contain',
        // backgroundColor:"#000"
    },
    textOne:{
        fontSize:40,
        fontWeight:'bold'
    },
    textTwo:{
        fontSize:16,
        color:'#ddd',
        textAlign:'center',
        marginHorizontal:20
    },
    loginButton:{
        backgroundColor:"#0d47a1",
        padding:10,
        width:100,
        borderRadius:30,
        marginHorizontal:10,
        width:150,
        fontSize:18
    },
    registerButton:{
        backgroundColor:"#fff",
        padding:10,
        width:100,
        borderRadius:30,
        borderWidth:1,
        marginHorizontal:10,
        width:150,
        fontSize:18
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
      },
      fragment: {
        backgroundColor: '#fff',
        padding: 16,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
      },
      fragmentText: {
        fontSize: 16,
        marginBottom: 10,
        color:COLORS.secondary,
        textAlign:"center",
        fontSize:20,
      },
      closeButton: {
        
        position:"absolute",
        top:10,
        left:10
      },
      closeButtonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
      },
    
})


export default styles