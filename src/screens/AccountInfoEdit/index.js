import { View, Text, SafeAreaView,Image, Dimensions, Modal } from 'react-native'
import React,{useEffect, useState} from 'react'
import styles from "./styles"
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'
import * as Animatable from 'react-native-animatable';
import COLORS from '../../utils/colors'
import AsyncStorage from '@react-native-async-storage/async-storage'
import FONTS from '../../utils/fonts'
import courtena from '../../api/courtena'
import IonIcon from"react-native-vector-icons/Ionicons"
import Avatar from '../../components/Avatar'
import ReactNativeModernDatepicker from 'react-native-modern-datepicker'
const AccountInfoEditScreen = ({navigation,route}) => {
  const [username,setUsername] = useState("")
  const [firstname,setFirstname] = useState("")
  const [lastname,setLastname] = useState("")
  const [email,setEmail] = useState("")
  const [country,setCountry] = useState("")
  const [city,setCity] = useState("")
  const [gender,setGender] = useState("")
  const [dob,setDob] = useState("")
  const [errMsg,setErrMsg] = useState("")
  const [isModalVisible, setModalVisible] = useState(false);
  const [isDateModalVisible, setDateModalVisible] = useState(false);
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  const toggleDateModal = () => {
    setDateModalVisible(!isDateModalVisible);
  };

  const updateUser = async() => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const data = {username:username,email:email,country:country,city:city,isFirstTime:false,gender:gender,dob:dob}
    if(gender && email && dob && firstname && lastname){
        await courtena.put("/customer/customer-update-info/"+customerData._id,{...data},{
            headers:{
                'Content-Type':'application/x-www-form-urlencoded',
                'Accept': '*/*',
                'Authorization': customerData.token
              }
        }).then(async (res) => {
            if(res.data.success){
                // alert("success")
                // await AsyncStorage.setItem("firstTimeUserInfoAdd","true")
                // navigation.navigate("Main")
                //       navigation.reset({
                //       index: 0,
                //       routes: [{ name: "Main" }],
                //   });
                alert("Information Updated")
            }else{
                alert(res.data.message)
            }
        }).catch((err) => {
            console.log(err)
        })
    }else{
        alert("Please fill required information")
    }
    
  }
  const getUserInfo = async() => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    await courtena.get("/customer/get-customer-info/"+customerData._id,{
        headers:{
            'Content-Type':'application/x-www-form-urlencoded',
            'Accept': '*/*',
            'Authorization': customerData.token
          }
    }).then((res) => {
        // console.log(res.data)
        if(res.data.success){
            setUsername(res.data.result.username)
            setEmail(res.data.result.email)
            setCountry(res.data.result.country)
            setCity(res.data.result.city)
            setFirstname(res.data.result.first_name)
            setLastname(res.data.result.last_name)
            setGender(res.data.result.gender)
            setDob(res.data.result.dob)
        }else{
            console.log(res.data.message)
        }
    }).catch((err) => {
        console.log(err)
    })
  }
  useEffect(() => {
    getUserInfo()
  },[])
  return (
    <SafeAreaView style={[styles.container]}>
        <View style={styles.header}>
                    <TouchableOpacity onPress={navigation.goBack}>
                        {/* <IonIcon name='arrow-back-circle' size={FONTS.largeIconSize} color={COLORS.dark} /> */}
                        <Text style={{fontSize:FONTS.buttonTextSize,color:COLORS.dark,fontFamily:FONTS.family}}>Back</Text>
                        </TouchableOpacity>
                </View>
                <View style={{justifyContent:"center",alignContent:"center"}}>
            {/* <Image style={{width:120,height:120,borderRadius:60}} source={require("../../../assets/images/login_image.png")}/> */}
            <Avatar width={120} height={120} name={firstname}/>
            {/* <Text style={{textAlign:"center",paddingVertical:4,paddingHorizontal:10,borderWidth:1,borderColor:"#00000012",borderRadius:15,marginTop:12,fontFamily:FONTS.family}}>Change photo</Text> */}
        </View>
    <View style={ styles.containerInner}>
        
    {/* <Image style={styles.image} source={require("../../../assets/images/login_img.png")}/> */}
                {/* <Text style={{ fontSize: FONTS.textDescription,fontWeight:FONTS.lightFontWeight, color: COLORS.accentLight, marginTop: 20 }}>Sign in to continue</Text> */}

                <Animatable.View
                    style={{justifyContent:"center",alignItems:"center",width:Dimensions.get("screen").width,paddingHorizontal:20}}
                    ref={this.validateInput}
                >
                    <View style={{flexDirection:"row",justifyContent:"center"}}>
                    <TextInput
                    style={{ marginTop: 20,width: "48%",marginRight:10, borderColor: "#00000012",borderRadius:12, borderWidth: 1,padding:20,color:COLORS.dark,backgroundColor:COLORS.white,fontFamily:FONTS.family }}
                    placeholder="First Name"
                    value={firstname}
                    editable={false} selectTextOnFocus={false}
                    placeholderTextColor={COLORS.lightGrey}
                    onChangeText = {(text) => 
                        {
                            setErrMsg(''),
                            setFirstname(text)
                        }
                    }
                />
                <TextInput
                    style={{ marginTop: 20,width: "48%", borderColor: "#00000012",borderRadius:12, borderWidth: 1,padding:20,color:COLORS.dark,backgroundColor:COLORS.white,fontFamily:FONTS.family }}
                    placeholder="Last Name"
                    value={lastname}
                    editable={false} selectTextOnFocus={false}
                    placeholderTextColor={COLORS.lightGrey}
                    onChangeText = {(text) => 
                        {
                            setErrMsg(''),
                            setLastname(text)
                        }
                    }
                />
                    </View>
                {/* <TextInput
                    style={{ marginTop: 20, borderColor: "#00000012",borderRadius:12, borderWidth: 1,padding:20,color:COLORS.dark,backgroundColor:COLORS.white,width:"100%",fontFamily:FONTS.family }}
                    placeholder="Username"
                    value={username}
                    editable={true} selectTextOnFocus={false}
                    placeholderTextColor={COLORS.accentLight}
                    onChangeText = {(text) => 
                        {
                            setErrMsg(''),
                            setUsername(text)
                        }
                    }
                /> */}

                <TextInput
                    style={{ marginTop: 20, borderColor: "#00000012",borderRadius:12, borderWidth: 1,padding:20,color:COLORS.dark,backgroundColor:COLORS.white,width:"100%",fontFamily:FONTS.family }}
                    placeholder="Email"
                    value={email}
                    editable={false} selectTextOnFocus={false}
                    placeholderTextColor={COLORS.accentLight}
                    // secureTextEntry={true}
                    onChangeText = {(text) => 
                        {
                            setErrMsg('')
                            setEmail(text)
                        }
                      }

                />
                <Animatable.View
            ref={this.validateInput}
            style={{ flexDirection: "row" }}
          >
            <TextInput
              style={{ marginTop: 24, width: "49%", height: 50, borderColor: COLORS.dividerBorderColor, borderRadius: 12, borderWidth: 1, paddingLeft: 20, color: COLORS.secondary, backgroundColor: COLORS.white,marginRight:5 }}
              placeholder="Gender"
              value={gender}
              editable={Platform.OS ==="ios"? false:true}
              placeholderTextColor="lightgrey"
              onPressIn={() => { setModalVisible(true) }}
            />
            <TextInput
              style={{ marginTop: 24, width: "49%", height: 50, borderColor: COLORS.dividerBorderColor, borderRadius: 12, borderWidth: 1, paddingLeft: 20, color: COLORS.secondary, backgroundColor: COLORS.white }}
              placeholder="Date of Birth"
              value={dob}
              editable={Platform.OS ==="ios"? false:true}
              placeholderTextColor="lightgrey"
              onPressIn={() => { setDateModalVisible(true) }}
            /></Animatable.View>

                {/* <TextInput
                    style={{ marginTop: 20, borderColor: "#00000012",borderRadius:12, borderWidth: 1,padding:20,color:COLORS.dark,backgroundColor:COLORS.white,width:"100%",fontFamily:FONTS.family }}
                    placeholder="Country"
                    value={country}
                    placeholderTextColor={COLORS.accentLight}
                    // secureTextEntry={true}
                    onChangeText = {(text) => 
                        {
                            setErrMsg('')
                            setCountry(text)
                        }
                      }

                /> */}

                {/* <TextInput
                    style={{ marginTop: 20, borderColor: "#00000012",borderRadius:12, borderWidth: 1,padding:20,color:COLORS.dark,backgroundColor:COLORS.white,width:"100%",fontFamily:FONTS.family }}
                    placeholder="City"
                    placeholderTextColor={COLORS.accentLight}
                    value={city}
                    // secureTextEntry={true}
                    onChangeText = {(text) => 
                        {
                            setErrMsg('')
                            setCity(text)
                        }
                      }

                /> */}
                <Text style={{ color: 'red', textAlign: 'center', marginTop: 10,fontFamily:FONTS.family }}>{errMsg}</Text>
                
                </Animatable.View>

                

                <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 10 }}>
                    <TouchableOpacity
                        onPress={updateUser}
                        style={{ width:Dimensions.get("screen").width-40,height:50, backgroundColor: COLORS.secondary, padding: 10, alignItems: 'center', justifyContent: 'center', borderRadius: 16, marginTop: 0 }}
                    >
                        <Text style={{ textAlign: 'center', color: COLORS.brandLight, fontSize: FONTS.buttonTextSize,fontFamily:FONTS.family }}>Save</Text>
                    </TouchableOpacity>

                </View>
            
            </View>
            <Modal
        animationType="slide"
        transparent={true}
        visible={isModalVisible}
        onRequestClose={toggleModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.fragment}>
            <Text style={styles.fragmentText}>Select Gender</Text>
            {/* <TouchableOpacity style={styles.closeButton} > */}
            <IonIcon name='close' size={30} style={styles.closeButton} color={COLORS.secondary} onPress={() => toggleModal()} />
            {/* </TouchableOpacity> */}

            <View style={{marginTop:14}}> 
              <Text style={{color:COLORS.dark,fontSize:16,fontFamily:FONTS.family,marginTop:12}} onPress={() => {
                setGender("Male")
                toggleModal()
              }}>Male</Text>
              <View style={{height:0.5,backgroundColor:COLORS.dividerBorderColor,width:"100%",marginVertical:12}}/>
              <Text onPress={() => {
                setGender("Female")
                toggleModal()
              }} style={{color:COLORS.dark,fontSize:16,fontFamily:FONTS.family,marginBottom:12}}>Female</Text>
            </View>
            {/* <Picker
              selectedValue={gender}
              onValueChange={(itemValue) => setGender(itemValue)}
            >
              <Picker.Item label="Male" value="Male" />
              <Picker.Item label="Female" value="Female" />
            </Picker> */}
          </View>
        </View>
      </Modal>
      <Modal
        animationType="slide"
        transparent={true}
        visible={isDateModalVisible}
        onRequestClose={toggleDateModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.fragment}>
            <Text style={styles.fragmentText}>Select Date of Birth</Text>
            {/* <TouchableOpacity style={styles.closeButton} > */}
            <IonIcon name='close' size={30} style={styles.closeButton} color={COLORS.secondary} onPress={() => toggleDateModal()} />
            {/* </TouchableOpacity> */}
            <ReactNativeModernDatepicker
              mode='date'
              onDateChange={(date) => {
                setDob(date)
                toggleDateModal()
              }}
            />

          </View>
        </View>
      </Modal>
    </SafeAreaView>
  )
}

export default AccountInfoEditScreen