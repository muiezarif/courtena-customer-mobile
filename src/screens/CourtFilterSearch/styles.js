import { StyleSheet,Dimensions } from "react-native";
const styles = StyleSheet.create({
    row:{
        flexDirection:'row',
        justifyContent:'space-between',
        padding:20,
        borderBottomWidth:1,
        borderColor:"lightgrey"
    },
    rowBtn:{
        flexDirection:'row',
        justifyContent:'space-between',
        padding:10
    },
    button:{
        paddingHorizontal:10,
        borderWidth:1,
        width:30,
        height:30,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:15,
        borderColor:'lightgrey'
    }
})


export default styles