import { View, Text, Pressable, SafeAreaView } from 'react-native'
import React, { useState } from 'react'
import styles from './styles'
const CourtFilterSearchScreen = () => {
    const [adults,setAdults] = useState(0)
  return (
    <SafeAreaView>
    <View>
        <View style={styles.row}>
            {/* titles */}
            <View>
                <Text style={{fontWeight:'bold'}}>Adults</Text>
                <Text style={{color:'#8d8d8d'}}>Age 13 or above</Text>
            </View>
            {/* Buttons with value   */}
            <View style={styles.rowBtn}>
                <Pressable onPress={() => {setAdults(Math.max(0,adults - 1))}} style={styles.button}>
                    <Text style={{color:'#8d8d8d'}}>-</Text>
                </Pressable>
                {/* <Pressable onPress={() => {console.log("clicked")}}> */}
                    <Text style={{marginHorizontal:16,fontSize:20}}>{adults}</Text>
                {/* </Pressable> */}
                <Pressable onPress={() => {setAdults(adults +1)}} style={styles.button}>
                    <Text style={{color:'#8d8d8d'}}>+</Text>
                </Pressable>
            </View>
        </View>
    </View>
    </SafeAreaView>
  )
}

export default CourtFilterSearchScreen