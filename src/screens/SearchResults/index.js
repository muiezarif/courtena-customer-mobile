import { View, Text, FlatList, SafeAreaView } from 'react-native'
import React from 'react'
import styles from './styles'
import feed from '../../../assets/data/feed'
import Venue from '../../components/Venue'
const SearchResultsScreen = (props) => {
    
  return (
    <SafeAreaView>
    <View>
      <FlatList
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}
      data={feed}
      renderItem={({item}) => <Venue feed={item} />}/>
    </View>
    </SafeAreaView>
  )
}

export default SearchResultsScreen