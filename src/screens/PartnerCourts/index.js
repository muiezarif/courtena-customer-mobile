import { View, Text, SafeAreaView, TextInput, FlatList, TouchableOpacity } from 'react-native'
import React, { useEffect, useState } from 'react'
import styles from './styles'
import feed from '../../../assets/data/feed'
import AntDesign from "react-native-vector-icons/AntDesign"
import FONTS from '../../utils/fonts'
import COLORS from '../../utils/colors'
import Court from '../../components/Court'
import courtena from '../../api/courtena'
import AsyncStorage from '@react-native-async-storage/async-storage'
import NoData from '../../components/NoData'
import Loading from '../../components/Loading'
import ResponseError from '../../components/ResponseError'
import ServerError from '../../components/ServerError'
const PartnerCourtsScreen = ({navigation,route}) => {
  const item = route.params
  const [courts,setCourts] = useState([])
  const [noData,setNoData] = useState(false)
  const [serverError,setServerError] = useState(false)
  const [resError,setResError] = useState(false)
  const [loading,setLoading] = useState(false)
  // console.log(item._id+" - "+item.partner)

  const getCourtsByPartnerVenue = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    setLoading(true)
    setNoData(false)
    setResError(false)
    setServerError(false)
      await courtena.get("/customer/get-partner-venue-courts/"+item.partner+"/"+item._id,{
        headers:{
          'Content-Type':'application/x-www-form-urlencoded',
          'Accept': '*/*',
          'Authorization': customerData.token
        }
      }).then((res) => {
        setLoading(false)
        // console.log(res.data)
        if(res.data.success){
          setCourts(res.data.result)
          if(res.data.result.length === 0){
            setNoData(true)
          }
        }else{
          setResError(true)
        }
      }).catch((err) => {
        setLoading(false)
        setServerError(true)
        console.log(err)
      })
  }
  useEffect(() => {
    getCourtsByPartnerVenue()
  },[])
  return (
    <SafeAreaView style={styles.container}>
    <View>
    <TouchableOpacity onPress={navigation.goBack}><AntDesign name='back' size={FONTS.regularIconSize} color={COLORS.accent} style={{marginLeft:20}} /></TouchableOpacity>
        {resError? <View style={{alignItems:"center",justifyContent:"center", height:"80%"}}><ResponseError/></View>:null}
        {serverError? <View style={{alignItems:"center",justifyContent:"center", height:"80%"}}><ServerError/></View>:null}
        {loading? <View style={{alignItems:"center",justifyContent:"center", height:"80%"}}><Loading/></View>:null}
        {noData ? <View style={{alignItems:"center",justifyContent:"center", height:"80%"}}><NoData/></View>:null}
      {courts ?<FlatList
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}
      data={courts}
      renderItem={({item}) => {return(
        <TouchableOpacity onPress={() => navigation.navigate("CourtDetail",item)}>
          <Court feed={item} />
        </TouchableOpacity>
      )}}/>: null}
    </View>
    </SafeAreaView>
  )
}

export default PartnerCourtsScreen