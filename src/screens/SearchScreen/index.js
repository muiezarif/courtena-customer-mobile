import { View, Text, SafeAreaView, TextInput, FlatList, TouchableOpacity, Image, Modal, ScrollView, Dimensions, Pressable, Platform } from 'react-native'
import { DatePickerIOS, DatePickerAndroid } from '@react-native-community/datetimepicker'
import DateTimePickerModal from 'react-native-modal-datetime-picker'
import React, { useEffect, useState } from 'react'
import styles from './styles'
import feed from '../../../assets/data/feed'
import AntDesign from "react-native-vector-icons/AntDesign"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import IonIcon from "react-native-vector-icons/Ionicons"
import FONTS from '../../utils/fonts'
import COLORS from '../../utils/colors'
import Court from '../../components/Court'
import courtena from '../../api/courtena'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useIsFocused, useNavigation } from '@react-navigation/native'
import NoData from '../../components/NoData'
import Loading from '../../components/Loading'
import ResponseError from '../../components/ResponseError'
import ServerError from '../../components/ServerError'
import ReactNativeModernDatepicker from 'react-native-modern-datepicker'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'
import Geolocation from 'react-native-geolocation-service';
import Geocoding from 'react-native-geocoding';
import { request, PERMISSIONS, RESULTS } from 'react-native-permissions'

const GOOGLE_MAPS_API = "AIzaSyCbknAy3ZfqL2hw9HKAc16jVDkWydyMAwE"
const SearchScreen = ({ route }) => {
  const data = route.params
  // console.log(data)
  const [myLat,setMyLat] = useState(null)
  const [myLng,setMyLng] = useState(null)
  const [courts, setCourts] = useState([])
  const [venues, setVenues] = useState([])
  const [search, setSearch] = useState([])
  const [noData, setNoData] = useState(false)
  const [date, setDate] = useState("")
  const [time, setTime] = useState("")
  const [serverError, setServerError] = useState(false)
  const [resError, setResError] = useState(false)
  const [loading, setLoading] = useState(false)
  const [isModalVisible, setModalVisible] = useState(false);
  const [distance, setDistance] = useState(false);
  const [price, setPrice] = useState(false);
  const [indoor, setIndoor] = useState(false);
  const [outdoor, setOutdoor] = useState(false);
  const [wall, setWall] = useState(false);
  const [crystal, setCrystal] = useState(false);
  const [panoramic, setPanoramic] = useState(false);
  const [roofedOutdoor, setRootedOutdoor] = useState(false);
  const [single, setSingle] = useState(false);
  const [double, setDouble] = useState(false);
  const [rentEquipment, setRentEquipment] = useState(false);
  const [lockers, setLockers] = useState(false);
  const [showers, setShowers] = useState(false);
  const [toilet, setToilet] = useState(false);
  const [freeParking, setFreeParking] = useState(false);
  const [paidParking, setPaidParking] = useState(false);
  const [wifi, setWifi] = useState(false);
  const [restaurant, setRestaurant] = useState(false);
  const [snackbar, setSnackbar] = useState(false);
  const [kidsPlayground, setKidsPlayground] = useState(false);
  const [specialAccess, setSpecialAccess] = useState(false);
  const [isDateModalVisible, setDateModalVisible] = useState(false);
  const [isLocationModalVisible, setLocationModalVisible] = useState(false);
  const isFocused = useIsFocused();
  const navigation = useNavigation()
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [isDatePicker, setIsDatePicker] = useState(false)
  const [dateTime, setDateTime] = useState()
  const [city, setCity] = useState("")
  const [platformMarginTop,setPlatformMarginTop] = useState(0)


  const handleDateChange = (newDate) => {
    setSelectedDate(newDate);
  };
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  const toggleDateModal = () => {
    setDateModalVisible(!isDateModalVisible);
  };
  const toggleLocationModal = () => {
    setLocationModalVisible(!isLocationModalVisible);
  };
  const parseDateTimeString = (dateTimeString) => {
    const dateTime = new Date(dateTimeString);

    // Extract date components
    const year = dateTime.getFullYear();
    const month = dateTime.getMonth() + 1; // Month is zero-based, so adding 1
    const day = dateTime.getDate();

    // Extract time components
    const hours = dateTime.getHours();
    const minutes = dateTime.getMinutes();
    const seconds = dateTime.getSeconds();

    // Format time with leading zeros
    const formattedTime = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;

    return {
      date: `${year}-${month.toString().padStart(2, '0')}-${day.toString().padStart(2, '0')}`,
      time: formattedTime,
    };
  };
  const getAllVenues = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    setLoading(true)
    setNoData(false)
    setResError(false)
    setServerError(false)
    courtena.get("/customer/get-venues", {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      setLoading(false)
      if (res.data.success) {
        setVenues(res.data.result)
        if (res.data.result.length === 0) {
          setNoData(true)
        }
      } else {
        setResError(true)
      }
    }).catch((err) => {
      setLoading(false)
      setServerError(true)
    })
  }
  const getAllCourts = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    setLoading(true)
    setNoData(false)
    setResError(false)
    setServerError(false)
    courtena.get("/customer/get-courts", {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      setLoading(false)
      if (res.data.success) {
        setCourts(res.data.result)
        if (res.data.result.length === 0) {
          setNoData(true)
        }
      } else {
        setResError(true)
      }
    }).catch((err) => {
      setLoading(false)
      setServerError(true)
    })
  }
  const resetFilters = async() => {
    setCity("")
    setDate("")
    setTime("")
    setDateTime("")
    setDistance(false)
    setPrice(false)
    setIndoor(false)
    setOutdoor(false)
    setRootedOutdoor(false)
    setWall(false)
    setCrystal(false)
    setPanoramic(false)
    setRentEquipment(false)
    setLockers(false)
    setToilet(false)
    setFreeParking(false)
    setWifi(false)
    setRestaurant(false)
    setSnackbar(false)
    setKidsPlayground(false)
    setSpecialAccess(false)
    setPaidParking(false)
    setShowers(false)
    setSingle(false)
    setDouble(false)
    
  }
  const getCourtsBySearch = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    // setCourts([])
    setLoading(true)
    setNoData(false)
    // setResError(false)
    // setServerError(false)
    const data = { city: city, date: date, time: time, filter: { distance: distance, price: price, indoor: indoor, outdoor: outdoor, roofedOutdoor: roofedOutdoor, wall: wall, crystal: crystal, panoramic: panoramic, rentEquipment: rentEquipment, lockers: lockers, toilet: toilet, freeParking: freeParking, wifi: wifi, restaurant: restaurant, snackbar: snackbar, kidsPlayground: kidsPlayground, disabledAccess: specialAccess, paidParking: paidParking, showers: showers, single: single, double: double } }
    await courtena.post("/customer/get-venues-by-search/", {...data}, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      // console.log(res.data)
      setLoading(false)
      if (res.data.success) {
        console.log("IN THIS")
        setCourts(res.data.result.courts)
        setVenues(res.data.result.venues)
        // console.log(venues)
        if (res.data.result.venues.length === 0) {
          setNoData(true)
        }
      } else {
        setResError(true)
      }
    }).catch((err) => {
      setLoading(false)
      setServerError(true)
      console.log(err)
    })
  }

  const VenuesSkeleton = () => {
    return (
      <SkeletonPlaceholder borderRadius={1}>
        
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
      </SkeletonPlaceholder>
    )


  }
    // Function to calculate the distance between two coordinates using Haversine formula
    function calculateDistance(lat1, lon1, lat2, lon2) {
      const R = 6371; // Earth radius in kilometers
      const dLat = (lat2 - lat1) * (Math.PI / 180);
      const dLon = (lon2 - lon1) * (Math.PI / 180);
      const a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(lat1 * (Math.PI / 180)) *
          Math.cos(lat2 * (Math.PI / 180)) *
          Math.sin(dLon / 2) *
          Math.sin(dLon / 2);
      const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      const distance = R * c; // Distance in kilometers
      return distance;
    }

    const askForPermission = (permission) => {
      request(permission).then((result) => {
        if (result === RESULTS.DENIED) {
          alert("Location permission is recommended to get the most out of courtena. Go to your settings and allow location permssion")
          
        }
        if (result === RESULTS.BLOCKED) {
          alert("Location permission is recommended to get the most out of courtena. Go to your settings and allow location permssion")
          
        }
        if (result === RESULTS.GRANTED) {
          // Initialize Geocoding with your API key
          Geocoding.init(GOOGLE_MAPS_API);
  
          // Get the device's location
          Geolocation.getCurrentPosition(
            (position) => {
              // alert(position)
              // Extract latitude and longitude from the position object
              const { latitude, longitude } = position.coords;
              setMyLat(latitude)
              setMyLng(longitude)
              // Reverse geocode the coordinates to get the city name
              Geocoding.from({ latitude, longitude })
                .then((json) => {
                  const cityName = json.results[0].address_components.find(
                    (component) =>
                      component.types.includes('locality') ||
                      component.types.includes('administrative_area_level_1')
                  ).long_name;
                    
                  setCity(cityName);
                  
                  // alert(cityName);
                  // alert(city)
                })
                .catch((error) => console.error(error));
            },
            (error) => console.error(error),
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
          );
        }
        
      });
    }

  const Venues = (props) => {
    // console.log(props.courtPrice)
    // State variables to store pricing range
    const [pricingFrom, setPricingFrom] = useState(0);
    const [pricingTo, setPricingTo] = useState(0);
    const [venueLat,setVenueLat] = useState(null)
    const [venueLng,setVenueLng] = useState(null)

    // Helper function to get pricing range from the feed
    const getPricingRange = () => {
      // Find the active pricing element from the beginning
      for (let index = 0; index < props.courtPrice.pricing.pricing.length; index++) {
        const element = props.courtPrice.pricing.pricing[index];
        if (element.active) {
          setPricingFrom(element.price);
          break;
        }
      }

      // Find the active pricing element from the end
      for (let index = props.courtPrice.pricing.pricing.length - 1; index >= 0; index--) {
        const element = props.courtPrice.pricing.pricing[index];
        if (element.active) {
          setPricingTo(element.price);
          break;
        }
      }
    };

    const getVenueLatLng = () => {
      // Initialize Geocoding with your Google Maps API key
      Geocoding.init(GOOGLE_MAPS_API);

      // Your target address in text form
      const targetAddress = props.data.address;

      // Use geocoding to get the coordinates
      Geocoding.from(targetAddress)
        .then((json) => {
          const { lat, lng } = json.results[0].geometry.location;
          setVenueLat(lat)
          setVenueLng(lng)
          console.log(`Latitude: ${lat}, Longitude: ${lng}`);
        })
        .catch((error) => console.error(error));
    }
    // Fetch pricing range when the component mounts
    useEffect(() => {
      getPricingRange();
      getVenueLatLng();
      
    }, [isFocused]);

    return (

      
        <View style={{ ...styles.courtCard }}>
          {/* <View style={styles.courtPriceTag}>
          <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardBadgeTextSize,fontWeight:FONTS.extraBoldFontWeight}}>SAR {pricingFrom} - SAR {pricingTo}</Text>
        </View> */}
          <View style={{ flexDirection: "column" }}>
          <Pressable onPress={() => navigation.navigate("VenueDetail", props.data)}>
            <View style={{ flexDirection: "row" }}>
              <Image source={{ uri: props.data.photos[0].replace("http://", "https://") }} style={styles.courtCardImage} />
              <View style={{ flexDirection: "column", marginLeft: 12, marginTop: 10 }}>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ width: Dimensions.get("screen").width/2.2, fontSize: FONTS.cardHeading, color: COLORS.dark, fontFamily: FONTS.family600 }}>{props.data.name}</Text>
                  {/* <IonIcon name='bookmark-outline' size={20} /> */}
                  {/* <Image source={require("../../../assets/images/unselected_fav.png")} style={{marginLeft:14,justifyContent:"flex-end"}}/> */}
                </View>
                <Text style={{ fontSize: FONTS.cardDescription, color: COLORS.lightGrey, fontFamily: FONTS.family, width: Dimensions.get("screen").width/2.4 }} numberOfLines={2} >{props.data.address}</Text>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between",paddingRight:5}}>
                  <Text style={{ marginTop: 10, color: COLORS.secondary, fontFamily: FONTS.family700 }}>{pricingFrom} SAR/hour</Text>
                  <View style={{ flexDirection: "row", borderRadius: 20, backgroundColor: COLORS.accent, justifyContent: "flex-end", alignItems: "center", padding: 5 }}>
                    <FontAwesome5 name='location-arrow' size={8} color={COLORS.lightGrey} />
                    <Text style={{ color: COLORS.lightGrey, fontWeight: FONTS.lightBoldFontWeight, fontSize: 10, marginLeft: 2, fontFamily: FONTS.family }}>{venueLat && venueLng ?calculateDistance(myLat,myLng,venueLat,venueLng).toFixed(1):" "} km</Text>
                  </View>
                </View>
              </View>
              {/* <View style={{flexDirection:"column"}}>
          <Text>Court Name</Text>
          </View> */}
            </View></Pressable>
            <View style={{ borderWidth: 1, borderColor: "#00000014", marginTop: 10 }} />
            <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ flexDirection: "row", paddingHorizontal: 12, marginTop: 10, zIndex: 1000 }}>
              <View style={{ height: 25, paddingHorizontal: 5, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.timing.mondayOn ? "M " + props.data.timing.mondayFrom : "M Off"}</Text>
              </View>
              <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.timing.tuesdayOn ? "T " + props.data.timing.tuesdayFrom : "T Off"}</Text>
              </View>
              <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.timing.wedOn ? "W " + props.data.timing.wedFrom : "W Off"}</Text>
              </View>
              <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.timing.thursdayOn ? "T " + props.data.timing.thursdayFrom : "T Off"}</Text>
              </View>
              <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.timing.fridayOn ? "F " + props.data.timing.fridayFrom : "F Off"}</Text>
              </View>
              <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.timing.satOn ? "S " + props.data.timing.satFrom : "S Off"}</Text>
              </View>
              <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5,marginRight:20 }}>
                <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.timing.sunOn ? "S " + props.data.timing.sunFrom : "S Off"}</Text>
              </View>
            </ScrollView>
          </View>

        </View>
      
    )
  }

  useEffect(() => {
    getCourtsBySearch()
    // getAllVenues()
    // getAllCourts()
    setCity(data.city)
    setDate(data.date)
    setTime(data.time)
    
    setSearch("")
    if(Platform.OS === "android"){
      setPlatformMarginTop(40)
    }else{
      setPlatformMarginTop(0)
    }
    if (Platform.OS === "ios") {
      
      // alert(askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS))
      askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS)
    } else if (Platform.OS === "android") {
      askForPermission(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
    }
  }, [isFocused])
  return (
    <SafeAreaView style={styles.container}>
      <DateTimePickerModal
        isVisible={isDatePicker}
        mode="datetime"
        onConfirm={(datetime) => {
          const { date, time } = parseDateTimeString(datetime);
          console.log(date + " " + time)
          setDate(date)
          setTime(time)
          setIsDatePicker(false)
        }}
        onCancel={() => setIsDatePicker(false)}
      />
      <Modal
        animationType="slide"
        transparent={true}
        visible={isLocationModalVisible}
        onRequestClose={toggleLocationModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.fragment}>
            <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Select Location</Text>
            <TouchableOpacity style={styles.closeButton} >
            <IonIcon name='close' size={30} color={COLORS.secondary} onPress={() => toggleLocationModal()} />
            </TouchableOpacity>


          </View>
        </View>
      </Modal>
      <Modal
        animationType="slide"
        transparent={true}
        visible={isDateModalVisible}
        onRequestClose={toggleDateModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.fragment}>
            <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Select Date & Time</Text>
            <TouchableOpacity style={styles.closeButton} >
            <IonIcon name='close' size={30} color={COLORS.secondary} onPress={() => toggleDateModal()} />
            </TouchableOpacity>
            <ReactNativeModernDatepicker
              // mode='date'
              onDateChange={(date) => setDate(date)}
              onTimeChange={(time) => setTime(time)}
            />

          </View>
        </View>
      </Modal>
      <Modal
        animationType="slide"
        transparent={true}
        visible={isModalVisible}
        onRequestClose={toggleModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.fragment}>
            <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Filter</Text>
            <TouchableOpacity onPress={() => toggleModal()}  style={styles.closeButton} >
            <IonIcon name='close' size={30}  color={COLORS.secondary} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => resetFilters()} style={{ position: "absolute", fontSize: 16, top: 20, right: 10}}>
            <Text style={{ textAlign: "right", fontFamily: FONTS.family }}>Reset</Text></TouchableOpacity>
            <View style={{ flexDirection: "column" }}>
              <Text style={{ color: COLORS.lightGrey, fontSize: 14, marginTop: 10, fontFamily: FONTS.family }}>Sort By</Text>
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <TouchableOpacity style={distance === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setDistance(!distance)}>
                  <Text style={{ fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Distance</Text>
                </TouchableOpacity>
                <TouchableOpacity style={price === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setPrice(!price)}>
                  <Text style={{ fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Price</Text>
                </TouchableOpacity>
              </View>
              <Text style={{ color: COLORS.lightGrey, fontSize: 14, marginTop: 10, fontFamily: FONTS.family }}>Type</Text>
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <TouchableOpacity style={indoor === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setIndoor(!indoor)}>
                  <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/indoor_icon.png")} />
                  <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Indoor</Text>
                </TouchableOpacity>
                <TouchableOpacity style={outdoor === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setOutdoor(!outdoor)}>
                  <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/outdoor_icon.png")} />
                  <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Outdoor</Text>
                </TouchableOpacity>

              </View>
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <TouchableOpacity style={roofedOutdoor === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setRootedOutdoor(!roofedOutdoor)}>
                  <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/roofed_outdoor.png")} />
                  <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Roofed Outdoor</Text>
                </TouchableOpacity>
              </View>
              <Text style={{ color: COLORS.lightGrey, fontSize: 14, marginTop: 10, fontFamily: FONTS.family }}>Features</Text>
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <TouchableOpacity style={wall === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setWall(!wall)}>
                  <Text style={{ fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Wall</Text>
                </TouchableOpacity>
                <TouchableOpacity style={crystal === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setCrystal(!crystal)}>
                  <Text style={{ fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Standard</Text>
                </TouchableOpacity>
                <TouchableOpacity style={panoramic === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setPanoramic(!panoramic)}>
                  <Text style={{ fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Panoramic</Text>
                </TouchableOpacity>
              </View>
              <Text style={{ color: COLORS.lightGrey, fontSize: 14, marginTop: 10, fontFamily: FONTS.family }}>Facilities</Text>
              <View style={{ flexDirection: "column", marginTop: 10 }}>
                <View style={{ flexDirection: "row", marginTop: 5 }}>
                  <TouchableOpacity style={rentEquipment === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setRentEquipment(!rentEquipment)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/rent_equipment.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Rent Equipment</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={lockers === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setLockers(!lockers)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/lockers.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Lockers</Text>
                  </TouchableOpacity>

                </View>
                <View style={{ flexDirection: "row", marginTop: 5 }}>
                  <TouchableOpacity style={toilet === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setToilet(!toilet)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/toilet.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Toilet</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={freeParking === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setFreeParking(!freeParking)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/parking.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Free parking</Text>
                  </TouchableOpacity>

                </View>
                <View style={{ flexDirection: "row", marginTop: 5 }}>
                  <TouchableOpacity style={wifi === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setWifi(!wifi)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/wifi.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Wifi</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={restaurant === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setRestaurant(!restaurant)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/restaurant.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Restaurant</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={snackbar === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setSnackbar(!snackbar)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/snackbar.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Snackbar</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flexDirection: "row", marginTop: 5 }}>
                  <TouchableOpacity style={kidsPlayground === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setKidsPlayground(!kidsPlayground)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/kids_playground.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Kids Playground</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={specialAccess === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setSpecialAccess(!specialAccess)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/wheelchair.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Special Access</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flexDirection: "row", marginTop: 5 }}>
                  <TouchableOpacity style={paidParking === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setPaidParking(!paidParking)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/paid_parking.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Paid Parking</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={showers === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setShowers(!showers)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/showers.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Showers</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <Text style={{ color: COLORS.lightGrey, fontSize: 14, marginTop: 10, fontFamily: FONTS.family }}>Size</Text>
              <View style={{ flexDirection: "row", marginTop: 10, marginBottom: 10 }}>
                <TouchableOpacity style={single === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setSingle(!single)}>
                  <Text style={{ fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Single</Text>
                </TouchableOpacity>
                <TouchableOpacity style={double === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setDouble(!double)}>
                  <Text style={{ fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Double</Text>
                </TouchableOpacity>
              </View>
            </View>

          </View>
        </View>
      </Modal>
      <Image style={styles.image} source={require("../../../assets/images/search_screen_header_img.png")} />
      <View style={styles.imageOverlay} />
      <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center",marginTop:platformMarginTop,paddingHorizontal:20}}>
      <TouchableOpacity onPress={() => navigation.goBack()}><Text style={{ marginTop: 10, fontSize: FONTS.buttonTextSize, color: COLORS.onboardingTitle, fontFamily: FONTS.family }} >Back</Text></TouchableOpacity>
      <TouchableOpacity onPress={() =>getCourtsBySearch()}><Text style={{ marginTop: 10, fontSize: FONTS.buttonTextSize, color: COLORS.onboardingTitle, fontFamily: FONTS.family }} >Apply</Text></TouchableOpacity>
      </View>
      <View style={{ flexDirection: "row", width: Dimensions.get("screen").width, alignItems: "center", justifyContent: "center", alignSelf: "center" }}>
        <View style={[styles.searchInputContainer, { width: "25%" }]}>
          <TouchableOpacity onPress={() => getCourtsBySearch()}><IonIcon name='location-outline' size={20} color={COLORS.dark} style={{ marginLeft: 5 }} /></TouchableOpacity>
          <TextInput placeholder='Nearby' style={{ fontSize: 14, paddingLeft: 5, width: 50, fontFamily: FONTS.family }} onChangeText={(text) => { setCity(text) }} value={city ? city : data.city} />
        </View>
        <View style={[styles.searchInputContainer, { width: "40%",marginHorizontal:8 }]}>
          {/* <TouchableOpacity onPress={() => getCourtsBySearch(search)}><AntDesign name='search1' size={FONTS.regularIconSize} color={COLORS.accent} style={{marginLeft:20}} /></TouchableOpacity> */}
          <TextInput placeholder='Date' value={date ? date + "," + time : data.today} onPressIn={() => setIsDatePicker(true)} style={{ fontSize: 14, paddingLeft: 20, width: 162, fontFamily: FONTS.family }} editable={true} />
        </View>
        <View style={[styles.searchInputContainer, { width: "25%", borderRadius: 40, alignContent: "center", justifyContent: "center" }]} >
          <TouchableOpacity onPress={() => setModalVisible(true)}><Image source={require("../../../assets/images/filter.png")} style={{width:20,height:20}} /></TouchableOpacity>
          <Text style={{ fontSize: 16, paddingLeft: 10, color: COLORS.dark, fontFamily: FONTS.family }}>Filter</Text>
        </View>
      </View>
      <View style={{ flexDirection: "column", justifyContent: "center", alignItems: "center", marginTop: 40 }}>

        {/* {resError ? <View style={{ alignItems: "center", justifyContent: "center", height: "80%" }}><ResponseError /></View> : null}
        {serverError ? <View style={{ alignItems: "center", justifyContent: "center", height: "80%" }}><ServerError /></View> : null}
        {loading ? <View style={{ alignItems: "center", justifyContent: "center", height: "80%" }}><Loading /></View> : null}
        {noData ? <View style={{ alignItems: "center", justifyContent: "center", height: "80%" }}><NoData /></View> : null} */}
                {loading ? <View style={{  width:Dimensions.get("screen").width }}><VenuesSkeleton /></View> : null}
                

        {venues && courts && loading === false? <View style={{width:Dimensions.get("screen").width,justifyContent:"center",alignItems:"center",paddingBottom:Platform.OS === "android" ?"40%":"20%"}}><FlatList
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          data={venues}
          contentContainerStyle={{width:Dimensions.get("screen").width,justifyContent:"center",alignItems:"center"}}
          renderItem={({ item, index }) => {
            // console.log(item)
            
            const foundCourt = courts.find(item2 => item2._id === item.courts[0]);
            console.log(foundCourt)
            if (foundCourt) {
              // console.log("Rendering")
              return (
                <Venues data={item} courtPrice={foundCourt} index={index} navigation={navigation} />
              )
            }

          }
          } /></View> : null}
          {noData ? <View style={{ justifyContent: "center", alignItems: "center",height:"60%" }}>
            <Text style={{ fontSize: FONTS.sectionHeading, color: COLORS.lightGrey, fontFamily: FONTS.family500 }}>Nothing found</Text>
            <Text style={{ marginTop: 8, fontSize: FONTS.textDescription, color: COLORS.lightGrey, fontFamily: FONTS.family }}>Please try to change or reset filters.</Text>
            <TouchableOpacity onPress={()=> {
                resetFilters()
                getCourtsBySearch()
              }} style={{ backgroundColor: COLORS.secondary, paddingHorizontal: 10, paddingVertical: 5, borderRadius: 50, marginTop: 16 }}>
              <Text style={{ color: COLORS.brandLight, fontSize: FONTS.textDescription, fontFamily: FONTS.family600 }}>Reset filters</Text>
            </TouchableOpacity>
          </View> : null}
      </View>
    </SafeAreaView>
  )
}

export default SearchScreen