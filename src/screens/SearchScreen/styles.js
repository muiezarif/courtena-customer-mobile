import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:COLORS.brandLight
    },
    courtCard:{
      // height:150,
      // width:"100%",
      width:Dimensions.get('screen').width-40,
      elevation:15,
      shadowOffset: { width: 0, height: 5 },
      shadowColor: COLORS.lightGrey,
      shadowOpacity: 0.5,
      marginBottom:15,
      // marginHorizontal:5,
      borderRadius:15,
      backgroundColor:COLORS.white,
      shadowRadius:4,
      paddingBottom:8
  },
    searchInputContainer:{
        height:50,
        backgroundColor:COLORS.white,
        marginTop:15,
        // marginHorizontal:10,
        borderRadius:8,
        flexDirection:"row",
        alignItems:'center'
    },
    courtCardImage:{
      height:80,
        // width:(Dimensions.get('screen').width-40)/3,
        // aspectRatio: 16 / 10,
        width:132,
        borderRadius:10,
        marginLeft:10,
        marginTop:10
  },
    image:{
        width:Dimensions.get("screen").width,
        height:Dimensions.get("screen").height/5.5,
        resizeMode:"cover",
        position:"absolute",
        top:0
    },
    imageOverlay:{
        width:Dimensions.get("screen").width,
        height:Dimensions.get("screen").height/5.5,
        position:"absolute",
        top:0,
        backgroundColor:COLORS.secondary,
        opacity:0.7
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
      },
      fragment: {
        backgroundColor: '#fff',
        padding: 16,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        
      },
      fragmentText: {
        fontSize: 16,
        marginBottom: 10,
        color:COLORS.secondary,
        textAlign:"center",
        fontSize:20
      },
      closeButton: {
        
        position:"absolute",
        top:10,
        left:10
      },
      closeButtonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
      },
      chipContainer:{
        flexDirection:"row",
        borderWidth:1,
        borderRadius:20,
        borderColor:"#00000014",
        paddingHorizontal:10,
        paddingVertical:5,
        alignContent:"center",
        justifyContent:"center",
        marginRight:5
      },
      selectedChipContainer:{
        flexDirection:"row",
        borderWidth:1,
        borderColor:COLORS.accent,
        borderRadius:20,
        backgroundColor:COLORS.accent,
        paddingHorizontal:10,
        paddingVertical:5,
        alignContent:"center",
        justifyContent:"center",
        marginRight:5
      }
})


export default styles