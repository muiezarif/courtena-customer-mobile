import { View, Text, SafeAreaView, Image, Dimensions, Modal, ScrollView, Platform, KeyboardAvoidingView } from 'react-native'
import React, { useState } from 'react'
import styles from "./styles"
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'
import * as Animatable from 'react-native-animatable';
import COLORS from '../../utils/colors'
import AsyncStorage from '@react-native-async-storage/async-storage'
import FONTS from '../../utils/fonts'
import courtena from '../../api/courtena'
import IonIcon from 'react-native-vector-icons/Ionicons'
import { Picker } from '@react-native-picker/picker';
import ReactNativeModernDatepicker from 'react-native-modern-datepicker';
import Loader from '../../components/Loader'
const FirstTimeUserInfoAddScreen = ({ navigation, route }) => {
  const [username, setUsername] = useState("")
  const [email, setEmail] = useState("")
  const [country, setCountry] = useState("Saudi Arabia")
  const [city, setCity] = useState("")
  const [errMsg, setErrMsg] = useState("")
  const [firstname, setFirstName] = useState("")
  const [lastname, setLastName] = useState("")
  const [password, setPassword] = useState("test1234")
  const [gender, setGender] = useState("Male")
  const [dob, setDob] = useState("")
  const [isLoading, setIsLoading] = useState(false)
  //   const [errMsg,setErrMsg] = useState("")
  const [isModalVisible, setModalVisible] = useState(false);
  const [isDateModalVisible, setDateModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  const toggleDateModal = () => {
    setDateModalVisible(!isDateModalVisible);
  };
  //   const navigation = useNavigation()



  const updateUser = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const data = { first_name: firstname, last_name: lastname, gender: gender, dob: dob, username: username, email: email, country: country, city: city, isFirstTime: true }
    if (email && firstname && lastname && gender && dob) {
      setIsLoading(true)
      await courtena.put("/customer/customer-update-info/" + customerData._id, { ...data }, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': '*/*',
          'Authorization': customerData.token
        }
      }).then(async (res) => {
        setIsLoading(false)
        if (res.data.success) {
          // alert("success")
          await AsyncStorage.setItem("firstTimeUserInfoAdd", "true")
          navigation.navigate("Main")
          navigation.reset({
            index: 0,
            routes: [{ name: "Main" }],
          });
        } else {
          alert(res.data.message)
        }
      }).catch((err) => {
        setIsLoading(false)
        console.log(err)
      })
    } else {
      alert("Please fill required information")
    }

  }
  return (
    <SafeAreaView style={[styles.container]}>
    {isLoading?<Loader/>:null}
      <Image style={styles.image} source={require("../../../assets/images/login_image.png")} />
      <View style={styles.imageOverlay} />

      <Image style={{ position: "absolute", zIndex: 101, width: 200, height: 190, top: "20%" }} source={require("../../../assets/images/onboarding_logo.png")} />
      <View style={{ flexDirection: "row", justifyContent: "space-between", position: "absolute", zIndex: 101, top: 50, width: Dimensions.get("screen").width, paddingHorizontal: 12 }}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Text style={{ color: COLORS.brandLight, fontSize: 16, fontFamily: FONTS.family }}>Back</Text></TouchableOpacity>

      </View>
      {/* <View style={{flexDirection:"row",justifyContent:"space-between",position:"absolute",zIndex:101,top:70,width:Dimensions.get("screen").width,paddingHorizontal:32}}>
        <Text onPress={() => {
          navigation.goBack()
        }} style={{color:COLORS.brandLight,fontSize:16}}>Back</Text>
        
      </View> */}

      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null} style={styles.containerInner}>
        {/* <View> */}
        <View >
          {/* <Image style={styles.image} source={require("../../../assets/images/register_img.png")}/> */}

          <Text style={{ fontSize: FONTS.titleHeading, fontWeight: FONTS.regularBoldFontWeight, lineHeight: 48, color: COLORS.secondary, textAlign: "left", marginTop: 30 }}>Join to Courtena </Text>
          <Text style={{ fontSize: FONTS.textDescription, lineHeight: 24, fontWeight: FONTS.lightFontWeight, color: COLORS.dark, marginTop: 10, textAlign: "left" }}>Enter your personal details to sign up</Text>

          <Animatable.View
            ref={this.validateInput}
            style={{ flexDirection: "row" }}
          >
            <TextInput
              style={{ marginTop: 24, width: "49%", height: 50, borderColor: COLORS.dividerBorderColor, backgroundColor: COLORS.white, borderRadius: 12, borderWidth: 1, paddingLeft: 20, color: COLORS.secondary, marginRight: 5 }}
              placeholder="First Name"
              placeholderTextColor="lightgrey"
              onChangeText={(text) => {
                setErrMsg(''),
                  setFirstName(text)
              }
              }
            />
            <TextInput
              style={{ marginTop: 24, width: "49%", height: 50, borderColor: COLORS.dividerBorderColor, borderRadius: 12, borderWidth: 1, paddingLeft: 20, color: COLORS.secondary, backgroundColor: COLORS.white }}
              placeholder="Last Name"
              placeholderTextColor="lightgrey"
              onChangeText={(text) => {
                setErrMsg(''),
                  setLastName(text)
              }
              }
            />

          </Animatable.View>
          <Animatable.View
            ref={this.validateInput}
            style={{ flexDirection: "column" }}
          >
            {/* <DatePicker
                date={dob}
                style={{width:"100%",borderRadius:16,backgroundColor:COLORS.white,borderColor:COLORS.secondary,borderWidth:1,marginTop:10}}
                mode="date"
                showIcon={false}
                format="YYYY-MM-DD"
                confirmBtnText='Confirm'
                cancelBtnText='Cancel'
                onDateChange={(value) => setDob(value)}/> */}
            {/* <TextInput
                    style={{ marginTop: 24,width: "100%",height:50, borderColor: COLORS.dividerBorderColor,borderRadius:12, borderWidth: 1,paddingLeft:20,color:COLORS.secondary,backgroundColor:COLORS.white }}
                    placeholder="Phone Number"
                    value={phoneNo}
                    placeholderTextColor="lightgrey"
                    onChangeText={(text) => setPhoneNo(text)}
                /> */}
            {/* <TextInput
                    style={{ marginTop: 24,height:50, borderColor: COLORS.dividerBorderColor,borderRadius:12, borderWidth: 1,paddingLeft:20,color:COLORS.secondary,backgroundColor:COLORS.white }}
                    placeholder="Username"
                    placeholderTextColor="lightgrey"
                    onChangeText = {(text) => 
                        {
                            setErrMsg(''),
                            setUsername(text)
                        }
                    }
                /> */}
            <TextInput
              style={{ marginTop: 24, height: 50, borderColor: COLORS.dividerBorderColor, borderRadius: 12, borderWidth: 1, paddingLeft: 20, color: COLORS.secondary, backgroundColor: COLORS.white }}
              placeholder="Email"
              placeholderTextColor="lightgrey"
              onChangeText={(text) => {
                setErrMsg(''),
                  setEmail(text)
              }
              }
            />
            {/* <TextInput
                    style={{ marginTop: 24,height:50, borderColor: COLORS.dividerBorderColor,borderRadius:12, borderWidth: 1,paddingLeft:20,color:COLORS.secondary,backgroundColor:COLORS.white }}
                    placeholder="Country"
                    enabled={false}
                    value={country}
                    placeholderTextColor="lightgrey"
                    onChangeText = {(text) => 
                        {
                            setErrMsg(''),
                            setCountry(text)
                        }
                    }
                /> */}
            {/* <TextInput
                    style={{ marginTop: 24,height:50, borderColor: COLORS.dividerBorderColor,borderRadius:12, borderWidth: 1,paddingLeft:20,color:COLORS.secondary,backgroundColor:COLORS.white }}
                    placeholder="City"
                    placeholderTextColor="lightgrey"
                    onChangeText = {(text) => 
                        {
                            setErrMsg(''),
                            setCity(text)
                        }
                    }
                /> */}
                <Animatable.View
            ref={this.validateInput}
            style={{ flexDirection: "row" }}
          >
            <TextInput
              style={{ marginTop: 24, width: "49%", height: 50, borderColor: COLORS.dividerBorderColor, borderRadius: 12, borderWidth: 1, paddingLeft: 20, color: COLORS.secondary, backgroundColor: COLORS.white,marginRight:5 }}
              placeholder="Gender"
              value={gender}
              editable={Platform.OS ==="ios"? false:true}
              placeholderTextColor="lightgrey"
              onPressIn={() => { setModalVisible(true) }}
            />
            <TextInput
              style={{ marginTop: 24, width: "49%", height: 50, borderColor: COLORS.dividerBorderColor, borderRadius: 12, borderWidth: 1, paddingLeft: 20, color: COLORS.secondary, backgroundColor: COLORS.white }}
              placeholder="Date of Birth"
              value={dob}
              editable={Platform.OS ==="ios"? false:true}
              placeholderTextColor="lightgrey"
              onPressIn={() => { setDateModalVisible(true) }}
            /></Animatable.View>


          </Animatable.View>


          <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 10,marginBottom:20 }}>
            <TouchableOpacity
              onPress={updateUser}
              style={{ width: Dimensions.get("screen").width - 64, backgroundColor: COLORS.secondary, padding: 15, alignItems: 'center', justifyContent: 'center', borderRadius: 16, marginTop: 10 }}
            >
              <Text style={{ textAlign: 'center', color: COLORS.brandLight, fontSize: FONTS.buttonTextSize,font:FONTS.family600 }}>Continue</Text>
            </TouchableOpacity>

            {/* <View style={{ flexDirection: 'row',marginTop: 20 }}>
                    <Text style={{ color: COLORS.white }}>Already have an account?</Text>
                    <Text onPress={() => {
                      navigation.navigate("Login")
                      navigation.reset({
                        index: 0,
                        routes: [{ name: "Login" }],
                      });
                      }} style={{ fontWeight: 'bold',color:COLORS.accent }}> Log In</Text>
                    </View> */}
          </View>
        </View>
        {/* </View> */}
      </KeyboardAvoidingView>
      <Modal
        animationType="slide"
        transparent={true}
        visible={isModalVisible}
        onRequestClose={toggleModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.fragment}>
            <Text style={styles.fragmentText}>Select Gender</Text>
            {/* <TouchableOpacity style={styles.closeButton} > */}
            <IonIcon name='close' size={30} style={styles.closeButton} color={COLORS.secondary} onPress={() => toggleModal()} />
            {/* </TouchableOpacity> */}

            <View style={{marginTop:14}}> 
              <Text style={{color:COLORS.dark,fontSize:16,fontFamily:FONTS.family,marginTop:12}} onPress={() => {
                setGender("Male")
                toggleModal()
              }}>Male</Text>
              <View style={{height:0.5,backgroundColor:COLORS.dividerBorderColor,width:"100%",marginVertical:12}}/>
              <Text onPress={() => {
                setGender("Female")
                toggleModal()
              }} style={{color:COLORS.dark,fontSize:16,fontFamily:FONTS.family,marginBottom:12}}>Female</Text>
            </View>
            {/* <Picker
              selectedValue={gender}
              onValueChange={(itemValue) => setGender(itemValue)}
            >
              <Picker.Item label="Male" value="Male" />
              <Picker.Item label="Female" value="Female" />
            </Picker> */}
          </View>
        </View>
      </Modal>
      <Modal
        animationType="slide"
        transparent={true}
        visible={isDateModalVisible}
        onRequestClose={toggleDateModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.fragment}>
            <Text style={styles.fragmentText}>Select Date of Birth</Text>
            {/* <TouchableOpacity style={styles.closeButton} > */}
            <IonIcon name='close' size={30} style={styles.closeButton} color={COLORS.secondary} onPress={() => toggleDateModal()} />
            {/* </TouchableOpacity> */}
            <ReactNativeModernDatepicker
              mode='date'
              onDateChange={(date) => {
                setDob(date)
                toggleDateModal()
              }}
            />

          </View>
        </View>
      </Modal>
    </SafeAreaView>

  )
}

export default FirstTimeUserInfoAddScreen