import { View, Text, SafeAreaView, ImageBackground, Image, TouchableOpacity, Animated, Modal, FlatList, Dimensions, TextInput, ActivityIndicator, Linking, Pressable, Platform, Alert, KeyboardAvoidingView } from 'react-native';
import React, { useEffect, useRef, useState } from 'react';
import styles from './styles';
import { ScrollView } from 'react-native-gesture-handler';
import COLORS from '../../utils/colors';
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FONTS from '../../utils/fonts';
import CarouselItem from '../../components/CarouselItem';
import Carousel from '../../components/Carousel';
import feed from '../../../assets/data/feed';
import Paginator from '../../components/OnboardingItem/Paginator';
import AsyncStorage from '@react-native-async-storage/async-storage';
import courtena from '../../api/courtena';
import Court from '../../components/Court';
import { useNavigation } from '@react-navigation/native';
import ReactNativeModernDatepicker, { getFormatedDate } from 'react-native-modern-datepicker';
import PlayHourItem from '../../components/PlayHourItem';
import moment from 'moment'
import TimeIntervalItem from '../../components/TimeIntervalItem';
import { encode } from 'base-64';
import { WebView } from 'react-native-webview';
import Geolocation from 'react-native-geolocation-service';
import Geocoding from 'react-native-geocoding';
import { check,request, PERMISSIONS, RESULTS } from 'react-native-permissions'
import { ApplePayButton,PaymentRequest } from 'react-native-payments';
import { NativeModules } from 'react-native';
const { ApplePayBridge } = NativeModules;


let isPaymentInProgress = false;


const GOOGLE_MAPS_API = "AIzaSyCbknAy3ZfqL2hw9HKAc16jVDkWydyMAwE"

import axios from 'axios';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import Loader from '../../components/Loader';
const PUBLISHABLE_MOYASAR_API_KEY = "pk_test_ih4qvc964KdbD2dxwGH6B7hRvbrWv46FbLnt2Z2N"
const SECRET_MOYASAR_API_KEY = "pk_test_ih4qvc964KdbD2dxwGH6B7hRvbrWv46FbLnt2Z2N"

const VenueDetailScreen = ({ navigation, route }) => {
  // Extract the item from the route parameters
  const item2 = route.params;
  var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  // Animated value for the horizontal scroll position
  const scrollX = useRef(new Animated.Value(0)).current;

  const [isFavVenue, setIsFavVenue] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  // State for the modal visibility
  const [isModalVisible, setModalVisible] = useState(false);
  const [isCourtDurationModalVisible, setCourtDurationModalVisible] = useState(false);
  const [isCourtDateModalVisible, setCourtDateModalVisible] = useState(false);
  const [isCourtTimeModalVisible, setCourtTimeModalVisible] = useState(false);
  const [isCourtPaymentModalVisible, setCourtPaymentModalVisible] = useState(false);
  const [isCourtAddCardModalVisible, setCourtAddCardModalVisible] = useState(false);
  const [isCourtConfirmPaymentModalVisible, setCourtConfirmPaymentModalVisible] = useState(false);
  const [isCourtReservationDetailsModalVisible, setCourtReservationDetailsModalVisible] = useState(false);
  const [isCourtReservationConfirmedModalVisible, setCourtReservationConfirmedModalVisible] = useState(false);
  const [isConfirmCardModal, setConfirmCardModal] = useState(false);
  const [onceSavedCard, setOnceSavedCard] = useState(false)
  const [platformMarginTop, setPlatformMarginTop] = useState(0)
  // State for the list of courts
  const [courts, setCourts] = useState([]);
  const [selectedCourtDetail, setSelectedCourtDetail] = useState()
  const [item, setItem] = useState()
  const [isModalPermissionVisible, setModalPermissionVisible] = useState(false);

  // State for confimed booking
  const [confirmedBooking, setConfirmedBooking] = useState()

  // State for payment related options
  const [cards, setCards] = useState([])
  const [selectedPayment, setSelectedPayment] = useState()
  const [duration, setDuration] = useState("")
  const [selectedTime, setSelectedTime] = useState("")
  const [selectedPrice, setSelectedPrice] = useState("")
  const [name, setName] = useState("")
  const [cvc, setCvc] = useState("")
  const [number, setNumber] = useState("")
  const [expirydate, setExpiryDate] = useState("")
  const [callback_url, setCallbackUrl] = useState("https://api.courtena.com/api/customer/thanks-verify-card/")
  const [confirm_payment_url, setConfirmPaymentUrl] = useState(false)
  const [cardToVerify, setCardToVerify] = useState()
  // State variables to store pricing range
  const [pricingFrom, setPricingFrom] = useState(0);
  const [pricingTo, setPricingTo] = useState(0);

  const [selectedDateItem, setSelectedDateItem] = useState(null);
  const [myLat, setMyLat] = useState(null)
  const [myLng, setMyLng] = useState(null)
  const [venueLat, setVenueLat] = useState(null)
  const [venueLng, setVenueLng] = useState(null)
  const [isGuest, setIsGuest] = useState(false)


  const handleItemPress = (index) => {
    // Set the selected item index when an item is pressed
    setSelectedDateItem(index);
  };


  // Function to calculate the distance between two coordinates using Haversine formula
  function calculateDistance(lat1, lon1, lat2, lon2) {
    const R = 6371; // Earth radius in kilometers
    const dLat = (lat2 - lat1) * (Math.PI / 180);
    const dLon = (lon2 - lon1) * (Math.PI / 180);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(lat1 * (Math.PI / 180)) *
      Math.cos(lat2 * (Math.PI / 180)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const distance = R * c; // Distance in kilometers
    return distance;
  }

  const closeAllModals = () => {
    setModalVisible(false)
    setCourtAddCardModalVisible(false)
    setCourtConfirmPaymentModalVisible(false)
    setCourtDateModalVisible(false)
    setCourtDurationModalVisible(false)
    setCourtPaymentModalVisible(false)
    setCourtTimeModalVisible(false)
    setCourtReservationDetailsModalVisible(false)
  }

  const togglePermissionModal = () => {
    setModalPermissionVisible(!isModalPermissionVisible);
  };

  const askForPermission = async(permission) => {
    await check(permission).then(async(result) => {
      if (result === RESULTS.DENIED || result === RESULTS.BLOCKED) {
        // alert("Location permission is recommended to get the most out of courtena. Go to your settings and allow location permssion")
        if(Platform.OS === "android"){
          setModalPermissionVisible(true)
        }
        
      }
      if (result === RESULTS.GRANTED) {
        const location = await AsyncStorage.getItem("location")
        const locationData = JSON.parse(location)
        if(location){
          if(locationData.lat && locationData.lng){
            // setMyLat(locationData.lat)
            // setMyLng(locationData.lng)
            updateMyLocation()
          }else{
            updateMyLocation()
          }
        }else{
          setModalPermissionVisible(true)
          // updateMyLocation()
        }
        
      }

    });
  }

  const updateMyLocation = () => {
// Initialize Geocoding with your API key
Geocoding.init(GOOGLE_MAPS_API);

// Get the device's location
Geolocation.getCurrentPosition(
  async(position) => {
    // alert(position)
    // Extract latitude and longitude from the position object
    const { latitude, longitude } = position.coords;
    setMyLat(latitude)
    setMyLng(longitude)
    await AsyncStorage.setItem("location",JSON.stringify({lat:latitude,lng:longitude}))
    // Reverse geocode the coordinates to get the city name
    // Geocoding.from({ latitude, longitude })
    //   .then((json) => {
    //     const cityName = json.results[0].address_components.find(
    //       (component) =>
    //         component.types.includes('locality') ||
    //         component.types.includes('administrative_area_level_1')
    //     ).long_name;

    //     // setCity(cityName);

    //     // alert(cityName);
    //     // alert(city)
    //   })
    //   .catch((error) => console.error(error));
  },
  (error) => {
    console.error(error)
    if(error.code === 1){
      setModalPermissionVisible(true)
    }
  },
  { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
);
  }

  const askForPermissionAgain = async (permission) => {
    await request(permission).then(async (result) => {
      if (result === RESULTS.GRANTED) {
        // alert("granted")
        // setLoading(true)
        updateMyLocation()
      }

    });
  }

  const checkGuest = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    setIsGuest(customerData.guest)
  }

  useEffect(() => {
    // alert("ios")
    if (Platform.OS === "ios") {
      // alert(askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS))
      askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS)
    } else if (Platform.OS === "android") {
      askForPermission(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
    }
    getVenueLatLng()
    checkGuest()

  }, [])

  const getVenueLatLng = () => {
    // Initialize Geocoding with your Google Maps API key
    Geocoding.init(GOOGLE_MAPS_API);

    // Your target address in text form
    const targetAddress = item2.address;

    // Use geocoding to get the coordinates
    Geocoding.from(targetAddress)
      .then((json) => {
        const { lat, lng } = json.results[0].geometry.location;
        setVenueLat(lat)
        setVenueLng(lng)
        // console.log(`Latitude: ${lat}, Longitude: ${lng}`);
      })
      .catch((error) => console.error(error));
  }

  // Helper function to get pricing range from the feed
  const getPricingRange = (item) => {
    // console.log(item)
    // Find the active pricing element from the beginning
    for (let index = 0; index < item.pricing.pricing.length; index++) {
      const element = item.pricing.pricing[index];
      if (element.active) {
        setPricingFrom(element.price);
        break;
      }
    }

    // Find the active pricing element from the end
    for (let index = item.pricing.pricing.length - 1; index >= 0; index--) {
      const element = item.pricing.pricing[index];
      if (element.active) {
        setPricingTo(element.price);
        break;
      }
    }
  };

  // State for court information
  const [courtInfo, setCourtInfo] = useState();

  // Get the current date and set the start and end dates
  const today = new Date();
  const startDate = getFormatedDate(today.setDate(today.getDate()), 'YYYY/MM/DD');
  const endDate = getFormatedDate(today.setDate(today.getDate() + 30), 'YYYY/MM/DD');

  // State for the selected date and time range
  const [date, setDate] = useState(startDate);
  const [selectedTimeRange, setSelectedTimeRange] = useState('');
  const [timeRange, setTimeRange] = useState([]);
  const [baseTimeRange, setBaseTimeRange] = useState([]);
  const [filteredTimeRange, setFilteredTimeRange] = useState([]);

  // Get the current date
  const currentDate = new Date();

  // Get the day of the week as an index (0 = Sunday, 1 = Monday, ..., 6 = Saturday)
  const dayOfWeek = currentDate.getDay();

  // Define an array of days to map the day index to the day name
  const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  // Get the name of the day using the day index
  const todayy = daysOfWeek[dayOfWeek];

  const checkOpenStatus = (isOpen) => {
    if (todayy === "Monday") {
      if (isOpen.mondayOn) {

        return formatTimeToAMPM(isOpen.mondayFrom) + "-" + formatTimeToAMPM(isOpen.mondayTo);
      } else {
        return "Close";
      }
    } else if (todayy === "Tuesday") {
      if (isOpen.tuesdayOn) {

        return formatTimeToAMPM(isOpen.tuesdayFrom) + "-" + formatTimeToAMPM(isOpen.tuesdayTo);
      } else {
        return "Close";
      }
    } else if (todayy === "Wednesday") {
      if (isOpen.wedOn) {

        return formatTimeToAMPM(isOpen.wedFrom) + "-" + formatTimeToAMPM(isOpen.wedTo);
      } else {
        return "Close";
      }
    } else if (todayy === "Thursday") {
      if (isOpen.thursdayOn) {

        return formatTimeToAMPM(isOpen.thursdayFrom) + "-" + formatTimeToAMPM(isOpen.thursdayTo);
      } else {
        return "Close";
      }
    } else if (todayy === "Friday") {
      if (isOpen.fridayOn) {

        return formatTimeToAMPM(isOpen.fridayFrom) + "-" + formatTimeToAMPM(isOpen.fridayTo);
      } else {
        return "Close";
      }
    } else if (todayy === "Saturday") {
      if (isOpen.satOn) {

        return formatTimeToAMPM(isOpen.satFrom) + "-" + formatTimeToAMPM(isOpen.satTo);
      } else {
        return "Close";
      }
    } else if (todayy === "Sunday") {
      if (isOpen.sunOn) {

        return formatTimeToAMPM(isOpen.sunFrom) + "-" + formatTimeToAMPM(isOpen.sunTo);
      } else {
        return "Close";
      }
    }



  };

  const showAlert = () => {
    Alert.alert(
      'Guest',
      'Please login to continue',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('No Pressed'),
          style: 'cancel',
        },
        {
          text: 'Login',
          onPress: async () => {
            await AsyncStorage.removeItem("@viewedOnboarding")
            await AsyncStorage.removeItem("customerRemainLoggedin")
            await AsyncStorage.removeItem("firstTimeUserInfoAdd")
            await AsyncStorage.removeItem("customer")
            await AsyncStorage.removeItem("token")
            navigation.navigate("AfterOnboarding")
            navigation.reset({
              index: 0,
              routes: [{ name: "AfterOnboarding" }],
            });
          },
        },
      ],
      { cancelable: false }
    );
  };

  const openMapWithAddress = (address) => {
    const encodedAddress = encodeURIComponent(address);
    const url = Platform.OS === "ios" ? `https://www.google.com/maps/search/?api=1&query=${encodedAddress}` : `google.navigation:q=${encodedAddress}`;
    Linking.openURL(url)

  };


  const dialNumber = (phoneNumber) => {
    const url = `tel:${phoneNumber}`;
    Linking.openURL(url)

  };




  // Helper function to check if two arrays have common elements
  function areArraysEqual(arr1, arr2) {
    for (let i = 0; i < arr1.length; i++) {
      if (arr2.includes(arr1[i])) {
        return true;
      }
    }
    return false;
  }

  // Helper function to remove 'min' from a string and convert it to a number
  function removeMinAndConvertToNumber(text) {
    const updatedText = text.replace('min', '').trim();
    const numberValue = parseInt(updatedText, 10);
    return numberValue;
  }

  // Helper function to extract day, month, and year from a date string
  const extractDateComponents = (dateString) => {
    const [year, month, day] = dateString.split('/');
    return { day: parseInt(day), month: parseInt(month), year: parseInt(year) };
  };

  const getNext30Days = () => {
    const currentDate = new Date();
    const dates = [];

    for (let i = 0; i < 30; i++) {
      const nextDate = new Date(currentDate);
      nextDate.setDate(currentDate.getDate() + i);
      const formattedDate = nextDate.toISOString().split('T')[0];
      dates.push(formattedDate);
    }

    return dates;
  };

  const getDayName = (dateString) => {
    const daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    const date = new Date(dateString);
    const dayIndex = date.getDay();
    return daysOfWeek[dayIndex];
  };

  const getDateFromDateString = (dateString) => {
    const date = new Date(dateString);
    return date.getDate();
  };

  // Helper function to add minutes to a time string
  const addMinutes = (timeString, addition) => {
    const [hours, minutes] = timeString.split(':');
    const date = new Date();
    date.setHours(parseInt(hours, 10));
    date.setMinutes(parseInt(minutes, 10));
    date.setMinutes(date.getMinutes() + addition);
    const updatedTimeString = date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: false });
    return updatedTimeString;
  };

  // Helper function to generate time intervals between start and end time
  const generateTimeIntervals = (startTime, endTime, intervalMinutes) => {
    const intervals = [];
    const [startHour, startMinute] = startTime.split(':').map(Number);
    const [endHour, endMinute] = endTime.split(':').map(Number);

    let currentHour = startHour;
    let currentMinute = startMinute;

    while (currentHour < endHour || (currentHour === endHour && currentMinute <= endMinute)) {
      const formattedHour = String(currentHour).padStart(2, '0');
      const formattedMinute = String(currentMinute).padStart(2, '0');
      intervals.push(`${formattedHour}:${formattedMinute}`);

      currentMinute += intervalMinutes;
      if (currentMinute >= 60) {
        currentHour += Math.floor(currentMinute / 60); // Increment hour
        currentMinute %= 60; // Reset minutes
      }
    }

    return intervals;
  };

  const handleNavigationStateChange = (newNavState) => {
    // Extract the new URL from the navigation state
    const newUrl = newNavState.url;
    // console.log(newUrl)
    // Check if the new URL matches the expected structure
    const expectedUrlPattern = /https:\/\/api\.courtena\.com\/api\/customer\/thanks-verify-card\//;
    if (expectedUrlPattern.test(newUrl)) {
      const queryStartIndex = newUrl.indexOf('?');
      if (queryStartIndex !== -1) {
        const queryString = newUrl.substring(queryStartIndex + 1);
        const queryParams = queryString.split('&').reduce((params, param) => {
          const [key, value] = param.split('=');
          params[key] = decodeURIComponent(value);
          return params;
        }, {});

        const status = queryParams['status'];
        const message = queryParams['message'];

        // Check if status is 'paid' and message is 'APPROVED'
        if (message === 'APPROVED' && !onceSavedCard) {
          // Perform your desired actions here
          // For example, display a success message or update the UI
          saveCard()
          setOnceSavedCard(true)
        }
      }
    }
  }

  const saveCard = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const newData = { card: cardToVerify, customer: customerData._id }
    await courtena.post("/customer/save-customer-card/", { ...newData }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token,
      },
    }).then((response) => {
      if (response.data.success) {

        getSavedCards()
        alert(response.data.message)
      } else {
        alert(response.data.message)
      }

    }).catch((err) => {
      alert(err.message)
    })
  }

  const checkVenueFav = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)

    await courtena.get("/customer/get-customer-isfav-venue/" + customerData._id + "/" + item2._id, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      if (res.data.success) {
        // console.log(res.data)
        setIsFavVenue(res.data.result.fav)
      } else {
        setIsFavVenue(false)
      }

    }).catch((err) => {
      console.log(err)
    })
  }
  const toggleVenueFav = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const data = { customer: customerData._id, venue: item._id }
    await courtena.post("/customer/toggle-fav-venue/", { ...data }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      if (res.data.success) {
        setIsFavVenue(res.data.result)
      } else {
        setIsFavVenue(false)
      }

    }).catch((err) => {
      console.log(err)
    })
  }

  // const onApplePayPress = async () => {
    // const customer = await AsyncStorage.getItem("customer")
    // const customerData = JSON.parse(customer)
    
  //   // const paymentRequest = {
  //   //   supportedMethods: ['apple-pay'],
  //   //   data: {
  //   //     merchantIdentifier: 'merchant.com.courtena', // Replace with your Merchant ID
  //   //     supportedNetworks: ['visa', 'mastercard', 'amex'],
  //   //     countryCode: 'SA',
  //   //     currencyCode: 'SAR',
  //   //     paymentMethodTokenizationParameters: {
  //   //       parameters: {
  //   //        gateway: 'moyasar',
  //   //        'moyasar:publishableKey': 'pk_test_ih4qvc964KdbD2dxwGH6B7hRvbrWv46FbLnt2Z2N',
  //   //     }
  //   //   }

  //   //   },
  //   // };
  
  //   // const METHOD_DATA = [{
  //   //   supportedMethods: ['apple-pay'],
  //   //   data: {
  //   //     merchantIdentifier: 'merchant.com.courtena',
  //   //     supportedNetworks: ['visa', 'mastercard', 'amex'],
  //   //     countryCode: 'SA',
  //   //     currencyCode: 'SAR',
  //   //   }
  //   // }];
  //   // const DETAILS = {
  //   //   id: 'courtena-booking',
  //   //   displayItems: [
  //   //     {
  //   //       label: `Booking courtena ${courtInfo.title} for ${duration} at ${formatTimeToAMPM(selectedTime)}  paying ${selectedPrice} SAR`,
  //   //       amount: { currency: 'SAR', value: selectedPrice },
  //   //     },
  //   //   ],
  //   //   total: {
  //   //     label: 'Courtena',
  //   //     amount: { currency: 'SAR', value: selectedPrice },
  //   //   },
  //   // };

  //   // const OPTIONS = {
  //   //   requestPayerName: true,
  //   //   requestPayerPhone: true,
  //   //   requestPayerEmail: true,
  //   // };
  
  //   // try {
  //   //   const token = new PaymentRequest(METHOD_DATA, DETAILS);
  //   //   await token.canMakePayments().then(async(canMakePayment) => {
  //   //     if(canMakePayment){
  //   //       await token.show().then(async(paymentResponse) => {
  //   //         // const card_token = paymentResponse.details.paymentToken;
  //   //         console.log(paymentResponse)
  //   //         const data = { info: JSON.stringify(paymentResponse) }

  //   //         // console.log(card_token)
  //   //         paymentResponse.complete("success")
            // await courtena.post("/customer/get-apple-pay-info/", { ...data }, {
            //   headers: {
            //     'Content-Type': 'application/x-www-form-urlencoded',
            //     'Accept': '*/*',
            //     'Authorization': customerData.token
            //   }
            // }).then(res => {
        
            // }).catch(err => {
              
            // })
  //   //       }).catch(err => {
  //   //         console.log(err)
  //   //       })
  //   //     }
  //   //   }).catch(error => {
  //   //     if(error.message === 'AbortError') {
  //   //       this.debug('Payment request was dismissed');
  //   //     }
  //   //   });
  //   //   // await token.show();
  
  //   //   // Add event listeners
  //   //   token.addEventListener('paymentauthorized', async event => {
  //   //     const paymentToken = event.paymentToken;
  //   //     console.log('Payment authorized:', paymentToken);
  //   //     // Handle payment authorization and proceed with booking process
  //   //   });
  
  //   //   token.addEventListener('paymenterror', event => {
  //   //     const error = event.error;
  //   //     console.error('Payment error:', error);
  //   //     Alert.alert('Payment Error', 'An error occurred during payment.');
  //   //   });
  
  //   //   token.addEventListener('cancel', event => {
  //   //     console.log('Payment canceled');
  //   //     // Handle payment cancellation
  //   //   });
  //   //   token.addEventListener('close', event => {
  //   //     console.log('Payment canceled');
  //   //     // Handle payment cancellation
  //   //   });
  
      
  //   // } catch (error) {
  //   //   console.error(error);
  //   //   Alert.alert('Payment Error', 'An error occurred during payment.');
  //   // }

  //   // try {
  //     if (isPaymentInProgress) {
  //       console.log('Payment is already in progress.');
  //       return;
  //     }
    
  //     isPaymentInProgress = true;
    
  //     const paymentDetails = {
  //       amount: selectedPrice,
  //       description: `Booking courtena ${courtInfo.title} for ${duration} at ${formatTimeToAMPM(selectedTime)}  paying ${selectedPrice} SAR`,
  //       // Add other payment details as needed
  //     };
    
  //     try {
  //       await ApplePayBridge.initiateApplePayPayment(paymentDetails);
  //       console.log('Payment initiated successfully.');
  //     } catch (error) {
  //       console.error('Error initiating payment:', error);
  //       // Handle error
  //     } finally {
  //       isPaymentInProgress = false;
  //     }
  //     const paymentToken = ''; // Get this token from Apple Pay process
  //     const amount = 100;
  //     const description = 'iOS Apple Pay Payment';
  //     const publishableAPIKey = 'pk_test_ih4qvc964KdbD2dxwGH6B7hRvbrWv46FbLnt2Z2N'; // Replace with your actual publishable API key
  //     // console.log('Payment result:', result);
  // // } catch (error) {
  // //     console.error('Payment error:', error);
  // // }
  // };

  const onApplePayPress = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    // Check if payment is already in progress
    // if (isPaymentInProgress) {
    //   console.log('Payment is already in progress.');
    //   return;
    // }

    // Set the flag to indicate payment is in progress
    // isPaymentInProgress = true;

    const paymentDetails = {
      amount: selectedPrice,
      description: `Booking courtena ${courtInfo.title} for ${duration} at ${formatTimeToAMPM(selectedTime)}  paying ${selectedPrice} SAR`,
      // Add other payment details as needed
    };

    await ApplePayBridge.initiateApplePayPayment(paymentDetails).then(async (result) => {
    // const paymentData = paymentToken.paymentData;
    const payment = JSON.parse(result)
    // console.log(paymentData)
      const data ={info:result,amount:selectedPrice,currency:"SAR",description:`Booking courtena ${courtInfo.title} for ${duration} at ${formatTimeToAMPM(selectedTime)}  paying ${selectedPrice} SAR`}
      await courtena.post("/customer/get-apple-pay-info/", { ...data }, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': '*/*',
          'Authorization': customerData.token
        }
      }).then(res => {
        console.log(res.data)
  
      }).catch(err => {
        console.log(err)
      })
      console.log(result)
    }).catch(err => {
      console.log(err)
    });
  };

  const getSavedCards = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const customerToken = await AsyncStorage.getItem("token")
    // setLoading(true)
    // setNoData(false)
    // setResError(false)
    // setServerError(false)
    await courtena.get("/customer/get-saved-cards/" + customerData._id, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      // setLoading(false)
      if (res.data.success) {
        // setCourts(res.data.result.courts)
        setCards(res.data.result)

      } else {
        // setResError(true)
        alert(res.data.message)
      }
    }).catch((err) => {
      // setLoading(false)
      // setServerError(true)
      console.log(err)
    })
  }

  const addCard = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const { month, year } = splitExpirationDate(expirydate);
    const apiKey = PUBLISHABLE_MOYASAR_API_KEY;
    const encodedApiKey = encode(apiKey);
    const customCallbackUrl = callback_url + encodedApiKey + "/" + customerData._id
    const data = { name: name, cvc: cvc, number: number, month: month, year: year, callback_url: callback_url }
    const formData = new FormData()
    formData.append("name", name)
    formData.append("cvc", cvc)
    formData.append("number", number)
    formData.append("month", month)
    formData.append("year", year)
    formData.append("callback_url", callback_url)
    // console.log(data)
    if (name && cvc && number && month && year && callback_url) {
      setIsLoading(true)
      await axios.post("https://api.moyasar.com/v1/tokens/", data, {
        headers: {
          Authorization: `Basic ${encodedApiKey}`,
        },
      }).then(async (res) => {
        setIsLoading(false)
        // console.log(res.data)
        setConfirmPaymentUrl(res.data.verification_url)
        // console.log(res.data)
        setConfirmPaymentUrl(res.data.verification_url)
        setCardToVerify(res.data)
        toggleCourtAddCardModal()
        toggleConfirmCardModal()

        // setLoading(false)

      }).catch((err) => {
        setIsLoading(false)
        // setLoading(false)
        // setServerError(true)
        alert(err.message)
        console.log(err)
      })
    } else {
      alert("Please fill all fields first")
    }

  }



  const payAndReserve = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const apiKey = PUBLISHABLE_MOYASAR_API_KEY;
    const encodedApiKey = encode(apiKey);
    const { day, month, year } = extractDateComponents(date);
    const data = { amount: selectedPrice, callback_url: callback_url, currency: "SAR", description: `Booking courtena ${courtInfo.title} for ${duration} at ${formatTimeToAMPM(selectedTime)}  paying ${selectedPrice} SAR`, source: { type: "token", token: selectedPayment.card.id }, bookingData: { duration: duration, date: date, time: selectedTimeRange, paymentAmount: selectedPrice, dateTimeInfo: { day: day, month: month, year: year, timeFrom: selectedTimeRange, timeTo: addMinutes(selectedTimeRange, removeMinAndConvertToNumber(duration)) }, court: courtInfo._id, partner: courtInfo.partner, venue: courtInfo.venue, sports: courtInfo.sports, customer: customerData._id } }
    setIsLoading(true)
    await courtena.post("/customer/customer-create-booking", { ...data }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token,
      },
    }).then((response) => {
      setIsLoading(false)
      setConfirmedBooking(response.data.result)
      toggleCourtConfirmPaymentModal()
      toggleCourtReservationConfirmedModal()
    }).catch((err) => {
      setIsLoading(false)
      alert(err.message)
    })
  }

  const getVenueDetails = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    await courtena.get("/customer/get-venue-detail/" + item2._id, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token,
      },
    }).then((res) => {
      // console.log(res.data.result)
      if (res.data.success) {
        setItem(res.data.result)
      }
    }).catch(err => {

    })
  }

  function splitExpirationDate(expiration) {
    const [month, year] = expiration.split('/');
    return {
      month: month || '',
      year: year || '',
    };
  }

  // Helper function to format time to AM/PM
  const formatTimeToAMPM = (timeString) => {
    const [hour, minute] = timeString.split(':').map(Number);
    const formattedHour = hour > 12 ? hour - 12 : hour;
    const period = hour >= 12 ? 'PM' : 'AM';
    return `${formattedHour}:${String(minute).padStart(2, '0')} ${period}`;
  };

  // Function to toggle the modal visibility
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  const toggleCourtDurationModal = () => {
    setCourtDurationModalVisible(!isCourtDurationModalVisible);
    // console.log(timeRange)
  };
  const toggleCourtDateModal = () => {
    setCourtDateModalVisible(!isCourtDateModalVisible);
  };
  const toggleCourtTimeModal = () => {
    setCourtTimeModalVisible(!isCourtTimeModalVisible);
    // setTimeRange(timeRange)
  };
  const toggleCourtPaymentModal = () => {
    setCourtPaymentModalVisible(!isCourtPaymentModalVisible);
  };
  const toggleCourtAddCardModal = () => {
    setCourtAddCardModalVisible(!isCourtAddCardModalVisible);
  };
  const toggleCourtConfirmPaymentModal = () => {
    setCourtConfirmPaymentModalVisible(!isCourtConfirmPaymentModalVisible);
  };
  const toggleCourtReservationDetailsModal = () => {
    setCourtReservationDetailsModalVisible(!isCourtReservationDetailsModalVisible);
  };
  const toggleCourtReservationConfirmedModal = () => {
    setCourtReservationConfirmedModalVisible(!isCourtReservationConfirmedModalVisible);
  };
  const toggleConfirmCardModal = () => {
    setConfirmCardModal(!isConfirmCardModal);
  };



  // Fetch court details for the selected item
  const getCourtDetail = async (item) => {
    const customer = await AsyncStorage.getItem('customer');
    const customerData = JSON.parse(customer);
    setIsLoading(true)
    await courtena
      .get('/customer/get-court-info/' + item._id, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Accept: '*/*',
          Authorization: customerData.token,
        },
      })
      .then((res) => {
        setIsLoading(false)
        if (res.data.success) {
          // setCourtInfo([])
          setCourtInfo(res.data.result);
          // setModalVisible(false)
          toggleModal()
          toggleCourtDurationModal()
          // setCourtDurationModalVisible(true)
        } else {
          // Handle error if needed
        }
      })
      .catch((err) => {
        setIsLoading(false)
        // Handle error if needed
      });
  };

  const getCourtDetail2 = async (item) => {
    const customer = await AsyncStorage.getItem('customer');
    const customerData = JSON.parse(customer);
    setIsLoading(true)
    await courtena
      .get('/customer/get-court-info/' + item._id, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Accept: '*/*',
          Authorization: customerData.token,
        },
      })
      .then((res) => {
        setIsLoading(false)
        if (res.data.success) {
          // setCourtInfo([])
          setCourtInfo(res.data.result);
          // setModalVisible(false)
          // toggleModal()
          // toggleCourtDurationModal()
          // setCourtDurationModalVisible(true)
        } else {
          // Handle error if needed
        }
      })
      .catch((err) => {
        setIsLoading(false)
        // Handle error if needed
      });
  };

  // Fetch courts for the selected partner and venue
  const getCourtsByPartnerVenue = async () => {
    const customer = await AsyncStorage.getItem('customer');
    const customerData = JSON.parse(customer);
    await courtena
      .get('/customer/get-partner-venue-courts/' + item2.partner + '/' + item2._id, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Accept: '*/*',
          Authorization: customerData.token,
        },
      })
      .then((res) => {
        // console.log(res.data)
        if (res.data.success) {
          setCourts(res.data.result);
          getPricingRange(res.data.result[0])
          if (res.data.result.length === 0) {
            // Handle no data if needed
          }
        } else {
          // Handle error if needed
        }
      })
      .catch((err) => {
        // Handle error if needed
      });
  };

  // Fetch courts when the component mounts
  useEffect(() => {
    getVenueDetails()
    getCourtsByPartnerVenue();
    getSavedCards()
    checkVenueFav()
    if (Platform.OS === "android") {
      setPlatformMarginTop(40)
    } else {
      setPlatformMarginTop(0)
    }
  }, []);

  useEffect(() => {

    if (courts) {

    }

  }, [courts])

  // Update time range when courtInfo changes
  useEffect(() => {
    // if (courtInfo && courtInfo.pricing !== undefined) {
    //   const startTime = courtInfo.pricing.dateTime.startTime;
    //   const endTime = courtInfo.pricing.dateTime.endTime;
    //   const intervalMinutes = 30;
    //   const intervals = generateTimeIntervals(startTime, endTime, intervalMinutes);
    //   setTimeRange(intervals);
    // }
  }, [courtInfo]);


  const Court = (props) => {
    // State variables to store pricing range
    const [pricingFrom, setPricingFrom] = useState(0);
    const [pricingTo, setPricingTo] = useState(0);

    // Helper function to get pricing range from the feed
    const getPricingRange = () => {
      // Find the active pricing element from the beginning
      for (let index = 0; index < props.feed?.pricing?.pricing.length; index++) {
        const element = props.feed?.pricing?.pricing[index];
        if (element.active) {
          setPricingFrom(element.price);
          break;
        }
      }

      // Find the active pricing element from the end
      for (let index = props.feed?.pricing?.pricing.length - 1; index >= 0; index--) {
        const element = props.feed?.pricing?.pricing[index];
        if (element.active) {
          setPricingTo(element.price);
          break;
        }
      }
    };

    // Fetch pricing range when the component mounts
    useEffect(() => {
      getPricingRange();
    }, []);



    return (
      <Pressable onPress={() => {
        setSelectedCourtDetail(props.feed)
        getCourtDetail(props.feed)
      }} key={props.index}>
        <View style={{ ...styles.courtCard }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image source={{ uri: props.feed.image.replace("http://", "https://") }} style={styles.courtCardImage} />
            <View style={{ flexDirection: 'column' }}>
              {/* Court Title */}
              <Text style={{ fontSize: FONTS.cardHeading, color: COLORS.dark, marginLeft: 12, fontFamily: FONTS.family600 }}>
                {props.feed.title}
              </Text>

              {/* Court Status */}
              <Text style={{ fontSize: FONTS.cardDescription, color: COLORS.secondary, marginLeft: 12, fontFamily: FONTS.family500 }}>
                {props.feed.advancedSettings.courtActive ? 'Active' : 'Inactive'}
              </Text>

              {/* Court Type */}
              <Text style={{ fontSize: FONTS.cardDescription, color: COLORS.secondary, marginLeft: 12, fontFamily: FONTS.family500 }}>
                {props.feed.courtType}
              </Text>

              {/* Pricing Range */}
              <Text style={{ fontSize: FONTS.cardDescription, color: COLORS.secondary, marginLeft: 12, fontFamily: FONTS.family700 }}>
                {pricingFrom} SAR - {pricingTo} SAR
              </Text>
            </View>
          </View>
        </View>
      </Pressable>
    );
  };

  function convertToMinutes(text) {
    // Remove "min" from the text
    const withoutMin = text.replace('min', '');

    // Convert the remaining part to an integer
    const minutes = parseInt(withoutMin, 10);

    return minutes;
  }

  const cancelBookingReservation = async () => {
    const customer = await AsyncStorage.getItem('customer');
    const customerData = JSON.parse(customer);
    // console.log(confirmedBooking._id)
    // const data = {}
    const data = { paymentId: confirmedBooking.payment.id, bookingId: confirmedBooking._id, court: confirmedBooking.court }
    await courtena.post("/customer/customer-cancel-reservation/", { ...data }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: '*/*',
        Authorization: customerData.token,
      },
    }).then((res) => {
      // console.log(res.data)
      if (res.data.success) {
        toggleCourtReservationDetailsModal()
        setModalVisible(false)
        setCourtAddCardModalVisible(false)
        setCourtConfirmPaymentModalVisible(false)
        setCourtDateModalVisible(false)
        setCourtDurationModalVisible(false)
        setCourtPaymentModalVisible(false)
        setCourtTimeModalVisible(false)
        setCourtReservationDetailsModalVisible(false)
      }
      alert(res.data.message)
    }).catch((err) => {
      alert("Error Canceling Booking")
      alert(err)
    })

  }

  const handleExpiryDateChange = (text) => {
    // Remove any non-digit characters
    const formattedText = text.replace(/\D/g, '');

    // Add a "/" after the first two characters
    let formattedExpiryDate = '';
    for (let i = 0; i < formattedText.length; i++) {
      if (i === 2) {
        formattedExpiryDate += '/';
      }
      formattedExpiryDate += formattedText[i];
    }

    // Update the state with the formatted text
    setExpiryDate(formattedExpiryDate);
  };

  const VenueDetailSkeleton = () => {
    return (
      <SkeletonPlaceholder borderRadius={1}>
        <SkeletonPlaceholder.Item borderRadius={16} height={Dimensions.get("screen").height / 3} width={"90%"} style={{ marginHorizontal: 20, marginTop: 20 }} />
        <SkeletonPlaceholder.Item height={30} width={"40%"} style={{ marginHorizontal: 20, marginTop: 20 }} />
        <SkeletonPlaceholder.Item height={30} width={"80%"} style={{ marginHorizontal: 20, marginTop: 20 }} />
        <SkeletonPlaceholder.Item height={30} width={"80%"} style={{ marginHorizontal: 20, marginTop: 20 }} />
        <SkeletonPlaceholder.Item height={30} width={"40%"} style={{ marginHorizontal: 20, marginTop: 20 }} />
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={80} height={80} borderRadius={16} />
          <SkeletonPlaceholder.Item width={80} height={80} borderRadius={16} marginLeft={10} />
          <SkeletonPlaceholder.Item width={80} height={80} borderRadius={16} marginLeft={10} />
          <SkeletonPlaceholder.Item width={80} height={80} borderRadius={16} marginLeft={10} />
          <SkeletonPlaceholder.Item width={80} height={80} borderRadius={16} marginLeft={10} />
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item height={30} width={"40%"} style={{ marginHorizontal: 20, marginTop: 20 }} />

        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={80} height={80} borderRadius={16} />
          <SkeletonPlaceholder.Item width={80} height={80} borderRadius={16} marginLeft={10} />
          <SkeletonPlaceholder.Item width={80} height={80} borderRadius={16} marginLeft={10} />
          <SkeletonPlaceholder.Item width={80} height={80} borderRadius={16} marginLeft={10} />
          <SkeletonPlaceholder.Item width={80} height={80} borderRadius={16} marginLeft={10} />
        </SkeletonPlaceholder.Item>

      </SkeletonPlaceholder>
    )


  }


  return (
    <SafeAreaView style={styles.container}>

      {item ? <View style={{ marginTop: platformMarginTop }}>

      <Modal
        animationType="slide"
        transparent={true}
        visible={isModalPermissionVisible}
        onRequestClose={togglePermissionModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.fragment}>
            <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Location Permission</Text>
            {/* <TouchableOpacity onPress={() => togglePermissionModal()} style={styles.closeButton} >
              <IonIcon name='close' size={30} color={COLORS.secondary} />
            </TouchableOpacity> */}
            {/* <TouchableOpacity onPress={() => resetFilters()} style={{ position: "absolute", fontSize: 16, top: 20, right: 10 }}><Text style={{ textAlign: "right", fontFamily: FONTS.family }}>Reset</Text></TouchableOpacity> */}
            <Text style={{color:COLORS.dark,fontFamily:FONTS.family500,textAlign:"center"}}>To get the most out of courtena allow location.</Text>
            <TouchableOpacity onPress={() => {
              if (Platform.OS === "ios") {

                // alert(askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS))
                askForPermissionAgain(PERMISSIONS.IOS.LOCATION_ALWAYS)
              } else if (Platform.OS === "android") {
                askForPermissionAgain(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
              }
              togglePermissionModal()
            }} style={{ width: "100%", justifyContent: "center", alignItems: "center", height: 50, backgroundColor: COLORS.secondary, borderRadius: 16, marginTop: 20 }}>
              <Text style={{ color: COLORS.brandLight, fontFamily: FONTS.family600, fontSize: 16 }} >Allow Permission</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
              togglePermissionModal()
            }} style={{ width: "100%", justifyContent: "center", alignItems: "center", height: 50, backgroundColor: COLORS.secondary, borderRadius: 16, marginTop: 20 }}>
              <Text style={{ color: COLORS.brandLight, fontFamily: FONTS.family600, fontSize: 16 }} >Cancel</Text>
            </TouchableOpacity>

          </View>
        </View>
      </Modal>
        {/* Modal for displaying courts */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={isModalVisible}
          onRequestClose={toggleModal}
        >
          {isLoading ? <Loader /> : null}
          <View style={styles.modalContainer}>
            <View style={styles.fragment}>
              <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Courts</Text>
              {/* Close button */}
              <TouchableOpacity style={styles.closeButton2} onPress={() => {

                toggleModal()
              }}>
                <IonIcon name='close' size={30} color={COLORS.secondary} />
              </TouchableOpacity>
              {/* FlatList to render courts */}
              {courts ? (
                <FlatList

                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  data={courts}
                  renderItem={({ item, index }) => {
                    if(item.advancedSettings.bookableOnline){
                      return(
                        <Court feed={item} navigation={navigation} index={index} />
                        )
                    }
                }}
                />
              ) : null}
            </View>
          </View>
        </Modal>

        {/* Modal for selecting court duration */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={isCourtDurationModalVisible}
          onRequestClose={toggleCourtDurationModal}
        >
          <View style={styles.modalContainer}>
            <View style={styles.fragment}>
              <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Select Duration</Text>
              {/* Close button */}
              <TouchableOpacity style={styles.closeButton} onPress={() => {
                toggleCourtDurationModal()
                toggleModal()
              }}>
                <Image source={require("../../../assets/images/venue_detail_back.png")} style={{ width: 24, height: 24 }} />
                {/* <IonIcon name='arrow-back' size={30} color={COLORS.secondary} /> */}
              </TouchableOpacity>
              <TouchableOpacity style={styles.exitButton} onPress={() => {
                closeAllModals()
              }}>
                <IonIcon name='close' size={30} color={COLORS.secondary} />
                {/* <IonIcon name='arrow-back' size={30} color={COLORS.secondary} /> */}
              </TouchableOpacity>
              {/* FlatList to render court Duration */}
              {courtInfo && timeRange ? <FlatList
                data={courtInfo?.pricing?.pricing}
                horizontal={false}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item, index }) => {
                  if (item.active) {
                    return (
                      <TouchableOpacity key={index} onPress={() => {
                        // bookCourt(item)

                        if ((courtInfo && courtInfo.pricing !== undefined) || (courtInfo && courtInfo.pricing !== null)) {
                          const startTime = courtInfo.pricing.dateTime.startTime;
                          const endTime = courtInfo.pricing.dateTime.endTime;
                          const intervalMinutes = convertToMinutes(item.interval);
                          const intervals = generateTimeIntervals(startTime, endTime, intervalMinutes);
                          // console.log(intervals)
                          const currentDateTime = new Date()
                          const currentHours = currentDateTime.getHours();
                          const currentMinutes = currentDateTime.getMinutes();
                          const filteredIntervals = intervals.filter((item) => {
                            const selectedTime = item.split(":")
                            const selectedHours = parseInt(selectedTime[0],10)
                            const selectedMinutes = parseInt(selectedTime[1],10)
                            if ((selectedHours < currentHours) || ((selectedHours === currentHours) && (selectedMinutes < currentMinutes))) {
                              return false
                            }
                            return true
                          })
                          console.log("FILTERED INTERVALS")
                          console.log(filteredIntervals)
                          setDate(getFormatedDate(currentDateTime))
                          setDuration(item.interval)
                          setSelectedPrice(item.price)
                          setBaseTimeRange(intervals)
                          setFilteredTimeRange(filteredIntervals)
                          setTimeRange(filteredIntervals);
                          // console.log(timeRange)
                          toggleCourtDurationModal()
                          toggleCourtTimeModal()
                        }
                      }}>
                        <PlayHourItem item={item} key={index} />
                      </TouchableOpacity>
                    )
                  }

                }}
              /> : null}
            </View>
          </View>
        </Modal>

        {/* Modal for selecting court date */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={isCourtDateModalVisible}
          onRequestClose={toggleCourtDateModal}
        >
          {isLoading ? <Loader /> : null}
          <View style={styles.modalContainer}>
            <View style={styles.fragment}>
              <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Select a Day</Text>
              {/* Close button */}
              <TouchableOpacity style={styles.closeButton} onPress={() => {
                toggleCourtDateModal()
                toggleCourtDurationModal()
              }}>
                <Image source={require("../../../assets/images/venue_detail_back.png")} style={{ width: 24, height: 24 }} />
                {/* <IonIcon name='arrow-back' size={30} color={COLORS.secondary} /> */}
              </TouchableOpacity>
              {/* FlatList to render court Duration */}
              {courtInfo && timeRange ? <ReactNativeModernDatepicker
                mode='datepicker'
                // disableDateChange={true}
                onDateChange={(date) => {
                  // alert(date)
                  const dateNew = moment(date, "YYYY/MM/DD").format('YYYY-MM-DD')
                  var dateDayName = new Date(dateNew);
                  const dayName = dateDayName.toLocaleDateString("en-Us", { weekday: "long" })
                  // console.log(dateDayName.getFullYear())
                  if (days[dateDayName.getDay()] == "Monday") {
                    if (item.timing.mondayOn) {
                      setDate(date)
                      toggleCourtDateModal()
                      toggleCourtTimeModal()
                      getCourtDetail2(selectedCourtDetail)
                    } else {
                      alert("Closed")
                      return
                    }

                  } else if (days[dateDayName.getDay()] == "Tuesday") {
                    if (item.timing.tuesdayOn) {
                      setDate(date)
                      toggleCourtDateModal()
                      toggleCourtTimeModal()
                      getCourtDetail2(selectedCourtDetail)
                    } else {
                      alert("Closed")
                      return
                    }
                  } else if (days[dateDayName.getDay()] == "Wednesday") {
                    if (item.timing.wedOn) {
                      setDate(date)
                      toggleCourtDateModal()
                      toggleCourtTimeModal()
                      getCourtDetail2(selectedCourtDetail)
                    } else {
                      alert("Closed")
                      return
                    }
                  } else if (days[dateDayName.getDay()] == "Thursday") {
                    if (item.timing.thursdayOn) {
                      setDate(date)
                      toggleCourtDateModal()
                      toggleCourtTimeModal()
                      getCourtDetail2(selectedCourtDetail)
                    } else {
                      alert("Closed")
                      return
                    }
                  } else if (days[dateDayName.getDay()] == "Friday") {
                    if (item.timing.fridayOn) {
                      setDate(date)
                      toggleCourtDateModal()
                      toggleCourtTimeModal()
                      getCourtDetail2(selectedCourtDetail)
                    } else {
                      alert("Closed")
                      return
                    }
                  } else if (days[dateDayName.getDay()] == "Saturday") {
                    if (item.timing.satOn) {
                      setDate(date)
                      toggleCourtDateModal()
                      toggleCourtTimeModal()
                      getCourtDetail2(selectedCourtDetail)
                    } else {
                      alert("Closed")
                      return
                    }
                  } else if (days[dateDayName.getDay()] == "Sunday") {
                    if (item.timing.sunOn) {
                      setDate(date)
                      toggleCourtDateModal()
                      toggleCourtTimeModal()
                      getCourtDetail2(selectedCourtDetail)
                    } else {
                      alert("Closed")
                      return
                    }
                  }
                  

                }
                }
                minimumDate={startDate}
                maximumDate={endDate}
                selected={date}
                style={{ borderRadius: 20, marginTop: 20 }}
                options={{
                  backgroundColor: COLORS.white,
                  textDefaultColor: COLORS.dark,
                  selectedTextColor: COLORS.accent,
                  mainColor: COLORS.secondary,
                  textSecondaryColor: COLORS.secondary,
                  textHeaderColor: COLORS.secondary,
                  borderColor: COLORS.white,
                  textFontSize: 18,

                }}
              /> : null}
            </View>
          </View>
        </Modal>

        {/* Modal for selecting court time */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={isCourtTimeModalVisible}
          onRequestClose={toggleCourtTimeModal}
        >
          {/* {isLoading?<Loader/>:null} */}
          <View style={styles.modalContainer}>
            <View style={styles.fragment}>
              <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Select day and time</Text>
              {/* Close button */}
              <TouchableOpacity style={styles.closeButton} onPress={() => {
                toggleCourtTimeModal()
                // toggleCourtDateModal()
                toggleCourtDurationModal()
              }}>
                <Image source={require("../../../assets/images/venue_detail_back.png")} style={{ width: 24, height: 24 }} />
                {/* <IonIcon name='arrow-back' size={30} color={COLORS.secondary} /> */}
              </TouchableOpacity>
              <TouchableOpacity style={styles.exitButton} onPress={() => {
                closeAllModals()
              }}>
                <IonIcon name='close' size={30} color={COLORS.secondary} />
                {/* <IonIcon name='arrow-back' size={30} color={COLORS.secondary} /> */}
              </TouchableOpacity>
              <View style={{ justifyContent: "center", alignItems: "center", marginTop: 16 }}><Text style={{ fontSize: 16, fontFamily: FONTS.family, color: COLORS.lightGrey }}>Duration {duration} ({selectedPrice} SAR)</Text></View>
              <FlatList
                style={{ marginVertical: 24 }}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                data={getNext30Days()}
                renderItem={(date) => {
                  // alert(date.item)
                  // Check if the item is selected and change the background color accordingly
                  const isSelected = date.index === selectedDateItem;
                  const backgroundColor = isSelected ? COLORS.accentLight : 'white'; // Customize the colors as needed
                  // console.log(getDayName(date.item))
                  return (
                    <TouchableOpacity onPress={() => {
                      handleItemPress(date.index)
                      // alert(date.index)
                      const newDateFormat = moment(date.item, "YYYY-MM-DD").format('YYYY/MM/DD')
                      var dateDayName = new Date(date.item);
                      const dayName = dateDayName.toLocaleDateString("en-Us", { weekday: "long" })
                      // console.log(dateDayName.getFullYear())
                      if (days[dateDayName.getDay()] == "Monday") {
                        if (item.timing.mondayOn) {
                          setDate(newDateFormat)
                          // toggleCourtDateModal()
                          // toggleCourtTimeModal()
                          getCourtDetail2(selectedCourtDetail)
                        } else {
                          alert("Closed")
                          return
                        }

                      } else if (days[dateDayName.getDay()] == "Tuesday") {
                        if (item.timing.tuesdayOn) {
                          setDate(newDateFormat)
                          // toggleCourtDateModal()
                          // toggleCourtTimeModal()
                          getCourtDetail2(selectedCourtDetail)
                        } else {
                          alert("Closed")
                          return
                        }
                      } else if (days[dateDayName.getDay()] == "Wednesday") {
                        if (item.timing.wedOn) {
                          setDate(newDateFormat)
                          // toggleCourtDateModal()
                          // toggleCourtTimeModal()
                          getCourtDetail2(selectedCourtDetail)
                        } else {
                          alert("Closed")
                          return
                        }
                      } else if (days[dateDayName.getDay()] == "Thursday") {
                        if (item.timing.thursdayOn) {
                          setDate(newDateFormat)
                          // toggleCourtDateModal()
                          // toggleCourtTimeModal()
                          getCourtDetail2(selectedCourtDetail)
                        } else {
                          alert("Closed")
                          return
                        }
                      } else if (days[dateDayName.getDay()] == "Friday") {
                        if (item.timing.fridayOn) {
                          setDate(newDateFormat)
                          // toggleCourtDateModal()
                          // toggleCourtTimeModal()
                          getCourtDetail2(selectedCourtDetail)
                        } else {
                          alert("Closed")
                          return
                        }
                      } else if (days[dateDayName.getDay()] == "Saturday") {
                        if (item.timing.satOn) {
                          setDate(newDateFormat)
                          // toggleCourtDateModal()
                          // toggleCourtTimeModal()
                          getCourtDetail2(selectedCourtDetail)
                        } else {
                          alert("Closed")
                          return
                        }
                      } else if (days[dateDayName.getDay()] == "Sunday") {
                        if (item.timing.sunOn) {
                          setDate(newDateFormat)
                          // toggleCourtDateModal()
                          // toggleCourtTimeModal()
                          getCourtDetail2(selectedCourtDetail)
                        } else {
                          alert("Closed")
                          return
                        }
                      }
                      
                      if(newDateFormat === currentDate){
                        setTimeRange(filteredTimeRange)
                      }else{
                        setTimeRange(baseTimeRange)
                      }
                      

                    }} key={item.index} style={{ width: 60, height: 68, marginRight: 8, justifyContent: "center", alignItems: "center", borderRadius: 16, borderWidth: isSelected ? 0 : 1, borderColor: COLORS.dividerBorderColor, backgroundColor: backgroundColor }}>
                      <Text style={{ fontFamily: FONTS.family500, fontSize: 20, color: COLORS.dark }}>{getDateFromDateString(date.item)}</Text>
                      <Text style={{ fontFamily: FONTS.family, fontSize: 12, color: COLORS.lightGrey }}>{getDayName(date.item)}</Text>
                      {/* <Text>{getDateDayName(item.item)}</Text> */}
                    </TouchableOpacity>
                  )
                }}
              />
              {/* FlatList to render court Duration */}
              {courtInfo && timeRange ? <View style={{ height: Dimensions.get("screen").height / 2, paddingBottom: 30 }}><FlatList
                data={timeRange}
                removeClippedSubviews={false}
                horizontal={false}
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item, index }) => {
                  
                  const currentDateTime = new Date()
                  const selectedTimeParts = item.split(":"); // Split selected time into hours and minutes
                  const selectedHours = parseInt(selectedTimeParts[0], 10);
                  const selectedMinutes = parseInt(selectedTimeParts[1], 10);
                  const currentHours = currentDateTime.getHours();
                  const currentMinutes = currentDateTime.getMinutes();
                  const timeIntervalsByBooking = courtInfo.bookingInfo.map(booking => {
                    if (date === booking.date) {
                      const duration = removeMinAndConvertToNumber(booking.duration)
                      const { timeFrom, timeTo } = booking.dateTimeInfo;
                      const intervals = generateTimeIntervals(timeFrom, timeTo, duration);
                      return { intervals };
                    } else {
                      return null
                    }
                  });
                  
                  const filteredTimeIntervalsByBooking2 = timeIntervalsByBooking.filter(entry => entry !== null);
                  if ((getFormatedDate(currentDateTime) === date && selectedHours < currentHours) || (getFormatedDate(currentDateTime) === date && (selectedHours === currentHours) && (selectedMinutes <= currentMinutes))) {
                    // Selected time is in the past
                    // console.log('Selected time is in the past.');
                  } else {
                    // console.log(item)
                    return (
                      <TouchableOpacity key={index} onPress={() => {
                        const currentDateTime = new Date()

                        const selectedTimeParts = item.split(":"); // Split selected time into hours and minutes
                        const selectedHours = parseInt(selectedTimeParts[0], 10);
                        const selectedMinutes = parseInt(selectedTimeParts[1], 10);

                        const currentHours = currentDateTime.getHours();
                        const currentMinutes = currentDateTime.getMinutes();

                        const timeIntervalsByBooking = courtInfo.bookingInfo.map(booking => {
                          if (date === booking.date) {
                            const duration = removeMinAndConvertToNumber(booking.duration)
                            const { timeFrom, timeTo } = booking.dateTimeInfo;
                            const intervals = generateTimeIntervals(timeFrom, timeTo, duration);
                            return { intervals };
                          } else {
                            return null
                          }
                        });
                        
                        // console.log("DATE TIME CHECK")
                        // console.log(getFormatedDate(currentDateTime))
                        // console.log(date)
                        const filteredTimeIntervalsByBooking = timeIntervalsByBooking.filter(entry => entry !== null);
                        if ((getFormatedDate(currentDateTime) === date && selectedHours < currentHours) || (getFormatedDate(currentDateTime) === date && selectedHours === currentHours && selectedMinutes <= currentMinutes)) {
                          // Selected time is in the past
                          // alert('Selected time is in the past.');
                        } else {

                          const selectedTime = item; // The selected time in "HH:mm" format

                          const isTimeInRange = filteredTimeIntervalsByBooking.some(booking => {
                            return booking.intervals.some(interval => {
                              const [intervalHours, intervalMinutes] = interval.split(":").map(Number);
                              const [selectedHours, selectedMinutes] = selectedTime.split(":").map(Number);

                              const intervalInMinutes = intervalHours * 60 + intervalMinutes;
                              const selectedInMinutes = selectedHours * 60 + selectedMinutes;

                              return selectedInMinutes >= intervalInMinutes && selectedInMinutes < intervalInMinutes + 30; // Assuming each interval is 30 minutes
                            });
                          });

                          if (isTimeInRange) {
                            alert("Booked")
                          } else {
                            setSelectedTime(item)
                            setSelectedTimeRange(item)
                            toggleCourtTimeModal()
                            toggleCourtPaymentModal()
                          }

                        }

                      }}>
                        <TimeIntervalItem item={item} filteredTimeIntervalsByBooking={filteredTimeIntervalsByBooking2} />
                      </TouchableOpacity>
                    )
                  }

                }}
              /></View> : null}
            </View>
          </View>
        </Modal>

        {/* Modal for selecting court payment */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={isCourtPaymentModalVisible}
          onRequestClose={toggleCourtPaymentModal}
        >
          <View style={styles.modalContainer}>
            <View style={styles.fragment}>
              <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Method Payment</Text>
              {/* Close button */}
              <TouchableOpacity style={styles.closeButton} onPress={() => {
                toggleCourtPaymentModal()
                toggleCourtTimeModal()
              }}>
                <Image source={require("../../../assets/images/venue_detail_back.png")} style={{ width: 24, height: 24 }} />
                {/* <IonIcon name='arrow-back' size={30} color={COLORS.secondary} /> */}
              </TouchableOpacity>
              <TouchableOpacity style={styles.exitButton} onPress={() => {
                closeAllModals()
              }}>
                <IonIcon name='close' size={30} color={COLORS.secondary} />
                {/* <IonIcon name='arrow-back' size={30} color={COLORS.secondary} /> */}
              </TouchableOpacity>
              {/* FlatList to render court Duration */}
              {cards ? <View style={{ paddingBottom: 30 }}>
                <FlatList
                  data={cards}
                  horizontal={false}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  renderItem={({ item, index }) => {
                    return (
                      <TouchableOpacity key={index} onPress={() => {
                        // setSelectedTimeRange(item)
                        toggleCourtPaymentModal()
                        toggleCourtConfirmPaymentModal()
                        setSelectedPayment(item)
                        // toggleCourtDurationModal()
                      }}>
                        <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", height: 60 }}>
                          <View style={{ flexDirection: "row", alignItems: "center" }}>
                            {item.card.brand === "master" && (<Image source={require("../../../assets/images/mastercard.png")} style={{ width: 36, height: 24 }} />)}
                            {item.card.brand === "visa" && (<Image source={require("../../../assets/images/visacard.png")} style={{ width: 36, height: 24 }} />)}
                            {item.card.brand === "amex" && (<Image source={require("../../../assets/images/amexcard.png")} style={{ width: 36, height: 24 }} />)}
                            <Text style={{ fontSize: 16, fontWeight: FONTS.lightBoldFontWeight, color: COLORS.dark, marginLeft: 12, fontFamily: FONTS.family }}>•••• {item.card.last_four}</Text>
                          </View>
                          {/* <TouchableOpacity onPress={() => setEditModalVisible(true)}><Image style={{ alignSelf: "baseline" }} source={require("../../../assets/images/more_img.png")} /></TouchableOpacity> */}
                        </View>
                        <View style={{ width: "100%", backgroundColor: COLORS.dividerBorderColor, height: 0.5 }} />
                      </TouchableOpacity>
                    )
                  }}
                />
              </View> : null}
              <View style={{
                height: 50,
                // alignItems: "center",
                justifyContent: "center",
                // backgroundColor: "#000",
                borderRadius: 16,
                marginBottom: 20
              }} >
                {/* <Text style={{ fontSize: FONTS.buttonTextSize, color: COLORS.brandLight, fontFamily: FONTS.family600 }}>Apple Pay</Text> */}
                <ApplePayButton buttonStyle="black"
        type="plain" onPress={() => onApplePayPress()}/>
              </View>
              <TouchableOpacity style={{
                height: 50,
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: COLORS.secondary,
                borderRadius: 16,
                marginBottom: 20
              }} onPress={() => {
                toggleCourtPaymentModal()
                toggleCourtAddCardModal()
              }}>
                <Text style={{ fontSize: FONTS.buttonTextSize, color: COLORS.brandLight, fontFamily: FONTS.family600 }}>Add new card</Text>
              </TouchableOpacity>
              
            </View>
          </View>
        </Modal>

        {/* Modal for adding new card */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={isCourtAddCardModalVisible}
          onRequestClose={toggleCourtAddCardModal}
        >
          {isLoading ? <Loader /> : null}
          <View style={styles.modalContainer}>
          <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null} style={styles.containerInner}>

            <View style={styles.fragment}>
              <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Add Card</Text>
              {/* Close button */}
              <TouchableOpacity style={styles.closeButton} onPress={() => {
                toggleCourtAddCardModal()
                toggleCourtPaymentModal()
              }}>
                <Image source={require("../../../assets/images/venue_detail_back.png")} style={{ width: 24, height: 24 }} />
                {/* <IonIcon name='arrow-back' size={30} color={COLORS.secondary} /> */}
              </TouchableOpacity>

              <View style={{ paddingBottom: 30 }}>
                <View style={{ backgroundColor: COLORS.secondary, padding: 16, borderRadius: 12 }}>
                  <View>
                    <Text style={{ color: COLORS.brandLight, fontFamily: FONTS.family, fontSize: 12 }}>Card number</Text>
                    <TextInput style={{ backgroundColor: COLORS.white, borderRadius: 12, height: 50, paddingHorizontal: 16, marginTop: 5 }} value={number} onChangeText={(text) => setNumber(text)} placeholder='0000 0000 0000 0000' maxLength={16} />
                  </View>
                  <View style={{ flexDirection: "row", marginTop: 10 }}>
                    <View style={{ width: "69%" }}>
                      <Text style={{ color: COLORS.brandLight, fontFamily: FONTS.family, fontSize: 12 }}>Expiration date</Text>
                      <TextInput style={{ backgroundColor: COLORS.white, borderRadius: 12, height: 50, paddingHorizontal: 16, marginTop: 5 }} value={expirydate} onChangeText={handleExpiryDateChange} placeholder='00/00' maxLength={5} />
                    </View>
                    <View style={{ width: "29%", marginLeft: 10 }}>
                      <Text style={{ color: COLORS.brandLight, fontFamily: FONTS.family, fontSize: 12 }}>Security Code</Text>
                      <TextInput style={{ backgroundColor: COLORS.white, borderRadius: 12, height: 50, paddingHorizontal: 16, marginTop: 5 }} value={cvc} onChangeText={(text) => setCvc(text)} placeholder='000' maxLength={3} />
                    </View>
                  </View>
                  <View style={{ marginTop: 10 }}>
                    <Text style={{ color: COLORS.brandLight, fontFamily: FONTS.family, fontSize: 12 }}>Card holder name</Text>
                    <TextInput style={{ backgroundColor: COLORS.white, borderRadius: 12, height: 50, paddingHorizontal: 16, marginTop: 5 }} value={name} onChangeText={(text) => setName(text)} placeholder='John Dou' />
                  </View>
                </View>
                <Text style={{ fontSize: 12, fontFamily: FONTS.family, color: COLORS.darkOpacity, marginTop: 16 }}>The seurity code is the 3-digit code on the back of your credit or debit card on the right side of the signature strip.</Text>
              </View>
              <TouchableOpacity style={{
                height: 50,
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: COLORS.secondary,
                borderRadius: 16,
                marginBottom: 20
              }} onPress={addCard}>
                <Text style={{ fontSize: FONTS.buttonTextSize, color: COLORS.brandLight, fontFamily: FONTS.family600 }}>Add Card</Text>
              </TouchableOpacity>
            </View>
            </KeyboardAvoidingView>
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          visible={isConfirmCardModal}
          onRequestClose={toggleConfirmCardModal}
        >
          <View style={styles.modalContainer}>
            <View style={[styles.fragment, { height: Dimensions.get("screen").height / 1.5 }]}>
              <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Confirm Card</Text>
              <IonIcon name='arrow-back' size={30} style={styles.closeButton} color={COLORS.secondary} onPress={() => {
                toggleConfirmCardModal()
                toggleCourtPaymentModal()
              }} />
              {confirm_payment_url ? <WebView onNavigationStateChange={handleNavigationStateChange} source={{ uri: confirm_payment_url }} /> : null}


            </View>
          </View>
        </Modal>

        {/* Modal for selecting court Confirm Payment */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={isCourtConfirmPaymentModalVisible}
          onRequestClose={toggleCourtConfirmPaymentModal}
        >
          {isLoading ? <Loader /> : null}
          <View style={styles.modalContainer}>
            <View style={styles.fragment}>
              <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Confirm and pay</Text>
              {/* Close button */}
              <TouchableOpacity style={styles.closeButton} onPress={() => {
                toggleCourtConfirmPaymentModal()
                toggleCourtPaymentModal()
              }}>
                <Image source={require("../../../assets/images/venue_detail_back.png")} style={{ width: 24, height: 24 }} />
                {/* <IonIcon name='arrow-back' size={30} color={COLORS.secondary} /> */}
              </TouchableOpacity>
              <TouchableOpacity style={styles.exitButton} onPress={() => {
                closeAllModals()
              }}>
                <IonIcon name='close' size={30} color={COLORS.secondary} />
                {/* <IonIcon name='arrow-back' size={30} color={COLORS.secondary} /> */}
              </TouchableOpacity>
              {/* FlatList to render court Duration */}
              <View style={{ margin: 10 }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                  <View style={{ flexDirection: "column" }}>
                    <Text style={{ color: COLORS.darkOpacity, fontSize: 14, fontFamily: FONTS.family600 }}>Duration</Text>
                    <Text style={{ color: COLORS.dark, fontSize: 16, fontFamily: FONTS.family }}>{duration}</Text>
                  </View>
                  {/* <TouchableOpacity style={{ borderWidth: 1, borderColor: COLORS.dividerBorderColor, borderRadius: 40, justifyContent: "center", alignItems: "center", paddingVertical: 5, paddingHorizontal: 10 }} onPress={() => {

                  }}>
                    <Text style={{ fontWeight: FONTS.regularBoldFontWeight, fontSize: 14, color: COLORS.dark }}>Edit</Text>
                  </TouchableOpacity> */}
                </View>
                <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 10 }}>
                  <View style={{ flexDirection: "column" }}>
                    <Text style={{ color: COLORS.darkOpacity, fontSize: 14, fontFamily: FONTS.family600 }}>Dates</Text>
                    <Text style={{ color: COLORS.dark, fontSize: 16, fontFamily: FONTS.family }}>{date}, {formatTimeToAMPM(selectedTime)}</Text>
                  </View>
                  {/* <TouchableOpacity style={{ borderWidth: 1, borderColor: COLORS.dividerBorderColor, borderRadius: 40, paddingVertical: 5, paddingHorizontal: 10 }} onPress={() => {

                  }}>
                    <Text style={{ fontWeight: FONTS.regularBoldFontWeight, fontSize: 14, color: COLORS.dark }}>Edit</Text>
                  </TouchableOpacity> */}
                </View>
                <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 10 }}>
                  <View style={{ flexDirection: "column" }}>
                    <Text style={{ color: COLORS.darkOpacity, fontSize: 14, fontFamily: FONTS.family600 }}>Court #</Text>
                    <Text style={{ color: COLORS.dark, fontSize: 16, fontFamily: FONTS.family }}>{courtInfo ? courtInfo.title : ""}</Text>
                  </View>
                  {/* <TouchableOpacity style={{ borderWidth: 1, borderColor: COLORS.dividerBorderColor, borderRadius: 40, paddingVertical: 5, paddingHorizontal: 10 }} onPress={() => {

                  }}>
                    <Text style={{ fontWeight: FONTS.regularBoldFontWeight, fontSize: 14, color: COLORS.dark }}>Edit</Text>
                  </TouchableOpacity> */}
                </View>
                <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 10 }}>
                  <View style={{ flexDirection: "column" }}>
                    <Text style={{ color: COLORS.darkOpacity, fontSize: 14, fontFamily: FONTS.family600 }}>Pay with</Text>
                    <Text style={{ color: COLORS.dark, fontSize: 16, fontFamily: FONTS.family }}>**** {selectedPayment ? selectedPayment.card.last_four : ""}</Text>
                  </View>
                  {/* <TouchableOpacity style={{ borderWidth: 1, borderColor: COLORS.dividerBorderColor, borderRadius: 40, paddingVertical: 5, paddingHorizontal: 10 }} onPress={() => {

                  }}>
                    <Text style={{ fontWeight: FONTS.regularBoldFontWeight, fontSize: 14, color: COLORS.dark }}>Edit</Text>
                  </TouchableOpacity> */}
                </View>
                <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                <TouchableOpacity style={{ borderRadius: 16, height: 50, backgroundColor: COLORS.secondary, justifyContent: "center", alignItems: "center" }} onPress={payAndReserve}>
                  <Text style={{ color: COLORS.brandLight, fontSize: 16, fontFamily: FONTS.family600 }}>Pay {selectedPrice ? selectedPrice : ""} SAR</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        {/* Modal for reservation details */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={isCourtReservationDetailsModalVisible}
          onRequestClose={toggleCourtReservationDetailsModal}
        >
          <View style={styles.modalContainer}>
            <View style={styles.fragment}>
              <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Reservation details</Text>
              {/* Close button */}
              <TouchableOpacity style={styles.closeButton} onPress={() => {
                toggleCourtReservationDetailsModal()
                setModalVisible(false)
                setCourtAddCardModalVisible(false)
                setCourtConfirmPaymentModalVisible(false)
                setCourtDateModalVisible(false)
                setCourtDurationModalVisible(false)
                setCourtPaymentModalVisible(false)
                setCourtTimeModalVisible(false)
                setCourtReservationDetailsModalVisible(false)
                // toggleCourtPaymentModal()
              }}>
                <Image source={require("../../../assets/images/venue_detail_back.png")} style={{ width: 24, height: 24 }} />
                {/* <IonIcon name='arrow-back' size={30} color={COLORS.secondary} /> */}
              </TouchableOpacity>
              {/* FlatList to render court Duration */}
              <View style={{ margin: 10 }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                  <View style={{ flexDirection: "column" }}>
                    <Text style={{ color: COLORS.darkOpacity, fontSize: 14, fontFamily: FONTS.family600 }}>Duration</Text>
                    <Text style={{ color: COLORS.dark, fontSize: 16, fontFamily: FONTS.family }}>{confirmedBooking ? confirmedBooking.duration : null}</Text>
                  </View>
                  {/* <TouchableOpacity style={{borderWidth:1,borderColor:COLORS.dividerBorderColor,borderRadius:40,justifyContent:"center",alignItems:"center",paddingVertical:5,paddingHorizontal:10}} onPress={() => {

                    } }>
                          <Text style={{fontWeight:FONTS.regularBoldFontWeight,fontSize:14,color:COLORS.dark}}>Edit</Text>
                    </TouchableOpacity> */}
                </View>
                <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 10 }}>
                  <View style={{ flexDirection: "column" }}>
                    <Text style={{ color: COLORS.darkOpacity, fontSize: 14, fontFamily: FONTS.family600 }}>Dates</Text>
                    <Text style={{ color: COLORS.dark, fontSize: 16, fontFamily: FONTS.family }}>{confirmedBooking ? confirmedBooking.date : null}, {confirmedBooking ? formatTimeToAMPM(confirmedBooking.time) : null}</Text>
                  </View>
                  {/* <TouchableOpacity style={{borderWidth:1,borderColor:COLORS.dividerBorderColor,borderRadius:40,paddingVertical:5,paddingHorizontal:10}} onPress={() => {

                    } }>
                          <Text style={{fontWeight:FONTS.regularBoldFontWeight,fontSize:14,color:COLORS.dark}}>Edit</Text>
                    </TouchableOpacity> */}
                </View>
                <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 10 }}>
                  <View style={{ flexDirection: "column" }}>
                    <Text style={{ color: COLORS.darkOpacity, fontSize: 14, fontFamily: FONTS.family600 }}>Court #</Text>
                    <Text style={{ color: COLORS.dark, fontSize: 16, fontFamily: FONTS.family }}>{courtInfo ? courtInfo.title : null}</Text>
                  </View>
                  {/* <TouchableOpacity style={{borderWidth:1,borderColor:COLORS.dividerBorderColor,borderRadius:40,paddingVertical:5,paddingHorizontal:10}} onPress={() => {

                    } }>
                          <Text style={{fontWeight:FONTS.regularBoldFontWeight,fontSize:14,color:COLORS.dark}}>Edit</Text>
                    </TouchableOpacity> */}
                </View>
                <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 10 }}>
                  <View style={{ flexDirection: "column" }}>
                    <Text style={{ color: COLORS.darkOpacity, fontSize: 14, fontFamily: FONTS.family600 }}>Paid</Text>
                    <Text style={{ color: COLORS.dark, fontSize: 16, fontFamily: FONTS.family }}>{confirmedBooking ? confirmedBooking.payment.amount_format : null}</Text>
                  </View>
                  {/* <TouchableOpacity style={{borderWidth:1,borderColor:COLORS.dividerBorderColor,borderRadius:40,paddingVertical:5,paddingHorizontal:10}} onPress={() => {

                    } }>
                          <Text style={{fontWeight:FONTS.regularBoldFontWeight,fontSize:14,color:COLORS.dark}}>Edit</Text>
                    </TouchableOpacity> */}
                </View>
                <View style={{ width: "100%", borderWidth: 0.5, borderColor: COLORS.dividerBorderColor, marginTop: 10 }} />
                <TouchableOpacity onPress={() => cancelBookingReservation()} style={{ borderRadius: 16, height: 50, backgroundColor: COLORS.red, justifyContent: "center", alignItems: "center" }}>
                  <Text style={{ fontFamily: FONTS.family, color: COLORS.brandLight, fontSize: 16, fontFamily: FONTS.family600 }}>Cancel Reservation</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        {/* Modal for reservation confirmed */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={isCourtReservationConfirmedModalVisible}
          onRequestClose={toggleCourtReservationConfirmedModal}
        >
          <View style={styles.modalReservationConfirmedContainer}>
            <View style={[styles.fragmentReservationConfirmed, { height: "100%" }]}>
              <ImageBackground style={{ height: Dimensions.get("screen").height, width: Dimensions.get("screen").width }} source={require("../../../assets/images/reservation_confirmed.png")}>
                <View style={{ height: Dimensions.get("screen").height, width: Dimensions.get("screen").width, backgroundColor: COLORS.secondary, position: "absolute", opacity: 0.5 }} />
                <Image style={{ width: Dimensions.get("screen").width, opacity: 0.4 }} source={require("../../../assets/images/confetti.png")} />
                <View style={{ position: "absolute", bottom: 0, width: Dimensions.get("screen").width, paddingBottom: Platform.OS === "ios" ? 40 : "30%" }}>
                  <Text style={{ color: COLORS.brandLight, fontSize: 32, fontFamily: FONTS.family600, lineHeight: 50, marginHorizontal: 20, marginBottom: 32, width: "80%" }}>You're reserved paddel area is confirmed!</Text>
                  <TouchableOpacity style={{ height: 50, backgroundColor: COLORS.accentLight, justifyContent: "center", alignItems: "center", borderRadius: 16, marginHorizontal: 20 }} onPress={() => {
                    toggleCourtReservationConfirmedModal()
                    toggleCourtReservationDetailsModal()
                  }}>
                    <Text style={{ color: COLORS.dark, fontSize: 16, fontFamily: FONTS.family600 }}>View Details</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{ height: 50, backgroundColor: COLORS.secondary, justifyContent: "center", alignItems: "center", borderRadius: 16, marginHorizontal: 20, marginTop: 10 }} onPress={() => {
                    toggleCourtReservationConfirmedModal()
                    setModalVisible(false)
                    setCourtAddCardModalVisible(false)
                    setCourtConfirmPaymentModalVisible(false)
                    setCourtDateModalVisible(false)
                    setCourtDurationModalVisible(false)
                    setCourtPaymentModalVisible(false)
                    setCourtTimeModalVisible(false)
                    setCourtReservationDetailsModalVisible(false)
                    navigation.navigate("Bookings")
                  }}>
                    <Text style={{ color: COLORS.brandLight, fontSize: 16, fontFamily: FONTS.family600 }}>Done</Text>
                  </TouchableOpacity>
                </View>
              </ImageBackground>
            </View>
          </View>
        </Modal>


        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: 20,
          }}
        >
          {/* Carousel */}
          {item ? <Carousel distance={myLng && venueLng ? calculateDistance(myLat, myLng, venueLat, venueLng).toFixed(1) : "Allow GPS"} data={item.photos} scrollX={scrollX} /> : null}

          {item ? <View style={styles.header}>
            {/* Back button */}
            <TouchableOpacity
              style={{ backgroundColor: COLORS.white, padding: 10, borderRadius: 30, alignContent: "center", justifyContent: "center", marginTop: 4, marginLeft: 4 }}
              onPress={() => navigation.navigate("Discover", { updatedLat: myLat, updatedLng: myLng })}
            >
              <Image source={require("../../../assets/images/venue_detail_back.png")} style={{ width: 24, height: 24 }} />
            </TouchableOpacity>

            {/* Share and Favorite buttons */}
            <View style={{ flexDirection: "row", right: 40 }}>
              {/* <TouchableOpacity
                style={{ backgroundColor: COLORS.white, padding: 10, borderRadius: 30, alignContent: "center", justifyContent: "center", marginTop: 4, marginRight: 10 }}
                onPress={navigation.goBack}
              >
                <Image source={require("../../../assets/images/share_venue_detail.png")} />
              </TouchableOpacity> */}
              <TouchableOpacity
                style={{ backgroundColor: COLORS.white, padding: 10, borderRadius: 30, alignContent: "center", justifyContent: "center", marginTop: 4 }}
                onPress={() => {
                  if (isGuest) {
                    alert("Guest not allowed")
                  } else {
                    toggleVenueFav()
                  }

                }}
              >
                <Image source={isFavVenue ? require("../../../assets/images/selected_fav.png") : require("../../../assets/images/unselected_fav.png")} style={{ width: 24, height: 24 }} />
              </TouchableOpacity>
            </View>
          </View> : null}

          {/* Venue Details */}
          {item ? <View style={{ marginTop: 16, paddingHorizontal: 0 }}>
            <Text style={{ fontSize: FONTS.titleHeading, color: COLORS.dark, fontFamily: FONTS.family700, paddingHorizontal: 16 }}>{item.name}</Text>
            <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10, paddingHorizontal: 16 }}>
              <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                <Image source={require("../../../assets/images/clock.png")} style={{ width: 20, height: 20 }} />
                <Text style={{ marginLeft: 5, fontSize: 14, color: COLORS.dark, fontFamily: FONTS.family }}>{checkOpenStatus(item.timing)}</Text>
              </View>
              {/* <View style={{ flexDirection: "row", marginLeft: 24 }}>
              <Image source={require("../../../assets/images/outdoor_icon.png")} />
              <Text style={{ marginLeft: 5, fontSize: 14, color: COLORS.dark }}>Outdoor</Text>
            </View>
            <View style={{ flexDirection: "row", marginLeft: 24 }}>
              <Image source={require("../../../assets/images/rules.png")} />
              <Text style={{ marginLeft: 5, fontSize: 14, color: COLORS.dark }}>24m x 16m</Text>
            </View> */}
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 10, paddingHorizontal: 16 }}>
              <Text style={{ width: 220, color: COLORS.dark, fontFamily: FONTS.family }}>{item.address}</Text>
              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity onPress={() => openMapWithAddress(item.address)} style={{ backgroundColor: COLORS.white, borderRadius: 20, justifyContent: "center", alignItems: "center", borderColor: "#00000012", borderWidth: 1, marginRight: 12 }}>
                  <Image source={require("../../../assets/images/route.png")} style={{ width: 24, height: 24, margin: 13 }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => dialNumber(item.venuePhone)} style={{ backgroundColor: COLORS.white, borderRadius: 20, justifyContent: "center", alignItems: "center", borderColor: "#00000012", borderWidth: 1 }}>
                  <Image source={require("../../../assets/images/phone.png")} style={{ width: 24, height: 24, margin: 13 }} />
                </TouchableOpacity>
              </View>

            </View>
            <View style={{ width: "100%", backgroundColor: "rgba(0, 0, 0, 0.08)", height: 1, marginTop: 19 }} />
            {/* Other details about the venue */}
            <View style={{ marginTop: 20, paddingHorizontal: 16 }}>
              <Text style={{ color: "rgba(22, 34, 52, 0.60)", fontSize: 14, fontFamily: FONTS.family }}>Description</Text>
              <Text style={{ lineHeight: 20, fontSize: FONTS.textDescription, color: COLORS.dark, marginTop: 12, fontFamily: FONTS.family }}>{item.description}</Text>
            </View>

            {/* Facilities */}
            <Text style={{ color: "rgba(22, 34, 52, 0.60)", fontSize: 14, marginTop: 20, fontFamily: FONTS.family, paddingHorizontal: 16 }}>Facilities</Text>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ marginTop: 10, paddingLeft: 16 }}>
              <View style={{ flexDirection: "row", marginRight: 20 }}>
                {/* Amenities icons */}
                <View style={{ flexDirection: "row" }}>

                  {item.amenities.cafeteria ? <View style={styles.amenitiesIconContainer}>
                    <IonIcon name='cafe' size={24} color={COLORS.dark} />
                    {/* <Image source={require("../../../assets/images/restaurant.png")}/> */}
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>Cafe
                    </Text>
                  </View> : null}
                  {item.amenities.disabledAccess ? <View style={styles.amenitiesIconContainer}>
                    <Image source={require("../../../assets/images/wheelchair.png")} style={{ width: 24, height: 24 }} />
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>Special Access</Text>
                  </View> : null}
                  {item.amenities.changeRoom ? <View style={styles.amenitiesIconContainer}>
                    <Image source={require("../../../assets/images/showers.png")} style={{ width: 24, height: 24 }} />
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>Showers</Text>
                  </View> : null}
                  {item.amenities.freeParking ? <View style={styles.amenitiesIconContainer}>
                    <Image source={require("../../../assets/images/parking.png")} style={{ width: 24, height: 24 }} />
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>Free Parking</Text>
                  </View> : null}
                  {item.amenities.lockers ? <View style={styles.amenitiesIconContainer}>
                    <Image source={require("../../../assets/images/lockers.png")} style={{ width: 24, height: 24 }} />
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>Lockers</Text>
                  </View> : null}
                  {item.amenities.materialRenting ? <View style={styles.amenitiesIconContainer}>
                    <Image source={require("../../../assets/images/rent_equipment.png")} style={{ width: 24, height: 24 }} />
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>Material Renting</Text>
                  </View> : null}
                  {item.amenities.playPark ? <View style={styles.amenitiesIconContainer}>
                    <Image source={require("../../../assets/images/kids_playground.png")} style={{ width: 24, height: 24 }} />
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>Kids Playground</Text>
                  </View> : null}
                  {item.amenities.privateParking ? <View style={styles.amenitiesIconContainer}>
                    <Image source={require("../../../assets/images/paid_parking.png")} style={{ width: 24, height: 24 }} />
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>Private Parking</Text>
                  </View> : null}
                  {item.amenities.restaurant ? <View style={styles.amenitiesIconContainer}>
                    <Image source={require("../../../assets/images/restaurant.png")} style={{ width: 24, height: 24 }} />
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>Restaurant</Text>
                  </View> : null}
                  {item.amenities.snackbar ? <View style={styles.amenitiesIconContainer}>
                    <Image source={require("../../../assets/images/snackbar.png")} style={{ width: 24, height: 24 }} />
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>Snackbar</Text>
                  </View> : null}
                  {item.amenities.store ? <View style={styles.amenitiesIconContainer}>
                    <FontAwesome5 name='store' size={20} color={COLORS.dark} />
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>Store</Text>
                  </View> : null}
                  {item.amenities.vendingMachine ? <View style={styles.amenitiesIconContainer}>
                    <Image source={require("../../../assets/images/vending_machine.png")} style={{ width: 24, height: 24 }} />
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>Vending Machine</Text>
                  </View> : null}
                  {item.amenities.wifi ? <View style={styles.amenitiesIconContainer}>
                    <Image source={require("../../../assets/images/wifi.png")} style={{ width: 24, height: 24 }} />
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>Wifi</Text>
                  </View> : null}
                </View>
              </View>
            </ScrollView>

            {/* Timing */}
            <Text style={{ color: "rgba(22, 34, 52, 0.60)", fontSize: 14, marginTop: 10, fontFamily: FONTS.family, paddingHorizontal: 16 }}>Timing</Text>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ marginTop: 10, paddingLeft: 16 }}>
              <View style={{ flexDirection: "row", marginRight: 20 }}>
                {/* Timing details */}
                <View style={{ flexDirection: "row" }}>
                  <View style={styles.timingIconContainer}>
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginBottom: 10, fontFamily: FONTS.family600 }}>Monday: {item.timing.mondayOn ? "Open" : "Close"}</Text>
                    <Image source={require("../../../assets/images/clock.png")} style={{ width: 24, height: 24 }} />
                    {item.timing.mondayFrom ? <Text style={{ fontSize: FONTS.textSmall, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>{formatTimeToAMPM(item.timing.mondayFrom)}-{formatTimeToAMPM(item.timing.mondayTo)}</Text> : ""}
                  </View>
                  <View style={styles.timingIconContainer}>
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginBottom: 10, fontFamily: FONTS.family600 }}>Tuesday: {item.timing.tuesdayOn ? "Open" : "Close"}</Text>
                    <Image source={require("../../../assets/images/clock.png")} style={{ width: 24, height: 24 }} />
                    {item.timing.tuesdayFrom ? <Text style={{ fontSize: FONTS.textSmall, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>{formatTimeToAMPM(item.timing.tuesdayFrom)}-{formatTimeToAMPM(item.timing.tuesdayTo)}</Text> : ""}
                  </View>
                  <View style={styles.timingIconContainer}>
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginBottom: 10, fontFamily: FONTS.family600 }}>Wednesday: {item.timing.wedOn ? "Open" : "Close"}</Text>
                    <Image source={require("../../../assets/images/clock.png")} style={{ width: 24, height: 24 }} />
                    {item.timing.wedFrom ? <Text style={{ fontSize: FONTS.textSmall, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>{formatTimeToAMPM(item.timing.wedFrom)}-{formatTimeToAMPM(item.timing.wedTo)}</Text> : ""}
                  </View>
                  <View style={styles.timingIconContainer}>
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginBottom: 10, fontFamily: FONTS.family600 }}>Thursday: {item.timing.thursdayOn ? "Open" : "Close"}</Text>
                    <Image source={require("../../../assets/images/clock.png")} style={{ width: 24, height: 24 }} />
                    {item.timing.thursdayFrom ? <Text style={{ fontSize: FONTS.textSmall, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family600 }}>{formatTimeToAMPM(item.timing.thursdayFrom)}-{formatTimeToAMPM(item.timing.thursdayTo)}</Text> : ""}
                  </View>
                  <View style={styles.timingIconContainer}>
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginBottom: 10, fontFamily: FONTS.family600 }}>Friday: {item.timing.fridayOn ? "Open" : "Close"}</Text>
                    <Image source={require("../../../assets/images/clock.png")} style={{ width: 24, height: 24 }} />
                    {item.timing.fridayFrom ? <Text style={{ fontSize: FONTS.textSmall, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>{formatTimeToAMPM(item.timing.fridayFrom)}-{formatTimeToAMPM(item.timing.fridayTo)}</Text> : ""}
                  </View>
                  <View style={styles.timingIconContainer}>
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginBottom: 10, fontFamily: FONTS.family600 }}>Saturday: {item.timing.satOn ? "Open" : "Close"}</Text>
                    <Image source={require("../../../assets/images/clock.png")} style={{ width: 24, height: 24 }} />
                    {item.timing.satFrom ? <Text style={{ fontSize: FONTS.textSmall, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>{formatTimeToAMPM(item.timing.satFrom)}-{formatTimeToAMPM(item.timing.satTo)}</Text> : ""}
                  </View>
                  <View style={styles.timingIconContainer}>
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginBottom: 10, fontFamily: FONTS.family600 }}>Sunday: {item.timing.sunOn ? "Open" : "Close"}</Text>
                    <Image source={require("../../../assets/images/clock.png")} style={{ width: 24, height: 24 }} />
                    {item.timing.sunFrom ? <Text style={{ fontSize: FONTS.textSmall, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>{formatTimeToAMPM(item.timing.sunFrom)}-{formatTimeToAMPM(item.timing.sunTo)}</Text> : ""}
                  </View>
                  <View style={styles.timingIconContainer}>
                    <Text style={{ fontSize: FONTS.textDescription, color: COLORS.dark, marginBottom: 10, fontFamily: FONTS.family600 }}>Holidays: {item.timing.holidayOn ? "Open" : "Close"}</Text>
                    <Image source={require("../../../assets/images/clock.png")} style={{ width: 24, height: 24 }} />
                    {item.timing.holidayFrom ? <Text style={{ fontSize: FONTS.textSmall, color: COLORS.dark, marginTop: 10, fontFamily: FONTS.family }}>{formatTimeToAMPM(item.timing.holidayFrom)}-{formatTimeToAMPM(item.timing.holidayTo)}</Text> : ""}
                  </View>
                </View>
              </View>
            </ScrollView>
          </View> : null}

          {/* Show Courts Button */}
          <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
            <View style={{ marginLeft: 16 }}>
              <Text style={{ fontSize: 24, color: COLORS.dark, fontFamily: FONTS.family500 }}>{pricingFrom ? pricingFrom : ""} SAR</Text>
              <Text style={{ color: COLORS.lightGrey, fontSize: 12, fontFamily: FONTS.family }}>per hour</Text>
            </View>
            <TouchableOpacity style={styles.showCourtsButton} onPress={() => {
              if (isGuest) {
                showAlert()
              } else {
                toggleModal()
              }

            }}>
              <Text style={{ fontSize: FONTS.buttonTextSize, fontFamily: FONTS.family600, color: COLORS.brandLight }}>Book now</Text>
              <Image source={require("../../../assets/images/next.png")} style={{ width: 25, height: 25, marginLeft: 4 }} />
            </TouchableOpacity>
          </View>
        </ScrollView>

      </View> : <VenueDetailSkeleton />}

    </SafeAreaView>
  );
}

export default VenueDetailScreen