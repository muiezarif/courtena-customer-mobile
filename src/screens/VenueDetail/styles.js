import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:COLORS.white
    },
    headerImage:{
        flex:1,
        height:400,
        borderBottomRightRadius:40,
        borderBottomLeftRadius:40,
        overflow:"hidden"
    },
    header:{
        marginTop:20,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"space-between",
        marginLeft:20,
        position:"absolute",
        width:Dimensions.get("screen").width
    },
    iconContainer:{
        position:"absolute",
        height:60,
        width:60,
        backgroundColor:COLORS.secondary,
        top:-30,
        right:10,
        borderRadius:30,
        justifyContent:"center",
        alignItems:"center",
        paddingLeft:3
    },
    priceRangeContainer:{
        height:50,
        width:160,
        borderTopLeftRadius:20,
        borderBottomLeftRadius:20,
        // borderRadius:20,
        backgroundColor:COLORS.dark,
        alignItems:"center",
        justifyContent:"center"
    },
    amenitiesIconContainer:{
        height:88,
        width:88,
        backgroundColor:COLORS.brandLight,
        borderRadius:16,
        padding:10,
        flexDirection:"column",
        justifyContent:"space-between",
        // justifyContent:"center",
        // alignItems:"center",
        marginHorizontal:4
        // padding:10
    },
    timingIconContainer:{
        height:100,
        width:140,
        backgroundColor:COLORS.brandLight,
        borderRadius:10,
        justifyContent:"center",
        alignItems:"center",
        marginHorizontal:4
        // padding:10
    },
    showCourtsButton:{
        height:50,
        width:180,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:COLORS.secondary,
        borderRadius:20,
        margin:20
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        zIndex:3000
      },
      modalReservationConfirmedContainer: {
       
      },
      fragmentReservationConfirmed: {
        backgroundColor: '#fff',
        // padding: 16,
        
      },
      fragment: {
        backgroundColor: '#fff',
        padding: 16,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
      },
      fragmentText: {
        fontSize: 16,
        marginBottom: 10,
        color:COLORS.secondary,
        textAlign:"center",
        fontSize:20,
        // fontWeight:500
      },
      closeButton: {
        
        position:"absolute",
        top:16,
        left:10
      },
      exitButton: {
        
        position:"absolute",
        top:16,
        right:10
      },
      closeButton2: {
        
        position:"absolute",
        top:10,
        left:10
      },
      closeButtonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
      },
      courtCardImage:{
        height:80,
        width:132,
        borderRadius:10,
        marginLeft:10,
        marginVertical:10
    },
    courtCard:{
        // height:150,
        // width:"100%",
        width:Dimensions.get('screen').width-40,
        elevation:15,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: COLORS.accentLight,
        shadowOpacity: 1,
        marginBottom:10,
        borderWidth:1,
        borderColor:"#00000020",
        // marginHorizontal:5,
        borderRadius:15,
        backgroundColor:COLORS.white,
        shadowRadius:4
    },
})


export default styles