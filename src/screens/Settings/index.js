import { View, Text, SafeAreaView, TextInput, TouchableOpacity, Image, Dimensions } from 'react-native'
import React, { useEffect, useState } from 'react'
import styles from './styles'
import AntDesign from "react-native-vector-icons/AntDesign"
import FONTS from '../../utils/fonts'
import COLORS from '../../utils/colors'
import AsyncStorage from '@react-native-async-storage/async-storage'
import IonIcon from "react-native-vector-icons/FontAwesome"
import MaterialIcons from "react-native-vector-icons/MaterialIcons"
import { ScrollView } from 'react-native-gesture-handler'
import courtena from '../../api/courtena'
import Avatar from '../../components/Avatar'
import Loader from '../../components/Loader'
const SettingsScreen = ({navigation,route}) => {
  const [username,setUsername] = useState("")
  const [firstname,setFirstname] = useState("")
  const [lastname,setLastname] = useState("")
  const [isGuest,setIsGuest] = useState(false)

  const checkGuest = async() => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    setIsGuest(customerData.guest)
  }

  const getUserInfo = async() => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    await courtena.get("/customer/get-customer-info/"+customerData._id,{
        headers:{
            'Content-Type':'application/x-www-form-urlencoded',
            'Accept': '*/*',
            'Authorization': customerData.token
          }
    }).then((res) => {
        // console.log(res.data)
        if(res.data.success){
            setUsername(res.data.result.username)
            setFirstname(res.data.result.first_name)
            setLastname(res.data.result.last_name)
            // setEmail(res.data.result.email)
            // setCountry(res.data.result.country)
            // setCity(res.data.result.city)
        }else{
            console.log(res.data.message)
        }
    }).catch((err) => {
        console.log(err)
    })
  }
  useEffect(() => {
    getUserInfo()
    checkGuest()
  },[])
  return (
    <SafeAreaView style={styles.container}>
      <View style={{width:Dimensions.get("screen").width,height:Dimensions.get("screen").height/2.8,position:"absolute"}}>
      <Image style={{width:Dimensions.get("screen").width,height:Dimensions.get("screen").height/2,position:"absolute"}} source={require("../../../assets/images/login_image.png")}/>
      <View style={{width:Dimensions.get("screen").width,height:Dimensions.get("screen").height/2,position:"absolute",backgroundColor:COLORS.secondary,opacity:0.7}}/>
      <View style={{position:"absolute",bottom:0,marginBottom:32,marginLeft:16,flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
      {/* <Image style={{width:60,height:60,borderRadius:50}} source={require("../../../assets/images/login_image.png")}/>
       */}
       <Avatar name={firstname} width={60} height={60}/>
      <Text style={{color:COLORS.brandLight,fontSize:20,marginLeft:16,fontFamily:FONTS.family500}}>{firstname} {lastname}</Text>
      </View>
      
      </View>
    <View style={{position:"absolute",padding:20,bottom:0,width:Dimensions.get("screen").width,height:Dimensions.get("screen").height/1.6,borderRadius:20,backgroundColor:COLORS.brandLight}}>
      <ScrollView  showsVerticalScrollIndicator={false}>
      <Text style={{fontSize:20,color:COLORS.secondary,fontFamily:FONTS.family500}}>Account</Text>
      <View style={{backgroundColor:COLORS.white,borderRadius:16,marginTop:16}}>
        <View style={{flexDirection:"column",padding:8}}>
        <TouchableOpacity onPress={() => {
              navigation.navigate("FavouriteCourts")
            }}>
          <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center"}}>
            <View style={{flexDirection:"row",alignItems:"center",margin:12}}>
                {/* <IonIcon name='bookmark-o' size={24} color={COLORS.dark}/> */}
                <Image source={require("../../../assets/images/account_fav.png")} style={{width:24,height:24}}/>
                <Text style={{marginLeft:12,color:COLORS.dark,fontSize:FONTS.buttonTextSize,fontFamily:FONTS.family}}>Favourite Courts</Text>
            </View>
            <MaterialIcons name='arrow-forward-ios' size={11} color={COLORS.lightGrey}/>
          </View></TouchableOpacity>
          <View style={{backgroundColor:COLORS.dividerBorderColor,height:1}}/>
          <TouchableOpacity onPress={() => {
            if(isGuest){
              alert("Guest not allowed")
            }else{
              navigation.navigate("AccountInfoEdit")
            }
              
            }}>
          <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center"}}>
            <View style={{flexDirection:"row",alignItems:"center",margin:12}}>
                {/* <IonIcon name='bookmark-o' size={24} color={COLORS.dark}/> */}
                <Image source={require("../../../assets/images/personal_details.png")} style={{width:24,height:24}}/>
                <Text style={{marginLeft:12,color:COLORS.dark,fontSize:FONTS.buttonTextSize,fontFamily:FONTS.family}}>Personal Details</Text>
            </View>
            <MaterialIcons name='arrow-forward-ios' size={11} color={COLORS.lightGrey}/>
          </View></TouchableOpacity>
          <View style={{backgroundColor:COLORS.dividerBorderColor,height:1}}/>
          <TouchableOpacity onPress={() => {
            if(isGuest){
              alert("Guest not allowed")
            }else{
              navigation.navigate("PaymentMethods")
            }
              
            }}>
          <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center"}}>
            <View style={{flexDirection:"row",alignItems:"center",margin:12}}>
                {/* <IonIcon name='bookmark-o' size={24} color={COLORS.dark}/> */}
                <Image source={require("../../../assets/images/payment_method.png")} style={{width:24,height:24}}/>
                <Text style={{marginLeft:12,color:COLORS.dark,fontSize:FONTS.buttonTextSize,fontFamily:FONTS.family}}>Payment Methods</Text>
            </View>
            <MaterialIcons name='arrow-forward-ios' size={11} color={COLORS.lightGrey}/>
          </View></TouchableOpacity>
          
        </View>
      </View>
      <View style={{backgroundColor:COLORS.white,borderRadius:16,marginTop:16}}>
        <View style={{flexDirection:"column",padding:8}}>
          <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center"}}>
            <View style={{flexDirection:"row",alignItems:"center",margin:12}}>
                {/* <IonIcon name='bookmark-o' size={24} color={COLORS.dark}/> */}
                <Image source={require("../../../assets/images/help_support.png")} style={{width:24,height:24}}/>
                <Text style={{marginLeft:12,color:COLORS.dark,fontSize:FONTS.buttonTextSize,fontFamily:FONTS.family}}>Help & Support</Text>
            </View>
            <MaterialIcons name='arrow-forward-ios' size={11} color={COLORS.lightGrey}/>
          </View>
          <View style={{backgroundColor:COLORS.dividerBorderColor,height:1}}/>
          <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center"}}>
            <View style={{flexDirection:"row",alignItems:"center",margin:12}}>
                {/* <IonIcon name='bookmark-o' size={24} color={COLORS.dark}/> */}
                <Image source={require("../../../assets/images/terms_privacy.png")} style={{width:24,height:24}}/>
                <Text style={{marginLeft:12,color:COLORS.dark,fontSize:FONTS.buttonTextSize,fontFamily:FONTS.family}}>Terms & Privacy</Text>
            </View>
            <MaterialIcons name='arrow-forward-ios' size={11} color={COLORS.lightGrey}/>
          </View>
          <View style={{backgroundColor:COLORS.dividerBorderColor,height:1}}/>
          <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center"}}>
            <View style={{flexDirection:"row",alignItems:"center",margin:12}}>
                {/* <IonIcon name='bookmark-o' size={24} color={COLORS.dark}/> */}
                <Image source={require("../../../assets/images/rate_us.png")} style={{width:24,height:24}}/>
                <Text style={{marginLeft:12,color:COLORS.dark,fontSize:FONTS.buttonTextSize,fontFamily:FONTS.family}}>Rate the app</Text>
            </View>
            <MaterialIcons name='arrow-forward-ios' size={11} color={COLORS.lightGrey}/>
          </View>
        </View>
      </View>
      {/* <TouchableOpacity onPress={() => {
        navigation.navigate("AccountInfoEdit")
      }}>
        <View style={styles.btnContainer}>
            <AntDesign name='user' size={FONTS.regularIconSize} color={COLORS.accent} style={{marginRight:10}} />
            <Text style={{fontSize:20,color:COLORS.accent}}>Account</Text>
        </View>
        </TouchableOpacity> */}
        <TouchableOpacity style={{paddingBottom:"20%"}} onPress={async () => {
          await AsyncStorage.removeItem("@viewedOnboarding")
          await AsyncStorage.removeItem("customerRemainLoggedin")
          await AsyncStorage.removeItem("firstTimeUserInfoAdd")
          await AsyncStorage.removeItem("customer")
          await AsyncStorage.removeItem("token")
          navigation.navigate("AfterOnboarding")
          navigation.reset({
            index: 0,
            routes: [{ name: "AfterOnboarding" }],
          });
        }}>
        <View style={styles.btnContainer}>
            {/* <AntDesign name='logout' size={FONTS.regularIconSize} color={COLORS.accent} style={{marginRight:10}} /> */}
            <Text style={{fontSize:14,color:COLORS.dark,fontFamily:FONTS.family}}>Sign Out</Text>
        </View>
        </TouchableOpacity>
        {/* <TouchableOpacity style={{marginBottom:50}} onPress={async () => {
          navigation.navigate("LocationAccess")
          
        }}>
        <View style={styles.btnContainer}>
            {/* <AntDesign name='logout' size={FONTS.regularIconSize} color={COLORS.accent} style={{marginRight:10}} /> */}
            {/* <Text style={{fontSize:14,color:COLORS.dark}}>Location Access</Text>
        </View>
        </TouchableOpacity>  */}
        </ScrollView>
    </View>
    {/* <Loader/> */}
    </SafeAreaView>
  )
}

export default SettingsScreen