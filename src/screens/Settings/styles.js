import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:COLORS.secondary,
        // height:Dimensions.get("screen").height
    },
    btnContainer:{
        justifyContent:"center",
        height:50,
        // backgroundColor:COLORS.secondary,
        marginTop:15,
        borderRadius:10,
        flexDirection:"column",
        alignItems:'center'
    },
})


export default styles