import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
import FONTS from "../../utils/fonts";
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:COLORS.primary,
        alignItems:'center',
        justifyContent:'center',
        // transform : [ { scaleX : 2 } ]
    }, 
    containerInner:{
        // flex:1,
        // backgroundColor:"#fff",
        // alignItems:'center',
        // justifyContent:'center',
        position:"absolute",
        bottom:"0%",
        backgroundColor:COLORS.brandLight,
        paddingHorizontal:32,
        zIndex:101,
        width:Dimensions.get('screen').width,
        // height:Dimensions.get('screen').height/2.2,
        // borderTopLeftRadius: 15,
        // borderTopRightRadius: 15,
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
        // paddingBottom:30
        // transform : [ { scaleX : 0.5 } ]
    },
    curved:{
        width: Dimensions.get('screen').width,
        height: 40,
        backgroundColor: "red",
        position: "absolute",
        top: -26,
        left: 39,
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
        transform: [{ scaleX: 2 }, { scaleY: 0.5 }],
    }, 
    image:{
        width:Dimensions.get('screen').width,
        height:Dimensions.get('screen').height/1.5,
        position:"absolute",
        resizeMode:'cover',
        top:0
        // backgroundColor:"#000"
    },
    imageOverlay:{
        width:Dimensions.get('screen').width,
        height:Dimensions.get('screen').height,
        
        position:"absolute",
        zIndex:100,
        backgroundColor:COLORS.primary,
        opacity:0.5        
        // backgroundColor:"#000"
    },
    textOne:{
        fontSize:40,
        fontWeight:'bold'
    },
    textTwo:{
        fontSize:16,
        color:'#ddd',
        textAlign:'center',
        marginHorizontal:20
    },
    loginButton:{
        backgroundColor:"#0d47a1",
        padding:10,
        width:100,
        borderRadius:30,
        marginHorizontal:10,
        width:150,
        fontSize:18
    },
    registerButton:{
        backgroundColor:"#fff",
        padding:10,
        width:100,
        borderRadius:30,
        borderWidth:1,
        marginHorizontal:10,
        width:150,
        fontSize:18
    },

      button: {
        backgroundColor: '#3498db',
        padding: 10,
        borderRadius: 5,
        marginBottom: 10,
      },
      buttonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
      },
      modalContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
      },
      fragment: {
        backgroundColor: '#fff',
        padding: 16,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
      },
      fragmentText: {
        fontSize: 16,
        marginBottom: 10,
        color:COLORS.secondary,
        textAlign:"center",
        fontSize:20
      },
      closeButton: {
        
        position:"absolute",
        top:10,
        left:10
      },
      closeButtonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
      },

})


export default styles