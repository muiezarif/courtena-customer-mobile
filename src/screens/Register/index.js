import { View, Text, SafeAreaView, Image, Dimensions, Modal, KeyboardAvoidingView, Platform } from 'react-native'
import React, { useState } from 'react'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'
import * as Animatable from 'react-native-animatable';
import styles from './styles';
import COLORS from '../../utils/colors';
import FONTS from '../../utils/fonts';
import courtena from "../../api/courtena"
import { parsePhoneNumberFromString } from 'libphonenumber-js';
import Loader from '../../components/Loader';
const RegisterScreen = () => {
  const [phoneNo, setPhoneNo] = useState("")
  const [firstname, setFirstName] = useState("")
  const [lastname, setLastName] = useState("")
  const [password, setPassword] = useState("test1234")
  const [gender, setGender] = useState("")
  const [dob, setDob] = useState("")
  const [errMsg, setErrMsg] = useState("")
  const [isModalVisible, setModalVisible] = useState(false);
  const [isDateModalVisible, setDateModalVisible] = useState(false);
  const [countryCode, setCountryCode] = useState("+966")
  const [isLoading, setIsLoading] = useState(false)
  // const []
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  const toggleDateModal = () => {
    setDateModalVisible(!isDateModalVisible);
  };
  const navigation = useNavigation()

  const Register = async () => {
    
    const parsedPhoneNumber = parsePhoneNumberFromString(phoneNo, 'SA');
    if (parsedPhoneNumber.isValid()) {
      if (phoneNo === "" || password === "") {
        alert("Please Fill the Required Fields")
      } else {
        setIsLoading(true)
        const data = { phone: phoneNo }
        await courtena.post("/customer/check-registered-phone", { ...data }).then((res) => {
          setIsLoading(false)
          if (res.data.success) {
            navigation.navigate("OTP", { phoneNo, password })
          } else {
            alert(res.data.message)
          }
        }).catch(err => {
          setIsLoading(false)
          alert(err.message)
        })

      }
    } else {
      alert("Not SA valid number")
    }


  }
  return (
    <SafeAreaView style={[styles.container]}>
      {isLoading?<Loader/>:null}
      <Image style={styles.image} source={require("../../../assets/images/login_image.png")} />
      <View style={styles.imageOverlay} />
      <Image style={{ position: "absolute", zIndex: 101, width: 170, height: 160, top: 200 }} source={require("../../../assets/images/onboarding_logo.png")} />
      <View style={{ flexDirection: "row", justifyContent: "space-between", position: "absolute", zIndex: 101, top: 50, width: Dimensions.get("screen").width, paddingHorizontal: 12 }}>
        <TouchableOpacity onPress={() => {
          navigation.goBack()
        }}><Text style={{ color: COLORS.brandLight, fontSize: 16, fontFamily: FONTS.family }}>Back</Text></TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("Login")}><Text style={{ color: COLORS.brandLight, fontSize: 16, fontFamily: FONTS.family }} >Login</Text></TouchableOpacity>
      </View>
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null} style={styles.containerInner}>
        <View style={{ paddingBottom: 30 }}>
          {/* <View style={styles.curved}/> */}
          <Text style={{ fontSize: FONTS.titleHeading, lineHeight: 48, color: COLORS.secondary, textAlign: "left", marginTop: 20, fontFamily: FONTS.family600 }}>Register with phone number </Text>
          <Text style={{ fontSize: FONTS.textDescription, lineHeight: 24, color: COLORS.dark, marginTop: 12, textAlign: "left", fontFamily: FONTS.family300 }}>Enter your phone number. We'll send you a code. It helps us keep your account secure.</Text>

          <Animatable.View
            ref={this.validateInput}
            style={{ flexDirection: "row", marginTop: 24 }}
          >
            <TextInput
              style={{ width: "25%", height: 50, borderColor: "lightgrey", backgroundColor: COLORS.white, borderRadius: 12, borderWidth: 1, textAlign: "center", color: COLORS.secondary, marginRight: 12, fontFamily: FONTS.family }}
              placeholder="+966"
              value={countryCode}
              editable={false}
              placeholderTextColor={COLORS.secondary}

            />
            <TextInput
              style={{ width: "70%", height: 50, borderColor: COLORS.secondary, borderRadius: 12, borderWidth: 1, paddingLeft: 20, color: COLORS.secondary, backgroundColor: COLORS.white, fontFamily: FONTS.family }}
              placeholder="Phone Number"
              placeholderTextColor="lightgrey"
              keyboardType='numeric'
              maxLength={9}
              onChangeText={(text) => {
                setErrMsg(''),
                  setPhoneNo(countryCode + text)
              }
              }
            />

            {/* <TextInput
                    style={{ marginTop: 20,width: 200, borderColor: COLORS.secondary,borderRadius:10, borderWidth: 1, padding: 20,color:COLORS.secondary }}
                    placeholder="Password"
                    placeholderTextColor={COLORS.secondary}
                    secureTextEntry={true}
                    onChangeText = {(text) => 
                        {
                            setErrMsg('')
                            setPassword(text)
                        }
                      }

                />
                <Text style={{ color: 'red', textAlign: 'center', marginTop: 10 }}>{errMsg}</Text> */}

          </Animatable.View>



          <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 30 }}>
            <TouchableOpacity
              onPress={Register}
              style={{ width: Dimensions.get("screen").width - 64, backgroundColor: COLORS.secondary, padding: 12, alignItems: 'center', justifyContent: 'center', borderRadius: 16, marginTop: 0 }}
            >
              <Text style={{ textAlign: 'center', color: COLORS.brandLight, fontSize: FONTS.buttonTextSize, fontFamily: FONTS.family600 }}>Continue</Text>
            </TouchableOpacity>

            {/* <Text style={{ marginTop: 20,color:COLORS.accent }}>Forgot Password ?</Text> */}

            {/* <View style={{ flexDirection: 'row', marginTop: 60 }}>
                        <View style={{ height: 40, width: 40, borderRadius: 40/2, backgroundColor: '#3f51b5', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#FFF' }}>f</Text>
                        </View>
                        <View style={{ height: 40, width: 40, borderRadius: 40/2, backgroundColor: '#f44336', marginHorizontal: 10, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#FFF' }}>G</Text>
                        </View>
                        <View style={{ height: 40, width: 40, borderRadius: 40/2, backgroundColor: '#1565c0', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#FFF' }}>in</Text>
                        </View>
                    </View> */}

            {/* <View style={{ flexDirection: 'row',marginTop: 20 }}>
                    <Text style={{ color: COLORS.white }}>Don't have an account?</Text>
                    <Text onPress={() => {
                      navigation.navigate("Register")
                      navigation.reset({
                        index: 0,
                        routes: [{ name: "Register" }],
                      });
                      }} style={{ fontWeight: 'bold',color:COLORS.accent }}> Register</Text>
                    </View> */}
          </View>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}

export default RegisterScreen