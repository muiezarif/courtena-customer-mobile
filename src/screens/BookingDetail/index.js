import { View, Text, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native'
import React, { useEffect, useState } from 'react'
import styles from './style'
import FONTS from '../../utils/fonts'
import COLORS from '../../utils/colors'
import courtena from '../../api/courtena'
import AsyncStorage from '@react-native-async-storage/async-storage'
import IonIcon from"react-native-vector-icons/Ionicons"
import Loading from '../../components/Loading'
const BookingDetailScreen = ({navigation,route}) => {
    const item = route.params
    // console.log(item._id)
    const [details,setDetails] = useState()
    const getBookingDetail = async() => {
        const customer = await AsyncStorage.getItem("customer")
        const customerData = JSON.parse(customer)
        await courtena.get("/customer/get-booking-detail/"+item._id+"/"+customerData._id,{
            headers:{
                'Content-Type':'application/x-www-form-urlencoded',
                'Accept': '*/*',
                'Authorization': customerData.token
              }
        }).then((res) => {
            console.log(res.data)
            setDetails(res.data.result)
        }).catch((err) => {
            console.log(err)
        })
    }

    useEffect(() => {
        getBookingDetail()
    },[])
    const formatTimeToAMPM = (timeString) => {
        const [hour, minute] = timeString.split(':').map(Number);
        const formattedHour = hour > 12 ? hour - 12 : hour;
        const period = hour >= 12 ? 'PM' : 'AM';
        return `${formattedHour}:${String(minute).padStart(2, '0')} ${period}`;
      };
  return (
    <SafeAreaView style={styles.container}>
        <View style={{marginLeft:20}}>
                    <TouchableOpacity onPress={navigation.goBack}><IonIcon name='arrow-back-circle' size={FONTS.largeIconSize} color={COLORS.dark} /></TouchableOpacity>
                </View>
        <ScrollView showsVerticalScrollIndicator={false}>
        <View>
            <View style={{alignItems:"center",marginTop:20}}> 
                <Text style={{fontSize:FONTS.titleHeading,fontWeight:FONTS.extraBoldFontWeight,color:COLORS.accent}}>Booking Details</Text>
            </View>
            {details ?<View style={{marginHorizontal:20,marginTop:20}}>

            <Text style={{fontSize:FONTS.sectionHeading,fontWeight:FONTS.regularBoldFontWeight,color:COLORS.accent}}>Venue Owner </Text>
            <Text style={{fontSize:FONTS.textDetail,fontWeight:FONTS.lightFontWeight,color:COLORS.accent,marginTop:10}}>Name: {details.partner.username} </Text>
            <Text style={{fontSize:FONTS.sectionHeading,fontWeight:FONTS.regularBoldFontWeight,color:COLORS.accent,marginTop:20}}>Venue </Text>
            <Text style={{fontSize:FONTS.textDetail,fontWeight:FONTS.lightFontWeight,color:COLORS.accent,marginTop:10}}>Name: {details.venue_details.name} </Text>
            <Text style={{fontSize:FONTS.textDetail,fontWeight:FONTS.lightFontWeight,color:COLORS.accent,marginTop:10}}>Address: {details.venue_details.city} - {details.venue_details.address} </Text>
            <Text style={{fontSize:FONTS.textDetail,fontWeight:FONTS.lightFontWeight,color:COLORS.accent,marginTop:10}}>Contact: {details.venue_details.venuePhone}  </Text>
            <Text style={{fontSize:FONTS.sectionHeading,fontWeight:FONTS.regularBoldFontWeight,color:COLORS.accent,marginTop:20}}>Court </Text>
            <Text style={{fontSize:FONTS.textDetail,fontWeight:FONTS.lightFontWeight,color:COLORS.accent,marginTop:10}}>Name: {details.court_details.title} </Text>
            <Text style={{fontSize:FONTS.textDetail,fontWeight:FONTS.lightFontWeight,color:COLORS.accent,marginTop:10}}>Max People: {details.court_details.maxPeople} </Text>
            <Text style={{fontSize:FONTS.textDetail,fontWeight:FONTS.lightFontWeight,color:COLORS.accent,marginTop:10}}>Court Type: {details.court_details.courtType} </Text>
            <Text style={{fontSize:FONTS.sectionHeading,fontWeight:FONTS.regularBoldFontWeight,color:COLORS.accent,marginTop:20}}>Booking Info </Text>
            <Text style={{fontSize:FONTS.textDetail,fontWeight:FONTS.lightFontWeight,color:COLORS.accent,marginTop:10}}>Duration: {details.bookings.duration}</Text>
            <Text style={{fontSize:FONTS.textDetail,fontWeight:FONTS.lightFontWeight,color:COLORS.accent,marginTop:10}}>Booking Date: {details.bookings.date}</Text>
            <Text style={{fontSize:FONTS.textDetail,fontWeight:FONTS.lightFontWeight,color:COLORS.accent,marginTop:10}}>Booking Time: {formatTimeToAMPM(details.bookings.dateTimeInfo.timeFrom)} - {formatTimeToAMPM(details.bookings.dateTimeInfo.timeTo)}</Text>
            <Text style={{fontSize:FONTS.textDetail,fontWeight:FONTS.lightFontWeight,color:COLORS.accent,marginTop:10}}>Payment: {details.bookings.paymentAmount} SAR</Text>
            <Text style={{fontSize:FONTS.textDetail,fontWeight:FONTS.lightFontWeight,color:COLORS.accent,marginTop:10}}>Payment Status: {details.bookings.paymentStatus}</Text>
            </View>:<Loading/>}
        </View>
        </ScrollView>
    </SafeAreaView>
  )
}

export default BookingDetailScreen