import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:COLORS.primary
    },
    searchInputContainer:{
        height:50,
        backgroundColor:COLORS.white,
        marginTop:15,
        marginHorizontal:20,
        borderRadius:10,
        flexDirection:"row",
        alignItems:'center'
    },
})


export default styles