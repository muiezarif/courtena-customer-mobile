import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
import { RFValue } from "react-native-responsive-fontsize";
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:COLORS.primary,
    },
    innerContainer:{
        // height:Dimensions.get("screen").height,
        width:Dimensions.get("screen").width,
        // paddingBottom:200
        // position:"absolute",
        // top:"10%",
        // paddingBottom:"10%"
        // bottom:"50%"
    },
    image:{
        width:Dimensions.get("screen").width,
        position:"absolute",
        resizeMode:'cover',
        top:0,
        // zIndex:1
    },
    popularCourtsContainer:{
        width:Dimensions.get("screen").width,
        // height:Dimensions.get("screen").height/2,
        backgroundColor:COLORS.brandLight,
        // marginBottom:"100%",
        marginTop:50,
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
        // position:"absolute",
        // bottom:0

    },
    header:{
        marginTop:20,
        flexDirection:"row",
        justifyContent:"space-between",
        paddingHorizontal:20
    },
    searchInputContainer:{
        width: Dimensions.get("screen").width - 40,
        height:50,
        backgroundColor:COLORS.white,
        marginTop:15,
        marginHorizontal:20,
        borderRadius:10,
        flexDirection:"row",
        alignItems:'center'
    },
    courtCard:{
        // height:150,
        // width:"100%",
        width:Dimensions.get('screen').width-40,
        elevation:15,
        shadowOffset: { width: 0, height: 5 },
        shadowColor: COLORS.lightGrey,
        shadowOpacity: 0.5,
        marginBottom:RFValue(15),
        // marginHorizontal:5,
        borderRadius:RFValue(15),
        backgroundColor:COLORS.white,
        shadowRadius:4,
        paddingBottom:8
    },
    venueCard:{
        height:280,
        width:Dimensions.get('screen').width / 1.8,
        elevation:15,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: COLORS.accentLight,
        shadowOpacity: 1,
        marginRight:20,
        borderRadius:15,
        backgroundColor:COLORS.white,
        shadowRadius:4
    },
    courtCardImage:{
        height:80,
        // width:(Dimensions.get('screen').width-40)/3,
        // aspectRatio: 16 / 10,
        width:132,
        borderRadius:10,
        marginLeft:10,
        marginTop:10
    },
    venueCardImage:{
        height:200,
        width:"100%",
        borderTopLeftRadius:15,
        borderTopRightRadius:15
    },
    courtPriceTag:{
        height:30,
        width:140,
        backgroundColor:COLORS.secondary,
        position:"absolute",
        top:0,
        right:0,
        zIndex:1,
        borderTopRightRadius:15,
        borderBottomLeftRadius:15,
        alignItems:"center",
        justifyContent:'center'
    },
    courtCardDetails:{
        height:100,
        width:"100%",
        position:"absolute",
        bottom:0,
        borderRadius:15,
        // backgroundColor:COLORS.secondary,
        paddingHorizontal:20,
        paddingTop:10
    },
    courtCardOverlay:{
        height:280,
        backgroundColor:COLORS.accentLight,
        position:"absolute",
        zIndex:100,
        width:Dimensions.get('screen').width / 1.8,
        borderRadius:15
    },
    venueCardDetails:{
        height:100,
        width:"100%",
        position:"absolute",
        bottom:0,
        borderRadius:15,
        backgroundColor:COLORS.secondary,
        paddingHorizontal:20,
        paddingTop:10
    },
    venueCardOverlay:{
        height:280,
        backgroundColor:COLORS.accentLight,
        position:"absolute",
        zIndex:100,
        width:Dimensions.get('screen').width / 1.8,
        borderRadius:15
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
      },
      fragment: {
        backgroundColor: '#fff',
        padding: 16,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        
      },
      fragmentText: {
        fontSize: 16,
        marginBottom: 10,
        color:COLORS.secondary,
        textAlign:"center",
        fontSize:20
      },
      closeButton: {
        
        position:"absolute",
        top:10,
        left:10
      },
      closeButtonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
      },
      chipContainer:{
        flexDirection:"row",
        borderWidth:1,
        borderRadius:20,
        borderColor:"#00000014",
        paddingHorizontal:10,
        paddingVertical:5,
        alignContent:"center",
        justifyContent:"center",
        marginRight:5
      },
      selectedChipContainer:{
        flexDirection:"row",
        borderWidth:1,
        borderColor:COLORS.accent,
        borderRadius:20,
        backgroundColor:COLORS.accent,
        paddingHorizontal:10,
        paddingVertical:5,
        alignContent:"center",
        justifyContent:"center",
        marginRight:5
      }
})


export default styles