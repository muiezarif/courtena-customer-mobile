import { Text, View, SafeAreaView, ScrollView, TextInput, FlatList, Image, Animated, Dimensions, TouchableOpacity, Pressable, StatusBar, Easing, Platform, Modal } from 'react-native'
import React, { useEffect, useRef, useState } from 'react'
import styles from './styles'
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import IonIcon from "react-native-vector-icons/Ionicons"
import MaterialIcons from "react-native-vector-icons/MaterialIcons"
import COLORS from '../../utils/colors'
import feed from '../../../assets/data/feed'
import Fontisto from 'react-native-vector-icons/Fontisto'
import FONTS from '../../utils/fonts'
import { useIsFocused, useNavigation,useRoute } from '@react-navigation/native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import courtena from '../../api/courtena'
import NoData from '../../components/NoData'
import Loading from '../../components/Loading'
import ResponseError from '../../components/ResponseError'
import ServerError from '../../components/ServerError'
const courtCardWidth = Dimensions.get('screen').width / 1.8
import DateTimePickerModal from 'react-native-modal-datetime-picker'
import { RFValue } from 'react-native-responsive-fontsize'
import DeviceInfo from 'react-native-device-info';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'
import Geolocation from 'react-native-geolocation-service';
import Geocoding from 'react-native-geocoding';
import { check, request, PERMISSIONS, RESULTS } from 'react-native-permissions'
import { SelectList } from 'react-native-dropdown-select-list'
import RadioButton from '../../components/RadioButton'

const GOOGLE_MAPS_API = "AIzaSyCbknAy3ZfqL2hw9HKAc16jVDkWydyMAwE"

const HomeScreen = (props) => {
  const scrollX = useRef(new Animated.Value(0)).current;
  const [scrollY] = useState(new Animated.Value(0)); // Add this line
  const [scrollY2] = useState(new Animated.Value(0)); // Add this line
  const navigation = useNavigation()
  const [myLat, setMyLat] = useState(null)
  const [myLng, setMyLng] = useState(null)
  const [courts, setCourts] = useState([])
  const [venues, setVenues] = useState([])
  const [allVenues, setAllVenues] = useState([])
  const [nameFilteredVenues, setNameFilteredVenues] = useState([])
  const [sortVenues, setSortVenues] = useState([])
  const [date, setDate] = useState("")
  const [time, setTime] = useState("")
  const [noData, setNoData] = useState(false)
  const [serverError, setServerError] = useState(false)
  const [resError, setResError] = useState(false)
  const [loading, setLoading] = useState(false)
  const [isModalVisible, setModalVisible] = useState(false);
  const [isModalCityVisible, setModalCityVisible] = useState(false);
  const [isModalSortVisible, setModalSortVisible] = useState(false);
  const [isModalPermissionVisible, setModalPermissionVisible] = useState(false);
  const [city, setCity] = useState("Anywhere")
  const [venueName, setVenueName] = useState("")
  const [cityRadioSelect, setCityRadioSelect] = useState("")
  const [sortRadioSelect, setSortRadioSelect] = useState("")
  const [defaultCity, setDefaultCity] = useState("")
  const [today, setToday] = useState("")
  const [toSortVenues, setToSortVenues] = useState([])
  const [toSortVenuesDistance, setToSortVenuesDistance] = useState([])
  const [locationEnabled, setLocationEnabled] = useState(false)
  const [isDatePicker, setIsDatePicker] = useState(false)
  const [showSearchDropdown, setShowSearchDropdown] = useState(false)
  const [checkIsFocused, setCheckIsFocused] = useState(true)
  const hasNotch = DeviceInfo.hasNotch();
  const isFocused = useIsFocused();
  const route = useRoute();
  const { updatedLat, updatedLng } = route.params || {};
  const [distance, setDistance] = useState(false);
  const [price, setPrice] = useState(false);
  const [indoor, setIndoor] = useState(false);
  const [outdoor, setOutdoor] = useState(false);
  const [wall, setWall] = useState(false);
  const [crystal, setCrystal] = useState(false);
  const [panoramic, setPanoramic] = useState(false);
  const [roofedOutdoor, setRootedOutdoor] = useState(false);
  const [single, setSingle] = useState(false);
  const [double, setDouble] = useState(false);
  const [rentEquipment, setRentEquipment] = useState(false);
  const [lockers, setLockers] = useState(false);
  const [showers, setShowers] = useState(false);
  const [toilet, setToilet] = useState(false);
  const [freeParking, setFreeParking] = useState(false);
  const [paidParking, setPaidParking] = useState(false);
  const [wifi, setWifi] = useState(false);
  const [restaurant, setRestaurant] = useState(false);
  const [snackbar, setSnackbar] = useState(false);
  const [kidsPlayground, setKidsPlayground] = useState(false);
  const [specialAccess, setSpecialAccess] = useState(false);
  const [showHeader, setShowHeader] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  const toggleSortModal = () => {
    setModalSortVisible(!isModalSortVisible);
  };
  const toggleCityModal = () => {
    setModalCityVisible(!isModalCityVisible);
  };

  const togglePermissionModal = () => {
    setModalPermissionVisible(!isModalPermissionVisible);
  };

  // Function to check if location services are enabled
const isLocationEnabled = async () => {
  console.log("In Location enable")
  const locationEnabled = await Geolocation.checkDeviceLocationSettings({
    showDialog: false, // Optional, set to true if you want to display a dialog to enable location
    openLocationSettings: false, // Optional, set to true to open location settings directly
  });
  
  return locationEnabled === 'enabled';
};

  const updateMyLocation = async () => {
    const location = await AsyncStorage.getItem("location")
    const locationData = JSON.parse(location)
    let lat
    let lng
    // Initialize Geocoding with your API key
    Geocoding.init(GOOGLE_MAPS_API);
    

    // Get the device's location
    await Geolocation.getCurrentPosition(
      async (position) => {
        // alert(position)
        // Extract latitude and longitude from the position object
        const { latitude, longitude } = position.coords;
        lat = latitude
        lng = longitude
        if(location){

        }else{
          await AsyncStorage.setItem("location",JSON.stringify({lat:latitude,lng:longitude}))
        }
        setMyLat(latitude)
        setMyLng(longitude)
        await Geocoding.from( {latitude, longitude} )
          .then((json) => {
            var cityName = json.results[0].address_components.find(
              (component) =>
                component.types.includes('locality') ||
                component.types.includes('administrative_area_level_1')
            ).long_name;

            const dataInCity = data.find((item) => {
              const value = item.value.toLowerCase();
              const name = cityName.toLowerCase();
              return value.includes(name) || name.includes(value);
            });
            cityName = dataInCity ? dataInCity.value : "Anywhere"
            setMyLat(latitude)
            setMyLng(longitude)

            setCity(cityName);
            setCityRadioSelect(cityName)
            if(myLng){
              getCourtsByCityName(cityName)
            }
            


            // Check if cityName is in the data array


            // if (cityInData) {
            //   setCity(cityName);
            //   getCourtsByCityName(cityName)
            //   console.log(`${cityName} is in the data array.`);
            //   // You can access cityInData.key and cityInData.value here
            // } else {
            //   setCity("Anywhere");
            //   getCourtsByCityName("")
            //   console.log(`${cityName} is not in the data array.`);
            // }


            // console.log(myLat)
            // console.log(myLng)
            // alert("location device updated")
            // console.log(cityName)
            // alert(cityName);
            // alert(city)
          })
          .catch((error) => console.error(error));

      },
      (error) => console.error(error),
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }
  const updateMyLocation2 = async (updatedLat,updatedLng) => {
    console.log("updateMyLocation2 called")
    setLoading(true)
    const location = await AsyncStorage.getItem("location")
    const locationData = JSON.parse(location)
    if(location){

    }else{
      await AsyncStorage.setItem("location",JSON.stringify({lat:updatedLat,lng:updatedLng}))
    }
    setMyLat(updatedLat)
    setMyLng(updatedLng)
    
    Geocoding.init(GOOGLE_MAPS_API);
    
    await Geocoding.from({latitude:updatedLat, longitude:updatedLng})
          .then((json) => {
            var cityName = json.results[0].address_components.find(
              (component) =>
                component.types.includes('locality') ||
                component.types.includes('administrative_area_level_1')
            ).long_name;
              // alert(cityName)
              const dataInCity = data.find((item) => {
                const value = item.value.toLowerCase();
                const name = cityName.toLowerCase();
                return value.includes(name) || name.includes(value);
              });
            cityName = dataInCity ? dataInCity.value : "Anywhere"
          
            setCity(cityName);
            setCityRadioSelect(cityName)
            // if(myLng){
              getCourtsByCityName(cityName)
            // }

          })
          .catch((error) => {
            // setLoading(false)
            console.error(error)
            getCourtsByCityName("")
          });

  }

  const headerHeight = Dimensions.get("screen").height / 3; // Adjust this to the desired header height

  const headerTranslateY = scrollY.interpolate({
    inputRange: [0, headerHeight],
    outputRange: [0, -headerHeight],
    extrapolate: 'clamp',
  });

  const headerOpacity = scrollY.interpolate({
    inputRange: [0, headerHeight + 100],
    outputRange: [0, 1], // Opacity transition becomes smoother
    extrapolate: 'clamp',
    easing: Easing.ease,
  });
  const popularCourtOpacity = scrollY.interpolate({
    inputRange: [0, headerHeight - 100],
    outputRange: [1, 0], // Opacity transition becomes smoother
    extrapolate: 'clamp',
    easing: Easing.ease,
  });

  const isHeaderVisible = scrollY2._value > headerHeight

  const resetFilters = async () => {

    setDistance(false)
    setPrice(false)
    setIndoor(false)
    setOutdoor(false)
    setRootedOutdoor(false)
    setWall(false)
    setCrystal(false)
    setPanoramic(false)
    setRentEquipment(false)
    setLockers(false)
    setToilet(false)
    setFreeParking(false)
    setWifi(false)
    setRestaurant(false)
    setSnackbar(false)
    setKidsPlayground(false)
    setSpecialAccess(false)
    setPaidParking(false)
    setShowers(false)
    setSingle(false)
    setDouble(false)

  }

  // Function to calculate the distance between two coordinates using Haversine formula
  function calculateDistance(lat1, lon1, lat2, lon2) {
    const R = 6371; // Earth radius in kilometers
    const dLat = (lat2 - lat1) * (Math.PI / 180);
    const dLon = (lon2 - lon1) * (Math.PI / 180);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(lat1 * (Math.PI / 180)) *
      Math.cos(lat2 * (Math.PI / 180)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const distance = R * c; // Distance in kilometers
    return distance;
  }

  const askForPermission = async (permission) => {
    // await check(permission).then(async (result) => {
    //   if (result === RESULTS.DENIED || result === RESULTS.BLOCKED) {
    //     // alert("Location permission is recommended to get the most out of courtena. Go to your settings and allow location permssion")
    //     if(Platform.OS === "android"){
    //       setModalPermissionVisible(true)
    //     }
    //     setCity("Anywhere")
    //     getCourtsByCityName("")
    //   }
    //   if (result === RESULTS.GRANTED) {
    //     // alert("granted")
    //     setLoading(true)
    //     updateMyLocation()
    //   }

    // });
  }

  const askForPermissionAgain = async (permission) => {
    await request(permission).then(async (result) => {
      if (result === RESULTS.GRANTED) {
        // alert("granted")
        setLoading(true)
        updateMyLocation()
      }

    });
  }




  const askForPermissionUpdate = (permission) => {

    // check(permission).then((result) => {
    //   if (result === RESULTS.DENIED || result === RESULTS.BLOCKED) {

    //     setCity("Anywhere")
    //     getCourtsByCityName("")
    //   }
    //   if (result === RESULTS.GRANTED) {
    //     setLoading(true)
        // updateMyLocation()
    //   }

    // });
  }
  // useEffect(() => {
  //   // alert("ios")
  //   if (Platform.OS === "ios") {

  //     // alert(askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS))
  //     askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS)
  //   } else if (Platform.OS === "android") {
  //     askForPermission(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
  //   }
  //   // getCourtsBySearch()


  // }, [])

  useEffect(() => {
    scrollY.addListener((event) => {
      if (event.value > headerHeight) {
        setShowHeader(true);
      } else {
        setShowHeader(false);
      }
    });

    return () => {
      scrollY.removeAllListeners();
    };
  }, [scrollY]);

  



  const parseDateTimeString = (dateTimeString) => {
    const dateTime = new Date(dateTimeString);

    // Extract date components
    const year = dateTime.getFullYear();
    const month = dateTime.getMonth() + 1; // Month is zero-based, so adding 1
    const day = dateTime.getDate();

    // Extract time components
    const hours = dateTime.getHours();
    const minutes = dateTime.getMinutes();
    const seconds = dateTime.getSeconds();

    // Format time with leading zeros
    const formattedTime = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;

    return {
      date: `${year}-${month.toString().padStart(2, '0')}-${day.toString().padStart(2, '0')}`,
      time: formattedTime,
    };
  };

  function formatDateTime(date) {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    setToday(`${year}-${month}-${day},${hours}:${minutes}`)

    return `${year}-${month}-${day},${hours}:${minutes}`;
  }
  const getUserInfo = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    await courtena.get("/customer/get-customer-info/" + customerData._id, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      // console.log(res.data)
      if (res.data.success) {
        // setUsername(res.data.result.username)
        // setEmail(res.data.result.email)
        // setCountry(res.data.result.country)
        setDefaultCity(res.data.result.city)
        // setCity(res.data.result.city)
      } else {
        // console.log(res.data.message)
      }
    }).catch((err) => {
      console.log(err)
    })
  }
  const getHomeData = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const customerToken = await AsyncStorage.getItem("token")
    setLoading(true)
    setNoData(false)
    setResError(false)
    setServerError(false)
    await courtena.get("/customer/get-home-data/", {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      setLoading(false)
      if (res.data.success) {
        setCourts(res.data.result.courts)
        setVenues(res.data.result.venues)
        if (res.data.result.courts.length === 0) {
          setNoData(true)
        }
      } else {
        setResError(true)
        alert(res.data.message)
      }
    }).catch((err) => {
      setLoading(false)
      setServerError(true)
      console.log(err)
    })
  }


  const getCourtsByCityName = async (cityName) => {

    setVenueName("")
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    setLoading(true)


    const data2 = { city: cityName, date: date, time: time, filter: { distance: distance, price: price, indoor: indoor, outdoor: outdoor, roofedOutdoor: roofedOutdoor, wall: wall, crystal: crystal, panoramic: panoramic, rentEquipment: rentEquipment, lockers: lockers, toilet: toilet, freeParking: freeParking, wifi: wifi, restaurant: restaurant, snackbar: snackbar, kidsPlayground: kidsPlayground, disabledAccess: specialAccess, paidParking: paidParking, showers: showers, single: single, double: double } }
    await courtena.post("/customer/get-venues-by-search/", { ...data2 }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {

      // alert("called res" + city)
      // console.log(res.data)
      // setLoading(false)
      if (res.data.success) {

        // alert("called res success " + city)
        // console.log("IN THIS")
        // console.log(res.data)
        setCourts(res.data.result.courts)
        setVenues(res.data.result.venues)
        addToSortVenueList(res.data.result.venues, res.data.result.courts)

        // console.log(venues)
        if (res.data.result.length === 0) {
          setNoData(true)
        }
      } else {

        setResError(true)
      }
    }).catch((err) => {

      setLoading(false)
      setServerError(true)
      console.log(err)
    })
  }

  const getAllVenues = async (cityName) => {
    setVenueName("")
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    const data = { city: cityName, date: date, time: time, filter: { distance: distance, price: price, indoor: indoor, outdoor: outdoor, roofedOutdoor: roofedOutdoor, wall: wall, crystal: crystal, panoramic: panoramic, rentEquipment: rentEquipment, lockers: lockers, toilet: toilet, freeParking: freeParking, wifi: wifi, restaurant: restaurant, snackbar: snackbar, kidsPlayground: kidsPlayground, disabledAccess: specialAccess, paidParking: paidParking, showers: showers, single: single, double: double } }
    await courtena.post("/customer/get-venues-by-search/", { ...data }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {

      if (res.data.success) {
        setCourts(res.data.result.courts)
        setAllVenues(res.data.result.venues)
        addToSortVenueList(res.data.result.venues, res.data.result.courts)
      }
    }).catch((err) => {
      setServerError(true)
      console.log(err)
    })
  }



  const getCourtsBySearch = async () => {
    setVenueName("")
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    setLoading(true)

    const data = { city: city, date: date, time: time, filter: { distance: distance, price: price, indoor: indoor, outdoor: outdoor, roofedOutdoor: roofedOutdoor, wall: wall, crystal: crystal, panoramic: panoramic, rentEquipment: rentEquipment, lockers: lockers, toilet: toilet, freeParking: freeParking, wifi: wifi, restaurant: restaurant, snackbar: snackbar, kidsPlayground: kidsPlayground, disabledAccess: specialAccess, paidParking: paidParking, showers: showers, single: single, double: double } }
    await courtena.post("/customer/get-venues-by-search/", { ...data }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      // console.log(res.data)
      // setLoading(false)
      if (res.data.success) {
        // console.log("IN THIS")
        // console.log(res.data)
        setCourts(res.data.result.courts)
        setVenues(res.data.result.venues)
        addToSortVenueList(res.data.result.venues, res.data.result.courts)

        // console.log(venues)
        if (res.data.result.length === 0) {
          setNoData(true)
        }
      } else {
        setResError(true)
      }
    }).catch((err) => {
      setLoading(false)
      setServerError(true)
      console.log(err)
    })
  }

  const getVenuesByName = async () => {
    setCity("Anywhere")
    setCityRadioSelect("Anywhere")
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    // setCourts([])
    // setVenues([])

    if (venueName) {
      setLoading(true)
      const data = { name: venueName }
      await courtena.post("/customer/get-venues-by-name/", { ...data }, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': '*/*',
          'Authorization': customerData.token
        }
      }).then((res) => {
        // console.log(res.data)

        if (res.data.success) {
          // console.log("IN THIS")
          // console.log(res.data)
          setCourts(res.data.result.courts)
          setVenues(res.data.result.venues)
          addToSortVenueList(res.data.result.venues, res.data.result.courts)
          // console.log(venues)
          if (res.data.result.length === 0) {
            setNoData(true)
          }
        } else {
          setResError(true)
        }
      }).catch((err) => {
        setLoading(false)
        setServerError(true)
        console.log(err)
      })
    } else {
      alert("Please enter name to search")
    }

  }

  const addToSortVenueList = async (itemVenues, itemCourts) => {
    const addedVenues = await Promise.all(itemVenues.map(async (item, index) => {
      const foundCourt = itemCourts.find((item2) => item2._id === item.courts[0]);
      let pricingFrom;
      let venueLat;
      let venueLng;
      let distance;
      // Find the active pricing element from the beginning
      // console.log("Found Court:", foundCourt);

      if (foundCourt && foundCourt.pricing && foundCourt.pricing.pricing) {
        // Find the active pricing element from the beginning
        for (let index = 0; index < foundCourt.pricing.pricing.length; index++) {
          const element = foundCourt.pricing.pricing[index];
          if (element.active) {
            pricingFrom = element.price;
            break;
          }
        }
      }

      // console.log("Pricing From:", pricingFrom);

      try {
        // Initialize Geocoding with your Google Maps API key
        Geocoding.init(GOOGLE_MAPS_API);

        // Your target address in text form
        const targetAddress = item.address;

        // Use geocoding to get the coordinates
        const json = await Geocoding.from(targetAddress);
        const { lat, lng } = json.results[0].geometry.location;
        venueLat = lat;
        venueLng = lng;
        distance = myLat && myLng ? calculateDistance(myLat, myLng, lat, lng) : "Allow GPS"

        // console.log(`Latitude: ${lat}, Longitude: ${lng}`);
      } catch (error) {
        console.error(error);
      }

      // Create a new object with the item, pricingFrom, venueLat, and venueLng
      return {
        ...item,
        pricingFrom,
        venueLat,
        venueLng,
        distance
      };
    }));

    // Now, addedVenues contains the items with additional information
    // console.log(addedVenues);
    setSortVenues(addedVenues)
    setLoading(false)

  };


  const sortVenuesByDistance = () => {
    setSortVenues(sortVenues.sort((a, b) => a.distance - b.distance));
  }

  const sortVenuesByDistanceDescending = () => {
    setSortVenues(sortVenues.sort((a, b) => b.distance - a.distance));
  }

  const sortVenuesByPrice = () => {
    setSortVenues(sortVenues.sort((a, b) => a.pricingFrom - b.pricingFrom));
  }
  const sortVenuesByPriceDescending = () => {
    setSortVenues(sortVenues.sort((a, b) => b.pricingFrom - a.pricingFrom));
  };

  const firstRender = async() => {
    const location = await AsyncStorage.getItem("location")
    const locationData = JSON.parse(location)
    console.log(location)
    if(location){
      if(locationData.lat && locationData.lng){
        // setMyLat(locationData.lat)
        // setMyLng(locationData.lng)
        updateLatLng()
      }else{
        // console.log(locationData)
        // if(checkIsFocused){
          updateLatLng()
        // }
          
      }
    }else{
      setCity("Anywhere")
      setCityRadioSelect("Anywhere")
      getCourtsByCityName("")
      setModalPermissionVisible(true)
    }
  }

  const updateLatLng = async() => {
    console.log("updateLatLng called")
    setLoading(true)
    let lat
    let lng
    // Initialize Geocoding with your API key
    Geocoding.init(GOOGLE_MAPS_API);
    if(myLng === null){
      // Get the device's location
    await Geolocation.getCurrentPosition(
      async (position) => {
        // alert(position)
        // Extract latitude and longitude from the position object
        const { latitude, longitude } = position.coords;
        lat = latitude
        lng = longitude
        setMyLat(latitude)
        setMyLng(longitude)
        await Geocoding.from( latitude, longitude )
          .then((json) => {
            var cityName = json.results[0].address_components.find(
              (component) =>
                component.types.includes('locality') ||
                component.types.includes('administrative_area_level_1')
            ).long_name;

            const dataInCity = data.find((item) => {
              const value = item.value.toLowerCase();
              const name = cityName.toLowerCase();
              return value.includes(name) || name.includes(value);
            });
            cityName = dataInCity ? dataInCity.value : "Anywhere"
            setMyLat(latitude)
            setMyLng(longitude)

            setCity(cityName);
            setCityRadioSelect(cityName)
            setLoading(false)
          })
          .catch((error) => {
            console.error(error.code)
            setLoading(false)
            getCourtsByCityName("")
          });

      },
      (error) => {
        console.error(error)
        setLoading(false)
        if(error.code === 1){
          setModalPermissionVisible(true)
          getCourtsByCityName("")
        }else if(error.code === 4){
          getCourtsByCityName("")
        }else if(error.code === 2){
          getCourtsByCityName("")
        }
        
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );

    }else{
      updateMyLocation2(myLat,myLng)
    }
    
  }


  useEffect(() => {
    // if (Platform.OS === "ios") {

    //   // alert(askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS))
    //   askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS)
    // } else if (Platform.OS === "android") {
    //   askForPermission(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
    // }
    formatDateTime(new Date)
    getUserInfo()
    
    // if(isFocused){
      
    // }
    // firstRender()

    // if (Platform.OS === "ios") {
    //   askForPermissionUpdate(PERMISSIONS.IOS.LOCATION_ALWAYS)
    // } else if (Platform.OS === "android") {
    //   askForPermissionUpdate(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
    // }
    // if(city){
    //   getAllVenues(city)
    // }

  }, [])
  useEffect(() => {
    console.log("focused:"+isFocused)
    if(isFocused){
      firstRender()
    }
  },[isFocused])

  useEffect(() => {

    // alert("LLLL")
    // if (Platform.OS === "ios") {

    //   // alert(askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS))
    //   askForPermissionUpdate(PERMISSIONS.IOS.LOCATION_ALWAYS)
    // } else if (Platform.OS === "android") {
    //   askForPermissionUpdate(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
    // }
    if(myLng !== null){
      getMyLng()
      
    }
    
  }, [myLng])

  const getMyLng = () => {
    // isLocationEnabled()
  // .then((enabled) => {
  //   if (enabled) {
      updateMyLocation2(myLat,myLng)
  //   } else {
  //     setCity("Anywhere")
  //     getCourtsByCityName("")
  //     console.log('Location services are disabled');
  //   }
  // })
  // .catch((error) => {
  //   console.error(error.message);
  // });
  }




  const onScroll = Animated.event([{ nativeEvent: { contentOffset: { y: scrollY } } }], { useNativeDriver: false });

  const Venues = React.memo((props) => {
    // console.log(props.courtPrice)
    // State variables to store pricing range
    // const [pricingFrom, setPricingFrom] = useState(0);
    // const [pricingTo, setPricingTo] = useState(0);
    // const [venueLat, setVenueLat] = useState(null)
    // const [venueLng, setVenueLng] = useState(null)

    // Helper function to get pricing range from the feed
    // const getPricingRange = () => {
    //   // Find the active pricing element from the beginning
    //   for (let index = 0; index < props.courtPrice.pricing.pricing.length; index++) {
    //     const element = props.courtPrice.pricing.pricing[index];
    //     if (element.active) {
    //       setPricingFrom(element.price);
    //       break;
    //     }
    //   }

    //   // Find the active pricing element from the end
    //   for (let index = props.courtPrice.pricing.pricing.length - 1; index >= 0; index--) {
    //     const element = props.courtPrice.pricing.pricing[index];
    //     if (element.active) {
    //       setPricingTo(element.price);
    //       break;
    //     }
    //   }
    // };

    // const setVenueCoordinates = (lat, lng) => {
    //   setVenueLat(lat);
    //   setVenueLng(lng);
    // };


    // const getVenueLatLng = () => {
    //   // Initialize Geocoding with your Google Maps API key
    //   Geocoding.init(GOOGLE_MAPS_API);

    //   // Your target address in text form
    //   const targetAddress = props.data.address;

    //   // Use geocoding to get the coordinates
    //   Geocoding.from(targetAddress)
    //     .then((json) => {
    //       const { lat, lng } = json.results[0].geometry.location;
    //       setVenueCoordinates(lat, lng);

    //       // console.log(`Latitude: ${lat}, Longitude: ${lng}`);
    //     })
    //     .catch((error) => console.error(error));
    // }

    // Fetch pricing range when the component mounts



    const inputRange = [
      (props.index - 1) * courtCardWidth,
      props.index * courtCardWidth,
      (props.index + 1) * courtCardWidth
    ]
    const opacity = scrollX.interpolate({ inputRange, outputRange: [0.3, 0, 0.3] })
    const scale = scrollX.interpolate({ inputRange, outputRange: [0.8, 1, 0.8] })
    return (


      <View key={props.index} style={{ ...styles.courtCard }}>
        {/* <View style={styles.courtPriceTag}>
          <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardBadgeTextSize,fontWeight:FONTS.extraBoldFontWeight}}>SAR {pricingFrom} - SAR {pricingTo}</Text>
        </View> */}
        <View style={{ flexDirection: "column" }}>
          <Pressable onPress={() => navigation.navigate("VenueDetail", props.data)}>
            <View style={{ flexDirection: "row" }}>
              <Image source={{ uri: props.data.photos[0].replace("http://", "https://") }} style={styles.courtCardImage} />
              <View style={{ flexDirection: "column", marginLeft: RFValue(12), marginTop: 10 }}>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ width: Dimensions.get("screen").width / 2.2, fontSize: FONTS.cardHeading, color: COLORS.dark, fontFamily: FONTS.family600 }}>{props.data.name}</Text>
                  {/* <IonIcon name='bookmark-outline' size={20} /> */}
                  {/* <Image source={require("../../../assets/images/unselected_fav.png")} style={{marginLeft:14,justifyContent:"flex-end"}}/> */}
                </View>
                <Text style={{ fontSize: FONTS.cardDescription, color: COLORS.lightGrey, fontFamily: FONTS.family, width: Dimensions.get("screen").width / 2.4 }} numberOfLines={2} >{props.data.district} ,{props.data.city}</Text>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", paddingRight: 5 }}>
                  {/* <Text style={{ marginTop: 10, color: COLORS.secondary, fontFamily: FONTS.family700 }}>{pricingFrom} SAR/hour</Text> */}
                  <Text style={{ marginTop: 10, color: COLORS.secondary, fontFamily: FONTS.family700 }}>{props.data.pricingFrom} SAR/hour</Text>
                  <View style={{ flexDirection: "row", borderRadius: 20, backgroundColor: COLORS.accent, justifyContent: "flex-end", alignItems: "center", padding: 5 }}>
                    <FontAwesome5 name='location-arrow' size={8} color={COLORS.lightGrey} />
                    <Text style={{ color: COLORS.lightGrey, fontSize: 10, marginLeft: 2, fontFamily: FONTS.family300 }}>{props.data.distance !== "Allow GPS" ? props?.data?.distance?.toFixed(1) + " km" : props.data.distance} </Text>
                  </View>
                </View>
              </View>
              {/* <View style={{flexDirection:"column"}}>
          <Text>Court Name</Text>
          </View> */}
            </View></Pressable>
          <View style={{ borderWidth: 1, borderColor: "#00000014", marginTop: 10 }} />
          <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ flexDirection: "row", paddingHorizontal: 12, marginTop: 10, zIndex: 1000 }}>
            <View style={{ height: 25, paddingHorizontal: 5, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
              <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.timing.mondayOn ? "M " + props.data.timing.mondayFrom : "M Off"}</Text>
            </View>
            <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
              <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.timing.tuesdayOn ? "T " + props.data.timing.tuesdayFrom : "T Off"}</Text>
            </View>
            <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
              <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.timing.wedOn ? "W " + props.data.timing.wedFrom : "W Off"}</Text>
            </View>
            <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
              <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.timing.thursdayOn ? "T " + props.data.timing.thursdayFrom : "T Off"}</Text>
            </View>
            <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
              <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.timing.fridayOn ? "F " + props.data.timing.fridayFrom : "F Off"}</Text>
            </View>
            <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
              <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.timing.satOn ? "S " + props.data.timing.satFrom : "S Off"}</Text>
            </View>
            <View style={{ paddingHorizontal: 5, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5, marginRight: 20 }}>
              <Text style={{ textAlign: "center", fontSize: 12, fontFamily: FONTS.family }}>{props.data.timing.sunOn ? "S " + props.data.timing.sunFrom : "S Off"}</Text>
            </View>
          </ScrollView>
        </View>

      </View>

    )
  })

  const Courts = (props, { index }) => {
    const [pricingFrom, setPricingFrom] = useState()
    const [pricingTo, setPricingTo] = useState()
    const getPricingRange = (array) => {

      for (let index = 0; index < array.length; index++) {
        const element = array[index];
        if (element.active) {
          setPricingFrom(element.price)
          break;
        }
      }
      for (let index = array.length - 1; index >= 0; index--) {
        const element = array[index];
        if (element.active) {
          setPricingTo(element.price)
          break;
        }
      }
    }

    return (
      <TouchableOpacity onPress={() => navigation.navigate("CourtDetail", props.data)}>
        <View style={{ ...styles.courtCard }}>
          {/* <View style={styles.courtPriceTag}>
          <Text style={{color:COLORS.accentLight,fontSize:FONTS.cardBadgeTextSize,fontWeight:FONTS.extraBoldFontWeight}}>SAR {pricingFrom} - SAR {pricingTo}</Text>
        </View> */}
          <View style={{ flexDirection: "column" }}>
            <View style={{ flexDirection: "row" }}>
              <Image source={{ uri: props.data.image }} style={styles.courtCardImage} />
              <View style={{ flexDirection: "column", marginLeft: 12, marginTop: 10 }}>
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ width: 180, fontSize: FONTS.cardHeading, fontWeight: FONTS.regularBoldFontWeight, color: COLORS.dark }}>{props.data.title}</Text>
                  <IonIcon name='bookmark-outline' size={20} style={{ marginLeft: 14, justifyContent: "flex-end" }} />
                </View>
                <Text style={{ fontSize: FONTS.cardDescription, color: COLORS.lightGrey }}>Location</Text>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ marginTop: 10, color: COLORS.secondary, fontWeight: FONTS.extraBoldFontWeight, width: 180 }}>SAR {pricingFrom} - SAR {pricingTo}</Text>
                  <View style={{ flexDirection: "row", borderRadius: 20, backgroundColor: COLORS.accent, justifyContent: "center", alignItems: "center", padding: 5 }}>
                    <FontAwesome5 name='location-arrow' size={8} color={COLORS.lightGrey} />
                    <Text style={{ color: COLORS.lightGrey, fontWeight: FONTS.lightBoldFontWeight, fontSize: 10, marginLeft: 2 }}>1.5km</Text>
                  </View>
                </View>
              </View>
              {/* <View style={{flexDirection:"column"}}>
          <Text>Court Name</Text>
          </View> */}
            </View>
            <View style={{ borderWidth: 1, borderColor: "#00000014", marginTop: 10, marginHorizontal: 20 }} />
            <View style={{ flexDirection: "row", marginLeft: 12, marginTop: 10 }}>
              <View style={{ width: 50, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12 }}>11:00</Text>
              </View>
              <View style={{ width: 50, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12 }}>11:00</Text>
              </View>
              <View style={{ width: 50, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12 }}>11:00</Text>
              </View>
              <View style={{ width: 50, height: 25, borderRadius: 20, borderWidth: 1, borderColor: "#00000014", justifyContent: "center", alignItems: "center", marginLeft: 5 }}>
                <Text style={{ textAlign: "center", fontSize: 12 }}>11:00</Text>
              </View>
            </View>
          </View>


        </View>
      </TouchableOpacity>
    )
  }
  const data = [
    { key: '1', value: 'Riyadh' },
    { key: '2', value: 'Jubail' },
    { key: '3', value: 'Jeddah' },
    { key: '4', value: 'Dammam' },
    { key: '5', value: 'Khobar' },
    { key: '6', value: 'Anywhere' }
  ]
  const dataSortBy = [
    { key: '1', value: 'Distance (Close to Far)' },
    { key: '2', value: 'Distance (Far to Close)' },
    { key: '3', value: 'Price (Low to High)' },
    { key: '4', value: 'Price (High to Low)' },
  ]

  const VenuesSkeleton = () => {
    return (
      <SkeletonPlaceholder borderRadius={1}>
        <SkeletonPlaceholder.Item height={30} width={"40%"} style={{ marginHorizontal: 20, marginTop: 15 }} />
        <SkeletonPlaceholder.Item height={20} width={"20%"} style={{ marginHorizontal: 20, marginTop: 20 }} />
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{ marginTop: 20, marginHorizontal: 20 }}>
          <SkeletonPlaceholder.Item width={120} height={60} borderRadius={16} />
          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item width={120} height={20} />
            <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>
      </SkeletonPlaceholder>
    )


  }

  return (
    <SafeAreaView style={styles.container}>
      <Text style={{ width: 0, height: 0 }}>{myLat + "" + myLng}</Text>
      <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" />
      {/* <ScrollView> */}
      <DateTimePickerModal
        isVisible={isDatePicker}
        mode="datetime"
        onConfirm={(datetime) => {
          const { date, time } = parseDateTimeString(datetime);
          // console.log(date + " " + time)
          setDate(date)
          setTime(time)
          setIsDatePicker(false)
        }}
        onCancel={() => setIsDatePicker(false)}
      />
      <Modal
        animationType="slide"
        transparent={true}
        visible={isModalPermissionVisible}
        onRequestClose={togglePermissionModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.fragment}>
            <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Location Permission</Text>
            {/* <TouchableOpacity onPress={() => togglePermissionModal()} style={styles.closeButton} >
              <IonIcon name='close' size={30} color={COLORS.secondary} />
            </TouchableOpacity> */}
            {/* <TouchableOpacity onPress={() => resetFilters()} style={{ position: "absolute", fontSize: 16, top: 20, right: 10 }}><Text style={{ textAlign: "right", fontFamily: FONTS.family }}>Reset</Text></TouchableOpacity> */}
            <Text style={{color:COLORS.dark,fontFamily:FONTS.family500,textAlign:"center"}}>To get the most out of courtena allow location.</Text>
            <TouchableOpacity onPress={() => {
              if (Platform.OS === "ios") {

                // alert(askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS))
                askForPermissionAgain(PERMISSIONS.IOS.LOCATION_ALWAYS)
              } else if (Platform.OS === "android") {
                askForPermissionAgain(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
              }
              togglePermissionModal()
            }} style={{ width: "100%", justifyContent: "center", alignItems: "center", height: 50, backgroundColor: COLORS.secondary, borderRadius: 16, marginTop: 20 }}>
              <Text style={{ color: COLORS.brandLight, fontFamily: FONTS.family600, fontSize: 16 }} >Allow Permission</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
              togglePermissionModal()
            }} style={{ width: "100%", justifyContent: "center", alignItems: "center", height: 50, backgroundColor: COLORS.secondary, borderRadius: 16, marginTop: 20 }}>
              <Text style={{ color: COLORS.brandLight, fontFamily: FONTS.family600, fontSize: 16 }} >Cancel</Text>
            </TouchableOpacity>

          </View>
        </View>
      </Modal>
      <Modal
        animationType="slide"
        transparent={true}
        visible={isModalVisible}
        onRequestClose={toggleModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.fragment}>
            <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Filter</Text>
            <TouchableOpacity onPress={() => toggleModal()} style={styles.closeButton} >
              <IonIcon name='close' size={30} color={COLORS.secondary} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => resetFilters()} style={{ position: "absolute", fontSize: 16, top: 20, right: 10 }}><Text style={{ textAlign: "right", fontFamily: FONTS.family }}>Reset</Text></TouchableOpacity>
            <View style={{ flexDirection: "column" }}>
              {/* <Text style={{ color: COLORS.lightGrey, fontSize: 14, marginTop: 10, fontFamily: FONTS.family }}>Sort By</Text>
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <TouchableOpacity style={distance === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setDistance(!distance)}>
                  <Text style={{ fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Distance</Text>
                </TouchableOpacity>
                <TouchableOpacity style={price === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setPrice(!price)}>
                  <Text style={{ fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Price</Text>
                </TouchableOpacity>
              </View> */}
              <Text style={{ color: COLORS.lightGrey, fontSize: 14, marginTop: 10, fontFamily: FONTS.family }}>Type</Text>
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <TouchableOpacity style={indoor === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setIndoor(!indoor)}>
                  <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/indoor_icon.png")} />
                  <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Indoor</Text>
                </TouchableOpacity>
                <TouchableOpacity style={outdoor === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setOutdoor(!outdoor)}>
                  <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/outdoor_icon.png")} />
                  <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Outdoor</Text>
                </TouchableOpacity>

              </View>
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <TouchableOpacity style={roofedOutdoor === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setRootedOutdoor(!roofedOutdoor)}>
                  <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/roofed_outdoor.png")} />
                  <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Roofed Outdoor</Text>
                </TouchableOpacity>
              </View>
              <Text style={{ color: COLORS.lightGrey, fontSize: 14, marginTop: 10, fontFamily: FONTS.family }}>Features</Text>
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <TouchableOpacity style={wall === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setWall(!wall)}>
                  <Text style={{ fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Wall</Text>
                </TouchableOpacity>
                <TouchableOpacity style={crystal === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setCrystal(!crystal)}>
                  <Text style={{ fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Standard</Text>
                </TouchableOpacity>
                <TouchableOpacity style={panoramic === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setPanoramic(!panoramic)}>
                  <Text style={{ fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Panoramic</Text>
                </TouchableOpacity>
              </View>
              <Text style={{ color: COLORS.lightGrey, fontSize: 14, marginTop: 10, fontFamily: FONTS.family }}>Facilities</Text>
              <View style={{ flexDirection: "column", marginTop: 10 }}>
                <View style={{ flexDirection: "row", marginTop: 5 }}>
                  <TouchableOpacity style={rentEquipment === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setRentEquipment(!rentEquipment)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/rent_equipment.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Rent Equipment</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={lockers === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setLockers(!lockers)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/lockers.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Lockers</Text>
                  </TouchableOpacity>

                </View>
                <View style={{ flexDirection: "row", marginTop: 5 }}>
                  <TouchableOpacity style={toilet === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setToilet(!toilet)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/toilet.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Toilet</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={freeParking === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setFreeParking(!freeParking)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/parking.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Free parking</Text>
                  </TouchableOpacity>

                </View>
                <View style={{ flexDirection: "row", marginTop: 5 }}>
                  <TouchableOpacity style={wifi === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setWifi(!wifi)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/wifi.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Wifi</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={restaurant === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setRestaurant(!restaurant)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/restaurant.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Restaurant</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={snackbar === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setSnackbar(!snackbar)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/snackbar.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Snackbar</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flexDirection: "row", marginTop: 5 }}>
                  <TouchableOpacity style={kidsPlayground === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setKidsPlayground(!kidsPlayground)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/kids_playground.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Kids Playground</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={specialAccess === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setSpecialAccess(!specialAccess)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/wheelchair.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Special Access</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flexDirection: "row", marginTop: 5 }}>
                  <TouchableOpacity style={paidParking === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setPaidParking(!paidParking)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/paid_parking.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Paid Parking</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={showers === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setShowers(!showers)}>
                    <Image style={{ width: 18, height: 18 }} source={require("../../../assets/images/showers.png")} />
                    <Text style={{ marginLeft: 10, fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Showers</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <Text style={{ color: COLORS.lightGrey, fontSize: 14, marginTop: 10, fontFamily: FONTS.family }}>Size</Text>
              <View style={{ flexDirection: "row", marginTop: 10, marginBottom: 10 }}>
                <TouchableOpacity style={single === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setSingle(!single)}>
                  <Text style={{ fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Single</Text>
                </TouchableOpacity>
                <TouchableOpacity style={double === false ? styles.chipContainer : styles.selectedChipContainer} onPress={() => setDouble(!double)}>
                  <Text style={{ fontSize: 14, color: COLORS.dark, fontWeight: FONTS.regularBoldFontWeight, fontFamily: FONTS.family }}>Double</Text>
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity onPress={() => {
              getCourtsBySearch()
              toggleModal()
            }} style={{ width: "100%", justifyContent: "center", alignItems: "center", height: 50, backgroundColor: COLORS.secondary, borderRadius: 16, marginTop: 20 }}>
              <Text style={{ color: COLORS.brandLight, fontFamily: FONTS.family600, fontSize: 16 }} >Apply</Text>
            </TouchableOpacity>

          </View>
        </View>
      </Modal>

      <Modal
        animationType="slide"
        transparent={true}
        visible={isModalSortVisible}
        onRequestClose={toggleSortModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.fragment}>
            <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Sort By</Text>
            <TouchableOpacity onPress={() => toggleSortModal()} style={styles.closeButton} >
              <IonIcon name='close' size={30} color={COLORS.secondary} />
            </TouchableOpacity>
            {/* <TouchableOpacity onPress={() => resetFilters()} style={{ position: "absolute", fontSize: 16, top: 20, right: 10 }}><Text style={{ textAlign: "right", fontFamily: FONTS.family }}>Reset</Text></TouchableOpacity> */}
            {dataSortBy.map((item) => (
              <View key={item.key} style={{ marginBottom: 5 }}>
                <RadioButton
                  label={item.value}
                  selected={sortRadioSelect === item.key}
                  onPress={() => { setSortRadioSelect(item.key) }}
                />
              </View>
            ))}
            <TouchableOpacity onPress={() => {
              // getCourtsBySearch()
              console.log(sortRadioSelect)
              if (sortRadioSelect == 1) {
                sortVenuesByDistance()
              } else if (sortRadioSelect == 2) {
                sortVenuesByDistanceDescending()
              } else if (sortRadioSelect == 3) {
                sortVenuesByPrice()
              } else if (sortRadioSelect == 4) {
                sortVenuesByPriceDescending()
              } else {
                alert("Please select sorting to apply")
              }
              toggleSortModal()
            }} style={{ width: "100%", justifyContent: "center", alignItems: "center", height: 50, backgroundColor: COLORS.secondary, borderRadius: 16, marginTop: 20 }}>
              <Text style={{ color: COLORS.brandLight, fontFamily: FONTS.family600, fontSize: 16 }} >Apply</Text>
            </TouchableOpacity>

          </View>
        </View>
      </Modal>

      <Modal
        animationType="slide"
        transparent={true}
        visible={isModalCityVisible}
        onRequestClose={toggleCityModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.fragment}>
            <Text style={[styles.fragmentText, { fontFamily: FONTS.family }]}>Select City</Text>
            <TouchableOpacity onPress={() => toggleCityModal()} style={styles.closeButton} >
              <IonIcon name='close' size={30} color={COLORS.secondary} />
            </TouchableOpacity>
            {/* <TouchableOpacity onPress={() => resetFilters()} style={{ position: "absolute", fontSize: 16, top: 20, right: 10 }}><Text style={{ textAlign: "right", fontFamily: FONTS.family }}>Reset</Text></TouchableOpacity> */}
            {data.map((item) => (
              <View key={item.key} style={{ marginBottom: 5 }}>
                <RadioButton
                  label={item.value}
                  selected={cityRadioSelect === item.value}
                  onPress={() => {
                    if (item.value === "Anywhere") {
                      setCity("Anywhere")
                      setCityRadioSelect(item.value)
                    } else {
                      setCity(item.value)
                      setCityRadioSelect(item.value)
                    }

                  }}
                />
              </View>
            ))}
            <TouchableOpacity onPress={() => {
              if (city === "Anywhere") {
                getCourtsByCityName("")
              } else {
                getCourtsBySearch()
              }

              toggleCityModal()
            }} style={{ width: "100%", justifyContent: "center", alignItems: "center", height: 50, backgroundColor: COLORS.secondary, borderRadius: 16, marginTop: 20 }}>
              <Text style={{ color: COLORS.brandLight, fontFamily: FONTS.family600, fontSize: 16 }} >Select</Text>
            </TouchableOpacity>



          </View>
        </View>
      </Modal>

      {/* {showHeader ? ( */}
      <Animated.View
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          // transform: [{ translateY: headerTranslateY }],
          zIndex: 1000, // Adjust this if needed
          // height: headerHeight,
          backgroundColor: COLORS.brandLight, // Set your desired background color
          justifyContent: 'center',
          alignItems: 'center',
          opacity: headerOpacity
          // Add any other styles you need for your sticky header
        }}
      >
        {/* <Image style={{
          width: Dimensions.get("screen").width,
          height: Dimensions.get("screen").height / 5.5,
          resizeMode: "cover",
          position: "absolute",
          top: 0
        }} source={require("../../../assets/images/search_screen_header_img.png")} /> */}
        <View style={{
          width: Dimensions.get("screen").width,
          height: Platform.OS === "ios" ? 120 : 100,
          position: "absolute",
          top: 0,
          backgroundColor: COLORS.brandLight,
          // opacity: 0.7
        }} />
        <View style={{ flexDirection: "column", width: Dimensions.get("screen").width, paddingHorizontal: 20, marginTop: hasNotch ? 50 : 20 }}>
          <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
            <Text style={{ fontSize: FONTS.sectionHeading, color: COLORS.secondary, marginTop: 20, fontFamily: FONTS.family500 }}>Popular Courts</Text>
            <View style={{ marginTop: 20, flexDirection: "row" }}>
              <TouchableOpacity style={{ marginRight: 20 }} onPress={() => setModalSortVisible(true)}><Image source={require("../../../assets/images/sort.png")} style={{ width: 20, height: 20 }} /></TouchableOpacity>
              <TouchableOpacity onPress={() => setModalVisible(true)}><Image source={require("../../../assets/images/filter.png")} style={{ width: 20, height: 20 }} /></TouchableOpacity>
            </View>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ fontSize: FONTS.smallTextDescription, color: COLORS.lightGrey, marginTop: 4, fontFamily: FONTS.family300 }}>In </Text>
            <TouchableOpacity onPress={() => setModalCityVisible(true)} style={{ flexDirection: "row", alignItems: "center" }}>
              <Text style={{ fontSize: FONTS.smallTextDescription, color: COLORS.lightGrey, marginTop: 4, fontFamily: FONTS.family300 }}>{city}</Text>
              <MaterialIcons name='arrow-drop-down' size={20} color={COLORS.lightGrey} />
            </TouchableOpacity>
          </View>
        </View>

      </Animated.View>
      {/* ) : null} */}

      <Image style={styles.image} source={require("../../../assets/images/home_img.png")} />

      <Animated.ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1, paddingTop: Platform.OS === "android" ? 50 : 0 }} onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: scrollY } } }], { useNativeDriver: false })} >
        <View style={styles.innerContainer}>
          <View style={[]}>
            <View style={[styles.header]}>
              <View style={{ paddingBottom: 15 }}>
                <Text style={{ fontSize: FONTS.titleHeading, color: COLORS.secondary, fontFamily: FONTS.family600 }}>Find Padel Courts Everywhere</Text>
                <View style={{ flexDirection: "row" }}>
                </View>
              </View>
            </View>
            <View style={{ flexDirection: "column", justifyContent: "center", marginTop: 30 }}>
              <View style={styles.searchInputContainer}>
                <IonIcon name='search' size={20} color={COLORS.dark} style={{ marginLeft: 14 }} />
                <TextInput placeholder='Enter court name' value={venueName} style={{ fontSize: 16, paddingLeft: 10, width: "75%", fontFamily: FONTS.family }} onChangeText={(val) => {
                  if (val.length > 1) {
                    setShowSearchDropdown(true)
                  } else {
                    setShowSearchDropdown(false)
                  }
                  setVenueName(val)
                  // Filter the venues based on the input
                  const filtered = allVenues.filter(venue => venue.name.replace(/\s/g, '').toLowerCase().includes(val.replace(/\s/g, '').toLowerCase()));
                  console.log(filtered)
                  setNameFilteredVenues(filtered);
                }} />
                <TouchableOpacity onPress={() => {
                  setVenueName("")
                  getCourtsByCityName("")
                }}><Text style={{ color: COLORS.dark, fontFamily: FONTS.family600, fontSize: 12 }}>Reset</Text></TouchableOpacity>
              </View>
              {venueName.length > 1 && showSearchDropdown ?
                <View style={{ height: 100, width: Dimensions.get("screen").width - 40, backgroundColor: COLORS.brandLight, position: "absolute", zIndex: 2000, top: 70, marginHorizontal: 20, borderRadius: 16 }}>
                  {nameFilteredVenues ? <FlatList
                    data={nameFilteredVenues}
                    renderItem={({ item }) => (
                      <TouchableOpacity onPress={() => {
                        setShowSearchDropdown(false)
                        setVenueName(item.name)
                      }}>
                        <Text style={{ color: COLORS.dark, width: "100%", paddingHorizontal: 20, paddingVertical: 5, fontFamily: FONTS.family600, borderColor: COLORS.dividerBorderColor, borderWidth: 1 }}>{item.name}</Text>
                      </TouchableOpacity>
                    )}
                  /> : null}
                </View>
                : null}
              <View style={{ width: Dimensions.get("screen").width - 40, marginHorizontal: 20, marginTop: 20 }}>
                <TouchableOpacity onPress={() => getVenuesByName()} style={{ backgroundColor: COLORS.accentLight, height: 50, borderRadius: 16, alignContent: "center", justifyContent: "center" }}>
                  <Text style={{ textAlign: "center", color: COLORS.dark, fontSize: FONTS.buttonTextSize, fontFamily: FONTS.family600 }}>Search</Text>
                </TouchableOpacity>
              </View>
            </View>

          </View>
          {resError ? <View style={{ alignItems: "center", justifyContent: "center", height: "60%" }}><ResponseError /></View> : null}
          {serverError ? <View style={{ alignItems: "center", justifyContent: "center", height: "60%" }}><ServerError /></View> : null}
          {loading ? <View style={styles.popularCourtsContainer}><VenuesSkeleton /></View> : null}
          {noData ? <View style={{ alignItems: "center", justifyContent: "center", height: "60%" }}><NoData /></View> : null}

          {venues && courts && loading === false ? <Animated.View style={[styles.popularCourtsContainer, { minHeight: Dimensions.get("screen").height - 50 }]}>
            {/* {showHeader === false ?  */}
            <Animated.View style={{ flexDirection: "column", marginHorizontal: 20, marginBottom: 10, opacity: popularCourtOpacity }}>
              <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                <Text style={{ fontSize: FONTS.sectionHeading, color: COLORS.secondary, marginTop: 20, fontFamily: FONTS.family500 }}>Popular Courts</Text>
                <View style={{ marginTop: 20, flexDirection: "row" }}>
                  <TouchableOpacity style={{ marginRight: 20 }} onPress={() => setModalSortVisible(true)}><Image source={require("../../../assets/images/sort.png")} style={{ width: 20, height: 20 }} /></TouchableOpacity>
                  <TouchableOpacity onPress={() => setModalVisible(true)}><Image source={require("../../../assets/images/filter.png")} style={{ width: 20, height: 20 }} /></TouchableOpacity>
                </View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ fontSize: FONTS.smallTextDescription, color: COLORS.lightGrey, marginTop: 4, fontFamily: FONTS.family300 }}>In </Text>
                <TouchableOpacity onPress={() => setModalCityVisible(true)} style={{ flexDirection: "row", alignItems: "center" }}>
                  <Text style={{ fontSize: FONTS.smallTextDescription, color: COLORS.lightGrey, marginTop: 4, fontFamily: FONTS.family300, textAlign: 'left' }}>{city}</Text>
                  <MaterialIcons name='arrow-drop-down' size={20} color={COLORS.lightGrey} />
                </TouchableOpacity>

              </View>
            </Animated.View>
            {/* : null} */}

            {/* <View style={{ alignItems: "center", justifyContent: "center", paddingBottom: Platform.OS === "ios" ? "20%" : "30%" }}>{venues ? venues.map((item, index) => {

              const foundCourt = courts.find(item2 => item2._id === item.courts[0]);

              return (<Venues data={item} courtPrice={foundCourt} index={index} />)
            }) : null}</View> */}

            <View style={{ alignItems: "center", justifyContent: "center", paddingBottom: Platform.OS === "ios" ? "20%" : "30%" }}>{sortVenues ? sortVenues.map((item, index) => {

              const foundCourt = courts.find(item2 => item2._id === item.courts[0]);

              return (<Venues data={item} courtPrice={foundCourt} index={index} />)
            }) : null}</View>

          </Animated.View> : null}

        </View>
      </Animated.ScrollView>
    </SafeAreaView>
  )
}


export default HomeScreen;

// Top sticky header for filter menu

