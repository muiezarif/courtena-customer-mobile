import { StyleSheet,Dimensions } from "react-native";
const styles = StyleSheet.create({
    input:{
        fontSize:18
    },
    container:{
        padding:20
    },
    locationText:{

    },
    row:{
        flexDirection:'row',
        alignItems:'center',
        marginVertical:10,
        borderBottomWidth:1,
        paddingVertical:15,
        borderColor:'lightgrey'
    },
    iconContainer:{
        backgroundColor:'#d4d4d4',
        padding:15,
        borderRadius:10,
        marginRight:15
    }
})


export default styles