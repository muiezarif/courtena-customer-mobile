import { View, TextInput,FlatList, Text, SafeAreaView } from 'react-native'
import React, { useEffect, useState } from 'react'
import styles from './styles'
import search from "../../../assets/data/search"
import Entypo from "react-native-vector-icons/Entypo"
const LocationSearchScreen = (props) => {
    const [searchInput,setSearchInput] = useState("")
    useEffect(() => {

    },[searchInput])
  return (
    <SafeAreaView>
    <View style={styles.container}>
      {/* input component */}
        <TextInput value={searchInput} style={styles.input} onChangeText={(val) => {setSearchInput(val)}} placeholder="Where do you want to play"/>
      {/* location lists */}
      <FlatList
      data={search}
      renderItem={({item}) => (
      <View style={styles.row}>
        <View style={styles.iconContainer}>
            <Entypo name='location-pin' size={25}/>
        </View>
        <Text style={styles.locationText}>{item.description}</Text>
      </View>
      )}
      />
    </View></SafeAreaView>
  )
}

export default LocationSearchScreen