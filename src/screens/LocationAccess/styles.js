import { StyleSheet,Dimensions } from "react-native";
import COLORS from "../../utils/colors";
import FONTS from "../../utils/fonts";
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:COLORS.primary,
        alignItems:'center',
        justifyContent:'center',
    }, 
    containerInner:{
        // flex:1,
        // backgroundColor:"#fff",
        alignItems:'center',
        // justifyContent:'center',
        position:"absolute",
        zIndex:101,
        bottom:50,
        width:Dimensions.get('screen').width
    }, 
    image:{
        width:Dimensions.get('screen').width,
        height:Dimensions.get('screen').height,
        position:"absolute",
        resizeMode:"cover",
        top:0
    },
    imageOverlay:{
        height:Dimensions.get('screen').height,
        backgroundColor:COLORS.primary,
        position:"absolute",
        zIndex:100,
        opacity:0.8,
        width:Dimensions.get('screen').width,
    },
    textOne:{
        color:COLORS.accentLight,
        fontFamily:FONTS.family600,
        fontSize:32,
        lineHeight:40,
        marginLeft:16,
        
    },
    textTwo:{
        fontSize:FONTS.textDescription,
        color:COLORS.accentLight,
        textAlign:'center',
        marginHorizontal:20
    },
    loginButton:{
        backgroundColor:COLORS.secondary,
        padding:16,
        // paddingHorizontal:30,
        // width:100,
        borderRadius:16,
        marginHorizontal:10,
        width:Dimensions.get('screen').width-32
    },
    registerButton:{
        backgroundColor:COLORS.accentLight,
        padding:16,
        // width:200,
        borderRadius:16,
        // borderWidth:1,
        marginHorizontal:10,
        width:Dimensions.get('screen').width/2.5,
        // fontSize:18
    }

})


export default styles