import { View, Text, SafeAreaView,Image, TouchableOpacity, StatusBar,Platform, Alert } from 'react-native'
import React, { useEffect } from 'react'
import styles from './styles'
import { useNavigation } from '@react-navigation/native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import COLORS from '../../utils/colors'
import FONTS from '../../utils/fonts'
import { request,PERMISSIONS,RESULTS } from 'react-native-permissions'
// import { request, PERMISSIONS, RESULTS } from 'react-native-permissions';

const LocationAccess = () => {
  const navigation = useNavigation()

  const askForPermission = (permission) => {
    request(permission).then((result) => {
      if(result === RESULTS.DENIED){
        alert("Location permission is recommended to get the most out of courtena. Go to your settings and allow location permssion")
        navigation.navigate("FirstTimeUserInfoAdd")
      }
      if(result === RESULTS.BLOCKED){
        alert("Location permission is recommended to get the most out of courtena. Go to your settings and allow location permssion")
        navigation.navigate("FirstTimeUserInfoAdd")
      }
      if(result === RESULTS.GRANTED){
        navigation.navigate("FirstTimeUserInfoAdd")
      }
      
    });
  }
  const checkLoggedIn = async () => {
    const loggedIn = await AsyncStorage.getItem("customerRemainLoggedin")
    if(loggedIn){
      navigation.navigate("Main")
      navigation.reset({
        index:0,
        routes:[{name:"Main"}]
      })
    }
  }
  // const checkLocationPermission = async () => {
  //   let permission;
  //   if (Platform.OS === 'android') {
  //     permission = PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;
  //   } else if (Platform.OS === 'ios') {
  //     permission = PERMISSIONS.IOS.LOCATION_WHEN_IN_USE;
  //   }

  //   const result = await request(permission);
  //   handlePermissionResult(result);
  // };
  // const handlePermissionResult = (result) => {
  //   switch (result) {
  //     case RESULTS.UNAVAILABLE:
  //       // Location service is unavailable on this device
  //       Alert.alert('Location service is unavailable');
  //       break;
  //     case RESULTS.DENIED:
  //       // Location permission has been denied
  //       Alert.alert('Location permission denied');
  //       break;
  //     case RESULTS.GRANTED:
  //       // Location permission has been granted
  //       Alert.alert('Location permission granted');
  //       break;
  //     case RESULTS.BLOCKED:
  //       // Location permission has been blocked
  //       Alert.alert('Location permission blocked');
  //       break;
  //   }
  // };
  useEffect(() => {
    // checkLoggedIn()
  },[])
  return (
    <SafeAreaView style={[styles.container]}>
      <StatusBar translucent backgroundColor="transparent"/>
      <Image style={styles.image} source={require("../../../assets/images/location_access.png")}/>
      <View style={styles.imageOverlay}/>
      <Image style={{position:"absolute",zIndex:101,width:200,height:190,top:"30%"}} source={require("../../../assets/images/onboarding_logo.png")}/>
      <TouchableOpacity style={{position:"absolute",top:"10%",left:32,zIndex:110}} onPress={() => navigation.goBack()}>
      <Text style={{fontSize:16,color:COLORS.brandLight}}>Back</Text>

      </TouchableOpacity>
    <View  style={styles.containerInner}>
      
      <View style={{flexDirection:'column'}}>
      <Text style={styles.textOne}>Allow Location Access</Text>
      <Text style={{fontSize:14,fontFamily:FONTS.family600,color:"rgba(227,224,252,0.70)",marginBottom:32,marginLeft:16,width:350,marginTop:10}}>We need to know your location to find
the best padel courts for you.</Text>
      {/* <Text style={styles.textTwo}>Welcome to Courtena. One place for everything</Text> */}
      <View style={{flexDirection:'row',paddingTop:20,alignItems:"center",justifyContent:"center"}}>
        <TouchableOpacity style={styles.loginButton} onPress={async () => {
            // checkLocationPermission()
            if(Platform.OS === "ios"){
              askForPermission(PERMISSIONS.IOS.LOCATION_ALWAYS)
            }else if(Platform.OS === "android"){
              askForPermission(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
            }
            }}>
          <Text style={{textAlign:"center",color:COLORS.accentLight,fontSize:FONTS.buttonTextSize,fontFamily:FONTS.family600}}>Allow Location</Text>
        </TouchableOpacity>
      </View>
      </View>
      
    </View>
    </SafeAreaView>
  )
}

export default LocationAccess