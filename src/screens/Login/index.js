import { View, Text, SafeAreaView, Image, Dimensions, KeyboardAvoidingView, Platform } from 'react-native'
import React, { useState } from 'react'
import styles from "./styles"
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'
import * as Animatable from 'react-native-animatable';
import COLORS from '../../utils/colors'
import AsyncStorage from '@react-native-async-storage/async-storage'
import FONTS from '../../utils/fonts'
import courtena from '../../api/courtena'
import Svg, { Path } from 'react-native-svg';
import {parsePhoneNumberFromString} from 'libphonenumber-js'
import Loader from '../../components/Loader'


const LoginScreen = () => {
  const [phoneNo, setPhoneNo] = useState("")
  const [countryCode, setCountryCode] = useState("+966")
  const [password, setPassword] = useState("test1234")
  const [errMsg, setErrMsg] = useState("")
  const [isLoading, setIsLoading] = useState(false)




  // const []
  const navigation = useNavigation()
  const loginCustomer = async () => {
    const parsedPhoneNumber = parsePhoneNumberFromString(phoneNo, 'SA');
    // if (parsedPhoneNumber.isValid()) {
      if (phoneNo === "" || password === "") {
        alert("Please Fill the required Fields")
      } else {
        setIsLoading(true)
        const data = { phone: phoneNo }
        await courtena.post("/customer/check-login-phone", { ...data }).then((res) => {
          setIsLoading(false)
          if (res.data.success) {
            navigation.navigate("OTP", { phoneNo, password, login: true })
          } else {
            alert(res.data.message)
          }
        }).catch(err => {
          setIsLoading(false)
          alert(err.message)
        })
        
      }
    // }else{
    //   alert("Phone number is not valid")
    // }
    

  }
  return (
    <SafeAreaView style={[styles.container]}>
      {isLoading?<Loader/>:null}
      <Image style={styles.image} source={require("../../../assets/images/login_image.png")} />
      <View style={styles.imageOverlay} />
      <Image style={{ position: "absolute", zIndex: 101, width: 170, height: 160, top: 200 }} source={require("../../../assets/images/onboarding_logo.png")} />
      <View style={{ flexDirection: "row", justifyContent: "space-between", position: "absolute", zIndex: 101, top: 50, width: Dimensions.get("screen").width, paddingHorizontal: 12 }}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Text style={{ color: COLORS.brandLight, fontSize: 16, fontFamily: FONTS.family }}>Back</Text></TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("Register")}><Text style={{ color: COLORS.brandLight, fontSize: 16, fontFamily: FONTS.family }}>Register</Text></TouchableOpacity>
      </View>
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null} style={styles.containerInner}>
        <View style={{ paddingBottom: 30 }}>
          {/* <View style={styles.curved}/> */}
          <Text style={{ fontSize: FONTS.titleHeading, lineHeight: 48, color: COLORS.secondary, textAlign: "left", marginTop: 20, fontFamily: FONTS.family600 }}>Login with phone number </Text>
          <Text style={{ fontSize: FONTS.textDescription, lineHeight: 24, color: COLORS.dark, marginTop: 12, textAlign: "left", fontFamily: FONTS.family300 }}>Enter your phone number. We'll send you a code. It helps us keep your account secure.</Text>

          <Animatable.View
            ref={this.validateInput}
            style={{ flexDirection: "row", marginTop: 24 }}
          >
            <TextInput
              style={{ width: "25%", height: 50, borderColor: "lightgrey", backgroundColor: COLORS.white, borderRadius: 12, borderWidth: 1, textAlign: "center", color: COLORS.secondary, marginRight: 12, fontFamily: FONTS.family }}
              placeholder="+966"
              value={countryCode}
              editable={false}
              placeholderTextColor={COLORS.secondary}

            />
            <TextInput
              style={{ width: "70%", height: 50, borderColor: COLORS.secondary, borderRadius: 12, borderWidth: 1, paddingLeft: 20, color: COLORS.secondary, backgroundColor: COLORS.white, fontFamily: FONTS.family }}
              placeholder="Phone Number"
              placeholderTextColor="lightgrey"
              keyboardType="numeric"
              maxLength={9}
              onChangeText={(text) => {
                setErrMsg(''),
                  setPhoneNo(countryCode + text)
              }
              }
            />

          </Animatable.View>



          <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 32 }}>
            <TouchableOpacity
              onPress={loginCustomer}
              style={{ width: Dimensions.get("screen").width - 64, backgroundColor: COLORS.secondary, padding: 12, alignItems: 'center', justifyContent: 'center', borderRadius: 16, marginTop: 0 }}
            >
              <Text style={{ textAlign: 'center', color: COLORS.brandLight, fontSize: FONTS.buttonTextSize, fontFamily: FONTS.family600 }}>Continue</Text>
            </TouchableOpacity>

          </View>

        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}

export default LoginScreen