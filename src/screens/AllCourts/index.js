import { View, Text, SafeAreaView, TextInput, FlatList, TouchableOpacity } from 'react-native'
import React, { useEffect, useState } from 'react'
import styles from './styles'
import feed from '../../../assets/data/feed'
import AntDesign from "react-native-vector-icons/AntDesign"
import FONTS from '../../utils/fonts'
import COLORS from '../../utils/colors'
import Court from '../../components/Court'
import courtena from '../../api/courtena'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useIsFocused } from '@react-navigation/native'
import NoData from '../../components/NoData'
import Loading from '../../components/Loading'
import ResponseError from '../../components/ResponseError'
import ServerError from '../../components/ServerError'
const AllCourtsScreen = () => {
  const [courts,setCourts] = useState([])
  const [search,setSearch] = useState([])
  const [noData,setNoData] = useState(false)
  const [serverError,setServerError] = useState(false)
  const [resError,setResError] = useState(false)
  const [loading,setLoading] = useState(false)
  const isFocused = useIsFocused();
  const getAllCourts = async () => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    setLoading(true)
    setNoData(false)
    setResError(false)
    setServerError(false)
    courtena.get("/customer/get-courts",{
      headers:{
        'Content-Type':'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      setLoading(false)
      if(res.data.success){
        setCourts(res.data.result)
        if(res.data.result.length === 0){
          setNoData(true)
        }
      }else{
        setResError(true)
      }
    }).catch((err) => {
      setLoading(false)
      setServerError(true)
    })
  }
  const getCourtsBySearch = async (term) => {
    const customer = await AsyncStorage.getItem("customer")
    const customerData = JSON.parse(customer)
    setCourts([])
    setLoading(true)
    setNoData(false)
    setResError(false)
    setServerError(false)
    courtena.get("/customer/get-courts/"+term,{
      headers:{
        'Content-Type':'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Authorization': customerData.token
      }
    }).then((res) => {
      setLoading(false)
      if(res.data.success){
          setCourts(res.data.result)
          if(res.data.result.length === 0){
            setNoData(true)
          }
      }else{
        setResError(true)
      }
    }).catch((err) => {
      setLoading(false)
      setServerError(true)
      console.log(err)
    })
  }

  useEffect(() => {
    getAllCourts()
    setSearch("")
  },[isFocused])
  return (
    <SafeAreaView style={styles.container}>
    <View>
        <View style={styles.searchInputContainer}>
            <TouchableOpacity onPress={() => getCourtsBySearch(search)}><AntDesign name='search1' size={FONTS.regularIconSize} color={COLORS.accent} style={{marginLeft:20}} /></TouchableOpacity>
            <TextInput placeholder='Search by name,court type' style={{fontSize:20,paddingLeft:10,width:"80%"}} onChangeText={(text) => {setSearch(text)}} value={search}/>
        </View>
        {resError? <View style={{alignItems:"center",justifyContent:"center", height:"80%"}}><ResponseError/></View>:null}
        {serverError? <View style={{alignItems:"center",justifyContent:"center", height:"80%"}}><ServerError/></View>:null}
        {loading? <View style={{alignItems:"center",justifyContent:"center", height:"80%"}}><Loading/></View>:null}
        {noData ? <View style={{alignItems:"center",justifyContent:"center", height:"80%"}}><NoData/></View>:null}
      {courts ? <FlatList
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        data={courts}
        renderItem={({item}) => <Court feed={item} />}/>:null}
    </View>
    </SafeAreaView>
  )
}

export default AllCourtsScreen