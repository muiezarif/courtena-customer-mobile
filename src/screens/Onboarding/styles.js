import { StyleSheet,Dimensions,Platform } from "react-native";
import COLORS from "../../utils/colors";
const styles = StyleSheet.create({
    container:{
        backgroundColor:COLORS.primary,
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        height:Dimensions.get('screen').height,
        // width:Dimensions.get('screen').width,
    }, 
    flatListStyle:{
        height:Dimensions.get("screen").height,
        width:Dimensions.get("screen").width,
        position:"absolute",
        // resizeMode:"cover",
        top:Platform.OS === "ios" ?-10:0,
    },
    containerInner:{
        // flex:1,
        // backgroundColor:"#fff",
        // alignItems:'center',
        // justifyContent:'center',
        height:Dimensions.get('screen').height,
        width:Dimensions.get('screen').width
    },
    onboardingClickables:{
        flexDirection:"row",
        position:"absolute",
        alignItems:"center"
        // bottom:0,
        // paddingHorizontal:30
    } 
})


export default styles