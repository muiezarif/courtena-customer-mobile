import { View, Text, SafeAreaView, FlatList,Animated,ActivityIndicator, StatusBar,Platform, TouchableOpacity } from 'react-native'
import React,{useState,useRef,useEffect} from 'react'
import styles from './styles'
import onboardingData from '../../../assets/data/onboardingData'
import OnboardingItem from '../../components/OnboardingItem'
import Paginator from '../../components/OnboardingItem/Paginator'
import NextButton from '../../components/OnboardingItem/NextButton'
import AsyncStorage from "@react-native-async-storage/async-storage";
import HomeScreen from '../Home'
import LoginScreen from '../Login'
import { useNavigation } from '@react-navigation/native'
import COLORS from '../../utils/colors'
import FONTS from '../../utils/fonts'
const Loading = () => {
    <View>
      <ActivityIndicator size="large"/>
    </View>
  }
const OnboardingScreen = () => {
    const [currentIndex,setCurrentIndex] = useState(0)
    const scrollX = useRef(new Animated.Value(0)).current
    const slidesRef = useRef(null)
    const navigation = useNavigation()
    const [loading,setLoading] = useState(true)
    const [viewedOnboarding,setViewedOnboarding] = useState(false)
    const viewableItemsChanged = useRef(({viewableItems})=> {
        setCurrentIndex(viewableItems[0].index)
    }).current
    const checkOnboarding = async () => {
        try{
            const userLoggedIn = await AsyncStorage.getItem('customerRemainLoggedin')
            const value = await AsyncStorage.getItem('@viewedOnboarding')
            const firstTimeUserInfo = await AsyncStorage.getItem("firstTimeUserInfoAdd")
            if(userLoggedIn !== null){
                if(firstTimeUserInfo){
                navigation.navigate("Main")
                // navigation.reset({
                //     index:0,
                //     routes: [{ name: "Main" }],
                // })
            }else{
                navigation.navigate("FirstTimeUserInfoAdd")
                navigation.reset({
                    index:0,
                    routes: [{ name: "FirstTimeUserInfoAdd" }],
                })
            }
            }else if(value !== null){
                setViewedOnboarding(true)
                navigation.navigate("AfterOnboarding")
                navigation.reset({
                    index:0,
                    routes: [{ name: "AfterOnboarding" }],
                })
            }
            
        }catch(err){
            console.log("Error @checkOnboarding:", err)
        }finally{
            setLoading(false)
        }
    }
    useEffect(() => {
     checkOnboarding()
    },[])
    const viewConfig = useRef({viewAreaCoveragePercentThreshold:50}).current
    const scrollTo = async() => {
        if(currentIndex < onboardingData.length - 1){
            slidesRef.current.scrollToIndex({index: currentIndex+1})
        }else{
            try {
                await AsyncStorage.setItem('@viewedOnboarding','true')
                
                navigation.navigate("AfterOnboarding")
            } catch (error) {
                console.log('Error @setItem',error)
            }
        }
    }
    const skipOnBoarding = async() => {
        try {
            await AsyncStorage.setItem('@viewedOnboarding','true')
            
            navigation.navigate("AfterOnboarding")
        } catch (error) {
            console.log('Error @setItem',error)
        }
    }
    return (
    <SafeAreaView style={styles.container}>
        <StatusBar translucent backgroundColor="transparent"/>
        <TouchableOpacity style={{position:"absolute",right:20,top:Platform.OS === "ios"? 50:"6%",zIndex:500}} onPress={skipOnBoarding}><Text style={{ color: COLORS.white, fontSize: 16, fontFamily: FONTS.family }}>Skip</Text></TouchableOpacity>

        {loading? <Loading/>: <View style={styles.containerInner}>
        <FlatList
        style={styles.flatListStyle}
        data={onboardingData}
        renderItem={({item}) => (
            <OnboardingItem item={item}/>
        )}
        keyExtractor={(item) => item.id}
        pagingEnabled={true}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        bounces={false}
        ref={slidesRef}
        scrollEventThrottle={40}
        onViewableItemsChanged={viewableItemsChanged}
        viewabilityConfig={viewConfig}
        onScroll={Animated.event([{nativeEvent:{contentOffset:{x:scrollX}}}],{useNativeDriver:false})}
      />
      {/* <View >
      
      
      </View> */}
      {/* <View style={styles.onboardingClickables}> */}
      <View style={{position:"absolute",left:16,bottom:Platform.OS === "ios"?30:10}}>
      <Paginator data={onboardingData} scrollX={scrollX}/>
      </View>
      <View style={{position:"absolute",right:50,bottom:Platform.OS === "ios"? 30:10}}>
      <NextButton scrollTo={scrollTo} percentage={(currentIndex +1) * (100 / onboardingData.length)}/>
      </View>
      {/* </View> */}
      
      {/* <View> */}
        
      {/* </View> */}
    </View>}
    
    </SafeAreaView>
  )
}

export default OnboardingScreen