/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
import "react-native-gesture-handler"
import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import Router from "./src/navigation/Router";
import COLORS from "./src/utils/colors";
import { SafeAreaView } from "react-native-safe-area-context";
import SplashScreen from "react-native-splash-screen";
import NetInfo from '@react-native-community/netinfo';


export default function App() {
  const [isConnected, setIsConnected] = useState(true); // Assuming connected by default
  const [state,setState] = useState({})

  useEffect(() => {
    SplashScreen.hide()
    const unsubscribe = NetInfo.addEventListener(state => {
      setIsConnected(state.isConnected);
      if(state.isConnected){

      }else{
        alert("Internet disconnected")
      }
    });

    return () => {
      unsubscribe(); // Clean up the listener when component unmounts
    };
  }, []);


  return (
    <>
      {/* <StatusBar barStyle="dark-content" backgroundColor={COLORS.primary}/> */}
      {/* <SafeAreaView > */}
          <Router/>
      {/* </SafeAreaView> */}
      
    </>
    // <NavigationContainer >
    //   <Stack.Navigator>
    //     <Stack.Screen
    //       options={{headerShown: false}}
    //       name="LoginScreen"
    //       component={LoginScreen}
    //     />
    //     <Stack.Screen
    //       options={{headerShown: false}}
    //       name="Register"
    //       component={RegisterScreen}
    //     />
    //     <Stack.Screen
    //       options={{headerShown: false}}
    //       name="ForgotPassword"
    //       component={ForgotPasswordScreen}
    //     />
    //     <Stack.Screen
    //       options={{headerShown: false}}
    //       name="Home"
    //       component={HomeScreen}
    //     />
    //     <Stack.Screen
    //       options={{headerShown: false}}
    //       name="SendNotification"
    //       component={SendNotificationScreen}
    //     />
    //     <Stack.Screen
    //       options={{headerShown: true,headerTintColor:"#00DAC8",headerStyle:{backgroundColor:"#0B1828"}}}
    //       name="Faqs"
    //       component={FaqsScreen}
    //     />
    //     <Stack.Screen
    //       options={{headerShown: true,headerTintColor:"#00DAC8",headerStyle:{backgroundColor:"#0B1828"}}}
    //       name="Referral"
    //       component={ReferralDetailScreen}
    //     />
    //   </Stack.Navigator>
    // </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
